<?php

	// Setup Holy Grail class
	$holy_grail_class = 'one_full';

	// Load post into global $loc_post context
	$loc_post = Timber::get_post();

	//	Disable Breadcrumbs
	add_action( '_fh_the_crumbs', '__return_false' );

	// Disable on-page social sharing
	add_action( '_fh_on_page_social_sharing', '__return_false' );

	// Remove default title, sidebars, and content
	add_action( '_fh_the_title', '__return_false' );

	// Remove main content action and add custom profile main content
	remove_action( '_fh_the_content', '_fh_content' );
	add_action( '_fh_the_content', '_om_main_content' );

?>

<?php
	//
	//	MAIN CONTENT
	//
	function _om_main_content() {

		global $loc_post;

	// Get featured image
	$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );

	// taxonomy info
	$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

	?>

	<div class="help-center">

		<div class="page-section space-4" style="background: #efefef;">
			<div class="container">
				<p class="space-0 pull-left" style="line-height: 32px;"><a href="/help-center/">Help Center</a> / <?php echo $term->name; ?></p>
				<div class="text-right pull-right hidden-xs" style="max-width: 400px;">
					<?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
				</div>
			</div>
		</div>

		<div class="container space-10">
			<div class="row">
				<div class="col-sm-8 col-lg-9">
					<div class="help-center-content">

						<h1 class="h2 text-left"><?php echo $term->name; ?></h1>
						<hr>

						<div class="row">
							<?php

							 $args = array(
							 'post_type' => 'help_center_article',
							 'help_center_categories' => $term->slug
							 );
							 $the_query = new WP_Query( $args );

							?>

							 <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
							 <div class="col-sm-6 space-2">
								 <h4><a href="<?php the_permalink(); ?>"><?php the_title() ;?></a></h4>
								 <?php the_excerpt(); ?>
							 </div>

							 <?php endwhile; else: ?> <div class="col-sm-12"><p>Sorry, there are no posts to display</p></div> <?php endif; ?>
							<?php wp_reset_query(); ?>

						</div>
					</div><!-- end help-center-content -->
				</div>
				<div class="col-sm-4 col-lg-3">
					<div class="inset inset-gray space-4">
						<h5>Support</h5>
						<p>Need additional help or just want to work with a really nice human? <br>Please <a href="/contact-us/">contact us</a>.</p>
					</div>

					<?php

						 global $wp_query;
						 $postid = $wp_query->post->ID;
						 echo get_post_meta($postid, 'help_center_sidebar', true);
						 wp_reset_query();
					?>

				</div>
			</div>
		</div>
	</div><!-- end help-center -->



<?php
// end MAIN CONTENT
}

	//	Grab the main template
	include( get_template_directory() . '/templates/holy_grail_master.php' );



?>
