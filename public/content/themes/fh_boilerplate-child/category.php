<?php

/**
 *
 * Default Boilerplate WordPress Category Page
 *
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */


// Setup Holy Grail class
// $holy_grail_class = 'one_full';


//	Disable Breadcrumbs
add_action( '_fh_the_crumbs', '__return_false' );

// Disable on-page social sharing
add_action( '_fh_on_page_social_sharing', '__return_false' );

// Remove default title, sidebars, and content
add_action( '_fh_the_title', '__return_false' );

// Remove main content action and add custom profile main content
remove_action( '_fh_the_content', '_fh_content' );
add_action( '_fh_the_content', '_om_main_content' );


	//
	//	MAIN CONTENT
	//
	function _om_main_content() {


	?> 

	<div class="blog-wrap container">
		<div class="row">
			<div class="col-sm-8 col-lg-9 col-sm-push-4 col-lg-push-3">
				<div class="blog-post-content">

					<div class="post-content">

						<?php echo '<h1>' . single_cat_title( '', false ) . '</h1>'; ?>
						<p><center><a href="/blog">View all categories</a></center></p>

						<div class="row">

							<?php global $query_string; // required
								$posts = query_posts($query_string.'&posts_per_page=10'); ?>

								<?php if (have_posts()) : while (have_posts()) : the_post(); 

									// Get featured image
									$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>

								<div class="post-wrap col-md-6">
									<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
										<a href="<?php the_permalink(); ?>"><img src="<?php echo $feat_image ?>" class="img-responsive" /></a>
										<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
										<?php the_excerpt(); ?>
										<a class="btn btn-muted" href="<?php the_permalink(); ?>">Read More <span class="fa fa-arrow-right"></span></a>
									</div>
								</div>

							<?php endwhile; ?>

						</div>

						<div class="row navigation">

							<div class="col-sm-12">
								<?php posts_nav_link('&nbsp;&nbsp;&nbsp;','<span class="fa fa-arrow-left"></span> Newer Posts','Older Posts <span class="fa fa-arrow-right"></span>'); ?>
							</div>

						</div>

						<?php else : ?>

							<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
								<h1>Not Found</h1>
							</div>

						<?php endif; ?>

						<?php wp_reset_query(); // reset the query ?>
						
					</div>
					
				</div>
			</div>
			
			<?php include( get_stylesheet_directory() . '/templates/part_blog_sidebar.php' ); ?>
			
		</div>
	</div>
		

<?php
// end MAIN CONTENT
} 


//	Grab the main template
include( get_template_directory() . '/templates/holy_grail_master.php' );

?>