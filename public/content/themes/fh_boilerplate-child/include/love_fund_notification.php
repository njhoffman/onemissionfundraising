<?php

    //
    //  Shows om love fund fund notification modal on checkout/cart if shopping for love fund
    //
    function _om_love_fund_modal() {

        $fundraisers_from_cookie = _om_get_tracked_fundraisers_from_cookie();

        // Check cookie to see if shopping for love fund fund
        if( _om_shopping_for_love_fund() ) {

            include( get_stylesheet_directory() . '/templates/part_love_fund_modal.php' );
        }
    }

    add_action( 'woocommerce_before_cart', '_om_love_fund_modal' );
    add_action( 'woocommerce_before_checkout_form', '_om_love_fund_modal' );

    //
    //  Inject last 3 fundraisers viewed into cookie for love fund notification
    //
    function _om_track_viewed_fundraiser( $id, $name, $url, $description, $logo ) {

        $viewed_arr = array(
            'id'   => $id,
            'name' => $name,
            'url'  => $url,
            'desc' => $description,
            'logo' => $logo
        );

        $cookie_arr = _om_get_tracked_fundraisers_from_cookie();

        if( $cookie_arr !== false ) {

            // New array
            $store_arr   = array( $viewed_arr );
            $i           = 1;

            // Iterate over old cookie array and add anything that's not a duplicate
            foreach( $cookie_arr as $fundraiser ) {

                if( $id !== $fundraiser['id'] && $i < 3 ) {

                    array_push( $store_arr, $fundraiser );
                    $i++;
                }
            }

        } else {

            $store_arr = array( $viewed_arr );
        }

        $inject_into_cookie = array(
            'last_viewed_arr' => json_encode( $store_arr )
        );

        Utils::inject_cookie( $inject_into_cookie );
    }

    //
    //  Get last 3 fundraisers viewed or return false if no fundraisers have been viewed
    //
    function _om_get_tracked_fundraisers_from_cookie() {

        if( isset( $_COOKIE['last_viewed_arr'] ) && ! empty( $_COOKIE['last_viewed_arr'] ) ) {

            return json_decode( stripslashes( $_COOKIE['last_viewed_arr'] ), true );
        }

        return false;
    }

    //
    //  Function to check for if we're shopping for love fund
    //
    function _om_shopping_for_love_fund() {

        if( isset( $_COOKIE['shopping_for_id'] ) && is_numeric( $_COOKIE['shopping_for_id'] ) && $_COOKIE['shopping_for_id'] ) {

            return false;
        }

        return true;
    }
