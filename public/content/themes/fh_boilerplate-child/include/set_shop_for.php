<?php

//
//  Cookie logic for when shopping for an org group
//  - Passed a ID for the profile page; looks up everything from there
//
function _om_set_shop_for( $group_id = 0 ) {

    global $om_global_warning;

    $shop_for_id = 0;

    if( $group_id ) {

        $shop_for_id = $group_id;

    } elseif( isset( $_GET['shopfor'] ) && is_numeric( $_GET['shopfor'] ) ) {

        $shop_for_id = $_GET['shopfor'];
    }

    if( $shop_for_id ) {

        $fundraiser_profile = get_page( $shop_for_id );
        $fundraiser_fields  = get_fields( $shop_for_id );

        $is_closed = _om_is_fundraiser_closed(
            $fundraiser_fields['fundraiser_timed_or_continuous'],
            $fundraiser_fields['fundraiser_start_date'],
            $fundraiser_fields['fundraiser_end_date']
         );

        // If fundraiser is not closed then set shop for normally
        if( ! $is_closed ) {

            if( $fundraiser_profile && $fundraiser_profile->post_status == 'publish' ) {

                $show_monetary_status              = true;
                $fundraiser_goal_total             = floatval( $fundraiser_fields['fundraiser_goal_amount'] );
                $fundraiser_goal_total_from_orders = 0;

                //
                //  Check to ensure we're showing the status of the fundraiser
                //
                if( isset( $fundraiser_fields['show_fundraiser_status'] ) ) {

                    $show_monetary_status = ( $fundraiser_fields['show_fundraiser_status'] ) ?
                        true :
                        false;
                }

                //
                //  Ensure a goal is set and show_monetary_status is enabled
                //
                if( $show_monetary_status /* && $fundraiser_goal_total*/ ) {

						  $fundraiser_goal_total_from_orders = _om_get_total_donation_amount($shop_for_id);
						  // Run adjustment if need be (goal_status_adjustment)
						  if(isset( $fundraiser_fields['goal_status_adjustment'] ) && ! empty( $fundraiser_fields['goal_status_adjustment'] ) ) {
							  $fundraiser_goal_total_from_orders += (float) _om_get_gsa_total($fundraiser_fields['goal_status_adjustment']);
						  }
                }

                //
                //  Check for logo
                //
                $logo_image = '';

                if( isset( $fundraiser_fields['logo_image']['sizes']['shop_single'] ) && ! empty( $fundraiser_fields['logo_image']['sizes']['shop_single'] ) ) {

                    $logo_image = $fundraiser_fields['logo_image']['sizes']['shop_single'];
                }

                //
                //  Build arr of data we need to inject into cookie for "shopping for"
                //
                $inject_into_cookie = array(
                    'shopping_for_id'                   => $shop_for_id,
                    'fundraiser_user_id'                => $fundraiser_profile->post_author,
                    'fundraiser_name'                   => $fundraiser_profile->post_title,
                    'fundraiser_guid'                   => get_permalink( $shop_for_id ),
                    'fundraiser_profile_logo'           => $logo_image,
                    'fundraiser_goal_total'             => $fundraiser_goal_total,
                    'fundraiser_goal_total_from_orders' => $fundraiser_goal_total_from_orders
                );

                Utils::inject_cookie( $inject_into_cookie );
            }

        } else {

            // If fundraiser is closed set the global warning variable
            $om_global_warning = 'Oops! The ' . $fundraiser_profile->post_title . ' fundraiser is now closed. You can shop to support the One Mission Love Fund or <strong><a href="/support/">Find a Fundraiser to support</a></strong>!';
        }
    }
}
add_action( 'init', '_om_set_shop_for' );

//
//  Clears the current shopping for
//
function _om_clear_shop_for() {

    if( isset( $_GET['clearshopfor'] ) && is_numeric( $_GET['clearshopfor'] ) ) {

        // Build arr of data we need to inject into cookie to clear current shopping for
        $inject_into_cookie = array(
            'shopping_for_id' => '',
            'fundraiser_user_id' => '',
            'fundraiser_name' => '',
            'fundraiser_guid' => ''
        );

        Utils::inject_cookie( $inject_into_cookie );
    }
}
add_action( 'init', '_om_clear_shop_for' );

?>
