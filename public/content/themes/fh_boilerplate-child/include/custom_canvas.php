<?php

	class CustomCanvas {

		public $wp_upload_base_dir;
		public $cc_base_dir;
		public $imagemagick_supported;
		public $max_php_upload_size_in_bytes;
		public $max_php_upload_size_human;
		public $uploading_file;
		public $uploaded_file;
		public $canvas_product_page;

		private $debug;

		public function __construct( $debug = false ) {

			$this->debug               = $debug;
			$wp_upload_dir             = wp_upload_dir();
			$this->wp_upload_base_dir  = $wp_upload_dir['basedir'];
			$this->cc_base_dir         = $wp_upload_dir['basedir'] . '/custom_canvas/';
			$this->cc_public_dir       = $wp_upload_dir['baseurl'] . '/custom_canvas/';
			$this->uploading_file      = false;
			$this->canvas_product_page = home_url() . '/product/custom-canvas/';

			// Check the uploads directory
			$this->uploadsDirCheckAndCreate();

			// Check PHP max filesize
			$this->max_php_upload_size_in_bytes = $this->getMaximumFileUploadSizeInBytes();
			$this->max_php_upload_size_human    = $this->getHumanReadableSize( $this->max_php_upload_size_in_bytes, null, 0 );

			$this->dbg( 'Max PHP upload size = ' . $this->max_php_upload_size_human );

			// Check for ImageMagick support
			$this->imagemagick_supported = $this->imageMagickCheck();

			// Check for uploaded file
			if( isset( $_FILES['img'] ) && $this->imagemagick_supported ) {

				$this->uploading_file = true;
				$this->dbg( 'File upload detected' );

				$this->uploaded_file = new uploadedImage( $_FILES['img'] );

				if( ! empty( $this->uploaded_file->validation_error ) ) {

					$this->dbg( 'Uploaded image error - ' . $this->uploaded_file->validation_error );

				} else {

					$preview_filename           = md5( $this->uploaded_file->name . 'cust_canvas_FH' ) . '_' . time() . '_preview.jpg';
					$print_filename             = md5( $this->uploaded_file->name . 'cust_canvas_FH' ) . '_' . time() . '_print.jpg';
					$preview_server_destination = $this->cc_base_dir . $preview_filename;
					$preview_public_destination = $this->cc_public_dir . $preview_filename;
					$print_server_destination   = $this->cc_base_dir . $print_filename;
					$print_public_destination   = $this->cc_public_dir . $print_filename;

					$this->uploaded_file->createWebSafeJPG( $preview_server_destination );
					move_uploaded_file( $this->uploaded_file->tmp_name, $print_server_destination );

					$this->dbg( 'Uploaded image validated successfully' );
					$this->dbg( 'Preview image : ' . $preview_public_destination );
					$this->dbg( 'Print image : ' . $print_public_destination );
					$this->dbg( 'Orientation : ' . $this->uploaded_file->orientation );
					$this->dbg( 'Min canvas width @ 72dpi : ' . $this->uploaded_file->minimum_width );
					$this->dbg( 'Min canvas height @ 72dpi : ' . $this->uploaded_file->minimum_height );

					// Inject preview data into cookie
					$inject_into_cookie = array(
		                'cc_preview_image' => $preview_public_destination,
		                'cc_print_image'   => $print_public_destination,
		                'cc_orientation'   => $this->uploaded_file->orientation,
		                'cc_min_width'     => $this->uploaded_file->minimum_width,
		                'cc_min_height'    => $this->uploaded_file->minimum_height
		            );

		            Utils::inject_cookie( $inject_into_cookie );

		            // Redirect to WooCommerce page for canvas product
		            wp_redirect( $this->canvas_product_page );
				}
			}

			return;
		}

		//
		//	Uploaded file error message (if applicable)
		//
		public function getError( $echo = false ) {

			if( $this->uploading_file && is_object( $this->uploaded_file ) ) {

				if( ! empty( $this->uploaded_file->validation_error ) ) {

					$out = $this->renderError( $this->uploaded_file->validation_error );
				}
			}

			if( $echo ) {

				echo $out;
			}

			return $out;
		}

		//
		//	Output error
		//
		public function renderError( $error, $echo = false ) {

			$out = '<div id="custom_canvas_error" class="text-center">';
			$out .= '<div class="alert alert-danger">';
			$out .= '<p>' . $error . '</p><br>';
			$out .= '<p>Please give us a call at <a href="tel:319-895-4072">319-895-4072</a> if you have questions. We are happy to help!</p>';
			$out .= '</div>';
			$out .= '</div>';

			if( $echo ) {

				echo $out;
			}

			return $out;
		}

		//
		//	Render the progress bar for upload progress
		//
		public function getProgressBar( $echo = false ) {

			$out = '<div class="progress active" style="display: none;">';
			$out .= '<div id="custom_canvas_upload_progress" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>';
			$out .= '</div>';

			if( $echo ) {

				echo $out;
			}

			return $out;
		}

		//
		//	Render the upload form for the image
		//
		public function getUploadForm( $echo = false ) {

			$warning = 'By uploading this I explicitly agree that <strong>I own all rights to this image</strong> or <strong>can prove full permission to reproduce it under free license, public domain, or fair usage agreements</strong>.';

			$form_out = '<p class="alert alert-info text-center">' . $warning . '</p>';
			$form_out .= '<form id="frm_custom_canvas" name="frm_custom_canvas" enctype="multipart/form-data" method="post" action="">';
			$form_out .= '  <div class="form-group">';
			$form_out .= '    <label for="img">Custom Canvas Image Upload</label>';
			$form_out .= '    <input type="file" id="img" name="img">';
			$form_out .= '  </div>';
			$form_out .= '  <button type="submit" class="btn btn-info">Upload A Photo!</button>';
			$form_out .= '</form>';

			if( $echo ) {

				echo $form_out;
			}

			return $form_out;
		}

		//
		//	Checks for custom canvas uploads directory and creates if doens't exist
		//
		private function uploadsDirCheckAndCreate() {

			if( ! is_dir( $this->cc_base_dir ) ) {

				// Check to make sure the "custom_canvas" subdirectory exists
				$this->dbg( 'Uploads directory does not exist, creating ' . $this->cc_base_dir );

				if( is_writable( $this->wp_upload_base_dir ) ) {

					mkdir( $this->cc_base_dir );

					$this->dbg( 'Uploads directory created ' . $this->cc_base_dir );

				} else {

					$this->dbg( 'WP Base uploads directory not writable ' . $this->wp_upload_base_dir );

					return false;
				}

			} else {

				// "custom_canvas" subdirectory exists
				$this->dbg( 'Uploads directory exists ' . $this->cc_base_dir );

				if( is_writable( $this->cc_base_dir ) ) {

					$this->dbg( 'Uploads directory writable ' . $this->cc_base_dir );

					return true;

				} else {

					$this->dbg( 'Uploads directory not writable ' . $this->cc_base_dir );

					return false;
				}
			}
		}

		//
		//	ImageMagick Check
		//	- ensures 'imagick' extension is loaded into PHP
		//	- ensures 'Imagick' class exists
		//
		private function imageMagickCheck( $suppress_debug = false ) {

			if( ! $suppress_debug ) {

				if( extension_loaded('imagick') ) {

					$this->dbg( 'ImageMagick extension is loaded' );

				} else {

					$this->dbg( 'ImageMagick extension is NOT loaded' );
				}

				if( class_exists("Imagick") ) {

					$this->dbg( 'ImageMagick class exists' );

				} else {

					$this->dbg( 'ImageMagick class DOES NOT exist' );
				}
			}

			return( extension_loaded('imagick') && class_exists("Imagick") );
		}

		//
		//	Utility function to convert PHP size configurations to bytes
		// 	- Based on http://stackoverflow.com/questions/13076480/php-get-actual-maximum-upload-size/22500394#22500394
		//
		private function convertPHPSizeToBytes( $string_size ) {

			if ( is_numeric( $string_size ) ) {

				return $string_size;
			}

			$string_suffix_char = substr( $string_size, -1 );
			$converted          = substr( $string_size, 0, -1 );

			switch( strtoupper( $string_suffix_char ) ) {

				case 'P':
					$converted *= 1024;
				case 'T':
					$converted *= 1024;
				case 'G':
					$converted *= 1024;
				case 'M':
					$converted *= 1024;
				case 'K':
					$converted *= 1024;
				break;
			}

			return $converted;
		}

		//
		//	Function that returns maximum upload filesize suppoted by PHP
		// 	- Based on http://stackoverflow.com/questions/13076480/php-get-actual-maximum-upload-size/22500394#22500394
		//
		private function getMaximumFileUploadSizeInBytes() {

			return min( $this->convertPHPSizeToBytes( ini_get( 'post_max_size' ) ), $this->convertPHPSizeToBytes( ini_get( 'upload_max_filesize' ) ) );
		}

		//
		//	Utility function to convert bytes to human readable format
		//
		private function getHumanReadableSize( $size, $unit = null, $decemals = 2 ) {

			$byte_units = array( 'B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB' );

			if ( ! is_null( $unit ) && ! in_array( $unit, $byte_units ) ) {

				$unit = null;
			}

			$extent = 1;

			foreach( $byte_units as $rank ) {

				if ( ( is_null( $unit ) && ( $size < $extent <<= 10 ) ) || ( $rank == $unit ) ) {

					break;
				}
			}

			return number_format( $size / ( $extent >> 10 ), $decemals ) . $rank;
		}

		//
		//	Local debug function
		//
		private function dbg( $out = '' ) {

			if( $this->debug && ! empty( $out ) ) {

				echo '<p style="color: red;"><strong>CUSTOM CANVAS DEBUG:</strong><br>';
				echo '&nbsp;&nbsp;' . $out;
				echo '</p>';
			}
		}
	}

	class uploadedImage {

		// From $_FILES['img'] array
		public $name;
		public $mimetype;
		public $tmp_name;
		public $error;
		public $size;

		// Public displayable validation error
		public $validation_error;

		// Public ImageMagick handle for uploaded image
		public $imagemagick_handle;

		// Configuration options for validation
		private $allowed_extensions;
		private $allowed_mimetypes;
		private $allowed_exif_imagetypes;
		public $minimum_canvas_width_in;
		public $minimum_canvas_height_in;
		public $minimum_dpi;

		public $minimum_width;
		public $minimum_height;
		public $orientation;

		public function __construct( $uploaded_img ) {

			$this->name     = $uploaded_img['name'];
			$this->mimetype = $uploaded_img['type'];
			$this->tmp_name = $uploaded_img['tmp_name'];
			$this->error    = $uploaded_img['error'];
			$this->size     = $uploaded_img['size'];


			// Configuration options:

			// Allowed file extensions
			$this->allowed_extensions = array(
				'jpg',
				'jpeg',
				'tif',
				'tiff',
				'png',
				'bmp',
				'gif'
			);

			// Allowed mimetypes
			$this->allowed_mimetypes = array(
				'image/jpeg',
				'image/tiff',
				'image/png',
				'image/bmp',
				'image/gif'
			);

			// Allowed exif_imagetypes
			$this->allowed_exif_imagetypes = array(
				IMAGETYPE_JPEG,
				IMAGETYPE_TIFF_II,
				IMAGETYPE_TIFF_MM,
				IMAGETYPE_PNG,
				IMAGETYPE_BMP,
				IMAGETYPE_WBMP,
				IMAGETYPE_GIF
			);

			// Minimum canvas size in inches
			$this->minimum_canvas_width_in  = 8;
			$this->minimum_canvas_height_in = 8;

			// Minimum DPI for printing
			$this->minimum_dpi = 72;

			// Check if file is supported
			if( $this->checkSupported() ) {

				// Set the ImageMagick handle
				$this->imagemagick_handle = new Imagick( $this->tmp_name );

				// Set the ImageMagick image units
				$this->imagemagick_handle->setImageUnits( Imagick::RESOLUTION_PIXELSPERINCH );

				// Grab image info for use in the application
				$this->imagemagick_info = $this->imagemagick_handle->identifyImage();

				// Check to ensure proper resolution and DPI
				$this->checkResolution();
			}

			return;
		}


		public function createWebSafeJPG( $location ) {

			$img = new Imagick( $this->tmp_name );


			// Check for CMYK colorspace
			if( $img->getImageColorspace() == Imagick::COLORSPACE_CMYK ) {

				$icc_profiles_dir = get_stylesheet_directory() . '/custom_canvas/icc_profiles/';

				// We're only interested if ICC profile(s) exist 
				$icc_profiles = $img->getImageProfiles( '*', false );
				$has_icc_profile = ( array_search( 'icc', $icc_profiles ) !== false );

				// If it doesnt have a CMYK ICC profile, we add one
				if( $has_icc_profile === false ) {

					$icc_cmyk = file_get_contents( $icc_profiles_dir . 'USWebUncoated.icc' );

					$img->profileImage( 'icc', $icc_cmyk );

					unset( $icc_cmyk );
				}

				// Add an RGB profile
				$icc_rgb = file_get_contents( $icc_profiles_dir . 'sRGB_v4_ICC_preference.icc' );
				// $icc_rgb = file_get_contents( $icc_profiles_dir . 'ProPhoto.icc' );
				$img->profileImage( 'icc', $icc_rgb );

				unset( $icc_rgb );
			}

			// Drops size of the image dramatically (removes all profiles) - vastly affects image color quality
			// $img->stripImage();

			// Set color depth at 8bit and resample image at 72 DPI for web
			$img->setImageDepth( 8 );
			$img->setImageResolution( 72, 72 );
			$img->resampleImage( 72, 72, Imagick::FILTER_LANCZOS, 1 );

			// Scale image within 400x400 pixels
			$img->scaleImage( 400, 0 );
			$dimensions = $img->getImageGeometry();
    		$height = $dimensions['height'];

    		if( $height > 400 ) {

    			$img->scaleImage( 0, 400 );
    		}

			$img->setCompression( Imagick::COMPRESSION_JPEG );
			$img->setCompressionQuality( 100 );
			$img->setImageFormat( 'jpeg' );

			// $img->stripImage();

			$img->writeImage( $location );

			$img->clear();
			$img->destroy();
		}


		//
		//	Does checks to ensure that the image uploaded is supported by the application
		//	- Checks that file extension is supported
		//	- Checks that file mimetype is supported
		//	- Checks that file exif_imagetype (PHP function) is supported
		//
		private function checkSupported() {

			// Check file extension
			if( ! in_array( $this->getFileExtension( $this->name ), $this->allowed_extensions ) ) {

				$this->validation_error = 'File extension not supported. Extensions supported are *.' . implode( ', *.', $this->allowed_extensions );

				return false;
			}

			// Check file mimetype
			if( ! in_array( $this->mimetype, $this->allowed_mimetypes ) ) {

				$this->validation_error = 'File mimetype not supported. Mimetypes supported are ' . implode( ', ', $this->allowed_mimetypes );

				return false;
			}

			// Check exif_imagetype
			if( ! in_array( exif_imagetype( $this->tmp_name ), $this->allowed_exif_imagetypes ) ) {

				$this->validation_error = 'File type not supported. Failed EXIF image type test.';

				return false;
			}

			return true;
		}

		//
		//	Check resolution of image to ensure minimum printing requirements
		//
		private function checkResolution() {

			// Run check for minimum resolution
			if( ! ( $this->imagemagick_info['resolution']['x'] >= $this->minimum_dpi && $this->imagemagick_info['resolution']['y'] >= $this->minimum_dpi ) ) {

				$this->validation_error = 'Image resolution must be at least ' . $this->minimum_dpi . ' DPI.';

				return false;
			}

			$width_at_min_dpi  = ( $this->imagemagick_info['geometry']['width'] / $this->minimum_dpi );
			$height_at_min_dpi = ( $this->imagemagick_info['geometry']['height'] / $this->minimum_dpi );

			if( $width_at_min_dpi <= $this->minimum_canvas_width_in ) {

				$this->validation_error = 'Image width is too small. Image width/height must be at least 8 inches at 72 DPI. Current width at 72 DPI is ' . number_format( $width_at_min_dpi, 2 ) . ' inches.';
				return false;
			}
			
			$this->minimum_width = $width_at_min_dpi;

			if( $height_at_min_dpi <= $this->minimum_canvas_height_in ) {

				$this->validation_error = 'Image height is too small. Image width/height must be at least 8 inches at 72 DPI. Current height at 72 DPI is ' . number_format( $height_at_min_dpi, 2 ) . ' inches.';

				return false;
			}

			$this->minimum_height = $height_at_min_dpi;

			if( $width_at_min_dpi == $height_at_min_dpi ) {

				$this->orientation = 'square';
			
			} elseif ( $width_at_min_dpi > $height_at_min_dpi ) {

				$this->orientation = 'landscape';

			} else {

				$this->orientation = 'portrait';
			}

			return true;
		}

		//
		//	Returns file extension
		//
		private function getFileExtension( $filename ) {

			$path_info = pathinfo( $filename );

			return strtolower( $path_info['extension'] );
		}
	}
?>