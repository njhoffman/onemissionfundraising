<?php

/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }

if ( ! class_exists( 'HubSpotApi' ) ) {

	class HubSpotApi {

		private $args = array();
		static private $api_key;
		static private $oauth_token;
		protected $base_uri = 'https://api.hubapi.com/';
		protected $route = '';

		public $field_map = array(
			'fundraiser_start_date' => 'start_date',
			'fundraiser_end_date' => 'end_date',
			'fundraiser_goal_amount' => 'goal_amount',
			'payee_check_name' => 'name_on_check',
			'payee_zip' => 'payee_zip_code',
			'leader_organization' => 'company',
			'leader_address' => 'address',
			'leader_city' => 'city',
			'leader_phone' => 'phone',
			'leader_state' => 'state',
			'leader_zip' => 'zip',
			'fundraiser_facebook' => 'facebook_url',
			'fundraiser_twitter' => 'twitter_url',
			'bulk_free_shipping' => 'bulk_shipping_code'
		);

		public $field_skip = array(
			'fundraiser_description',
			'logo_image',
			'banner_image',
			'banner_youtube_video_id',
			'image_or_youtube_banner',
			'document_501c3',
      '501c3_document',
			'thank_you_message',
			'_yoast_wpseo_opengraph-image',
			'yoast_wpseo_opengraph-image',
			'enable_501c',
			'fundraiser_instagram',
			'fundraiser_website',
			'enable_social_sharing',
			'gallery_type',
			'show_fundraiser_status',
			'pending_approval',
			'is_featured',
			'fundraiser_city_state',
      'dsq_thread_id',
      'goal_status_adjustment',
      'group_product_category'
		);


		function __construct( $api_key = null ) {
			static::$api_key = $api_key;
		}

		protected function build_request( $route, $args = array(), $method = 'GET' ) {
			// Start building query.
			$this->set_headers();
			$this->args['method'] = $method;
			$this->route = $route;

			// Generate query string for GET requests.
			if ( 'GET' === $method ) {
				$this->route = add_query_arg( array_filter( $args ), $route );
			} elseif ( 'application/json' === $this->args['headers']['Content-Type'] ) {
				$this->args['body'] = wp_json_encode( $args );
			} else {
				$this->args['body'] = $args;
			}

			return $this;
		}


		protected function fetch() {

			if ( ! empty( static::$api_key ) ) {
				$this->route = add_query_arg( 'hapikey', static::$api_key, $this->route );
			}

			// Make the request.
			$response = wp_remote_request( $this->base_uri . $this->route, $this->args );

			// Retrieve Status code & body.
			$code = wp_remote_retrieve_response_code( $response );
			$body = json_decode( wp_remote_retrieve_body( $response ) );

			// Return WP_Error if request is not successful.
			if ( ! $this->is_status_ok( $code ) ) {
				return new WP_Error( 'response-error', sprintf( __( 'Status: %d', 'wp-hubspot-api' ), $code ), $body );
			}
			$this->clear();

			return $body;
		}

		protected function set_headers() {
			// Set request headers.
			$this->args['headers'] = array(
            'Content-Type' => 'application/json',
	        );

			if ( ! empty( static::$oauth_token ) ) {
				$this->args['headers'] = array(
					'Authorization' => 'Bearer '. static::$oauth_token,
				);
			}
		}

		protected function clear() {
			$this->args = array();
		}

		protected function is_status_ok( $code ) {
			return ( 200 <= $code && 300 > $code );
		}

		function create_or_update_contact_by_email($email, $properties) {
			$args = array(
				'properties' => $properties
			);
			return $this->build_request( 'contacts/v1/contact/createOrUpdate/email/' . $email, $args,  'POST' )->fetch();
		}

		function update_contact_by_email($email, $properties) {
			$args = array(
				'properties' => $properties
			);
			return $this->build_request( 'contacts/v1/contact/email/' . $email . '/profile', $args,  'POST' )->fetch();
		}

		function add_contact_to_list_by_id($list_id, $contact_id) {
			$args = array(
				'vids' => [$contact_id]
			);
			return $this->build_request( 'contacts/v1/lists/' . $list_id . '/add', $args,  'POST' )->fetch();
		}

		function get_contact_by_email($email) {
			return $this->build_request( 'contacts/v1/contact/email/' . $email . '/profile')->fetch();
		}

	}
}

// $HubSpotApi = new HubSpotApi('52a34663-1340-42ab-9aa0-76585ec6be15');
$HubSpotApi = new HubSpotApi('6e921279-82ea-41aa-b462-0f3a6fe18750');

function _om_save_post_update_hubspot($post_id, $post_after, $post_before) {
	global $HubSpotApi;
	$post_meta = get_post_meta($post_id);
	// don't update hubspot if not a fundraiser page update
	if ($post_after->post_parent !== OM_FUNDRAISER_PAGE_PARENT_ID) {
		return;
	}
	$author_id = $post_after->post_author;
	$author = get_userdata($author_id);
	$post_title = $post_after->post_title;
	$fundraiser_url = get_permalink($post_after);
	$shop_for = $fundraiser_url . '?shopfor=' . $post_id;
	$visibility = $post_after->post_status == 'private' ? 'Private' : 'Public';
	$hubspot_fields = array(
		array('property' => 'fund_name', 'value' => $post_title),
		array('property' => 'fund_id', 'value' => $post_id),
		array('property' => 'fund_permalink', 'value' => $fundraiser_url),
		array('property' => 'shop_for_link', 'value' => $shop_for),
		array('property' => 'visibility', 'value' => ucfirst($visibility))
	);
	foreach ($post_meta as $key => $val) {
		// currently not in hubspot
		$hubspot_key = $key;
		$val = $val[0];
		if ($key === 'fundraiser_type') {
			$val = str_replace('&', ';', $val);
		} else if ($key === 'fundraiser_timed_or_continuous') {
			$hubspot_key = 'continuous';
			$val = false;
		} else if ($key === 'fundraiser_start_date' || $key === 'fundraiser_end_date') {
			$hubspot_key = $HubSpotApi->field_map[$key];
			$val = strtotime($val) * 1000;
		} else if (array_key_exists($key, $HubSpotApi->field_map)) {
			$hubspot_key = $HubSpotApi->field_map[$key];
		}
		if (!in_array($key, $HubSpotApi->field_skip) && substr($key, 0, 1) !== '_') {
			array_push($hubspot_fields, array( "property" => $hubspot_key, "value" => $val ));
		}
	}
	$hs_update = $HubSpotApi->update_contact_by_email($author->user_email, $hubspot_fields);
	_dbg("Hubspot Post Update for $author->user_email", $hs_update);
	_dbg($hubspot_fields);
}
add_action('save_post', '_om_save_post_update_hubspot', 1, 3);

function _om_register_update_hubspot($user_id) {
	global $HubSpotApi;
	$user = get_userdata($user_id);
	$user_meta = get_user_meta($user_id);

	$first_name = $user_meta['first_name'][0];
	$last_name = $user_meta['last_name'][0];

	if (empty($first_name) && isset($_POST['billing_first_name'])) {
		$first_name = $_POST['billing_first_name'];
	}

	if (empty($last_name) && isset($_POST['billing_last_name'])) {
		$last_name = $_POST['billing_last_name'];
	}

	// update hubspot contact fields, should create contact automatically on account creation
	$hubspot_fields = array(
		array('property' => 'fund_creation_status', 'value' => 'Pre-Fund'),
	);

	if (!empty($first_name)) {
		array_push($hubspot_fields, array('property' => 'firstname', 'value' => $first_name));
	}
	if (!empty($last_name)) {
		array_push($hubspot_fields, array('property' => 'lastname', 'value' => $last_name));
	}
	if (isset($_POST['purchase_with_purpose'])) {
		$pwp = $_POST['purchase_with_purpose'] == '1' ? 'true' : 'false';
		array_push($hubspot_fields, array('property' => 'purchasewithpurpose_subscribeme', 'value' => $pwp));
	}
	$hs_update = $HubSpotApi->create_or_update_contact_by_email($user->user_email, $hubspot_fields);
	// $hs_list_update = $HubSpotApi->add_contact_to_list_by_id('131', $hs_update->vid);

}
add_action('user_register', '_om_register_update_hubspot', 10, 1);
