<?php

	//
	//	Custom Canvas WooCommerce Page
	//
	function _om_custom_canvas_product_init() {

		global $post;

		if( is_product() && $post->ID == OM_CUSTOM_CANVAS_POST_ID ) {

			if( 
				isset( $_COOKIE['cc_preview_image'] ) && ! empty( $_COOKIE['cc_preview_image'] ) &&
				isset( $_COOKIE['cc_print_image'] ) && ! empty( $_COOKIE['cc_print_image'] ) &&
				isset( $_COOKIE['cc_orientation'] ) && ! empty( $_COOKIE['cc_orientation'] ) &&
				isset( $_COOKIE['cc_min_width'] ) && ! empty( $_COOKIE['cc_min_width'] ) &&
				isset( $_COOKIE['cc_min_height'] ) && ! empty( $_COOKIE['cc_min_height'] )
			) {

				// Do nothing, we're good - got what we need to do product customization
			
			} else {

				// We're missing data - redirect to custom canvas page
				wp_redirect( home_url() . '/custom-canvas/?ccerror=missingimg' );
				exit;
			}

			$cc_vars = array(
				'cc_preview_image' => $_COOKIE['cc_preview_image'],
				'cc_orientation'   => $_COOKIE['cc_orientation'],
				'cc_min_width'     => $_COOKIE['cc_min_width'],
				'cc_min_height'    => $_COOKIE['cc_min_height']
			);

			// Enqueue assets for custom canvas
			wp_register_script( 'custom_canvas_product_view', get_stylesheet_directory_uri() . '/js/custom_canvas_product_view.js', array( 'jquery' ) );
			wp_localize_script( 'custom_canvas_product_view', 'cc_vars', $cc_vars );
			wp_enqueue_script( 'custom_canvas_product_view' );
		}
	}

	add_action( 'wp_enqueue_scripts', '_om_custom_canvas_product_init' );

	//
	//	Add button after product title
	//
	function _om_custom_canvas_after_header() {

		global $post;

		if( is_product() && $post->ID == OM_CUSTOM_CANVAS_POST_ID ):
		
		?>

			<br>
			<div class="text-center">

				<a class="btn btn-info" href="<?php echo home_url(); ?>/custom-canvas/">Upload a different image&nbsp; <i class="fa fa-undo"></i></a>

			</div>
			<br>

		<?php

		endif;
	}
	add_action( 'woocommerce_single_product_summary', '_om_custom_canvas_after_header' );
?>
