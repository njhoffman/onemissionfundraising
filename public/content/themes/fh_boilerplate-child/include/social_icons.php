<?php

//
//  CHILD THEME OVERRIDE:
//  Logic to go through and display social icons with links
//  - Utilizes ACF global options page: http://www.advancedcustomfields.com/resources/how-to/how-to-get-values-from-an-options-page/
//  - Utilizes Font Awesome: http://fontawesome.io/
//
if ( ! function_exists( '_fh_social_icons' ) ) :
function _fh_social_icons( $links = array(), $echo = true, $object = '', $rss_icon_enabled = true ) {

    // $links = array( array( 'fa-facebook' => 'http://facebook.com/mr-orgasmo', ... ), ... );

    // If object is empty set to blog name
    $object = ( empty( $object ) ) ? get_bloginfo( 'name' ) : $object;

    global $social_icons;

    // If there's nothing usable passed in then look for ACF-set options
    if( ! sizeof( $links ) ) {

        $socials_arr = array();

        // Grab ACF Repeater field
        $options = get_field( 'social_accounts', 'option' );

        if( $options ) {

	        foreach( $options as $key => $option ) {

	            $socials_arr[ $option['icon_key'] ] = $option['social_account_url'];
	        }
        }

    } else {

        // Sets social links array to passed in value, probably for an author block
        $socials_arr = $links;
    }

    // Check for enabled RSS link
    if( $rss_icon_enabled ) {

        $rss_enabled = get_field( 'rss_link_enabled', 'option' );

        if( $rss_enabled == 'true' ) {

            $socials_arr[ 'rss' ] = get_bloginfo_rss( 'rss2_url' );
        }
    }

    // Holds output HTML
    $html = '';

    // Iterate over socail links array and output social links
    foreach( $socials_arr as $icon_class => $link ) {

        if( !empty( $link ) ) {

            // Custom icon image override
            if( $icon_class == 'facebook' ) {

                $inner_html = '<img src="' . get_stylesheet_directory_uri() . '/img/social/fbook.png" alt="' . $object . $social_icons[ $icon_class ]['join'] . $social_icons[ $icon_class ]['network'] . '">';

            } elseif ( $icon_class == 'pinterest' ) {

                $inner_html = '<img src="' . get_stylesheet_directory_uri() . '/img/social/pinterest.png" alt="' . $object . $social_icons[ $icon_class ]['join'] . $social_icons[ $icon_class ]['network'] . '">';

            } elseif ( $icon_class == 'twitter' ) {

                $inner_html = '<img src="' . get_stylesheet_directory_uri() . '/img/social/twitter.png" alt="' . $object . $social_icons[ $icon_class ]['join'] . $social_icons[ $icon_class ]['network'] . '">';

            } else {

                $inner_html = '<i class="fa fa-' . $icon_class . '"></i>';
            }

            $html .= '<a href="' . $link . '" target="_blank" rel="no-follow" class="social_icon" title="' . $object . $social_icons[ $icon_class ]['join'] . $social_icons[ $icon_class ]['network'] . '">' . $inner_html . '</a> ' . "\n";
        }
    }

    // Echo or return html
    if( $echo ) {

        echo $html;
        return;
    }

    return $html;
}
endif;

?>