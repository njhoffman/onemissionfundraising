
<?php

	function _om_get_fundraisers_orders_meta() {
		global $post;
		global $wpdb;

		$meta = array('count' => 0, 'months' => array());

		// completed|processing|canceled|on-hold
		$query_status = array( 'wc-processing', 'wc-completed', 'wc-on-hold', 'wc-cancelled', 'wc-pending' );

		// Setup args for custom query
		$order_args = array(
			'posts_per_page' => -1,
			'post_type' =>      'shop_order',
			'post_status' =>    $query_status,
			'orderby' =>        'post_date',
			'order' =>          'DESC',
			'meta_query' => array(
				array(
					'key' =>   'Fundraiser User ID',
					'value' => get_current_user_id()
				)
			)
		);

		// $gsaObj = _om_get_goal_status_adjustments();

		// Run query
		$orders = new WP_Query( $order_args );
		if( $orders->have_posts()) {
			$meta['count'] = $orders->post_count;
			while($orders->have_posts()) {
				$orders->the_post();
				$order_dt = date('m-Y', strtotime($post->post_date));
				if (isset($meta['months'][$order_dt])) {
					$meta['months'][$order_dt] = ++$meta['months'][$order_dt];
				} else {
					$meta['months'][$order_dt] = 0;
				}

			}
		}
		return $meta;
	}

	function _om_gsa_map($gsa) {
		$gsa_data = array('table' => array(), 'chart' => array());
		$gsa_date = (float) $gsa['date'];
		$gsa_data['table']['date']                   = $gsa_date;
		$gsa_data['table']['order_date']             = date('F d, Y', $gsa_date);
		$gsa_data['table']['amount']                 = $gsa['amount'];
		$gsa_data['table']['description']            = $gsa['description'];
		$gsa_data['table']['product_donation']       = 0;
		$gsa_data['table']['product_purchases']      = 0;
		$gsa_data['table']['offline_donation']       = floatval($gsa['amount']);
		$gsa_data['table']['cash_donation']          = 0;
		$gsa_data['chart']['order_total']            = floatval($gsa['amount']);
		$gsa_data['chart']['orders']                 = 1;
		$gsa_data['chart']['cash_donation']          = 0;
		$gsa_data['chart']['product_donation']       = 0;
		$gsa_data['chart']['subscription_donation']  = 0;
		$gsa_data['chart']['donation_total']         = floatval($gsa['amount']);
		$gsa_data['chart']['offline_donation']       = floatval($gsa['amount']);
		$gsa_data['chart']['order_month_ts']         = mktime(0, 0, 0, date('m', $gsa_date), 1, date('Y', $gsa_date));
		return $gsa_data;
	}

	function _om_get_fundraisers_orders( $local_debug = false ) {
		$query_start = microtime(true);

		global $post;
		global $wpdb;
		global $statsd;

		// post_status -> wc-pending|wc-processing|wc-on-hold|wc-completed|wc-cancelled|wc-refunded|wc-failed

		// completed|processing|canceled|on-hold
		$query_status = array( 'wc-processing', 'wc-completed', 'wc-on-hold', 'wc-cancelled', 'wc-pending' );

		// Setup args for custom query
		$order_args = array(
			'posts_per_page' => -1,
			'post_type' =>      'shop_order',
		   'post_status' =>    $query_status,
			'orderby' =>        'post_date',
			'order' =>          'DESC',
			'meta_query' => array(
				array(
					'key' =>   'Fundraiser User ID',
					'value' => get_current_user_id()
				)
			)
		);

		$gsaObj = _om_get_goal_status_adjustments();
		$ret_data = array('chart' => array(), 'table' => array());
		$ret_data['total'] = array('orders' => 0, 'donations' => 0, 'total' => 0, 'unknown' => 0, 'cash' => 0, 'subscription' => 0);

		$subscribers = array();

		// Run query
		$orders = new WP_Query( $order_args );

		if( $orders->have_posts()) {

			$i = 0;

			while( $orders->have_posts() ) {

				// Load the post into context
				$orders->the_post();

				// Grab ID, dates/timestamps, format dates/timestamps, grab fields
				$order_id         = $post->ID;
				$order_date       = $post->post_date;
				$order_timestamp  = strtotime( $order_date );
				$order_human_date = date( 'F j, Y', $order_timestamp );
				$order_graph_date = date( 'n/y', $order_timestamp );
				$order_comment    = $post->post_excerpt;
				$fields           = get_post_custom( $order_id );

				$subscription_donation = 0;
        $product_purchases = 0;
        $product_donation = false;
				$cash_donation = 0;
				$unknown_donation = 0;
        $net_donation = 0;
        $order_tax = 0;
        $credit_card_fee = 0;
        $order_shipping = 0;
        $transaction_fee = 0;
        $tip_total = 0;
        $order_total = 0;
        $order_total_without_tip = 0;

        $donation_attr = isset($fields['Public Donation Attribution']) && !empty($fields['Public Donation Attribution'][0]) ?
          $fields['Public Donation Attribution'][0] : '';
        $total_donation_amt   = tofloat( $fields['Total Donation Amount'][0] );


        // if ($order_id == 150193) {
        //   _dbg($fields);
        // }
        if (isset($fields['Order Total'])) {
          $product_purchases = tofloat($fields['Product Purchases'][0]);
          $subscription_donation = tofloat($fields['Subscription Donation'][0]);
          $cash_donation = tofloat($fields['Cash Donation'][0]);
          $product_donation = tofloat($fields['Product Donation'][0]);
          $order_total_without_tip = tofloat($fields['Order Total'][0]);
          $net_donation = tofloat($fields['Total Net Donation'][0]);
          $order_shipping = tofloat($fields['Shipping'][0]);
          $order_tax = tofloat($fields['Tax'][0]);
          $credit_card_fee = tofloat($fields['Stripe Fee'][0]);
          $transaction_fee = tofloat($fields['Transaction Fee'][0]);
        } else {
          if (isset($fields['OMF Subscription Order']) && $fields['OMF Subscription Order'] == true) {
            $sub_id = isset($fields['_subscription_renewal']) ? $fields['_subscription_renewal'][0] : false;
            $sub_order_data = array( 'id' => $order_id );
            if (isset($fields['_paid_date'])) {
              $subscription_donation += tofloat($fields['_order_total'][0]);
              $sub_order_data['paid'] = $fields['_paid_date'][0];
              if (isset($fields['_completed_date'])) {
                $sub_order_data['completed'] = $fields['_completed_date'][0];
              }
              $sub_order_data['total'] = $fields['_order_total'][0];
            }

            if ($sub_id && array_key_exists($sub_id, $subscribers)) {
              array_push($subscribers[$sub_id]['orders'], $sub_order_data);
            } else {
              if ($sub_id == false || $sub_id == '') {
                // have to perform a separate query for subscriptions that have no existing renewals yet, barf
                $parent_order = new WC_Order($order_id);
                foreach ($parent_order->get_items() as $po) {
                  if (isset($po['product_id'])) {
                    $po_id = $po['product_id'];
                    $po_subs = wcs_get_subscriptions(array('order_id' => $order_id, 'product_id' => $po_id));
                    if (isset($po_subs) && count($po_subs) > 0) {
                      $sub = array_pop($po_subs);
                      $sub_id = $sub->id;
                      break;
                    }
                  }
                }
              } else {
                $sub = wcs_get_subscription($sub_id);
              }
              if ($sub && $sub_id > 0) {
                //
                // need to get subscription price and interval information
                // TODO: cache this, almost always the same!
                $sub_primary_order = new WC_Order($sub_id);
                $sub_orders = $sub_primary_order->get_items();
                $sub_total = null;
                foreach ($sub_orders as $sub_order) {
                  $sub_prod_id = !empty($sub_order->get_product_id()) ?
                    $sub_order->get_product_id() : false;
                  if ($sub_prod_id) {
                    $sub_prod = wc_get_product($sub_prod_id);
                    $sub_total = $sub_prod->subscription_price . ' every ';
                    $sub_total .=  $sub_prod->subscriptiion_period_interval == 1 ?  $sub_prod->subscription_period
                      : $sub_prod->subscription_period_interval . ' ' . $sub_prod->subscription_period . 's';
                  }
                }

                if (!empty($sub_total)) {
                  $subscribers[$sub_id] = array(
                    'status' => get_post_status($sub_id),
                    'name' => $fields['_billing_first_name'][0] . ' ' . $fields['_billing_last_name'][0],
                    'start' => $sub->get_date('start'),
                    'next_payment' => $sub->get_date('next_payment'),
                    'last_payment' => $sub->get_date('last_order_date_paid'),
                    'orders' => array($sub_order_data),
                    'total' => $sub_total
                  );
                }
              }
            }
          }

          // get cash donations
          $od = new WC_Order($order_id);
          $od_items = $od->get_items();
          foreach($od_items as $odi) {
            // cash donations (i.e. custom donations) have changed database entries over the years, because why not
            if ($odi['name'] == 'Custom Donation' || $odi['name'] == 'CUSTOM-DONATION') {
              if (isset($odi['item_meta']['_line_total'])) {
                $cash_donation += tofloat($odi['item_meta']['_line_total'][0]);
              } else if (isset($odi['total'])) {
                $cash_donation += tofloat($odi['total']);
              } else {
                $total = $fields['_order_total'][0];
                // if no item meta found and only 1 order, assume cash donation is entire order
                if (count($od_items) == 1) {
                  $cash_donation += tofloat($total);
                } else {
                  $unknown_donation += tofloat($total);
                }
              }
            }
          }

          // Get base order $ without shipping/tax
          $order_total    = tofloat( $fields['_order_total'][0] );
          $order_tax      = tofloat( $fields['_order_tax'][0] );
          $order_shipping = tofloat( $fields['_order_shipping'][0] );
          $order_base     = $order_total - $order_tax - $order_shipping;
          $credit_card_fee = number_format(0.029 * $order_total + 0.3, 2, '.', '');
          $net_donation   = number_format($total_donation_amt - $credit_card_fee, 2, '.', '');
        }

        // an order with this status should not be included
        $post_status = get_post_status();
        if ($post_status == 'wc-cancelled'
          || $post_status == 'wc-on-hold'
          || $post_status == 'wc-pending') {
          continue;
        }

        // inject goal status adjustments by date
        foreach($gsaObj['gsas'] as $idx => $gsa) {
          $gsa_date = $gsa['date'];
          // need to insert new entries
          if ($gsa_date &&
            ($gsa_date > $order_timestamp || ($gsa_data > $order_timestamp && $i === 0 ))) {
            $gsa_assign = _om_gsa_map($gsa);
            $ret_data['table'][$i] = $gsa_assign['table'];
            $order_graph_date = date('n/y', $gsa['date']);
            // Add order/donation totals for graph
            if(! isset( $ret_data['chart'][$order_graph_date] ) ) {
              $ret_data['chart'][$order_graph_date] = $gsa_assign['chart'];
            } else {
              $ret_data['chart'][$order_graph_date]['orders']++;
              $ret_data['chart'][$order_graph_date]['order_total']           += tofloat($gsa['amount']);
              $ret_data['chart'][$order_graph_date]['donation_total']        += tofloat($gsa['amount']);
              $ret_data['chart'][$order_graph_date]['offline_donation']      += tofloat($gsa['amount']);
            }
            $ret_data['total']['total'] += tofloat($gsa['amount']);
            unset($gsaObj['gsas'][$idx]);
            $i++;
          }
        }

        $od = new WC_Order($order_id);
        // get tip
        foreach( $od->get_items('fee') as $item_id => $item_fee ){
          $tip_total = $item_fee->get_total();
        }
        if ($product_donation === false) {
          $product_donation = number_format(0.4 * ($order_base - $cash_donation - $subscription_donation), 2, '.', '');
          $product_purchases = number_format(($order_base - $cash_donation - $subscription_donation), 2, '.', '');
        }

        // Format customer address
        $formatted_address = '';
        $formatted_address .= ( ! empty( $fields['_billing_company'][0] ) ) ? $fields['_billing_company'][0] . '<br>' : '';
        $formatted_address .= $fields['_billing_address_1'][0] . '<br>';
        $formatted_address .= ( ! empty( $fields['_billing_address_2'][0] ) ) ? $fields['_billing_address_2'][0] . '<br>' : '';
        $formatted_address .= $fields['_billing_city'][0] . ', ' . $fields['_billing_state'][0] . ' ' . $fields['_billing_postcode'][0];


        // Format customer email
        $formatted_email = '<a href="mailto:' . $fields['_billing_email'][0] . '" target="_blank">' . $fields['_billing_email'][0] . '</a>';

        if ($transaction_fee == 0) {
          // Transaction Fee = (Cash Donation + Product Purchases + Tax + Shipping) * 0.029 + 0.30
          $transaction_fee = ($cash_donation + $product_purchases + $order_tax + $order_shipping) * 0.029 + 0.3; // - $fee_total_tax;
        }

        if ($order_total_without_tip === 0) {
          $order_total_without_tip = $order_total - $tip_total;
        }
        if ($order_total === 0) {
          $order_total = $order_total_without_tip + $tip_total;
        }

        $ret_data['total']['orders']       += $order_total_without_tip;
        $ret_data['total']['unknown']      += $unknown_donation;
        $ret_data['total']['subscription'] += $subscription_donation;
        $ret_data['total']['cash']         += $cash_donation;
        $ret_data['total']['donations']    += str_replace(",", "", $fields['Total Donation Amount'][0]);
        $ret_data['total']['total']        += str_replace(",", "", $fields['Total Donation Amount'][0]);

        _dbg("$order_id $product_purchases $cash_donation");

        if( $order_total > 0 ) {

          // Add order/donation details and totals for admin table
          $ret_data['table'][$i]['ID']                    = $order_id;
          $ret_data['table'][$i]['order_date']            = $order_human_date;
          $ret_data['table'][$i]['order_taxes']           = $order_tax;
          $ret_data['table'][$i]['order_taxes_shipping']  = number_format($order_tax + $order_shipping, 2, '.', ',');
          $ret_data['table'][$i]['order_shipping']        = number_format($order_shipping, 2, '.', ',');
          // $ret_data['table'][$i]['order_total']           = $order_total;
          $ret_data['table'][$i]['total']                 = number_format($order_total_without_tip, 2, '.', ',');
          $ret_data['table'][$i]['transaction_fee']       = number_format($transaction_fee, 2, '.', ',');
          $ret_data['table'][$i]['order_total']           = number_format($order_total, 2, '.', ',');
          $ret_data['table'][$i]['product_donation']      = number_format($product_donation, 2, '.', ',');
          $ret_data['table'][$i]['product_purchases']     = number_format($product_purchases, 2, '.', ',');
          $ret_data['table'][$i]['offline_donation']      = 0;
          $ret_data['table'][$i]['cash_donation']         = number_format($cash_donation, 2, '.', ',');
          $ret_data['table'][$i]['subscription_donation'] = number_format($subscription_donation, 2, '.', ',');
          $ret_data['table'][$i]['net_donation']          = number_format($net_donation, 2, '.', ',');
          $ret_data['table'][$i]['donation_total']        = number_format($total_donation_amt, 2, '.', ',');
          // $ret_data['table'][$i]['donation_total']        = number_format($cash_donation + $subscription_donation + $product_donation, 2, '.', '');
          $ret_data['table'][$i]['name']                  = $fields['_billing_first_name'][0] . ' ' . $fields['_billing_last_name'][0];
          $ret_data['table'][$i]['first_name']            = $fields['_billing_first_name'][0];
          $ret_data['table'][$i]['last_name']             = $fields['_billing_last_name'][0];
          $ret_data['table'][$i]['address']               = $formatted_address;
          $ret_data['table'][$i]['address_raw']           = $fields['_billing_address_1'][0];
          $ret_data['table'][$i]['address_raw_2']         = $fields['_billing_address_2'][0];
          $ret_data['table'][$i]['company_name']          = $fields['_billing_company'][0];
          $ret_data['table'][$i]['city_state_zip']        = $fields['_billing_city'][0] . ', ' . $fields['_billing_state'][0] . ' ' . $fields['_billing_postcode'][0];
          $ret_data['table'][$i]['city']                  = $fields['_billing_city'][0];
          $ret_data['table'][$i]['state']                 = $fields['_billing_state'][0];
          $ret_data['table'][$i]['zip_code']              = $fields['_billing_postcode'][0];
          $ret_data['table'][$i]['email']                 = $formatted_email;
          $ret_data['table'][$i]['note']                  = $order_comment;
          $ret_data['table'][$i]['donation_attr']         = $donation_attr;

          // Add order/donation totals for graph
          if( isset( $ret_data['chart'][$order_graph_date] ) ) {
            $ret_data['chart'][$order_graph_date]['orders']++;
            $ret_data['chart'][$order_graph_date]['order_total']           += $order_total_without_tip;
            $ret_data['chart'][$order_graph_date]['donation_total']        += $total_donation_amt;
            $ret_data['chart'][$order_graph_date]['cash_donation']         += $cash_donation;
            $ret_data['chart'][$order_graph_date]['subscription_donation'] += $subscription_donation;
            $ret_data['chart'][$order_graph_date]['product_donation']      += $product_donation;
          } else {
            $ret_data['chart'][$order_graph_date]['order_month_ts']        = mktime(0, 0, 0, date('m', $order_timestamp), 1, date('Y', $order_timestamp));
            $ret_data['chart'][$order_graph_date]['orders']                = 1;
            // $ret_data['chart'][$order_graph_date]['order_total']           = $order_base - $total_donation_amt;
            $ret_data['chart'][$order_graph_date]['order_total']           = $order_total_without_tip;
            $ret_data['chart'][$order_graph_date]['donation_total']        = $total_donation_amt;
            $ret_data['chart'][$order_graph_date]['cash_donation']         = $cash_donation;
            $ret_data['chart'][$order_graph_date]['subscription_donation'] = $subscription_donation;
            $ret_data['chart'][$order_graph_date]['product_donation']      = $product_donation;
            $ret_data['chart'][$order_graph_date]['offline_donation']      = 0;
          }

          $i++;
        }
      }
    }


    // output remaining gsas
    foreach($gsaObj['gsas'] as $idx => $gsa) {
      if (isset($gsa['date']) && !empty($gsa['date'])) {
        $gsa_assign = _om_gsa_map($gsa);
        $ret_data['table'][$i] = $gsa_assign['table'];
        $order_graph_date = date('n/y', (float) $gsa['date']);
        // Add order/donation totals for graph
        if(! isset( $ret_data['chart'][$order_graph_date] ) ) {
          $ret_data['chart'][$order_graph_date] = $gsa_assign['chart'];
        } else {
          $ret_data['chart'][$order_graph_date]['orders']++;
          $ret_data['chart'][$order_graph_date]['order_total']           += tofloat($gsa['amount']);
          $ret_data['chart'][$order_graph_date]['donation_total']        += tofloat($gsa['amount']);
          $ret_data['chart'][$order_graph_date]['offline_donation']      += tofloat($gsa['amount']);
        }
        $ret_data['total']['total'] += tofloat($gsa['amount']);
        $i++;
      } else {
        $user_id = get_current_user_id();
        _om_warn("no date found for gsa amount: " . $gsa['amount'] . " user id: $user_id");
      }
      unset($gsaObj['gsas'][$idx]);
    }

    // adjust for goal status adjustment
    $user_id = get_current_user_id();
    $profile_id = get_user_meta( $user_id, 'fundraiser_profile_id' )[0];
    $goal_adjustment = get_post_meta($profile_id)['goal_status_adjustment'][0];

    $ret_data['subscribers'] = $subscribers;
    $ret_data['total']['total'] += $goal_adjustment;

    $query_end = microtime(true);
    _om_info("Get supporter orders time: " . ($query_end - $query_start) . " seconds\n");
    $statsd->timing('supporters.query', (($query_end - $query_start) * 1000));

    return $ret_data;
  }

  function _om_convert_graph_data( $data = array() ) {
    // make data usable to graphing API
    $graph_data = array();
    $next_value = end($data);
    $next_key = key($data);

    if (isset($next_key) && isset($next_key[0])) {
      $current_year = (int) date('y');
      $current_month = (int) date('n');
      $beginning_month = (int) explode('/', $next_key)[0];
      $beginning_year = (int) explode('/', $next_key)[1];


      for ($i = $beginning_year; $i <= $current_year; $i++) {

        $month_start = $i === $beginning_year ? $beginning_month : 1;
        $month_end = $i === $current_year  ? $current_month : 12;

        for ($n = $month_start; $n <= $month_end; $n++) {

          if ($next_key) {
            $next_month = (int) explode('/', $next_key)[0];
            $next_year = (int) explode('/', $next_key)[1];
          }

          if ($next_month === $n && $next_year === $i) {
            $graph_data[] = array(
              date('M-y', mktime(0, 0, 0, $n, 1, $i)),
              floatval($next_value['cash_donation']),
              floatval($next_value['product_donation']),
              floatval($next_value['offline_donation']),
              floatval($next_value['subscription_donation'])
            );
            unset($data[$next_key]);
            $next_value = end($data);
            $next_key = key($data);
          } else {
            $graph_data[] = array( date('M-y', mktime(0, 0, 0, $n, 1, $i)), 0, 0, 0, 0);
          }
        }
      }
      $ret_data = array_merge(array( array( 'Date', 'Cash', 'Product', 'Offline', 'Subscription' ) ), $graph_data);
      return $ret_data;
    }
    return "";
  }

	function _om_set_goal_status_adjustment($date, $amount, $description) {
		$user_id = get_current_user_id();
		$profile_id = get_user_meta( $user_id, 'fundraiser_profile_id' )[0];
		$goal_adjustment = get_post_meta($profile_id)['goal_status_adjustment'][0];
		$res = update_post_meta($profile_id, 'goal_status_adjustment', $goal_adjustment . '|' .
			$date . '::' . $amount . '::' . $description);
	}

	function _om_get_goal_status_adjustments() {
		$ret = array('total' => 0, 'gsas' => array());
		$user_id = get_current_user_id();
		$profile_id = get_user_meta( $user_id, 'fundraiser_profile_id' )[0];
		$goal_adjustment = get_post_meta($profile_id)['goal_status_adjustment'][0];
		$gsas = explode('|', $goal_adjustment);
		$gsas = array_filter($gsas);
		foreach ($gsas as $gsa) {
			$gsa_fields = explode('::', $gsa);
			if (count($gsa_fields) >= 2) {
				array_push($ret['gsas'], array(
					'date'        => $gsa_fields[0],
					'amount'      => $gsa_fields[1],
					'description' => isset($gsa_fields[2]) ? $gsa_fields[2] : ''
				));
				$ret['total'] += floatval($gsa_fields[1]);
			} else if (!empty($gsa)) {
				array_push($ret['gsas'], array(
					'date'        => '',
					'amount'      => $gsa,
					'description' => ''
				));
				$ret['total'] += floatval($gsa);
			}
		}
		// arrange gsas in descending order like other orders
		usort($ret['gsas'], function($a, $b) {
			return ($a['date'] > $b['date']) ? -1 : 1;
		});
		return $ret;
	}
?>
