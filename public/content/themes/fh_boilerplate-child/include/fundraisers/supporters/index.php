<?php

	//
	//	Setup rendering for the fundraiser status page
	//
	function _om_fundraiser_supporters() {
		$orders_data   = _om_get_fundraisers_orders();

		$omOrdersTable = new OM_Fund_Orders_Table( $orders_data['table'] );
		$omOrdersTable->prepare_items();
		$emptyTable = new OM_Fund_Orders_Table(array());

		$subscribersTable = new OM_Fund_Subscribers_Table( $orders_data['subscribers'] );
		$subscribersTable->prepare_items();

		$user_id = get_current_user_id();
		$profile_id = get_user_meta( $user_id, 'fundraiser_profile_id', true );
		$profile_custom = get_post_custom($profile_id);
		$goal_amount = get_post_meta($profile_id, 'fundraiser_goal_amount');
		$goal_amount = isset($goal_amount[0]) ? $goal_amount[0] : $goal_amount;
		$goal_amount = empty($goal_amount) ? 'Unlimited' : '$' . number_format($goal_amount, 2, '.', '');

		?>

		<!-- order note dialog -->
		<div id="order-note-dialog" class="dialog white-popup-block mfp-hide" style="max-width: 350px">
			<h4 class="text-center subheader"></h4>
			<div>
				<p class="order-note">
			</div>
			<div class="buttons clearfix">
				<button class="btn btn-lg btn-block btn-primary close">Close</button>
			</div>
		</div>

		<!-- monthly statement dialog -->
		<div id="monthly-statement-dialog" class="dialog white-popup-block mfp-hide" style="max-width: 1200px">
			<h4 class="text-center subheader">Loading...</h4>
			<select id="monthly-details-select">
<?php
		foreach ($orders_data['chart'] as $date => $data) {

			$month = explode('/', $date)[0];
			$year = explode('/', $date)[1];
			$ts = mktime(0, 0, 0, $month + 1, 0, $year);
			echo '<option value="' . $ts . '">' . date('F, Y', $ts) . '</option>';
		}
?>

			</select>
			<button class="page-title-action csv-export">CSV Export</button>
			<div>
				<?php $emptyTable->display() ?>
			</div>
			<div class="buttons clearfix">
				<button class="btn btn-lg btn-block btn-primary close">Close</button>
			</div>
		</div>


		<!-- offline donation dialog -->
		<form id="offline-donation-dialog" class="dialog white-popup-block mfp-hide" style="max-width:400px;">
			<input type="hidden" name="offline_old_string" />
			<h4 class="text-center subheader">Offline Donation</h4>
			<div class="form-group">
				<label>Date</label>
				<input type="text" class="form-control date datepicker" name="offline_date" />
			</div>
			<div class="form-group">
				<label>Amount</label>
				<div class="amount-wrap">
					<span class="money-sign">$</span>
					<input type="number" step="1" min="0" class="form-control amount" name="offline_amount" />
				</div>
			</div>
			<div class="form-group">
				<input type="text" placeholder="Description..." class="form-control description" name="offline_description" />
			</div>
			<button class="btn btn-lg btn-block btn-primary add">Add Donation</button>
			<button class="btn btn-lg btn-block btn-primary update">Update Donation</button>
			<button class="btn btn-lg btn-block btn-secondary cancel">Cancel</button>
		</form>

		<!-- offline donation delete confirmation dialog -->
		<form id="delete-confirmation-dialog" class="dialog white-popup-block mfp-hide" style="max-width:300px;">
			<input type="hidden" name="offline_old_string" />
			<h4 class="text-center subheader">Are You Sure You Want to Delete This Entry?</h4>
			<p class="title"></p>
			<button class="btn btn-lg btn-block btn-primary delete">Delete</button>
			<button class="btn btn-lg btn-block btn-secondary cancel">Cancel</button>
		</form>

		<!-- subscribers dialog -->
		<form id="subscribers-dialog" class="dialog white-popup-block mfp-hide" style="max-width:1200px;">
			<h4 class="text-center subheader">Subscribers</h4>
			<div>
				<?php $subscribersTable->display() ?>
			</div>
			<div class="buttons clearfix">
				<button class="btn btn-lg btn-block btn-primary close">Close</button>
			</div>
		</form>


		<!-- main content -->
		<div class="wrap fundraiser_supporters">
		<h1>Fund Performance</h1>

		<hr><br>

		<div class="fundadmin-container">
			<div class="row">
			<div class="supporter_summary col-md-4 col-sm-12">
				<div class="stats">
					<p><label>Goal</label><span><?php echo $goal_amount ?></span></p>
					<p><label>Total Raised</label><span>$<?php echo number_format($orders_data['total']['total'], 2, '.', ','); ?></span></p>
					<p><label>Number of Transactions</label><span><?php echo count($orders_data['table']); ?></span></p>
					<p class="hidden"><label>Uncategorized</label><span>$<?php echo number_format($orders_data['total']['unknown'], 2, '.', ','); ?></span></p>
<?php if (!empty($orders_data['subscribers'])): ?>
					<button class="btn btn-secondary subscribers-button">View Subscriptions</button>
<?php endif; ?>
				</div>
				<div class="statements">
					<strong>Monthly Transaction Details</strong><br />
			<select id="monthly-statement-select">
				<option value="">(Select ...)</option>
<?php
		foreach ($orders_data['chart'] as $date => $data) {

			$month = explode('/', $date)[0];
			$year = explode('/', $date)[1];
			$ts = mktime(0, 0, 0, $month + 1, 0, $year);
			echo '<option value="' . $ts . '">' . date('F, Y', $ts) . '</option>';
		}
?>
			</select>
				</div>
			</div>
			<div class="col-md-8 col-sm-12" id="chart_div">
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-12">
				<button class="btn btn-lg btn-primary enter-offline-donation">Enter Offline Donation</button>
			</div>
		</div>
		<div class="row">
			<form id="supporter_list" class="col-md-12" method="get">
				<div class="donation-type">
					<label>Type</label>
					<select name="donation_type">
						<option value="">All</option>
						<option value="1">Cash</option>
						<option value="2">Product</option>
						<option value="3">Offline</option>
						<option value="4">Subscription</option>
					</select>
				</div>
				<input type="hidden" name="page" value="<?php echo $_GET['page'] ?>" />
				<?php $omOrdersTable->display() ?>
				<h4 class="subheader"></h4>

			</form>
		</div>

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>

		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
		<script type="text/javascript">

		google.load("visualization", "1", {packages:["corechart"]});
		google.setOnLoadCallback(drawChart);

		var chart;
		function drawChart() {
			var rawData = <?php echo json_encode($orders_data['chart']); ?>;
			var jsonData = <?php echo json_encode( _om_convert_graph_data( $orders_data['chart'] ) );  ?>;
			// console.log(rawData, jsonData);
			var data = google.visualization.arrayToDataTable(jsonData);

			var options = {
				height: 350,
				bar: { groupWidth: '90%' },
				title: 'Donations By Month',
				colors: ['#1184bb', '#666666', '#11bb84', '#bb1184'],
				legend: { position: 'bottom', textStyle: { fontSize: 9.5 } },
				hAxis: { titleTextStyle: {color: 'black'} },
				isStacked: true,
				vAxis: { viewWindow: { min: 0 }, format: '$#,###' }
			};

			chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
			chart.draw(data, options);

			google.visualization.events.addListener(chart, 'select', function() {
				var sel = chart.getSelection()[0];
				if (!sel) { return; }

				var row = sel['row'];
				if (row === null) { return; }

				var col = sel['column'];
				var type = data.getColumnIndex(col);
				var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
				var ds = jsonData[row + 1][0];
				var month = months.indexOf(ds.split('-')[0]) + 1;

				ds = ds.replace(ds.split('-')[0], month).replace('-', '/');
				var ts = rawData[ds] ? rawData[ds]['order_month_ts'] : null;
				openMonthlyStatement(null, ts, type);
			});
		}

		function getMonthlyStatement(ts, type) {
			var data = {};
			$('#monthly-statement-dialog table').hide();
			$('#monthly-statement-dialog h4.subheader').text('Loading...');
			$('button.csv-export').data('ts', ts);
			$('button.csv-export').data('type', type);
			$('button.csv-export').show();
			$('i.export-status').remove();


			$.ajax({
				url: ajaxurl,
				// Add action and nonce to our collected data
				data: $.extend({
					_ajax_fundraiser_supporters_nonce: $('#_ajax_fundraiser_supporters_nonce').val(),
					action: 'ajax_fetch_fundraiser_supporters',
					type: type,
					ts: ts
				}, data),
				// Handle the successful result
				success: function( response ) {
					var monthNames = [
						"January", "February", "March",
						"April", "May", "June", "July",
						"August", "September", "October",
						"November", "December"
						];

					var dt = new Date((parseInt(ts) + 36000) * 1000);
					if (type == 1) {
						$('#monthly-statement-dialog h4.subheader').text(monthNames[dt.getMonth()] + ', ' + dt.getFullYear() + ' Cash Donations');
					} else if (type == 2) {
						$('#monthly-statement-dialog h4.subheader').text(monthNames[dt.getMonth()] + ', ' + dt.getFullYear() + ' Product Donations');
					} else if (type == 3) {
						$('#monthly-statement-dialog h4.subheader').text(monthNames[dt.getMonth()] + ', ' + dt.getFullYear() + ' Offline Donations');
					} else if (type == 4) {
						$('#monthly-statement-dialog h4.subheader').text(monthNames[dt.getMonth()] + ', ' + dt.getFullYear() + ' Subscription Donations');
					} else {
						$('#monthly-statement-dialog h4.subheader').text(monthNames[dt.getMonth()] + ', ' + dt.getFullYear() + ' Transaction Details');
					}
					$('#monthly-statement-dialog table').show();


					// WP_List_Table::ajax_response() returns json
					var response = $.parseJSON( response );
					if ( response.rows.length )
						$('#monthly-statement-dialog #the-list').html( response.rows );
					// Update column headers for sorting
					if ( response.column_headers.length )
						$('#monthly-statement-dialog thead tr, #monthly-statement-dialog tfoot tr').html( response.column_headers );
					// Update pagination for navigation
					if ( response.pagination.bottom.length )
						$('#monthly-statement-dialog .tablenav.top .tablenav-pages').html( $(response.pagination.top).html() );
					if ( response.pagination.top.length )
						$('#monthly-statement-dialog .tablenav.bottom .tablenav-pages').html( $(response.pagination.bottom).html() );

					$('#monthly-statement-dialog table').show();
				}
			});
		}

		function openMonthlyStatement(e, ts, type) {
			var ts = ts || $(this).data('date') || $(this).val();
			var type = type || null;
			$.magnificPopup.open({
				items: {
					type: 'inline',
					src: '#monthly-statement-dialog',
				},
				closeOnBgClick: false,
				preloader: false,
				callbacks: {
					beforeOpen: function() {
						getMonthlyStatement(ts, type);
						$('#monthly-details-select option[value="' + ts + '"]').prop('selected', true);
						$('#monthly-details-select').on('change', function() {
							var newTime = parseInt($(this).val());
							getMonthlyStatement(newTime, type);
						});
					},
					afterClose: function() {
						$('button.csv-export').show();
						$('i.export-status').hide();
						$('#monthly-details-select').off('change');
					}
				}
			});
		}

		function exportCsv(e) {
			// this.disabled = true;
			var ts = $(this).data('ts') !== undefined ? '&ts=' + $(this).data('ts') : '';
			var type = $(this).data('type') !== undefined ? '&type=' + $(this).data('type') : '';
			$(this).after('<i class="export-status">Exporting...</i>');
			$(this).hide();
			window.location = window.location.href.replace('#', '') + '&fundraiser-monthly-csv=true' + ts + type;
		};


		$(document).on('ready', function() {

			// edit offline donation
			$('.gsa a.edit').on('click', function(e) {
				var title = $(this).data('title');
				var amount = $(this).data('amount');
				var dt = $(this).data('date');
				e.preventDefault();
				$.magnificPopup.open({
					items: {
						type: 'inline',
						src: '#offline-donation-dialog',
					},
					closeOnBgClick: false,
					preloader: false,
					callbacks: {
						beforeOpen: function() {
							var format_dt = new Date((dt + 36000) * 1000);
							format_dt = format_dt.getMonth() + 1 + '/' + format_dt.getDate() + '/' + format_dt.getFullYear();
							$('#offline-donation-dialog .date').val(format_dt);
							$('#offline-donation-dialog .amount').val(amount);
							$('#offline-donation-dialog .description').val(title);
							$('#offline-donation-dialog .add').hide();
							$('#offline-donation-dialog .update').show();
							$('#offline-donation-dialog input[type="hidden"]').val(dt + '::' + amount + '::' + title);
						}
					}
				});
			});

			// delete offline donation
			$('.gsa a.delete').on('click', function(e) {
				var title = $(this).data('title');
				var amount = $(this).data('amount');
				var dt = $(this).data('date');
				e.preventDefault();
				$.magnificPopup.open({
					items: {
						type: 'inline',
						src: '#delete-confirmation-dialog',
					},
					closeOnBgClick: false,
					preloader: false,
					callbacks: {
						beforeOpen: function() {
							$('#delete-confirmation-dialog .title').text(title);
							$('#delete-confirmation-dialog input[type="hidden"]').val(dt + '::' + amount + '::' + title);
						}
					}
				});
			});

			// order note dialog
			$('a.order-note').on('click', function(e) {
				var note = $(this).data('note');
				var purchaser = $(this).data('purchaser');
				e.preventDefault();
				$.magnificPopup.open({
					items: {
						type: 'inline',
						src: '#order-note-dialog',
					},
					closeOnBgClick: false,
					preloader: false,
					callbacks: {
						beforeOpen: function() {
							$('#order-note-dialog .order-note').text(note);
							$('#order-note-dialog .subheader').text('Note From ' + purchaser);
						}
					}
				});
			});
			$('#order-note-dialog .close').on('click', function(e) {
				e.preventDefault();
				$('#order-note-dialog').magnificPopup('close');
			});


			// open offline donation dialog
			$('button.enter-offline-donation').on('click', function(e) {
				e.preventDefault();
				$.magnificPopup.open({
					items: {
						type: 'inline',
						src: '#offline-donation-dialog',
					},
					closeOnBgClick: false,
					preloader: false,
					callbacks: {
						beforeOpen: function() {
							$('#offline-donation-dialog .add').show();
							$('#offline-donation-dialog .update').hide();
							$('#offline-donation-dialog input[type="hidden"]').val('');
						}
					}
				});
			});

			// add offline donation dialog
			$('#offline-donation-dialog button.add').on('click', function(e) {
				e.preventDefault();
				var data = $('#offline-donation-dialog').serializeArray();
				$.ajax({
					url: ajaxurl,
					// Add action and nonce to our collected data
					data: $.extend({
						action: 'ajax_add_offline_donation',
					}, data),
					// Handle the successful result
					success: function( response ) {
						window.location.reload();
					},
					error: function(err) {
						console.error(err);
					}
				});
			});

			$('#offline-donation-dialog button.cancel').on('click', function(e) {
				e.preventDefault();
				$('#offline-donation-dialog').magnificPopup('close');
			});

			// update offline donation dialog
			$('#offline-donation-dialog button.update').on('click', function(e) {
				e.preventDefault();
				var data = $('#offline-donation-dialog').serializeArray();
				$.ajax({
					url: ajaxurl,
					// Add action and nonce to our collected data
					data: $.extend({
						action: 'ajax_update_offline_donation',
					}, data),
					// Handle the successful result
					success: function( response ) {
						window.location.reload();
					},
					error: function(err) {
						console.error(err);
					}
				});
			});

			// monthly statement dialog
			$('#monthly-statement-select').on('change', openMonthlyStatement);

			$('#monthly-statement-dialog button.close').on('click', function(e) {
				e.preventDefault();
				$('#monthly-statement-dialog').magnificPopup('close');
			});

			// csv export
			$('.csv-export').on('click', exportCsv);

			// delete confirmation dialog
			$('#delete-confirmation-dialog button.delete').on('click', function(e) {
				e.preventDefault();
				var data = $('#delete-confirmation-dialog').serializeArray();
				$.ajax({
					url: ajaxurl,
					// Add action and nonce to our collected data
					data: $.extend({
						action: 'ajax_delete_offline_donation',
					}, data),
					success: function( response ) {
						window.location.reload();
					},
					error:  function(err) {
						console.error(err);
					}
				});
			});

			$('#delete-confirmation-dialog .cancel').on('click', function(e) {
				e.preventDefault();
				$('#delete-confirmation-dialog').magnificPopup('close');
			});

			// subscribers dialog
			$('.subscribers-button').on('click', function(e) {
				e.preventDefault();

				$.magnificPopup.open({
					items: {
						type: 'inline',
						src: '#subscribers-dialog',
					}
				});
			});

			$('#subscribers-dialog .close').on('click', function(e) {
				e.preventDefault();
				$('#subscribers-dialog').magnificPopup('close');
			});

			// datepickers
			$('.datepicker').each(function(idx, el) {
				var ret = $(el).datepicker();
				console.log(ret);
			});

			// fundraiser type filter
			$('.donation-type select').on('change', function(e) {
				e.preventDefault();
				var type = parseInt($(this).val());
				$('#supporter_list table').hide();
				$('#supporter_list .tablenav-pages').hide();
				$('#supporter_list h4.subheader').text('Loading...');
				$.ajax({
					url: ajaxurl,
					// Add action and nonce to our collected data
					data: {
						_ajax_fundraiser_supporters_nonce: $('#_ajax_fundraiser_supporters_nonce').val(),
						action: 'ajax_fetch_fundraiser_supporters',
						details: false,
						type: type
					},
					// Handle the successful result
					success: function( response ) {

						// WP_List_Table::ajax_response() returns json
						var response = $.parseJSON( response );
						if ( response.rows.length )
							$('#supporter_list #the-list').html( response.rows );
						// Update column headers for sorting
						if ( response.column_headers.length )
							$('#supporter_list thead tr, #supporter_list tfoot tr').html( response.column_headers );
						// Update pagination for navigation
						if ( response.pagination.bottom.length )
							$('#supporter_list .tablenav.top .tablenav-pages').html( $(response.pagination.top).html() );
						if ( response.pagination.top.length )
							$('#supporter_list .tablenav.bottom .tablenav-pages').html( $(response.pagination.bottom).html() );

						$('#supporter_list h4.subheader').text('');
						$('#supporter_list .tablenav-pages').show();
						$('#supporter_list table').show();
					},
					error: function(err) {
						console.error(err);
					}

				});
			});

		});

		</script>
	</div>

		<?php

	}
?>
