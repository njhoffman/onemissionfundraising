<?php

	//
	//	Include WP_List_Table to extend for custom orders display
	//
	if( ! class_exists( 'WP_List_Table' ) ) {

		require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
	}

	//
	//	Setup orders table
	//
	class OM_Fund_Subscribers_Table extends WP_List_Table {

		public $data;
		public $per_page;
		public $totals;

		function __construct( $data, $per_page = 250) {

			global $status, $page;

			$this->data = $data;
			$this->per_page = $per_page;

			parent::__construct( array(
				'singular'=> 'wp_om_fund_subscriber',  // Singular label (CSS selector safe)
				'plural' =>  'wp_om_fund_subscribers', // Plural label (CSS selector safe)
				'ajax' =>    false                // No AJAX support
			) );
		}

		function no_items() {
			_e( 'No subscribers found ');
		}

		function column_default( $item, $column_name ){
			switch( $column_name ) {
				case "status":
					_dbg($item['status']);
					$status = '<mark class="on-hold">Unknown</mark>';
					if ($item['status'] == 'wc-active') {
						$status = '<mark class="active">Active</mark>';
					} else if ($item['status'] == 'wc-on-hold') {
						$status = '<mark class="on-hold">On hold</mark>';
					} else if ($item['status'] == 'wc-cancelled' || $item['status'] == 'wc-pending-cancel') {
						$status = '<mark class="cancelled">Canceled</mark>';
					}
					return $status;
					break;
				case "name":
					return $item['name'];
					break;
				case "start":
					return $item['start'];
				case "total":
					return '$' . $item['total'];
					break;
				case "next_payment":
					if ($item['next_payment'] == 0) {
						return "";
					}
					$next_dt = new DateTime($item['next_payment']);
					return $next_dt->format('F j, Y');
					break;
				case "last_payment":
					if ($item['last_payment'] == 0) {
						return "";
					}
					$last_dt = new DateTime($item['last_payment']);
					return $last_dt->format('F j, Y');
					break;
				case "orders":
					return count($item['orders']);
				default:
					return print_r( $item, true );
			}
		}

		function get_columns() {

			$columns = array(
				'status' => 'Status',
				'name'   => 'Name',
				'total'  => 'Total',
				'start' => 'Start Date',
				'next_payment' => 'Next Payment',
				'last_payment' => 'Last Payment',
				'orders' => 'Renewals'
			);

			return $columns;
		}

		function prepare_items() {

			$columns = $this->get_columns();
			$hidden = array();
			$sortable = array();
			$data = [];

			$this->_column_headers = array( $columns, $hidden, $sortable );
			foreach ($this->data as $record) {
				if (!isset($record['total'])) {
					$record['total'] = '';
				}
				if (isset($record['start'])) {
					$start_dt = new DateTime($record['start']);
					$record['start'] = $start_dt->format('F j, Y');
				}
				array_push($data, $record);
			}
			$current_page = $this->get_pagenum();

			if( is_array( $data ) && ! empty( $data ) ) {
				$data = array_slice( $data, ( ( $current_page - 1 ) * $this->per_page ), $this->per_page );
				$this->items = $data;
				$this->set_pagination_args( array(
					'total_items' => count($data),
					'per_page'    => $this->per_page,
					'total_pages' => ceil( count($data) / $this->per_page ),
					'orderby'	=> ! empty( $_REQUEST['orderby'] ) && '' != $_REQUEST['orderby'] ? $_REQUEST['orderby'] : 'title',
					'order'		=> ! empty( $_REQUEST['order'] ) && '' != $_REQUEST['order'] ? $_REQUEST['order'] : 'asc'
				) );
			}
			return $data;
		}


		function display_rows() {
			//Get the records registered in the prepare_items method
			$records = $this->items;

			//Get the columns registered in the get_columns and get_sortable_columns methods
			list( $columns, $hidden ) = $this->get_column_info();

			//Loop for each record
			if(!empty($records)){foreach($records as $rec){
				//Open the line
				echo '<tr>';
				foreach ( $columns as $column_name => $column_display_name ) {

					//Style attributes for each col
					$class = "class='$column_name column-$column_name'";
					$style = "";
					if ( in_array( $column_name, $hidden ) ) $style = ' style="display:none;"';
					$attributes = $class . $style;
					//Display the cell
					switch ( $column_name ) {
						default:
							echo '<td ' . $attributes . '>' . $this->column_default($rec, $column_name) . '</td>';
							break;
					}
				}
				//Close the line
				echo'</tr>';
			}}
		}

		function display() {
			wp_nonce_field( 'ajax-fundraiser-supporters-nonce', '_ajax_fundraiser_supporters_nonce' );
			if (!empty($this->_pagination_args['order'])) {
				echo '<input id="order" type="hidden" name="order" value="' . $this->_pagination_args['order'] . '" />';
			}
			if (!empty($this->_pagination_args['orderby'])) {
				echo '<input id="orderby" type="hidden" name="orderby" value="' . $this->_pagination_args['orderby'] . '" />';
			}
			parent::display();
		}
	}

?>
