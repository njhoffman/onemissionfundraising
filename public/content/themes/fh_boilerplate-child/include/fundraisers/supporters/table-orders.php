<?php

	//
	//	Include WP_List_Table to extend for custom orders display
	//
	if( ! class_exists( 'WP_List_Table' ) ) {

		require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
	}

	//
	//	Setup orders table
	//
	class OM_Fund_Orders_Table extends WP_List_Table {

		public $data;
		public $per_page;
		public $show_details;
		public $totals;

		function __construct( $data, $per_page = 25, $show_details = false, $strip_html = false, $is_csv = false ) {

			global $status, $page;

			$this->data = $data;
			$this->per_page = $per_page;
			$this->show_details = $show_details;
			$this->strip_html = $strip_html;
			$this->is_csv = $is_csv;

			parent::__construct( array(
				'singular'=> 'wp_om_fund_order',  // Singular label (CSS selector safe)
				'plural' =>  'wp_om_fund_orders', // Plural label (CSS selector safe)
				'ajax' =>    false                // No AJAX support
			) );
		}

		function no_items() {
			_e( 'No orders found ');
		}

		function column_default( $item, $column_name ){
			if ($this->show_details != true) {
				switch( $column_name ) {
					case "date":
						return $item['order_date'];
						break;
					case "supporter":
						return $item['name'];
						break;
					case "supporter_list_name":
						return $item['donation_attr'];
						break;
					case "supporter_details":
						return $item['email'] . '<br />' . $item['address'];
						break;
					case "donation_gross_amount":
						return 'Total Donation: $' . $item['donation_total'];
						break;
					case "note":
						return !empty($item['note'])
							? '<a class="order-note" data-note="' . $item['note'] . '" href="#" ' .
							'data-purchaser="' . $item['name'] . '">Read Note</a>' : '';
						break;
					// Show the whole array for troubleshooting purposes
					default:
						return print_r( $item, true );
				}
			} else {
				switch( $column_name ) {
					case "transaction":
						return $item['ID'] . '<br />' . $item['order_date'];
						break;
					case "supporter":
						return $item['name'] . '<br />' .$item['email'] . '<br />' . $item['address']; //. '<br />' . $item['city_state_zip'];
						break;
					case "transaction_details":
						return 'Cash Donation: $' . $item['cash_donation'] . '<br />' .
							($item['subscription_donation'] > 0 ? 'Subscription Purchase: $ ' . $item['subscription_donation'] :
								'Product Purchases: $' . $item['product_purchases']) . '<br />' .
								'Tax &amp; Shipping: $' . $item['order_taxes_shipping'] . '<br />' .
								'Total: $' . $item['total'];
						break;
					case "donation_details":
						return 'Cash Donation: $' . $item['cash_donation'] . '<br />' .
							($item['subscription_donation'] > 0 ? 'Subscription Donation: $' . $item['subscription_donation'] :
								'Product Donation: $' . $item['product_donation']) . '<br />' .
							'Total Donation: $' . $item['donation_total'];
						break;
					case "transaction_fee":
							return '$' . $item['transaction_fee'];
						break;
					case "net_donation":
							return '$' . $item['net_donation'];
						break;
					case "note":
						return !empty($item['note'])
							? '<a class="order-note" data-note="' . $item['note'] . '" href="#" ' .
							'data-purchaser="' . $item['name'] . '">Read Note</a>' : '';
						break;
					// Show the whole array for troubleshooting purposes
					default:
						return print_r( $item, true );
				}
			}
		}

		function get_columns() {

			if ($this->show_details != true) {
				$columns = array(
					'date'                  => 'Date',
					'supporter'             => 'Name',
					'supporter_list_name'   => 'Supporter List Name',
					'supporter_details'     => 'Supporter Details',
					'donation_gross_amount' => 'Donation Gross Amount <img src="' . get_stylesheet_directory_uri() .
														'/img/info-icon.svg" title="Before Credit Card Fees" class="help-icon donation_gross_amount" />',
					'note'                  => 'Note'
				);
			} else {
				$columns = array(
					'transaction'         => 'Transaction',
					'supporter'           => 'Supporter',
					'transaction_details' => 'Transaction Details',
					'donation_details'    => 'Donation Details',
					'transaction_fee'     => 'Transaction Fee',
					'net_donation'        => 'Net Donation',
					'note'                => 'Note'
				);
			}

			return $columns;
		}

		function prepare_items($ts = null, $type = null, $strip_tags = false, $is_csv = false) {

			$columns = $this->get_columns();
			$hidden = array();
			$sortable = array();
			$totals = array(
				'count' => 0,
				'product_purchases' => 0,
				'tax_and_shipping' => 0,
				'total' => 0,
				'cash_donation' => 0,
				'subscription_donation' => 0,
				'product_donation' => 0,
				'total_donation' => 0
			);

			$this->_column_headers = array( $columns, $hidden, $sortable );

			if ($ts) {
				$data = [];
				$filter_month = date('F', $ts);
				$filter_year = date('Y', $ts);
				foreach ($this->data as $record) {
					$r_month = date('F',strtotime($record['order_date']));
					$r_year = date('Y',strtotime($record['order_date']));
				   if ($r_month == $filter_month && $r_year == $filter_year) {
						// if type filter is set, only a show a subset of records
						if ($type == 1 && intval($record['cash_donation']) == 0) {
							continue;
						} else if ($type == 2 && intval($record['product_donation']) == 0) {
							continue;
						} else if ($type == 3 && intval($record['offline_donation']) == 0) {
							continue;
						} else if ($type == 4 && intval($record['subscription_donation']) == 0) {
							continue;
						}
						if ($strip_tags == true) {
							$record = array_map('strip_tags', $record);
						}
						$totals['cash_donation'] += $record['cash_donation'];
						$totals['product_donation'] += $record['product_donation'];
						$totals['subscription_donation'] += $record['subscription_donation'];
						$totals['total_donation'] += $record['donation_total'];
						$totals['product_purchases'] += $record['product_purchases'];
						$totals['tax_and_shipping'] += $record['order_taxes_shipping'];
						$totals['total'] += $record['total'];
						array_push($data, $record);
					}
				}
				$data = array_reverse($data);
			} else {
				$data = [];
				foreach ($this->data as $record) {
					if ($type == 1 && intval($record['cash_donation']) == 0) {
						continue;
					} else if ($type == 2 && intval($record['product_donation']) == 0) {
						continue;
					} else if ($type == 3 && intval($record['offline_donation']) == 0) {
						continue;
					} else if ($type == 4 && intval($record['subscription_donation']) == 0) {
						continue;
					}
					if ($strip_tags) {
						$record = array_map('strip_tags', $record);
					}
					array_push($data, $record);
				}
			}
			$current_page = $this->get_pagenum();
			$totals['count'] = count( $data );

			if( is_array( $data ) && ! empty( $data ) ) {
				$data = array_slice( $data, ( ( $current_page - 1 ) * $this->per_page ), $this->per_page );
				$this->items = $data;
				$this->totals = $totals;
				$this->set_pagination_args( array(
					'total_items' => $totals['count'],
					'per_page'    => $this->per_page,
					'total_pages' => ceil( $totals['count'] / $this->per_page ),
					'orderby'	=> ! empty( $_REQUEST['orderby'] ) && '' != $_REQUEST['orderby'] ? $_REQUEST['orderby'] : 'title',
					'order'		=> ! empty( $_REQUEST['order'] ) && '' != $_REQUEST['order'] ? $_REQUEST['order'] : 'asc'
				) );
			}
			return $data;
		}

		function display_totals() {
			echo '<tr>';
			echo '<td><strong>Total Count: </strong>' . $this->totals['count'] . '</td>';
			echo '<td>&nbsp;</td>';
			echo '<td><strong>Product Purchases: </strong>$' .  $this->totals['product_purchases'] .
				'<br /><strong>Tax &amp; Shipping: </strong>$' . $this->totals['tax_and_shipping'] .
				'<br /><strong>Total: </strong>' . $this->totals['total'] . '</td>';
			echo '<td><strong>Cash Donation: </strong>$' .  $this->totals['cash_donation'] .
				'<br /><strong>Product Donation: </strong>$' . $this->totals['product_donation'] .
				'<br /><strong>Subscription Donation: </strong>$' . $this->totals['subscription_donation'] .
				'<br /><strong>Total Donation: </strong>$' . $this->totals['total_donation'] . '</td>';
			echo '</tr>';

		}

		function ajax_response() {
			check_ajax_referer( 'ajax-fundraiser-supporters-nonce', '_ajax_fundraiser_supporters_nonce' );
			$ts = isset($_REQUEST['ts']) ? $_REQUEST['ts'] : false;
			$type =  isset($_REQUEST['type']) ? $_REQUEST['type'] : false;

			$this->prepare_items($ts, $type, false);
			extract( $this->_args );
			extract( $this->_pagination_args, EXTR_SKIP );


			ob_start();
			if ( ! empty( $_REQUEST['no_placeholder'] ) )
				$this->display_rows();
			else
				$this->display_rows_or_placeholder();
			// $this->display_totals();
			$rows = ob_get_clean();

			ob_start();
			$this->print_column_headers();
			$headers = ob_get_clean();

			ob_start();
			$this->pagination('top');
			$pagination_top = ob_get_clean();

			ob_start();
			$this->pagination('bottom');
			$pagination_bottom = ob_get_clean();

			$response = array( 'rows' => $rows );
			$response['pagination']['top'] = $pagination_top;
			$response['pagination']['bottom'] = $pagination_bottom;
			$response['column_headers'] = $headers;

			if ( isset( $this->totals['count']) )
				$response['total_items_i18n'] = sprintf( _n( '1 item', '%s items', $this->totals['count']), number_format_i18n( $this->totals['count']) );

			if ( isset( $total_pages ) ) {
				$response['total_pages'] = $total_pages;
				$response['total_pages_i18n'] = number_format_i18n( $total_pages );
			}

			die( json_encode( $response ) );
		}

		function display_rows() {
			//Get the records registered in the prepare_items method
			$records = $this->items;

			//Get the columns registered in the get_columns and get_sortable_columns methods
			list( $columns, $hidden ) = $this->get_column_info();

			//Loop for each record
			if(!empty($records)){foreach($records as $rec){
				//Open the line
				echo '<tr>';
				foreach ( $columns as $column_name => $column_display_name ) {

					//Style attributes for each col
					$class = "class='$column_name column-$column_name'";
					$style = "";
					if ( in_array( $column_name, $hidden ) ) $style = ' style="display:none;"';
					$attributes = $class . $style;
					$gsa = ! isset($rec['ID']) ? true : false;
					if ($gsa) {
						if ($this->show_details != true) {
							switch( $column_name) {
								case 'date':
									echo '<td colspan=3 class="gsa">' . $rec['description'] . '<br />' .
										date('F d, Y', $rec['date']) .
											'<br />' .
											'<a data-title="' . $rec['description'] .  '" ' .
												'data-amount="' . $rec['amount'] . '" ' .
												'data-date="' . $rec['date']. '" ' .
												'class="edit" href="#">Edit</a>' .
											'<span class="pipe">|</span>' .
											'<a data-title="' . $rec['description'] . '" ' .
												'data-amount="' . $rec['amount'] . '" ' .
												'data-date="' . $rec['date']. '" ' .
												'class="delete" href="#">Delete</a>' .
										'</td>';
									break;
								case 'supporter_details':
									echo '<td colspan=3 class="gsa">Total Donation: $' . $rec['amount'] . '</td>';
									break;
								default:
									break;
							}
						} else {
							switch( $column_name) {
								case 'transaction':
									echo '<td colspan=3 class="gsa">' . $rec['description'] . '<br />' .
										date('F d, Y', $rec['date']) .
											'<br />' .
											'<a data-title="' . $rec['description'] .  '" ' .
												'data-amount="' . $rec['amount'] . '" ' .
												'data-date="' . $rec['date']. '" ' .
												'class="edit" href="#">Edit</a>' .
											'<span class="pipe">|</span>' .
											'<a data-title="' . $rec['description'] . '" ' .
												'data-amount="' . $rec['amount'] . '" ' .
												'data-date="' . $rec['date']. '" ' .
												'class="delete" href="#">Delete</a>' .
										'</td>';
									break;
								case 'donation_details':
									echo '<td colspan=4 class="gsa">Total Donation: $' . $rec['amount'] . '</td>';
									break;
								default:
									break;
							}
						}
					} else {
						//Display the cell
						switch ( $column_name ) {
							default:
								echo '<td ' . $attributes . '>' . $this->column_default($rec, $column_name) . '</td>';
								break;
						}
					}
				}
				//Close the line
				echo'</tr>';
			}}
		}

		function display() {
			wp_nonce_field( 'ajax-fundraiser-supporters-nonce', '_ajax_fundraiser_supporters_nonce' );
			if (!empty($this->_pagination_args['order'])) {
				echo '<input id="order" type="hidden" name="order" value="' . $this->_pagination_args['order'] . '" />';
			}
			if (!empty($this->_pagination_args['orderby'])) {
				echo '<input id="orderby" type="hidden" name="orderby" value="' . $this->_pagination_args['orderby'] . '" />';
			}
			parent::display();
		}
	}

?>
