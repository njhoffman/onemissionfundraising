<div id="uploadimage-dialog" class="dialog white-popup-block mfp-hide" style="width: 600px; max-width: 80%;">
	<input type="file" style="display: none" id="uploadimage_file" />
	<input type="hidden" id="uploadimage_target_id" />
	<input type="hidden" id="uploadimage_target_file" />
	<input type="hidden" id="uploadimage_aspect_ratio" />
	<h2 class="text-center">Add Image</h2>
	<h3 class="text-center status">Opening Select File Dialog...</h3>
	<div class="crop-ratio"></div>
	<div class="crop-status">
		<span class="width"></span>
		<span class="x">x</span>
		<span class="height"></span>
	</div>
	<div>
		<img class="loading" src="<?php echo get_stylesheet_directory_uri(); ?>/img/loading.gif" />
	</div>
	<div>
		<img style="max-width: 100%" id="uploadimage-image" src=""/>
	</div>
	<div class="buttons row">
		<div class="col-sm-6">
			<button class="btn btn-lg btn-block btn-default cancel">Cancel</button>
		</div>
		<div class="col-sm-6">
			<button class="btn btn-lg btn-block btn-success apply">Apply</button>
		</div>
	</div>
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.css" rel="stylesheet">
<script src="<?php echo get_template_directory_uri() . '-child/js/vendor/jquery-ui.min.js'; ?>"></script>

<script>

jQuery(document).ready(function($) {

	var cropper;
	$('button.change-post-image').on('click', function(e) {
		e.preventDefault();
		$('#uploadimage_file').trigger('click');
		var targetId = $(this).data('target_id');
		$('#uploadimage_target_id').val(targetId);
		var aspectRatio = $(this).data('aspect_ratio');
		$('#uploadimage_aspect_ratio').val(aspectRatio);
		var targetFile = $(this).data('target_file');
		$('#uploadimage_target_file').val(targetFile);
		console.info('add image', targetId, aspectRatio, targetFile);
	});

	$('input[type="file"]').on('change', function() {
		var self = this;

		$.magnificPopup.open({
			items: {
				type: 'inline',
					src: '#uploadimage-dialog',
			},
			closeOnBgClick: false,
			preloader: false,
			callbacks: {
				beforeOpen: function() {
				}
			}
		});
	});

	$('button.apply').on('click', function(e) {
		e.preventDefault();
		var targetId = '#' + $('#uploadimage_target_id').val();
		$(targetId).show().attr('src', cropper.getCroppedCanvas().toDataURL());
		$(targetId).removeAttr('srcset', '');
		$(targetId).removeAttr('sizes', '');
		var targetFile = '#' + $('#uploadimage_target_file').val();
		$(targetFile).attr('targetid', targetId);
		cleanUp();
		submitPostPicture();

	});

	$('button.cancel').on('click', function(e) {
		e.preventDefault();
		cleanUp();
	});

	function cleanUp() {
		if (cropper) {
			cropper.destroy();
		}
		$('#uploadimage-image').attr('src', '');
		$('#uploadimage-dialog').magnificPopup('close');
		$('#uploadimage-dialog .status').text('Opening Select File Dialog...');
		$('#uploadimage-dialog .loading').show();
		$('#uploadimage_file').val('');
	}

	$('input[type="file"]').on('change', function() {
		var self = this;
		$('#uploadimage-dialog .status').text('Loading Selected Image...');
		window.setTimeout( function() {
			readURL(self, '#uploadimage-image');
		}, 500);
	});

	document.body.onfocus = function() {
		// happens if cancel is selected
		// cleanUp();
	};

	function readURL(input, target) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$(target).attr('src', e.target.result);
				var aspectRatio = $("#uploadimage_aspect_ratio").val();
				$('.crop-ratio').text(aspectRatio + ':1');
				$('#uploadimage-dialog .status').text('Crop Image to Aspect Ratio');
				$('#uploadimage-dialog .loading').hide();
			};
			reader.readAsDataURL(input.files[0]);
		}
	}

	$('#uploadimage-image').on('load', function() {
		var self = this;
		var width = this.naturalWidth;
		var height = this.naturalHeight;
		var aspectRatio = parseFloat($("#uploadimage_aspect_ratio").val());
		if (isNaN(aspectRatio)) {
			aspectRatio = 1.0;
		}
		$('.crop-status .x').show();
		console.info('height', height, 'width', width, 'aspectRatio', aspectRatio);
		console.info($('#uploadimage_aspect_ratio').val());
		cropper = new Cropper(this, {
			aspectRatio: aspectRatio,
			crop: function(e) {
				$('.crop-status .width').html(parseInt(e.detail.width) + 'px')
					$('.crop-status .height').html(parseInt(e.detail.height) + 'px')
				// 	console.log(parseInt(e.detail.x));
				// console.log(parseInt(e.detail.y));
				// console.log(parseInt(e.detail.width));
				// console.log(parseInt(e.detail.height));
				// console.log(e.detail.rotate);
				// console.log(e.detail.scaleX);
				// console.log(e.detail.scaleY);
			}
		});
	});

	function dataURItoBlob(dataURI) {
		var byteString = atob(dataURI.split(',')[1]);
		var mimeType = dataURI.split(',')[0].split(':')[1].split(';')[0];
		var ab = new ArrayBuffer(byteString.length);
		var ia = new Uint8Array(ab);
		for (var i = 0; i < byteString.length; i++) {
			ia[i] = byteString.charCodeAt(i);
		}
		return new Blob([ab], { type: mimeType });
	}

	function submitPostPicture() {
		var dataURL = $('#post_image_image img').attr('src');
		var fieldName = "post_image";

		var blob = dataURItoBlob(dataURL);
		var fd = new FormData();
		var postId = $('#post_ID').val();
		fd.append('action', 'ajax_update_post_image');
		fd.append('post_id', postId);

		var fileName = fieldName + '.' + blob.valueOf().type.slice(-3);
		fd.append(fieldName, blob, fileName);

		if (fd) {
			var submitStatus = $('.submit-status').length > 0
				? $('.submit-status')
				: false;

			var progressBar = $('.progressbar').length > 0
				? $('.progressbar').progressbar()
				: false;

			var progressPercent = $('.progress-percent').length > 0
				? $('.progress-percent')
				: false;


			if (submitStatus) { submitStatus.show().find('.submit-text').text('Uploading file ...'); }
			if (progressBar) { progressBar.show(); }
			if (progressPercent) { progressPercent.text('0%') }

			$.ajax({
				type: 'POST',
				url: fundraiserObj.ajaxUrl,
				dataType: 'json',
				data: fd,
				processData: false,
				contentType: false,
				xhr: function() {
					var xhr = new window.XMLHttpRequest();
					xhr.upload.addEventListener('progress', function(evt) {
						if (evt.lengthComputable) {
							var percentComplete = evt.loaded / evt.total;
							percentComplete = parseInt(percentComplete * 100);
							if (progressBar) {
								progressBar.progressbar("value", percentComplete);
							}
							if (progressPercent) {
								progressPercent.html(percentComplete + '%');
								if (submitStatus && percentComplete === 100) {
									submitStatus.text('Upload complete');
									progressBar.hide();
								}
							}
						}
					}, false);
					return xhr;
				},
				success: function(response) {

				},
				error: function(error){
					console.error(error);
					debugger;
				}
			});
		}
	}

});

</script>
