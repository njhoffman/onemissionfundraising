<?php

//
//  Create a "profile" page on fundraiser organization on registration
//

function _om_auto_create_fundraiser_pages( $user_id ) {

    $user_meta = get_user_meta( $user_id );

    // Flag is set from Gravity Forms for fundraiser org signup
    if( isset( $user_meta[ 'is_fundraiser_signup' ] ) ) {

        // Create pages and associate with user
        $profile_page = array(
            'post_title' => $user_meta[ 'fundraiser_name' ][0],
            'post_content' => '',
            'post_status' => 'private',
            'post_type' => 'page',
				'comment_status' => 'open',
            'post_author' => $user_id,
            'post_parent' => OM_FUNDRAISER_PAGE_PARENT_ID,
            'page_template' => OM_FUNDRAISER_PAGE_TEMPLATE
        );

        $profile_page_id = wp_insert_post( $profile_page, TRUE );

        // Add in association for fundraiser profile ID within meta field for user
        update_user_meta( $user_id, 'fundraiser_profile_id', $profile_page_id );

        // Mapping for ACF profile page fields to user meta keys
        // - 'acf' => 'user_meta'
        $acf_to_user_meta = array(
            'fundraiser_description' => 'fundraiser_description',
            'fundraiser_type' => 'fundraiser_type',
            'fundraiser_city_state' => 'fundraiser_city_state',
            'fundraiser_website' => 'fundraiser_website',
            'fundraiser_facebook' => 'fundraiser_facebook',
            'fundraiser_twitter' => 'fundraiser_twitter',
            'fundraiser_instagram' => 'fundraiser_instagram',
            'fundraiser_goal_amount' => 'fundraiser_goal_amount',
            'fundraiser_timed_or_continuous' => 'fundraiser_timed_or_continuous'
        );

        // Iterate over ACF => User Meta map and pre-populate the correct ACF fields
        foreach ( $acf_to_user_meta as $acf_key => $user_meta_key ) {

            update_field( $acf_key, $user_meta[ $user_meta_key ][0], $profile_page_id );
        }

        // Add the "is_featured" meta value as false
        // - Needs to be there for fundraiser profile browsing
        // - For some reason this needs to be an ACF key
        // - As per http://bit.ly/1t18kyW
        update_field( 'field_535e97b700bd8', 0, $profile_page_id );

        // Special handling for dates
        // - converts into ACF-friendly format mm/dd/yyyy
        if( $user_meta[ 'fundraiser_timed_or_continuous' ][0] == 'timed' ) {

            $php_date_conv_format = 'm/d/Y';

            $start_date = strtotime( $user_meta[ 'fundraiser_start_date' ][0] );
            $end_date   = strtotime( $user_meta[ 'fundraiser_end_date' ][0] );
            $start_date_conv = date( $php_date_conv_format, $start_date );
            $end_date_conv   = date( $php_date_conv_format, $end_date );

            update_field( 'fundraiser_start_date', $start_date_conv, $profile_page_id );
            update_field( 'fundraiser_end_date', $end_date_conv, $profile_page_id );
        }
    }
}
add_action( 'gform_user_registered', '_om_auto_create_fundraiser_pages', 999999, 1 );
