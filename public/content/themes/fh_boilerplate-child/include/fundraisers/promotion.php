<?php


	function _om_fundraiser_promotion() {

		// Get profile page information
		$user_id = get_current_user_id();
		$profile_id = get_user_meta( $user_id, 'fundraiser_profile_id', true );
		$profile_id = ( $profile_id ) ? $profile_id : 0;

		$fundraiser_profile = get_page( $profile_id );
		$fundraiser_name = $fundraiser_profile->post_title;
		$fundraiser_guid = get_permalink( $profile_id );

		$fundraiser_fields = get_post_meta($profile_id);
		$desc = strip_tags($fundraiser_fields['fundraiser_description'][0]);

		$fb_image = isset($fundraiser_fields['yoast_wpseo_opengraph-image'])
			? $fundraiser_fields['yoast_wpseo_opengraph-image'][0]
			: (isset($fundraiser_fields['_yoast_wpseo_opengraph-image']) ?
				$fundraiser_fields['_yoast_wpseo_opengraph-image'][0]
				: 'http://placehold.it/360x190?text=No+Yoast+Image');

		$taxonomy_options = get_option('wpseo_taxonomy_meta');

		$fundraiser_share_link = get_permalink( $profile_id) . '?shopfor=' . $fundraiser_profile->ID;

		// fundraiser favorite products
		$product_category = get_field('group_product_category', $fundraiser_profile);
		$favprods_missing = "No Favorite Products Assigned";
		$favprods_image = 'http://placehold.it/360x190?text=' . urlencode($favprods_missing);
		$favprods_title = 'No Favorite Products';
		$favprods_link = "";
		$favprods_desc = '';
		if (!empty($product_category)) {
			$favprods_title = $product_category->name;
			$favprods_category = get_term($product_category->term_id);
			$favprods_link = get_category_link($product_category->term_id) . '?shopfor=' . $fundraiser_profile->ID;

			if (isset($taxonomy_options)
				&& isset($taxonomy_options['product_cat'])
				&& isset($taxonomy_options['product_cat'][$product_category->term_id])) {
						$favprods_image = $taxonomy_options['product_cat'][$product_category->term_id]['wpseo_opengraph-image'];
				}
		}

		// one mission global featured items
		$featured_items_title = 'No Featured Items';
		$featured_items_image = 'http://placehold.it/360x190?text=No+Yoast+Image';
		$featured_items_desc = '';
		$featured_items_link = '';
		$featured_items_category = _om_get_featured_items_category(OM_FUNDRAISER_FEATURED_ITEMS_PARENT_ID);
		if (!empty($featured_items_category)) {
			// first result should be newest category
			$featured_items_link = get_category_link($featured_items_category->term_id) . '?shopfor=' . $fundraiser_profile->ID;
			$featured_items_title = $featured_items_category->name;
			$featured_items_desc = strip_tags($featured_items_category->description);
			if (isset($taxonomy_options)
				&& isset($taxonomy_options['product_cat'])
				&& isset($taxonomy_options['product_cat'][$featured_items_category->term_id])) {
					$featured_items_image = $taxonomy_options['product_cat'][$featured_items_category->term_id]['wpseo_opengraph-image'];
			}
		}


		// one mission global featured categories
		$featured_categories = _om_get_featured_categories();
		if (!empty($featured_categories)) {
			foreach($featured_categories as $featured_cat) {
				$featured_cat_image = 'http://placehold.it/250x130?text=' . urlencode('No image found');
				if (isset($taxonomy_options)
					&& isset($taxonomy_options['product_cat'])
					&& isset($taxonomy_options['product_cat'][$featured_cat->term_id])
					&& isset($taxonomy_options['product_cat'][$featured_cat->term_id]['wpseo_opengraph-image'])) {
						$featured_cat_image = $taxonomy_options['product_cat'][$featured_cat->term_id]['wpseo_opengraph-image'];
				}
				$featured_cat->image = $featured_cat_image;
				$featured_cat->url = get_term_link($featured_cat) . '?shopfor=' . $fundraiser_profile->ID;

			}
		}


		?>

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
		<div class="fundadmin-container promotion wrap">
			<h1>Tools to Promote Your Fund</h1>
			<p>Fundraising success depends upon your commitment to promoting your Fund.  Reach out to your supporters frequently.  Share your story often and invite supporters to give, buy products, and share...again and again.  They will, if you ask.</p>
			<hr>
			<h4>Your Fund Link</h4>
			<p>Below is the magic link for your Fund.  It includes the "shopfor" extension that will automatically connect your supporters' shopping and giving experience with your Fund. So, share this link in its entirety every chance you get.</p>
			<div class="inset inset-green">
				<code><a target="_blank" href="<?php echo $fundraiser_share_link; ?>"><?php echo $fundraiser_share_link; ?></a></code>
			</div>

			<hr>

			<div class="row">
				<div class="col-md-6">
					<h4>Share Your Fund</h4>
					<div class="facebook-preview">
						<div class="image">
							<img src="<?php echo $fb_image; ?>" />
						</div>
						<div class="content">
							<div class="title"><?php echo $fundraiser_name; ?></div>
							<div class="description"><?php echo $desc; ?></div>
							<div class="domain">onemission.fund</div>
						</div>
					</div>
					<div class="social-wrapper">
						<div class="icons">
							<strong>Share:</strong>&nbsp;
							<a class="btn-facebook"
								href="http://www.facebook.com/sharer.php?u=<?php echo urlencode($fundraiser_share_link) ?>&title=<?php echo urlencode( $fundraiser_name ); ?>"
								class="btn facebook" rel="nofollow" target="_blank" title="Share on Facebook">
								<span class="fa fa-facebook"></span>
							</a>
							<a class="btn-twitter" href="http://twitter.com/home?status=<?php echo $fundraiser_share_link; ?>+%28<?php echo urlencode( $fundraiser_profile->post_title ); ?>%29" class="btn twitter" rel="nofollow" target="_blank" title="Share on Twitter">
								<span class="fa fa-twitter"></span>
							</a>
							<a class="btn-google" href="#" class="btn google" rel="nofollow" title="Email a Friend" onclick="javascript:window.location='mailto:?subject=Check out this fundraiser for <?php echo $fundraiser_profile->post_title; ?>&body=Check out the fundraiser here: ' + '<?php echo $fundraiser_share_link; ?>'">
								<span class="fa fa-envelope"></span>
							</a>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<h4>Share Your Favorite Products</h4>
					<div class="favorite-products">
						<!-- <p>You haven't set up your favorite products yet. Your fund ambassador can help.</p> -->
						<!-- <a href="#">Read more >></a> -->
						<!-- Custom Fundraiser Products -->
						<div class="facebook-preview">
							<div class="image">
								<img src="<?php echo $favprods_image; ?>" />
							</div>
							<div class="content">
								<div class="title"><?php echo $favprods_title; ?></div>
								<div class="description"><?php echo $favprods_desc; ?></div>
								<div class="domain">onemission.fund</div>
							</div>
						</div>
						<div class="social-wrapper">
							<div class="icons">
								<strong>Share:</strong>&nbsp;
								<a class="btn-facebook"
									href="http://www.facebook.com/sharer.php?u=<?php echo urlencode($favprods_link) ?>&title=<?php echo urlencode( $favprods_title ); ?>" class="btn facebook" rel="nofollow" target="_blank" title="Share on Facebook">
									<span class="fa fa-facebook"></span>
								</a>
								<a class="btn-twitter" href="http://twitter.com/home?status=<?php echo $favprods_link ?>+%28<?php echo urlencode( $fundraiser_profile->post_title ); ?>%29" class="btn twitter" rel="nofollow" target="_blank" title="Share on Twitter">
									<span class="fa fa-twitter"></span>
								</a>
								<a class="btn-google" href="#" class="btn google" rel="nofollow" title="Email a Friend" onclick="javascript:window.location='mailto:?subject=Check out this fundraiser for <?php echo $fundraiser_profile->post_title; ?>&body=Check out the fundraiser here: ' + '<?php echo $favprods_link; ?>'">
									<span class="fa fa-envelope"></span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr />
			<div class="row">
				<div class="col-md-12">
					<h4>Share One Mission Featured Products</h4>
					<p>One Mission regularly features new items, offers discounts, and creates seasonal specials.  Share these features with your supporters so they can support you again and again.  You just have to share it!</p>
				</div>
				<div class="col-md-6 centered clearfix">
					<div class="featured-items">
						<!-- <p>You haven't set up your favorite products yet. Your fund ambassador can help.</p> -->
						<!-- <a href="#">Read more >></a> -->
						<!-- Custom Fundraiser Products -->
						<div class="facebook-preview">
							<div class="image">
								<img src="<?php echo $featured_items_image ?>" />
							</div>
							<div class="content">
								<div class="title"><?php echo $featured_items_title; ?></div>
								<div class="description"><?php echo $featured_items_desc; ?></div>
								<div class="domain">onemission.fund</div>
							</div>
						</div>
						<div class="social-wrapper">
							<div class="icons">
								<strong>Share:</strong>&nbsp;
								<a class="btn-facebook"
									href="http://www.facebook.com/sharer.php?u=<?php echo urlencode($featured_items_link) ?>&title=<?php echo urlencode( 'Supporting ' . $fundraiser_profile->post_title ) . ' - ' . $featured_items_title; ?>"
									class="btn facebook" rel="nofollow" target="_blank" title="Share on Facebook">
									<span class="fa fa-facebook"></span>
								</a>
								<a class="btn-twitter" href="http://twitter.com/home?status=<?php echo $featured_items_link ?>+%28<?php echo urlencode( $fundraiser_profile->post_title ); ?>%29" class="btn twitter" rel="nofollow" target="_blank" title="Share on Twitter">
									<span class="fa fa-twitter"></span>
								</a>
								<a class="btn-google" href="#" class="btn google" rel="nofollow" title="Email a Friend" onclick="javascript:window.location='mailto:?subject=Check out this fundraiser for <?php echo $fundraiser_profile->post_title; ?>&body=Check out the fundraiser here: ' + '<?php echo $featured_items_link; ?>'">
									<span class="fa fa-envelope"></span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr />
			<div class="row">
				<div class="col-md-12">
					<h4>Share One Mission Product Categories</h4>
					<p>One Mission has a variety of great products that appeal to a wide range of supporters.  So, stay in front of your supporters and share different product categories at least once per week!</p>
				</div>
				<div class="col-md-12 centered">
					<div class="featured-categories">
						<div class="slider-arrow left"><button class="btn btn-large btn-default"><i class="fa fa-chevron-left"></i></button></div>
						<div class="slider">
							<div class="wrapper">
							<?php foreach($featured_categories as $featured_cat): ?>
								<div class="featured-category">
									<div class="facebook-preview">
										<div class="image">
											<a href="<?php echo $featured_cat->url; ?>" target="_blank">
												<div class="title"><?php echo $featured_cat->name; ?></div>
												<img src="<?php echo $featured_cat->image ?>" />
											</a>
										</div>
									</div>
									<?php $share_link = ''; ?>
									<div class="social-wrapper">
										<div class="icons">
											<strong>Share:</strong>&nbsp;
											<a class="btn-facebook"  href="http://www.facebook.com/sharer.php?u=<?php echo urlencode($featured_cat->url) ?>&title=<?php echo urlencode( $featured_cat->name ); ?>" class="btn facebook" rel="nofollow" target="_blank" title="Share on Facebook">
												<span class="fa fa-facebook"></span>
											</a>
											<a class="btn-twitter" href="http://twitter.com/home?status=<?php echo $featured_cat->url ?>+%28<?php echo urlencode( $featured_cat->name ); ?>%29" class="btn twitter" rel="nofollow" target="_blank" title="Share on Twitter">
												<span class="fa fa-twitter"></span>
											</a>
											<a class="btn-google" href="#" class="btn google" rel="nofollow" title="Email a Friend" onclick="javascript:window.location='mailto:?subject=Check out this fundraiser for <?php echo $fundraiser_profile->post_title; ?>&body=Check out the fundraiser here: ' + '<?php echo $featured_cat->url; ?>'">
												<span class="fa fa-envelope"></span>
											</a>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
							</div>
						</div>
						<div class="slider-arrow right"><button class="btn btn-large btn-default"><i class="fa fa-chevron-right"></i></button></div>
					</div>
				</div>
			</div>
			<hr />
			<h3 class="text-uppercase">Game Changers</h3>
			<br>
			<div class="row">
				<div class="col-sm-4 space-2">
					<div class="inset inset-condensed text-center">
						<img class="gamechanger-img" src="/content/themes/fh_boilerplate-child/img/MVPs.jpg">
						<h4 class="text-uppercase">MVP Codes</h4>
						<p>Engage your Most Valuable Promoters with discount codes. <br><strong><a href="https://onemission.fund/help-center/most-valuable-promoters/" target="_blank">Learn More</a></strong></p>
						<p class="space-0"><strong>Contact Your Fund Ambassador</strong></p>
					</div>
				</div>
				<div class="col-sm-4 space-2">
					<div class="inset inset-condensed text-center">
						<img class="gamechanger-img" src="/content/themes/fh_boilerplate-child/img/Bulk-Free-Shipping.jpg">
						<h4 class="text-uppercase">Bulk Free Shipping</h4>
						<p>Create urgency and offer supporters savings with free shipping. <br><strong><a href="https://onemission.fund/help-center/bulk-free-shipping/" target="_blank">Learn More</a></strong></p>
						<p class="space-0"><strong>Contact Your Fund Ambassador</strong></p>
					</div>
				</div>
				<div class="col-sm-4 space-2">
					<div class="inset inset-condensed text-center">
						<img class="gamechanger-img" src="/content/themes/fh_boilerplate-child/img/Custom-Products.jpg">
						<h4 class="text-uppercase">Custom Products</h4>
						<p>Share your Fund's message on apparel, mugs, and art. <br><strong><a href="https://onemission.fund/help-center/add-custom-products/" target="_blank">Learn More</a></strong></p>
						<p class="space-0"><strong>Contact Your Fund Ambassador</strong></p>
					</div>
				</div>
			</div>
		</div>

		<script type="text/javascript">
			$(document).ready(function() {
				var slideSpeed = 350;
				var $slider = $('.slider .wrapper');
				var lastX = $('.featured-category:last').position().left;
				refreshButtons();

				$('.slider-arrow.left').on('click', function() {
					if ($(this).hasClass('disabled')) {
						return;
					}
					disableLeft();
					$slider.animate({
						left: "+=255"
					}, slideSpeed, 'swing',
					function() {
						refreshButtons();
					});
				});

				$('.slider-arrow.right').on('click', function() {
					if ($(this).hasClass('disabled')) {
						return;
					}
					disableRight();
					$slider.animate({
						left: "-=255"
					}, slideSpeed, 'swing',
					function() {
						refreshButtons();
					});
				});

				function refreshButtons() {
					console.info($slider.position().left, lastX);

					if ($slider.position().left >= 0) { disableLeft(); }
					else { enableLeft(); }

					if (Math.abs($slider.position().left) >= lastX) { disableRight(); }
					else { enableRight(); }
				}

				function disableLeft() { $('.slider-arrow.left').addClass('disabled'); }
				function disableRight() { $('.slider-arrow.right').addClass('disabled'); }
				function enableLeft() { $('.slider-arrow.left').removeClass('disabled'); }
				function enableRight() { $('.slider-arrow.right').removeClass('disabled'); }

			});
		</script>

	<?php
	}
