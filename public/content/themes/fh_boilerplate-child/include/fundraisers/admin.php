<?php

//

if( ! is_super_admin()):

	$curr_path = $_SERVER['PHP_SELF'];
	$admin_path = parse_url(admin_url(), PHP_URL_PATH);
	$req_method = $_SERVER['REQUEST_METHOD'];
	$user_id = get_current_user_id();
	if (!empty($user_id) && $req_method == 'GET' && strpos($curr_path, $admin_path) !== false) {
		$profile_id = get_user_meta( $user_id, 'fundraiser_profile_id' )[0];
		$fundraiser_post = get_page($profile_id);
		$pagename = isset($_GET['page']) ? $_GET['page'] : '';

		// if they dont have a fund approved force them to stay on fundraiser-profile-editor page

		if ($pagename != 'fundraiser-profile-editor') {
			if (!$fundraiser_post || ($fundraiser_post->post_status != 'publish' && $fundraiser_post->post_status != 'private')) {
				wp_redirect(admin_url('?page=fundraiser-profile-editor'));
				exit(0);
			}
		}
	}

	//
	//  Add custom menu pages
	//
	function _om_menu_pages() {

		add_menu_page(
			'Profile Editor',
			'Profile Editor',
			'edit_pages',
			'fundraiser-profile-editor',
			'_om_fundraiser_profile_editor',
			'dashicons-admin-tools',
			3
		);

		// dynamically added so woocommerce is initialized
		// add_menu_page(
		// 	'Supporters',
		// 	'Supporters',
		// 	'edit_pages',
		// 	'fundraiser-supporters',
		// 	'_om_fundraiser_supporters',
		// 	'dashicons-chart-area',
		// 	4
		// );

		add_menu_page(
			'Promotion',
			'Promotion',
			'edit_pages',
			'fundraiser-promotion',
			'_om_fundraiser_promotion',
			'dashicons-networking',
			25
		);

	}
	add_action( 'admin_menu' , '_om_menu_pages' );


	// add thermometer widget to dashboard
	function _om_dashboard_thermometer() {
		$user_id = get_current_user_id();
		$profile_id = get_user_meta( $user_id, 'fundraiser_profile_id' )[0];
		$fundraiser_post = get_page($profile_id);
		$fundraiser_meta = get_fields($profile_id);

		$fundraiser_goal_total_from_orders = _om_get_total_donation_amount();

		$gsa_total = 0;
		if (isset($fundraiser_meta['goal_status_adjustment'])) {
			$gsa_total = _om_get_gsa_total($fundraiser_meta['goal_status_adjustment']);
		}
		$fundraiser_goal_total_from_orders += (float) $gsa_total;

		$show_monetary_status = true;

		if( isset( $fundraiser_post->show_fundraiser_status ) ) {
			$show_monetary_status = ( $fundraiser_post->show_fundraiser_status === '1' ) ?  true : false;
		}

		if( $show_monetary_status ) {
			$clean_goal_amount = str_replace(',', '', $fundraiser_post->fundraiser_goal_amount );
			$clean_goal_status = str_replace(',', '', $fundraiser_goal_total_from_orders );
			$hide_progress_bar = false;
			if( floatval( $clean_goal_amount ) ) {
				$current_percent = floatval( $clean_goal_status ) / floatval( $clean_goal_amount );
				if( $current_percent >= 1 ) {
					$css_bar_percent      = 100;
					$progress_bar_precent = 100;
				} else {
					$css_bar_percent      = $current_percent * 100;
					$progress_bar_precent = number_format( ( $current_percent * 100 ) );
					$progress_bar_precent = ( (float) $progress_bar_precent > 100 ) ? 100 : $progress_bar_precent;
				}
			} else {
				$hide_progress_bar = true;
			}
		}

		if( $show_monetary_status && ! $hide_progress_bar ) {
			// Progress bar
			echo '<div id="fundraiser_thermometer_wrap">';
			echo '<div class="progress progress-striped active">';
			echo '<div id="profile_progress_bar" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="' . $progress_bar_precent . '" aria-valuemin="0" aria-valuemax="100" style="width: ' . $css_bar_percent . '%;">';
			echo $progress_bar_precent . '%';
			echo '</div>';
			echo '</div>';
			echo '<span class="left_text">$' . number_format( $clean_goal_status, 2 ) . '<br>Raised</span>';
			echo '<span class="right_text">$' . number_format( $clean_goal_amount, 2 ) . '<br>Goal!</span>';
			echo '</div>';
		} elseif( $show_monetary_status && floatval( $clean_goal_status ) ) {
			echo '<p class="h4"><strong>$' . number_format( $clean_goal_status, 2 ) . ' Raised!</strong></p>';
		} elseif( $show_monetary_status ) {
			echo '<p>No donations have been raised!</p>';
		}
	}

	function _om_dashboard_recent_supporters() {

		// Look up orders via $loc_post->post_author (Fundraiser User ID)
		$query_status = array( 'wc-processing', 'wc-completed' );
		$user_id = get_current_user_id();

		// Setup args for custom orders query
		$order_args = array(
			'posts_per_page' => -1,
			'post_type' =>      'shop_order',
			'post_status' =>    $query_status,
			'orderby' =>        'post_date',
			'order' =>          'DESC',
			'meta_query' => array(
				array(
					'key' =>   'Fundraiser User ID',
					'value' => $user_id
				)
			)
		);

		$sponsors_limit = 5;
		$sponsors_html  = '';


		// Run orders query
		$orders = new WP_Query( $order_args );

		if( !empty( $orders->posts ) ) {

			$i = 0;
			foreach( $orders->posts as $order ) {
				$order_id         = $order->ID;
				$fields           = get_post_custom( $order->ID );
				$billing_fname    = $fields['_billing_first_name'][0];
				$billing_linitial = substr( $fields['_billing_last_name'][0], 0, 1 );
				$billing_name = $fields['_billing_first_name'][0] . ' ' . $fields['_billing_last_name'][0];
				$billing_company  = $fields['_billing_company'][0];
				$order_date       = $order->post_date;
				$order_timestamp  = strtotime( $order_date );
				$order_human_date = date( 'F j, Y', $order_timestamp );
				$order_dt = new DateTime($order_date);
				$now = new DateTime();
				// Generate HTML for each sponsor if less than $sponsors_limit
				if( $i < $sponsors_limit && $now->sub(new DateInterval('P5D')) < $order_dt ) {
					$sponsor_html = '<td>' . $order_human_date . '</td>';

					// Public donation attribution
					if(
						isset( $fields['Public Donation Attribution'] ) &&
						isset( $fields['Public Donation Attribution'][0] ) &&
						! empty( $fields['Public Donation Attribution'][0] )
					) {
						$sponsor_html .= '<td>' . $fields['Public Donation Attribution'][0] . '</td>';
						$sponsor_html .= '<td>' . $fields['_billing_city'][0] . ', ' . $fields['_billing_state'][0] . '</td>';
					} else if (!empty($billing_name))  {
						$sponsor_html .= '<td>' . $billing_name . '</td>';
						$sponsor_html .= '<td>' . $fields['_billing_city'][0] . ', ' . $fields['_billing_state'][0] . '</td>';
					} else {
						if( isset( $states_lookup[ $fields['_billing_state'][0] ] ) ) {
							$sponsor_html .= '<td>' . $states_lookup[ $fields['_billing_state'][0] ] . '</td>';
						} else {
							// Shouldn't excecute
							$sponsor_html .= '<td>' . $fields['_billing_state'][0] . '</td>';
						}
						$sponsor_html .= '<td>&#160;</td>';
					}
					$sponsor_html .= '<td>$' . $fields['Total Donation Amount'][0] . '</td>';

					// Donation date
					$sponsors_html .= '<tr>' . $sponsor_html . '</tr>';
					$i++;
				}
			}
		}
		if (!empty($sponsors_html)) {
			$sponsors_html = '<div class="segment well">' .
				'<table class="wp-list-table widefat fixed striped" id="supporters_list_container">' .
				'<tbody">' . $sponsors_html .
				'</tbody></table></div>';
		} else {
			$sponsors_html = '<p>No supporters in the past 5 days.</p>';
		}
		echo $sponsors_html;
	}

	function _om_dashboard_notifications() {
		$notifications = get_field('fundraiser_notifications', 'options');
		if (strlen($notifications) == 0) {
			echo 'There are currently no notifications to display.';
		} else {
			echo $notifications;
		}
	}

	function _om_dashboard_game_changers() {
?>
			<div class="row">
				<div class="col-sm-4 space-2">
					<div class="inset inset-condensed text-center">
						<img class="gamechanger-img" src="/content/themes/fh_boilerplate-child/img/MVPs.jpg">
						<h4 class="text-uppercase">MVP Codes</h4>
						<p>Engage your Most Valuable Promoters with discount codes. <br><strong><a href="https://onemission.fund/help-center/most-valuable-promoters/" target="_blank">Learn More</a></strong></p>
						<p class="space-0"><strong>Contact Your Fund Ambassador</strong></p>
					</div>
				</div>
				<div class="col-sm-4 space-2">
					<div class="inset inset-condensed text-center">
						<img class="gamechanger-img" src="/content/themes/fh_boilerplate-child/img/Bulk-Free-Shipping.jpg">
						<h4 class="text-uppercase">Bulk Free Shipping</h4>
						<p>Create urgency and offer supporters savings with free shipping. <br><strong><a href="https://onemission.fund/help-center/bulk-free-shipping/" target="_blank">Learn More</a></strong></p>
						<p class="space-0"><strong>Contact Your Fund Ambassador</strong></p>
					</div>
				</div>
				<div class="col-sm-4 space-2">
					<div class="inset inset-condensed text-center">
						<img class="gamechanger-img" src="/content/themes/fh_boilerplate-child/img/Custom-Products.jpg">
						<h4 class="text-uppercase">Custom Products</h4>
						<p>Share your Fund's message on apparel, mugs, and art. <br><strong><a href="https://onemission.fund/help-center/add-custom-products/" target="_blank">Learn More</a></strong></p>
						<p class="space-0"><strong>Contact Your Fund Ambassador</strong></p>
					</div>
				</div>
			</div>
<?php
	}


	function _om_setup_fundraiser_widgets() {
		wp_add_dashboard_widget('fundraiser_progress_widget', 'Fund Progress', '_om_dashboard_thermometer');
		wp_add_dashboard_widget('fundraiser_supporters_widget', 'Recent Supporters', '_om_dashboard_recent_supporters');
		wp_add_dashboard_widget('fundraiser_notifications_widget', 'One Mission News', '_om_dashboard_notifications');
		wp_add_dashboard_widget('fundraiser_game_changers_widget', 'Game Changers', '_om_dashboard_game_changers');

		// force widget to top
		global $wp_meta_boxes;
		$normal_dashboard = $wp_meta_boxes['dashboard']['normal']['core'];
		$fundraiser_widget_backup = array( 'fundraiser_progress_widget' => $normal_dashboard['fundraiser_progress_widget'] );
		unset( $normal_dashboard['fundraiser_progress_widget'] );
		$sorted_dashboard = array_merge( $fundraiser_widget_backup, $normal_dashboard );
		$wp_meta_boxes['dashboard']['normal']['core'] = $sorted_dashboard;
	}
	add_action('wp_dashboard_setup', '_om_setup_fundraiser_widgets');

	function _om_setup_remove_dashboard_widgets() {
		global $wp_meta_boxes;
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['wpseo-dashboard-overview']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	}
	add_action('wp_dashboard_setup', '_om_setup_remove_dashboard_widgets');

	//
	//  Redirects user to profile page editing admin area
	//
	function _om_profile_edit_redirect() {

		$user_id = get_current_user_id();
		$profile_id = get_user_meta( $user_id, 'fundraiser_profile_id', true );
		$profile_id = ( $profile_id ) ? $profile_id : 0;
		$profile_edit_url = admin_url( 'post.php?post=' . $profile_id . '&action=edit' );

		echo '<br><strong>Loading profile... just a moment!</strong>';
		?>
		<script type="text/javascript">
			window.location="<?php echo $profile_edit_url; ?>";
		</script>
		<?php
	}

	function _om_fundraiser_post_image_box() {
		require_once(__DIR__ . "/dialogs/upload-image.php");
		$post_thumb = get_the_post_thumbnail();
		if (empty($post_thumb)) {
			$post_thumb = "<img src='' />";
		}

		echo '<input type="file" style="display: none" id="post_image_file" />';
		echo '<div id="post_image_image">' . $post_thumb . '</div>';
		echo '<p class="submit-status"><span class="submit-text"></span>';
		echo '<span class="progress-percent"></span></p><div class="progressbar"></div>';
		echo '<button class="change-post-image" data-target_id="post_image_image img" data-target_file="post_image_file" data-aspect_ratio="0">';
		if (empty($post_thumb)) {
			echo 'Add Post Image';
		} else {
			echo 'Change Post Image';
		}
		echo '</button>';
	}

	function _om_fundraiser_meta_boxes($type, $post) {
		global $wp_meta_boxes;
		if ($type == 'post') {
			remove_meta_box('postimagediv', 'post', 'side');
			add_meta_box('post_image', __('Post Image', 'textdomain'), '_om_fundraiser_post_image_box', 'post', 'side', 'low');
		}
	}
	add_action( 'add_meta_boxes', '_om_fundraiser_meta_boxes', 999999, 2 );

?>

<?php
else:
	//
	//  Remove page attribute editing from pages section
	//
	function _om_add_meta_boxes($type, $post) {
		remove_meta_box( 'rawhtml_meta_box', 'page', 'side' );
		if ($type == 'page' && $post->post_parent == OM_FUNDRAISER_PAGE_PARENT_ID) {
			add_meta_box('fundraiser_author',
				__('Fundraiser Author', 'textdomain'), '_om_fundraiser_author_box', 'page', 'side', 'high');
			add_meta_box('fundraiser_files',
				__('Fundraiser Files', 'textdomain'), '_om_fundraiser_files_box', 'page', 'side', 'low');
		}
	}
	add_action( 'add_meta_boxes', '_om_add_meta_boxes', 999999, 2 );

	function _om_fundraiser_author_box($post) {
		$author = get_userdata($post->post_author);
		if (!empty($author)) {
			$view_link = admin_url('user-edit.php?user_id=' . $author->data->ID);
			if ($author->data->user_nicename !== $author->data->user_login) {
				echo '<p><strong>Author: </strong>' . $author->data->user_nicename . '</p>';
				echo '<p><strong>Author ID: </strong><a target="_blank" href="' . $view_link . '">'
					. $author->data->user_nicename . ' (' . $author->data->ID . '</a></p>';
			} else {
				echo '<p><strong>Author:</strong> <a target="_blank" href="' . $view_link . '">' .
					$author->data->user_nicename . ' (' . $author->data->ID . ')</a></p>';
			}
			echo '<p><strong>Email: </strong><a href="mailto:' . $author->data->user_email . '">' .  $author->data->user_email . '</a></p>';
			echo '<p><strong>Roles: </strong>' . implode($author->roles , ' ') . '</p>';
			echo '<p><a target="_blank" href="' . $view_link . '">View/Edit</a></p>';
		}
	}

	function _om_fundraiser_files_box($post) {

		$banner_url = get_field('banner_image')['url'];
		$logo_url = get_field('logo_image')['url'];
		$document_501c = wp_get_attachment_url(get_field('document_501c'));
		if (!empty($banner_url)) {
			$filename = array_pop(explode("/", $banner_url));
			echo '<p><strong>Banner: </strong><a target="_blank" href="' . $banner_url . '">' . $filename . '</a></p>';
		}
		if (!empty($logo_url)) {
			$filename = array_pop(explode("/", $logo_url));
			echo '<p><strong>Logo: </strong><a target="_blank" href="' . $logo_url . '">' . $filename . '</a></p>';
		}

		if (!empty($document_501c)) {
			$filename = array_pop(explode("/", $document_501c));
			echo '<p><strong>Document 501c: </strong><a target="_blank" href="' . $document_501c . '">' . $filename . '</a></p>';
		}

	}

	//
	//  Remove Columns from page editing tables
	//
	function _fh_remove_pages_columns( $columns ) {

		// // remove the Yoast SEO columns
		// unset( $columns['wpseo-score'] );
		// unset( $columns['wpseo-title'] );
		// unset( $columns['wpseo-metadesc'] );
		// unset( $columns['wpseo-focuskw'] );
      //
		// // remove default WP columns
		// //unset( $columns['comments'] );
		// unset( $columns['author'] );
		// unset( $columns['date'] );

		return $columns;

	}
	add_filter ( 'manage_edit-page_columns', '_fh_remove_pages_columns' );

?>

<?php
	endif;
?>
