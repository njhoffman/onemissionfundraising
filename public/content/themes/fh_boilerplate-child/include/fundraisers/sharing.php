<?php

	function _om_fundraiser_sharing_tools() {

		// Get profile page information
		$user_id = get_current_user_id();
		$profile_id = get_user_meta( $user_id, 'fundraiser_profile_id', true );
		$profile_id = ( $profile_id ) ? $profile_id : 0;

		$fundraiser_profile = get_page( $profile_id );
		$fundraiser_name = $fundraiser_profile->post_title;
		$fundraiser_guid = get_permalink( $profile_id );

		?>
		<style>
			.wrap { text-align: center; }
			.wrap ul { margin-top: 5px; }
			.wrap ul li a { text-decoration: none; }
			textarea { height: 200px; width: 80%; }
		</style>

		<div class="wrap">
			<h2>Social Sharing Tools</h2><hr>

			<p><strong>Fundraiser Profile URL</strong>:<br><a href="<?php echo $fundraiser_guid ?>" target="_blank"><?php echo $fundraiser_guid; ?></a></p>

			<span><strong>Share Your Profile:</strong></span>
			<br>
			<ul style="margin-top: 5px;">
				<li><a href="http://twitter.com/home?status=<?php echo $fundraiser_guid ?>+%28<?php echo $fundraiser_name ?>%29" class="btn twitter" rel="nofollow" target="_blank" title="Share on Twitter"><span class="dashicons dashicons-twitter"></span> Share on Twitter</a></li>
			    <li><a href="http://www.facebook.com/sharer.php?u=<?php echo $fundraiser_guid ?>&t=<?php echo $fundraiser_name ?>" class="btn facebook" rel="nofollow" target="_blank" title="Share on Facebook"><span class="dashicons dashicons-facebook"></span> Share on Facebook</a></li>
			    <li><a href="https://plus.google.com/share?url=<?php echo $fundraiser_guid ?>"  class="btn google" rel="nofollow" target="_blank" title="Share on Google+"><span class="dashicons dashicons-googleplus"></span> Share on Google+</a></li>
			</ul>

			<br>

			<h2>Embeddable HTML Widget</h2><hr>

			<p style="text-align:center;"><strong>Preview:</strong> (185px wide)</p>

			<!-- 185 x 185 px -->
			<div style="display:table;min-height:183px;width:183px;margin-left:auto;margin-right:auto;border:1px solid #ccc;background: #fff;font-family: arial, sans-serif;line-height: 19px;border-radius: 3px;">
				<div style="display: table-cell; padding: 10px; text-align: center; vertical-align: middle;">
					<a target="_blank" style="color: #666;text-decoration: none;" href="<?php echo get_permalink( $profile_id ); ?>?shopfor=<?php echo $profile_id; ?>">
						<img style="margin-left:auto; margin-right:auto;" src="https://onemissionfundraising.com/content/themes/fh_boilerplate-child/img/html_widget_icon.jpg"><br>
						<span style="font-size:12px">Shop to support:</span><br>
						<span style="font-size:14px;font-family:arial, sans-serif;font-weight:bold;"><?php echo $fundraiser_name; ?></span>
						<span style="width: 100%;display: block;border-top: 1px dotted #ccc;margin-top: 5px;margin-bottom: 5px;"></span>
						<span style="font-size:12px;font-family:Courier New, Courier, monospace, sans-serif;">One Mission Fundraising</span><br>
						<span style="font-size:11px;font-family:arial, sans-serif;color: #aaa;">onemissionfundraising.com</span>
					</a>
				</div>
			</div>

			<p style="text-align:center;"><strong>Embedable HTML Code:</strong></p>

			<textarea onclick="this.select();">&lt;!-- START One Mission Embedable HTML Widget --&gt;
&lt;div style="display:table;min-height:183px;width:183px;margin-left:auto;margin-right:auto;border:1px solid #ccc;background: #fff;font-family: arial, sans-serif;line-height: 19px;border-radius: 3px;"&gt;&lt;div style="display: table-cell; padding: 10px; text-align: center; vertical-align: middle;"&gt;&lt;a target="_blank" style="color: #666;text-decoration: none;" href="<?php echo get_permalink( $profile_id ); ?>?shopfor=<?php echo $profile_id; ?>"&gt;&lt;img style="margin-left:auto; margin-right:auto;" src="https://onemissionfundraising.com/content/themes/fh_boilerplate-child/img/html_widget_icon.jpg"&gt;&lt;br&gt;&lt;span style="font-size:12px">Shop to support:&lt;/span&gt;&lt;br&gt;&lt;span style="font-size:14px;font-family:arial, sans-serif;font-weight:bold;"><?php echo $fundraiser_name; ?>&lt;/span&gt;&lt;span style="width: 100%;display: block;border-top: 1px dotted #ccc;margin-top: 5px;margin-bottom: 5px;">&lt;/span&gt;&lt;span style="font-size:12px;font-family:Courier New, Courier, monospace, sans-serif;">One Mission Fundraising&lt;/span&gt;&lt;br&gt;&lt;span style="font-size:11px;font-family:arial, sans-serif;color: #aaa;">onemissionfundraising.com&lt;/span&gt;&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;
&lt;!-- END One Mission Embedable HTML Widget --&gt;</textarea>

			<p>(copy and paste into HTML website/HTML compatible location)</p>

		</div>
		<?php
	}
