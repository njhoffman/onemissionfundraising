<?php


	function _om_format_money($num) {
		return '$' . number_format(tofloat($num), 2, '.', '');
	}

	function _om_fundraiser_monthly_csv_init() {
		if (isset($_GET['fundraiser-monthly-csv'])) {
			$ts = isset($_GET['ts']) ? $_GET['ts'] : null;
			$type = isset($_GET['type']) ? $_GET['type'] : null;
			$orders_data   = _om_get_fundraisers_orders();
			$omOrdersTable = new OM_Fund_Orders_Table( $orders_data['table'], 200, true, true, true);
			$fundraiserData = $omOrdersTable->prepare_items($ts, $type, true, true, true);
			$csvData = [];
			foreach ($fundraiserData as $fd) {
				array_push($csvData, array(
					'Order ID' => $fd['ID'],
					'Order Date' => $fd['order_date'],
					'Donation Attribution' => $fd['donation_attr'],
					'First Name' => $fd['first_name'],
					'Last Name' => $fd['last_name'],
					'Company Name' => $fd['company_name'],
					'Street Address' => $fd['address_raw'],
					'Street Address (2)' => $fd['address_raw_2'],
					'City' => $fd['city'],
					'State' => $fd['state'],
					'Zip' => $fd['zip_code'],
					'Email Address' => $fd['email'],
					'Product Price' => _om_format_money($fd['product_purchases']),
					'Cash Donation' => _om_format_money($fd['cash_donation']),
					'Sales Tax' => _om_format_money($fd['order_taxes']),
					'Shipping' => _om_format_money($fd['order_shipping']),
					'Order Total' => _om_format_money($fd['total']),
					'Product Donation' => _om_format_money($fd['product_donation']),
					'Cash Donation' => _om_format_money($fd['cash_donation']),
					'Subscription Donation' => _om_format_money($fd['subscription_donation']),
					'Total Donation' => _om_format_money($fd['donation_total']),
					'Credit Card Fee' => _om_format_money($fd['transaction_fee']),
					'Net Donation' => _om_format_money($fd['net_donation']),
					'Order Notes' => $fd['note']
				));
			}

			// _dbg($fundraiserData);
			// $content = _om_arr_to_csv($fundraiserData);
			$content = _om_arr_to_csv($csvData);

			header("Content-type: application/x-msdownload",true,200);
			header("Content-Disposition: attachment; filename=MonthlyReport.csv");
			header("Pragma: no-cache");
			header("Expires: 0");
			echo $content;
			exit(0);
		}
	}
	add_action('admin_init', '_om_fundraiser_monthly_csv_init');

	function _om_add_fundraiser_supporters_menu_item() {
		add_menu_page(
			'Supporters',
			'Supporters',
			'edit_pages',
			'fundraiser-supporters',
			'_om_fundraiser_supporters',
			'dashicons-chart-area',
			4
		);
	}

	function _om_init_fundraiser_supporters() {
		if (_om_check_user_role('fundraiser')) {
			add_action('admin_menu', '_om_add_fundraiser_supporters_menu_item');
		}
	}
	add_action('woocommerce_init', '_om_init_fundraiser_supporters');


	function _om_ajax_fetch_fundraiser_supporters() {
		$orders_data   = _om_get_fundraisers_orders();
		$details = true;
		if (isset($_REQUEST['details'])) {
			$details = $_REQUEST['details'];
			// wow this is stupid
			if ($details == 'false') {
				$details = false;
			}
		}
		$wp_list_table = new OM_Fund_Orders_Table($orders_data['table'], 200, $details);
		$wp_list_table->ajax_response();
	}
	add_action('wp_ajax_ajax_fetch_fundraiser_supporters','_om_ajax_fetch_fundraiser_supporters');

	function _om_ajax_delete_offline_donation() {
		$offline_old_string = isset($_REQUEST[0]) ? $_REQUEST[0]['value'] : '';
		if ($offline_old_string) {
			$user_id = get_current_user_id();
			$profile_id = get_user_meta( $user_id, 'fundraiser_profile_id' )[0];
			$goal_adjustment = get_post_meta($profile_id)['goal_status_adjustment'][0];
			$goal_adjustment = str_replace('|' . $offline_old_string, '', $goal_adjustment);
			$res = update_post_meta($profile_id, 'goal_status_adjustment', $goal_adjustment);
		}
	}
	add_action('wp_ajax_ajax_delete_offline_donation','_om_ajax_delete_offline_donation');

	function _om_ajax_update_offline_donation() {
		$offline_old_string = isset($_REQUEST[0]) ? $_REQUEST[0]['value'] : '';
		$new_string = '|' . strtotime($_REQUEST[1]['value']) . '::' . $_REQUEST[2]['value'] . '::' . $_REQUEST[3]['value'];
		if ($offline_old_string) {
			$user_id = get_current_user_id();
			$profile_id = get_user_meta( $user_id, 'fundraiser_profile_id' )[0];
			$goal_adjustment = get_post_meta($profile_id)['goal_status_adjustment'][0];
			$goal_adjustment = str_replace('|' . $offline_old_string, $new_string, $goal_adjustment);
			$res = update_post_meta($profile_id, 'goal_status_adjustment', $goal_adjustment);
		}
	}
	add_action('wp_ajax_ajax_update_offline_donation','_om_ajax_update_offline_donation');

	function _om_ajax_add_offline_donation() {
		$date = isset($_REQUEST[1]) ? strtotime($_REQUEST[1]['value']) : '';
		$amount = isset($_REQUEST[2]) ? $_REQUEST[2]['value'] : '';
		$description = isset($_REQUEST[3]) ? $_REQUEST[3]['value'] : '';
		if ($date && $amount) {
			_om_set_goal_status_adjustment($date, $amount, $description);
		}
	}
	add_action('wp_ajax_ajax_add_offline_donation', '_om_ajax_add_offline_donation');

	include('supporters/index.php');
	include('supporters/orders.php');
	include('supporters/table-orders.php');
	include('supporters/table-subscribers.php');


