<?php
	// entry point for profile editor
	// contains global functions, does initial setup, loads appropriate page

	function _om_fundraiser_profile_editor_get_steps() {
		return [
			'Basic Information',
			'Your Goal &amp; Timeline',
			'Tell Your Story',
			'Add Images/Video',
			'Say "Thank You"',
			'Contact Information',
			'Your Social Links',
			"Game Changers",
			'Ready to Launch?'
		];
	}

	function _om_fundraiser_profile_editor_set_field_map($fundraiser_meta, $fundraiser_post) {
		include __DIR__ . '/profile-editor/field_map.php';

		$completion_status = _om_fundraiser_profile_editor_get_completion_status();

		foreach ($field_map as $field => $props) {
			if (isset($fundraiser_meta[$field]) && isset($fundraiser_meta[$field][0])) {
					$field_map[$field]['value'] = $fundraiser_meta[$field][0];
			}
			$field_map[$field]['errors'] = Array();
			// if fundraiser has not been approved, don't lock down any fields yet
			if ($completion_status != 'approved' && $completion_status != 'pending') {
				$field_map[$field]['locked'] = false;
			} elseif (isset($field_map[$field]['locked']) && $field_map[$field]['locked'] == true)	{
				$field_map[$field]['required'] = false;
			}
		}
		return $field_map;
	}

	function _om_fundraiser_profile_editor_get_pages(&$field_map = [], $fundraiser_meta = []) {
		$pages = Array();
		$steps = _om_fundraiser_profile_editor_get_steps();
		for ($i = 0; $i < count($steps); $i++) {
			// incomplete fields have not been saved at all yet, required fields have been as empty/null
			$required_fields = Array();
			$incomplete_fields = Array();
			foreach ($field_map as $field => $props) {
				if ($props['page'] == ($i + 1)) {
					if ( !isset($fundraiser_meta[$field]) || !isset($fundraiser_meta[$field][0]) ) {
						if ($field != 'banner_image'
							&& $field != 'enable_501c'
							&& $field != 'document_501c'
							&& $field != 'banner_youtube_video_id'
							&& $field != '_yoast_wpseo_opengraph-image'
							&& $field != 'yoast_wpseo_opengraph-image'
							&& $field != "image_or_youtube_banner" ) {
							array_push($incomplete_fields, $field);
						 }
					} else if ($props['required'] == true && $fundraiser_meta[$field][0] == "") {
						array_push($required_fields, $field);
						array_push($field_map[$field]['errors'], 'Required');
					}
				}
			}

			$page_status = 'completed';
			if (count($required_fields) > 0) {
				$page_status = "error";
			} else if (count($incomplete_fields) > 0) {
				$page_status = "incomplete";
			}

			$pages[$i] = Array(
				'status'            => $page_status,
				'incomplete_fields' => $incomplete_fields,
				'title'             => $steps[$i],
				'required_fields'   => $required_fields
			);
		}

		// custom field status
		if (isset($field_map['image_or_youtube_banner']['value']) && !isset($field_map['logo_image']['value'])) {
			$pages[3]['status'] = "error";
			array_push($required_fields, 'logo_image');
			array_push($field_map['logo_image']['errors'], 'Required');
		}

		if (isset($field_map['enable_501c']['value'])
			&& $field_map['enable_501c']['value'] == 'yes'
			&& isset($field_map['document_501c']['value'])
			&& $field_map['document_501c']['value'] == ''	) {
			// if non-profit and they tried to submit form without document, add required error
			$pages[5]['status'] = "error";
			array_push($required_fields, 'document_501c');
			array_push($field_map['document_501c']['errors'], 'Required');
		}

		$completion_status = _om_fundraiser_profile_editor_get_completion_status($pages);
		$num_pages = count($steps);

		// special handling for last page and game changers page
		switch ($completion_status) {
			case "incomplete":
				$pages[$num_pages - 1]['status'] = 'disabled';
				break;
			case "complete":
				$pages[$num_pages - 1]['status'] = 'launch';
				break;
			case "pending":
				$pages[$num_pages - 1]['status'] = 'pending';
				break;
			case "approved":
				foreach ($pages as $idx => $page) {
					$pages[$idx]['status'] = 'approved';
				}
				$pages[$num_pages - 2]['status'] = 'hidden';
				$pages[$num_pages - 1]['status'] = 'hidden';
				break;
		}
		return $pages;
	}

	function _om_fundraiser_profile_editor_get_completion_status($pages = []) {
		// incomplete: profile has incomplete areas and not submitted for approval
		// complete: provile has no complete areas and not submitted for approval
		// pending: profile submitted for approval
		// approved: profile approved by admin
		$user_id = get_current_user_id();
		$profile_id = get_user_meta( $user_id, 'fundraiser_profile_id' )[0];
		$fundraiser_meta = get_post_meta($profile_id);
		$fundraiser_post = get_post($profile_id);

		$status = 'complete';

		$ready_to_launch = true;
		foreach ($pages as $idx => $page) {
			if ( (count($pages) - 1 > $idx) && $page['status'] != 'completed') {
				$status = 'incomplete';
			}
		}

		if (isset($fundraiser_meta['pending_approval'][0])) {
			$status = 'pending';
		}

		if ($fundraiser_post->post_status == 'publish' || $fundraiser_post->post_status == 'private') {
			$status = 'approved';
		}
		return $status;
	}

	function _om_fundraiser_profile_editor_get_step($pages) {
		// TODO: make this a constant
		if (!isset($_GET['page']) || $_GET['page'] !== 'fundraiser-profile-editor') {
			return -1;
		}
		// assign current step to the first page with errors or incomplete fields
		$curr_step = 2;
		$curr_step_status = false;
		for ($i = 0; $i < count($pages); $i++) {
			$page = $pages[$i];
			if ($page['status'] == 'error' && $curr_step_status !== 'error') {
				$curr_step_status = 'error';
				$curr_step = $i + 1;
			} else if ($page['status'] == 'incomplete' && !$curr_step_status) {
				$curr_step = $i + 1;
				$curr_step_status = 'incomplete';
			}
		}

		$curr_step = _om_fundraiser_profile_editor_get_completion_status($pages) === 'complete' ? count($pages) : $curr_step;
		// curr step override from link
		$curr_step = isset( $_GET["step"] ) ? $_GET["step"] : $curr_step;
		return $curr_step;
	}

	function _om_fundraiser_profile_editor_output_page($curr_step, $field_map, $pages, $fundraiser_post, $completion_status) {
		$show_setup_dialog = false;
		?>

			<div class="row">
				<div class="col-md-12">
					<form id="profile-editor">
						<input type="hidden" name="current_step" value="<?php echo $curr_step; ?>" />
						<?php
								// pages should only depend on fieldmap
								include __DIR__ . '/profile-editor/page' . ($curr_step) . '.php';
						?>
					</form>
				</div>
			</div>


		<?php require_once(__DIR__ . "/profile-editor/dialogs/setup-fund.php"); ?>
		<?php require_once(__DIR__ . "/profile-editor/dialogs/start-fund.php"); ?>

		<?php
	}

	function _om_fundraiser_profile_editor() {

		$steps = _om_fundraiser_profile_editor_get_steps();
		$num_pages = count($steps);

		$user_id = get_current_user_id();
		$profile_id = get_user_meta( $user_id, 'fundraiser_profile_id' )[0];
		$fundraiser_post = get_page($profile_id);
		$fundraiser_meta = get_post_meta($profile_id);
		$fundraiser_title = $fundraiser_post->post_title;
		$fundraiser_name = $fundraiser_post->post_name;
		$fundraiser_meta['fundraiser_title'] = Array($fundraiser_title);
		$fundraiser_meta['fundraiser_name'] = Array($fundraiser_name);

		$field_map = _om_fundraiser_profile_editor_set_field_map($fundraiser_meta, $fundraiser_post);
		// TODO: get rid of need to pass fundraiser_meta by reference
		$pages = _om_fundraiser_profile_editor_get_pages($field_map, $fundraiser_meta, $num_pages);
		$completion_status = _om_fundraiser_profile_editor_get_completion_status($pages);
		$curr_step = _om_fundraiser_profile_editor_get_step($pages);

		if (! isset($_GET['step']) && ($completion_status === 'pending' || $completion_status ==='completed' )) {
			$curr_step = $num_pages;
		}

		foreach ($pages as $idx => $page) {
			$pages[$idx]['active'] = ($curr_step == ($idx + 1));
		}

		_om_fundraiser_profile_editor_output_page($curr_step, $field_map, $pages, $fundraiser_post, $completion_status);

		// _dbg("\n\n***fundraiser pages ***\n\n");
		// _dbg($pages);
		// _dbg("\n\n");

		// _dbg("\n\n***fundraiser fields ***\n\n");
		// _dbg($field_map);
		// _dbg("\n\n");
	}


