<?php

    //
    //  On successful post of fundraiser news then assign category
    //  - Looks for user type
    //
    function _om_on_post_save_set_fundraiser_category( $post_id ) {
        
        // If this is a revision, get real post ID
        if ( $parent_id = wp_is_post_revision( $post_id ) ) {

            $post_id = $parent_id;
        }

        // Check user type
        $post = get_post( $post_id );
        $user_data = get_userdata( $post->post_author );

        // If user roles within fundraiser then set the fundraiser news category automatically
        if( isset( $user_data->roles ) && in_array( 'fundraiser', $user_data->roles ) ) {

            // Unhook this function so it doesn't loop infinitely
            remove_action( 'save_post', '_om_on_post_save_set_fundraiser_category' );

            // Add category (OM_FUNDRAISER_NEWS_CATEGORY)
            wp_set_post_categories( $post_id, array( OM_FUNDRAISER_NEWS_CATEGORY ) );

            // Re-hook function
            add_action( 'save_post', '_om_on_post_save_set_fundraiser_category' );
        }
    }
    add_action( 'save_post', '_om_on_post_save_set_fundraiser_category' );

    //
    //  Filter out globally on fundraiser news category
    //  - Allow on archive pages + category pages
    //
    function _om_filter_fundraiser_category( $query ) {

        if(
            // Allow category browsing
            ! is_category() &&
            // Allow author browsing
            ! is_author() &&
            // Allow browsing on fundraiser page template
            ! is_page_template( OM_FUNDRAISER_PAGE_TEMPLATE ) &&
            // Allow on single post (not technically needed =P)
            // ! is_single() &&
            // Allow in admin
            ! is_admin() &&
            // Remove if page is fundraiser parent page
            ! is_page( OM_FUNDRAISER_PAGE_PARENT_ID )
        ) {

            $query->set( 'cat', -OM_FUNDRAISER_NEWS_CATEGORY );
        }
    }
    add_action( 'pre_get_posts', '_om_filter_fundraiser_category' );


    //
    //  Exclude fundraiser news category dropdown widget
    //
    function _om_exclude_widget_categories( $args ){

        $exclude = OM_FUNDRAISER_NEWS_CATEGORY;
        $args['exclude'] = $exclude;

        return $args;
    }

    add_filter( 'widget_categories_dropdown_args', '_om_exclude_widget_categories' );


    //
    //  Disable forward/backward posts
    //
    function _fh_prev_next_posts() { return; }
