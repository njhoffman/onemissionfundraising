<?php
	$thank_you_message = !empty($field_map['thank_you_message']['value']) ? $field_map['thank_you_message']['value'] : '';
?>
<div class="row">
	<div class="col-sm-8">
		<h2 class="h3">Say "Thank You"</h2>
		<p>It’s very important to say thank you. In your thank you message consider asking supporters to share your story with family and friends. They’ve already shown that they care about your cause. They will likely be happy to share.</p>
		<p>The message you create below will be displayed on the receipt page as well as on the emailed receipt that your supporters will receive.</p>
		<?php
				$field_error = array_pop($field_map['thank_you_message']['errors']);
		?>
		<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
			<?php
				// $settings = array('media_buttons' => false, 'textarea_name' => 'thank_you_message', 'quicktags' => false);
				// wp_editor($thank_you_message, 'thank_you_message', $settings)
			?>
			<textarea style="width: 90%" rows="5" name="thank_you_message"><?php echo $thank_you_message; ?></textarea>
			<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="inset inset-help">
			<h4>TIPS</h4>
			<hr>
			<p>For non-profit groups, we recommend that you include your EIN number below your main thank you.</p>
			<br>
			<h4>HELP CENTER ARTICLES</h4>
			<hr>
			<p>- <a href="https://onemission.fund/help-center/say-thanks/" target="_blank">Say Thanks</a>
			<br>- <a href="https://onemission.fund/help-center/30-ways-say-thank/" target="_blank">30 Ways to Say "Thank You"</a></p>
		</div>
	</div>
</div>

<div class="button-footer">
	<div class="button-left">
		<button type="submit" name="save_later" class="btn btn-lg btn-default <?php echo $completion_status == 'approved' ? 'hidden' : '' ?>">
			Save For Later
		</button>
	</div>
	<div class="button-center">
		<p class="submit-status">
			<span class="submit-text">Submitting... </span>
			<span class="progress-percent"></span>
		</p>
		<div class="progressbar"></div>
	</div>
	<div class="button-right">
		<button type="submit" name="save_continue" class="btn btn-lg btn-success">
			<?php echo ($completion_status == 'approved' ? 'Update' : 'Save &amp; Continue'); ?>
		</button>
	</div>
</div>
