<?php
	$fundraiser_title = isset($field_map['fundraiser_title']) && isset($field_map['fundraiser_title']['value'])
		? $field_map['fundraiser_title']['value'] : '';
	$fundraiser_name = isset($field_map['fundraiser_name']) && isset($field_map['fundraiser_name']['value'])
		? $field_map['fundraiser_name']['value'] : '';
	$fundraiser_city = isset($field_map['fundraiser_city']) && isset($field_map['fundraiser_city']['value'])
		? $field_map['fundraiser_city']['value'] : '';
	$fundraiser_state = isset($field_map['fundraiser_state']) && isset($field_map['fundraiser_state']['value'])
		? $field_map['fundraiser_state']['value'] : '';
	$fundraiser_country = isset($field_map['fundraiser_country']) && isset($field_map['fundraiser_country']['value'])
		? $field_map['fundraiser_country']['value'] : '';
	$fundraiser_type = isset($field_map['fundraiser_type']) && isset($field_map['fundraiser_type']['value'])
		? explode('&', $field_map['fundraiser_type']['value']) : array();

	$country_is_us = strtolower($fundraiser_country) === 'united states' ||
		strtolower($fundraiser_country) === 'united states of america' ||
		strtolower($fundraiser_country) === 'u.s.' ||
		strtolower($fundraiser_country) === 'u.s.a.' ||
		strtolower($fundraiser_country) === 'usa' ||
		strtolower($fundraiser_country) === 'us';


	if (empty($fundraiser_title) || empty($fundraiser_type)) {
		$show_setup_dialog = true;
	}

	$fundraiser_url = get_permalink($fundraiser_post);

?>

<div class="row">
	<div class="col-sm-7 col-lg-8">
		<h2 class="h3">Basic Information</h2>
		<p>Please check all of your basic information and make sure everything is correct.</p>
		<?php
				$field_error = array_pop($field_map['fundraiser_title']['errors']);
				$field_locked = $field_map['fundraiser_title']['locked'] == true ? 'disabled' : '';
		?>
		<div class="form-group space-3">
			<div class="help-text title">
				A big part of fundraising is “story telling”. The title of your fund is like the title of your story. Be descriptive yet keep it as short as possible.
			</div>
			<label class="h4" for="title">Title of Fund<span class="required">*</span></label>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/info-icon.svg" class="help-icon title" />
			<input type="text" class="form-control" id="title" name="fundraiser_title" <?php echo $field_locked; ?>
				value="<?php echo $fundraiser_title ?>"
			/>
			<span class="lock-control dashicons dashicons-lock input-lock <?php echo $field_locked; ?>" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
			<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
		</div>
		<?php
				$field_error = array_pop($field_map['fundraiser_name']['errors']);
				$field_locked = $field_map['fundraiser_name']['locked'] == true ? 'disabled' : '';
		?>
		<div class="form-group space-3">
			<div class="help-text url">
				Each fund has their own customized page on the One Mission website.
				We’ve used your fund title to create the address for your fund. If you’d like to adjust that page address (maybe to make it shorter), please update this field.
				FYI, each word will automatically be separated by a hyphen and if another page on the site already exists with the same name, then we’ll have to adjust yours for you.
			</div>
			<label class="h4" for="url">Customize Your Fund Address<span class="required">*</span></label>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/info-icon.svg" class="help-icon title" />
			<p class="fundraiser-url">
				<?php echo str_replace($fundraiser_name, '<strong>' . $fundraiser_name . '</strong>',$fundraiser_url); ?>
			</p>
			<input type="text" class="form-control" id="url" name="fundraiser_name" <?php echo $field_locked; ?>
				value="<?php echo $fundraiser_name; ?>"
			/>
			<span class="lock-control dashicons dashicons-lock input-lock <?php echo $field_locked; ?>" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
			<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
		</div>
		<?php
				// $field_error = array_pop($field_map['fundraiser_city']['errors']);
		?>
		<div class="form-group space-3">
			<div class="help-text location">
				Let your supporters know where in the world your fund is making a positive impact.
				This will be displayed on your fund’s profile page.
				(This may or may not be your physical location)
				For example, if you’re going on a Mission trip to Haiti, you are making an impact in Haiti &mdash;
				note that the fund payee must be located in the US (we’ll collect that information later).
			</div>
			<label class="h4">Where In the World Are You Making an Impact?<span class="required">*</span></label>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/info-icon.svg" class="help-icon location" />
			<?php if ($country_is_us): ?>
			<div class="row">
				<div class="col-sm-6 space-2">
					<input type="text" class="form-control" id="city" name="fundraiser_city" placeholder="City"
					value="<?php echo $fundraiser_city; ?>" />
				</div>
				<div class="col-sm-6 space-2">
					<input type="text" class="form-control" id="state" name="fundraiser_state" placeholder="State"
						value="<?php echo $fundraiser_state; ?>" />
				</div>
			</div>
			<?php endif; ?>
			<input type="text" class="form-control" id="country" name="fundraiser_country" placeholder="Country"
				value="<?php echo $fundraiser_country; ?>" />
			<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
		</div>
		<?php
				$field_error = array_pop($field_map['fundraiser_type']['errors']);
				$field_locked = $field_map['fundraiser_type']['locked'] == true ? 'disabled' : '';
		?>
		<div class="form-group space-3">
			<label class="h4" for="type">Fund Type <small>(Select Up to Two)</small><span class="required">*</span></label>
			<span class="lock-control dashicons dashicons-lock <?php echo $field_locked; ?> space-top-1" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
			<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
			<div class="fundraiser-type">
				<div class="checkbox space-top-0">
					<label>
						<input type="checkbox" name="fundraiser_type" value="churches-youth-groups" <?php echo $field_locked; ?>
							<?php echo (in_array('churches-youth-groups', $fundraiser_type) ? 'checked' : ''); ?> />
						Churches &amp; Youth Groups
					</label>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="fundraiser_type" value="schools-fine-arts-athletics" <?php echo $field_locked; ?>
							<?php echo (in_array('schools-fine-arts-athletics', $fundraiser_type) ? 'checked' : ''); ?> />
						Schools, Fine Arts, &amp; Athletics
					</label>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="fundraiser_type" value="non-profit-organizations" <?php echo $field_locked; ?>
							<?php echo (in_array('non-profit-organizations', $fundraiser_type) ? 'checked' : ''); ?> />
						Non-Profit Organizations
					</label>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="fundraiser_type" value="mission-groups-missionaries" <?php echo $field_locked; ?>
							<?php echo (in_array('mission-groups-missionaries', $fundraiser_type) ? 'checked' : ''); ?> />
						Mission Groups &amp; Missionaries
					</label>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="fundraiser_type" value="medical-causes" <?php echo $field_locked; ?>
							<?php echo (in_array('medical-causes', $fundraiser_type) ? 'checked' : ''); ?> />
						Medical Causes
					</label>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="fundraiser_type" value="adoptive-parents" <?php echo $field_locked; ?>
							<?php echo (in_array('adoptive-parents', $fundraiser_type) ? 'checked' : ''); ?> />
						Adoptive Parents
					</label>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="fundraiser_type" value="community-benefits" <?php echo $field_locked; ?>
							<?php echo (in_array('community-benefits', $fundraiser_type) ? 'checked' : ''); ?> />
						Community Benefits
					</label>
				</div>
				<div class="checkbox">
					<label>
						<input type="checkbox" name="fundraiser_type" value="other-groups-individuals" <?php echo $field_locked; ?>
							<?php echo (in_array('other-groups-individuals', $fundraiser_type) ? 'checked' : ''); ?> />
						Other Groups &amp; Individuals
					</label>
				</div>
			</div>
		</div>

	</div>
	<div class="col-sm-5 col-lg-4">
		<div class="inset inset-help">
			<h4>TIPS</h4>
			<hr>
			<p>Each fund has their own customized page on the One Mission website.</p>
			<p>We’ve used your fund title to create the address for your fund. If you’d like to adjust that page address (maybe to make it shorter), please update this field.</p>
			<p>FYI, each word will automatically be separated by a hyphen and if another page on the site already exists with the same name, then we’ll have to adjust yours for you.
			</p>
		</div>
	</div>
</div>


<div class="button-footer">
	<div class="button-left">
		<button type="submit" name="save_later" class="btn btn-lg btn-default <?php echo $completion_status == 'approved' ? 'hidden' : '' ?>">
			Save For Later
		</button>
	</div>
	<div class="button-center">
		<p class="submit-status">
			<span class="submit-text">Submitting...</span>
			<span class="progress-percent"></span></p>
		<div class="progressbar"></div>
	</div>
	<div class="button-right">
		<button type="submit" name="save_continue" class="btn btn-lg btn-success">
			<?php echo $completion_status == 'approved' ? 'Update' : 'Save &amp; Continue'; ?>
		</button>
	</div>
</div>

