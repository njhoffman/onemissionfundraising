<div class="row">
	<?php
		$fundraiser_url = get_permalink($fundraiser_post);
		if ($pages[8]['status'] == 'pending'):
	?>
	<div class="col-md-12">
		<h2 class="h3">Fund Pending Approval</h2>
		<p>
			Great job and thank you for registering your Fund!  Your Fund will now be reviewed and we’ll get back to you shortly.
		</p>
		<p>While we are reviewing your registration it is a great time to study our <a href="https://onemission.fund/help_center/" target="_blank">Help Center</a> and plan how you will promote your Fund.</p>
		<p>Here’s a link to Help Center articles focused on <a href="https://onemission.fund/help_center/promoting-your-fund/" target="_blank">Promoting Your Fund</a>.</p>
		<p>And finally, you should be receiving an email from us momentarily confirming receipt of your registration.  If you do not receive that email please check your promotions or junk mail folder. While you are at it, please add <a href="mailto:web@onemissionfundraising.com">web@onemissionfundraising.com</a> to your contacts to be sure you receive our emails.</p>
		<div class="text-center">
			<a class="btn btn-lg btn-purple launch-btn" target="_blank"
				href="<?php echo $fundraiser_url . '?preview=true&warn=true'; ?>">Preview Your Page</a>
		</div>
		<br />
		</div>
		<div class="text-center">
			<button class="btn btn-lg btn-success launch-btn"><a href="/">Return Home</a></button>
		</div>
	</div>

<?php else: ?>
	<div class="col-md-12">
		<h2 class="h3">Ready to Launch?</h2>
		<p class="lead">Great! Let's do this. Want to preview your Fund page? Click the button below:</p>
		<div class="text-center">
			<a class="btn btn-lg btn-purple launch-btn" target="_blank"
				href="<?php echo $fundraiser_url . '?preview=true&warn=true'; ?>">Preview Your Page</a>
		</div>
		<br>
		<hr>
		<br>
		<p class="lead">
			To launch your Fund, it needs to be reviewed and approved by your Fund Ambassador.
			Click the button below to let your ambassador know that you are ready to proceed.
		</p>
		<div class="text-center">
			<button class="btn btn-lg btn-success launch-btn submit" name="launch">Submit for Review</button>
		</div>
	</div>
<?php endif; ?>
</div>
