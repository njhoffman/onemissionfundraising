<?php
	$description = !empty($field_map['fundraiser_description']['value'])
		? trim($field_map['fundraiser_description']['value']) : '';

	$description_length = strlen(utf8_decode(strip_tags($description)));
	if ($description_length < 0) {
		$description_length = 0;
	}

?>
<div class="row">
	<div class="col-sm-8">
		<h2 class="h3">Tell Your Story<span class="required">*</span></h2>
		<p>The story you write below will appear on your Fund page.  Motivate your supporters by telling a compelling story, asking for support, and creating urgency to act.</p>
		<?php
				$field_error = array_pop($field_map['fundraiser_description']['errors']);
		?>
		<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
			<label></label>

			<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
			<?php
				$settings = array('media_buttons' => false, 'textarea_name' => 'fundraiser_description', 'quicktags' => false);
				wp_editor($description, 'fundraiser_description', $settings)
			?>
			<p class="character-count space-top-1" data-currcount="<?php echo $description_length; ?>" data-count="200"><?php echo $description_length; ?> characters (200 minimum)</p>
		</div>
	</div>
	<div class="col-sm-4">
		<p class="space-0 text-center">Where you'll find your story:</p>
		<img src="/content/themes/fh_boilerplate-child/img/admin_help/story-demo.png" />
		<br>
		<br>
		<div class="inset inset-help">
			<h4>TIPS</h4>
			<hr>
			<strong>Be sure to:</strong>
			<ul>
				<li>- Add emotion and make it personal
				<li>- Add important details
				<li>- Connect financial details to your goal and timeline
				<li>- Make your supporters the hero of your story.
			</ul>
			<br>
			<h4>HELP CENTER ARTICLES</h4>
			<hr>
			<p>- <a href="https://onemission.fund/help-center/creating-your-story/" target="_blank">Creating Your Story</a></p>
		</div>
	</div>
</div>

<div class="button-footer">
	<div class="button-left">
		<button type="submit" name="save_later" class="btn btn-lg btn-default <?php echo $completion_status == 'approved' ? 'hidden' : '' ?>">
			Save For Later
		</button>
	</div>
	<div class="button-center">
		<p class="submit-status">
			<span class="submit-text">Submitting...</span>
			<span class="progress-percent"></span></p>
		<div class="progressbar"></div>
	</div>
	<div class="button-right">
		<button type="submit" name="save_continue" class="btn btn-lg btn-success">
			<?php echo ($completion_status == 'approved' ? 'Update' : 'Save &amp; Continue'); ?>
		</button>
	</div>
</div>
