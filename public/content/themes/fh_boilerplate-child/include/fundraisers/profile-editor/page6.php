<?php

$user_id = get_current_user_id();
$curr_user = get_user_by('id', $user_id);
$user_first_name = get_user_meta($user_id, 'first_name')[0];
$user_last_name = get_user_meta($user_id, 'last_name')[0];

$l_organization = isset($field_map['leader_organization']['value']) ? $field_map['leader_organization']['value'] : '';
$l_address = isset($field_map['leader_address']['value']) ? $field_map['leader_address']['value'] : '';
$l_city = isset($field_map['leader_city']['value']) ? $field_map['leader_city']['value'] : '';
$l_state = isset($field_map['leader_state']['value']) ? $field_map['leader_state']['value'] : '';
$l_zip = isset($field_map['leader_zip']['value']) ? $field_map['leader_zip']['value'] : '';
$l_phone = isset($field_map['leader_phone']['value']) ? $field_map['leader_phone']['value'] : '';
$l_email = isset($curr_user->user_email) && !empty($curr_user->user_email) ? $curr_user->user_email : $curr_user->user_login;

$p_check_name = isset($field_map['payee_check_name']['value']) ? $field_map['payee_check_name']['value'] : '';
$p_name = isset($field_map['payee_name']['value']) ? $field_map['payee_name']['value'] : '';
$p_organization = isset($field_map['payee_organization']['value']) ? $field_map['payee_organization']['value'] : '';
$p_address = isset($field_map['payee_address']['value']) ? $field_map['payee_address']['value'] : '';
$p_city = isset($field_map['payee_city']['value']) ? $field_map['payee_city']['value'] : '';
$p_state = isset($field_map['payee_state']['value']) ? $field_map['payee_state']['value'] : '';
$p_zip = isset($field_map['payee_zip']['value']) ? $field_map['payee_zip']['value'] : '';
$p_phone = isset($field_map['payee_phone']['value']) ? $field_map['payee_phone']['value'] : '';

$enable_501c = isset($field_map['enable_501c']['value']) ? $field_map['enable_501c']['value'] : '';
_dbg($enable_501c);

$document_501c_url = "";
$document_501c_name = "";
if (!empty($field_map['document_501c']['value'])) {
	$document_501c_url = wp_get_attachment_url($field_map['document_501c']['value']);
	$document_501c_parts = explode("/", $document_501c_url);
	$document_501c_name = array_pop($document_501c_parts);
}
?>

<div class="row">
	<div class="col-sm-12">
		<h2 class="h3">Contact Information</h2>
		<div class="contact-wrapper row">
			<div class="form-horizontal col-sm-6 space-6">
				<h4>Fund Leader Contact</h4>
				<p class="subtext">You are the Fund leader. Let us know how we can reach you.<br class="hidden-xs"><br class="hidden-xs"></p>
				<hr />
				<div class="form-group">
					<label class="col-sm-4">Name:</label>
					<div class="col-sm-8">
						<input class="input-disabled form-control" name="leader_name" type="text" disabled
						value="<?php echo $user_first_name . ' ' . $user_last_name; ?>" />
						<span class="lock-control input-lock dashicons dashicons-lock disabled" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
					</div>
				</div>
		<?php
			$field_error = array_pop($field_map['leader_organization']['errors']);
			$field_locked = $field_map['leader_organization']['locked'] == true ? 'disabled' : '';
		?>
				<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
					<label class="col-sm-4">Organization:</label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="leader_organization"
						<?php echo $field_locked; ?>
						value="<?php echo $l_organization ?>" />
						<span class="lock-control input-lock dashicons dashicons-lock <?php echo $field_locked; ?>" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
						<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
					</div>
				</div>
		<?php
			$field_error = array_pop($field_map['leader_address']['errors']);
			$field_locked = $field_map['leader_address']['locked'] == true ? 'disabled' : '';
		?>
				<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
					<label class="col-sm-4">Address:<span class="required">*</span></label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="leader_address"
						<?php echo $field_locked; ?>
						value="<?php echo $l_address ?>" />
						<span class="lock-control input-lock dashicons dashicons-lock <?php echo $field_locked; ?>" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
						<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
					</div>
				</div>
		<?php
			$field_error = array_pop($field_map['leader_city']['errors']);
			$field_locked = $field_map['leader_city']['locked'] == true ? 'disabled' : '';
		?>
				<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
					<label class="col-sm-4">City:<span class="required">*</span></label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="leader_city"
						<?php echo $field_locked; ?>
						value="<?php echo $l_city; ?>" />
						<span class="lock-control input-lock dashicons dashicons-lock <?php echo $field_locked; ?>" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
						<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
					</div>
				</div>
		<?php
			$field_error = array_pop($field_map['leader_state']['errors']);
			$field_locked = $field_map['leader_state']['locked'] == true ? 'disabled' : '';
		?>
				<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
					<label class="col-sm-4">State:<span class="required">*</span></label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="leader_state"
						<?php echo $field_locked; ?>
						value="<?php echo $l_state; ?>" />
						<span class="lock-control input-lock dashicons dashicons-lock <?php echo $field_locked; ?>" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
						<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
					</div>
				</div>
		<?php
			$field_error = array_pop($field_map['leader_zip']['errors']);
			$field_locked = $field_map['leader_zip']['locked'] == true ? 'disabled' : '';
		?>
				<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
					<label class="col-sm-4">Zip:<span class="required">*</span></label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="leader_zip"
						<?php echo $field_locked; ?>
						value="<?php echo $l_zip; ?>" />
						<span class="lock-control input-lock dashicons dashicons-lock <?php echo $field_locked; ?>" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
						<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
					</div>
				</div>
		<?php
			$field_error = array_pop($field_map['leader_phone']['errors']);
			$field_locked = $field_map['leader_phone']['locked'] == true ? 'disabled' : '';
		?>
				<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
					<label class="col-sm-4">Phone:<span class="required">*</span></label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="leader_phone"
						<?php echo $field_locked; ?>
						value="<?php echo $l_phone; ?>" />
						<span class="lock-control input-lock dashicons dashicons-lock <?php echo $field_locked; ?>" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
						<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4">Email Address:</label>
					<div class="col-sm-8">
						<input class="form-control" type="text" disabled value="<?php echo $l_email; ?>" />
						<span class="lock-control input-lock dashicons dashicons-lock disabled" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
					</div>
				</div>


			</div>

			<div class="form-horizontal col-sm-6">
				<h4 style="display: inline-block;">Payee Information</h4>
				<span class="same-as-leader checkbox" style="padding:0;">
					<label for="same-as-leader"><input id="same-as-leader" class="same-as-leader-checkbox" type="checkbox" /> Same as Leader</label>
				</span>
				<p class="subtext">
					Each month by the 15th, we’ll send physical checks by mail for the total,
					net amount raised during the previous month to the payee and address you specify below.
				</p>
				<hr />
		<?php
			$field_error = array_pop($field_map['payee_check_name']['errors']);
			$field_locked = $field_map['payee_check_name']['locked'] == true ? 'disabled' : '';
		?>
				<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
					<label class="col-sm-5">Name on Check:</label>
					<div class="col-sm-7">
						<input class="form-control" type="text" name="payee_check_name"
						<?php echo $field_locked; ?>
						value="<?php echo $p_check_name; ?>" />
						<span class="lock-control input-lock dashicons dashicons-lock <?php echo $field_locked; ?>" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
						<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
					</div>
				</div>
				<label class="check-mailed-to">Check Mailed to:</label>
				<hr />
		<?php
			$field_error = array_pop($field_map['payee_name']['errors']);
			$field_locked = $field_map['payee_name']['locked'] == true ? 'disabled' : '';
		?>
				<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
					<label class="col-sm-4">Name:<span class="required">*</span></label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="payee_name"
						<?php echo $field_locked; ?>
						value="<?php echo $p_name; ?>" />
						<span class="lock-control input-lock dashicons dashicons-lock <?php echo $field_locked; ?>" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
						<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
					</div>
				</div>
		<?php
			$field_error = array_pop($field_map['payee_organization']['errors']);
			$field_locked = $field_map['payee_organization']['locked'] == true ? 'disabled' : '';
		?>
				<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
					<label class="col-sm-4">Organization:</label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="payee_organization"
						<?php echo $field_locked; ?>
						value="<?php echo $p_organization; ?>" />
						<span class="lock-control input-lock dashicons dashicons-lock <?php echo $field_locked; ?>" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
						<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
					</div>
				</div>
		<?php
			$field_error = array_pop($field_map['payee_address']['errors']);
			$field_locked = $field_map['payee_address']['locked'] == true ? 'disabled' : '';
		?>
				<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
					<label class="col-sm-4">Address:<span class="required">*</span></label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="payee_address"
						<?php echo $field_locked; ?>
						value="<?php echo $p_address; ?>" />
						<span class="lock-control input-lock dashicons dashicons-lock <?php echo $field_locked; ?>" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
						<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
					</div>
				</div>
		<?php
			$field_error = array_pop($field_map['payee_city']['errors']);
			$field_locked = $field_map['payee_city']['locked'] == true ? 'disabled' : '';
		?>
				<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
					<label class="col-sm-4">City:<span class="required">*</span></label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="payee_city"
						<?php echo $field_locked; ?>
						value="<?php echo $p_city; ?>" />
						<span class="lock-control input-lock dashicons dashicons-lock <?php echo $field_locked; ?>" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
						<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
					</div>
				</div>
		<?php
			$field_error = array_pop($field_map['payee_state']['errors']);
			$field_locked = $field_map['payee_state']['locked'] == true ? 'disabled' : '';
		?>
				<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
					<label class="col-sm-4">State:<span class="required">*</span></label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="payee_state"
						<?php echo $field_locked; ?>
						value="<?php echo $p_state; ?>" />
						<span class="lock-control input-lock dashicons dashicons-lock <?php echo $field_locked; ?>" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
						<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
					</div>
				</div>
		<?php
			$field_error = array_pop($field_map['payee_zip']['errors']);
			$field_locked = $field_map['payee_zip']['locked'] == true ? 'disabled' : '';
		?>
				<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
					<label class="col-sm-4">Zip:<span class="required">*</span></label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="payee_zip"
						<?php echo $field_locked; ?>
						value="<?php echo $p_zip; ?>" />
						<span class="lock-control input-lock dashicons dashicons-lock <?php echo $field_locked; ?>" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
						<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
					</div>
				</div>
		<?php
			$field_error = array_pop($field_map['payee_phone']['errors']);
			$field_locked = $field_map['payee_phone']['locked'] == true ? 'disabled' : '';
		?>
				<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
					<label class="col-sm-4">Phone:<span class="required">*</span></label>
					<div class="col-sm-8">
						<input class="form-control" type="text" name="payee_phone"
						<?php echo $field_locked; ?>
						value="<?php echo $p_phone; ?>" />
						<span class="lock-control input-lock dashicons dashicons-lock <?php echo $field_locked; ?>" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
						<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
					</div>
				</div>
			</div>
		</div>
		<hr>


<?php
	$fund_type = explode('&', $field_map['fundraiser_type']['value']);
	if (in_array('churches-youth-groups', $fund_type)
		|| in_array('schools-fine-arts-athletics', $fund_type)
		|| in_array('non-profit-organizations', $fund_type)):

?>

		<?php
				$field_error = array_pop($field_map['enable_501c']['errors']);
				$field_locked = $field_map['enable_501c']['locked'] == true ? 'disabled' : '';
		?>
		<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
			<label>Non-Profit Status Verification (501c3)</label>
			<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
			<p>Is the Payee above a non-profit entity (501c3)?</p>
			<div>
				<label class="radio-inline">
					<input type="radio" name="enable_501c" value="yes"
					<?php echo $field_locked; ?>
					<?php echo ($enable_501c == 'yes' || $enable_501c == '1' ?  ' checked ' : ''); ?>
						/>Yes
				</label>
				<label class="radio-inline">
					<input type="radio" name="enable_501c" value="no"
					<?php echo $field_locked; ?>
					<?php echo ($enable_501c == 'no' || $enable_501c == '0' ?  ' checked ' : ''); ?>
					/>No
				</label>
				<span class="lock-control dashicons dashicons-lock <?php echo $field_locked; ?>" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
			</div>

			<?php $field_error = array_pop($field_map['document_501c']['errors']); ?>
			<div class="for-profit-registered" style="display: <?php echo ($enable_501c == 'yes' ? 'block': 'none'); ?>">
				Please upload a copy of the Payee's IRS determination letter verifying its tax exempt status under section 501c3.
				<div class="file-wrapper">
					<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
					<input type="file" name="document_501c" accept=".doc,.docx,.txt,.rtf,.pdf,.xls,.xlsx,.ppt,.pptx,.epub,.od,.odx" />
					<?php if (! empty($document_501c_url) ): ?>
					<div class="current-file">
						<strong>Current File:</strong>
						<a href="<?php echo $document_501c_url; ?>"><?php echo $document_501c_name; ?></a>
					</div>
					<?php else: ?>
					<input type="hidden" name="document_501c" value="" />
					<?php endif; ?>
				</div>
			</div>
		</div>

		<?php endif; ?>

	</div>
</div>

<div class="button-footer">
	<div class="button-left">
		<button type="submit" name="save_later" class="btn btn-lg btn-default <?php echo $completion_status == 'approved' ? 'hidden' : '' ?>">
			Save For Later
		</button>
	</div>
	<div class="button-center">
		<p class="submit-status">
			<span class="submit-text">Submitting...</span>
			<span class="progress-percent"></span></p>
		<div class="progressbar"></div>
	</div>
	<div class="button-right">
		<button type="submit" name="save_continue" class="btn btn-lg btn-success">
			<?php echo ($completion_status == 'approved' ? 'Update' : 'Save &amp; Continue'); ?>
		</button>
	</div>
</div>


<script>
jQuery(document).ready(function($) {
	function copyLeader(el) {
		el.value = this.val();
	}

	var handlers = [];
	$('.same-as-leader input').on('click', function() {
		if (this.checked) {
			$('input').each(function(i, el) {
				if (/payee/.test(el.name)) {
					var source = $('input[name="' + el.name.replace('payee', 'leader') + '"]');
					if (source && source.length > 0) {
						el.disabled = true;
						if (source.val()) {
							el.value = source.val();
						}
						handlers.push(source.on('keyup', copyLeader.bind(source, el)));
					}
				}
			});
		} else {
			handlers.forEach(function(handler) {
				handler.off();
			});
			handlers = [];
			$('input').each(function(i, el) {
				if (/payee/.test(el.name)) {
					el.disabled = false;
				}
			});
		}
	});

	$('input[name="enable_501c"]').on('click', function() {
		if (this.value === 'yes') {
			$('.for-profit-registered').show();
		} else {
			$('.for-profit-registered').hide();
		}
	});

	$('input[type="file"]').on('change', function() {
		var self = this;
		$('.current-file a').addClass('deleted');
	});


});
</script>
