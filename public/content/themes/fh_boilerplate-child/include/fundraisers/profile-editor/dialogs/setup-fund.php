<form id="setupfund-form" class="dialog white-popup-block mfp-hide" style="max-width:500px;">
	<h4 class="text-center subheader">Start a Fund</h4>
	<h2 class="text-center">Great, We've Got the Basics</h2>
	<p class="text-center">
		Thank you for registering your Fund.
	</p>
	<div class="middle-notice">
		<span class="email-icon dashicons dashicons-email-alt"></span>
		<p><strong>Important!</strong> We just sent you a Welcome Email. Please check your inbox now and make sure you received the email.</p>
	</div>
	<p class="text-center">Now, let's get all of the details worked out and tell your story.</p>

	<p class="text-center">
		Got any questions at this point?<br />
		<a href="/help-center/" target="_blank">Help Center</a> | <a href="/about-us/terms-conditions/" target="_blank">Terms of Service</a>
	</p>
	<button class="btn btn-lg btn-block btn-primary keep-going">Keep Going</button>
</form>

<script type="text/javascript">
	// just shows informational dialog, just close dialog when clicked
$(document).ready(function() {
	$('#setupfund-form .keep-going').on('click', function(e) {
		e.preventDefault();
		$('#setupfund-form').magnificPopup('close');
	});
});
</script>

<style type="text/css">
	#setupfund-form p {
		margin: 1.5em 0;
	}
	#setupfund-form .middle-notice {
		margin-left: auto;
		margin-right: auto;
		border-top: solid 2px #28809b;
		border-bottom: solid 2px #28809b;
		color: #28809b;
		padding-left: 40px;
		padding-right: 40px;
	}
	#setupfund-form .middle-notice p {
		margin: 10px 0px;
	}
	#setupfund-form .email-icon {
		margin-right: 15px;
		margin-top: 10px;
		font-size: 60px;
		width: 60px;
		height: 60px;
		color: #28809b;
		float: left;

	}

</style>

