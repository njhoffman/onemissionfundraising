<?php global $states_lookup; ?>
<form id="startfund-form" class="dialog white-popup-block mfp-hide" style="max-width:450px;">
	<h4 class="text-center subheading">Start A Fund</h4>
	<h2 class="text-center">About Your Fund</h2>
	<div class="form-group">
		<div class="help-text title">
			A big part of fundraising is “story telling”. The title of your fund is like the title of your story. Be descriptive yet keep it as short as possible.
		</div>
		<label for="startfund-name">Fund Title</label>
		<span class="required">*</span>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/info-icon.svg" class="help-icon title" />
		<input type="text" class="form-control name" id="startfund-name" name="name" placeholder="Fund Title">
	</div>
	<div class="form-group">
		<label>Fund Type</label>&nbsp;<i>(Select No More than Two)</i>
		<span class="required">*</span>
		<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
		<div class="fundraiser-type">
			<label>
				<input type="checkbox" name="fundraiser_type" value="churches-youth-groups" />
				Churches &amp; Youth Groups
			</label><br />
			<label>
				<input type="checkbox" name="fundraiser_type" value="schools-fine-arts-athletics" />
				Schools, Fine Arts, &amp; Athletics
			</label><br />
			<label>
				<input type="checkbox" name="fundraiser_type" value="non-profit-organizations" />
				Non-Profit Organizations
			</label><br />
			<label>
				<input type="checkbox" name="fundraiser_type" value="mission-groups-missionaries" />
				Mission Groups &amp; Missionaries
			</label><br />
			<label>
				<input type="checkbox" name="fundraiser_type" value="medical-causes" />
				Medical Causes
			</label><br />
			<label>
				<input type="checkbox" name="fundraiser_type" value="adoptive-parents" />
				Adoptive Parents
			</label><br />
			<label>
				<input type="checkbox" name="fundraiser_type" value="community-benefits" />
				Community Benefits
			</label><br />
			<label>
				<input type="checkbox" name="fundraiser_type" value="other-groups-individuals" />
				Other Groups &amp; Individuals
			</label>
		</div>
	</div>
	<div class="form-group">
		<div class="help-text location">
			Let your supporters know where in the world your fund is making a positive impact.
			This will be displayed on your fund’s profile page.
			(This may or may not be your physical location)
			For example, if you’re going on a Mission trip to Haiti, you are making an impact in Haiti &mdash;
			note that the fund payee must be located in the US (we’ll collect that information later).
		</div>
		<label>Where In The World Are You Making an Impact?</label>
		<span class="required">*</span>
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/info-icon.svg" class="help-icon location" />
		<div class="country-options">
			<label><input type="radio" checked value="United States" name="country" />United States</label>
			<label><input type="radio" value="" name="country" />Other</label>
		</div>
		<div class="othercountry-fields">
			<input type="hidden" class="form-control country" name="country" placeholder="Country" /><span class="required">*</span>
		</div>
		<div class="unitedstates-fields">
			<input type="text" class="form-control city" id="startfund-city" name="city" placeholder="City">
			<select class="form-control state" id="startfund-state" name="state">
				<option value="">State</option>
				<?php
					foreach ($states_lookup as $abbrev => $state) {
						echo "<option value='". $state . "'>" . $abbrev . "</option>";
					}
				?>
			</select>
		</div>
	</div>

	<p class="center">
		<small>One Mission is a platform for World Changers. Even if your fund just changes one person's world, we want to work with you. No for-profit businesses or anything political please.</small>
	</p>
	<p class="status"></p>
	<button type="submit" class="btn btn-lg btn-block btn-success start-fund">Start My Fund</button>
</form>

