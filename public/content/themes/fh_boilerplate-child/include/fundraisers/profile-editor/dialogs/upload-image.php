<div id="uploadimage-dialog" class="dialog white-popup-block mfp-hide" style="width: 600px; max-width: 80%;">
	<input type="file" style="display: none" id="uploadimage_file" />
	<input type="hidden" id="uploadimage_target_id" />
	<input type="hidden" id="uploadimage_target_file" />
	<input type="hidden" id="uploadimage_aspect_ratio" />
	<h2 class="text-center">Add Image</h2>
	<h3 class="text-center status">Opening Select File Dialog...</h3>
	<div class="crop-ratio"></div>
	<div class="crop-status">
		<span class="width"></span>
		<span class="x">x</span>
		<span class="height"></span>
	</div>
	<div>
		<img class="loading" src="<?php echo get_stylesheet_directory_uri(); ?>/img/loading.gif" />
	</div>
	<div>
		<img style="max-width: 100%" id="uploadimage-image" src=""/>
	</div>
	<div class="buttons row">
		<div class="col-sm-6">
			<button class="btn btn-lg btn-block btn-default cancel">Cancel</button>
		</div>
		<div class="col-sm-6">
			<button class="btn btn-lg btn-block btn-success apply">Apply</button>
		</div>
	</div>
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/0.8.1/cropper.min.css" rel="stylesheet">

<script>

jQuery(document).ready(function($) {

	var cropper;

	$('button.apply').on('click', function(e) {
		e.preventDefault();
		var targetId = '#' + $('#uploadimage_target_id').val();
		$(targetId).show().attr('src', cropper.getCroppedCanvas().toDataURL());
		var targetFile = '#' + $('#uploadimage_target_file').val();
		$(targetFile).attr('targetid', targetId);
		debugger;
		cleanUp();
	});

	$('button.cancel').on('click', function(e) {
		e.preventDefault();
		cleanUp();
	});

	function cleanUp() {
		if (cropper) {
			cropper.destroy();
		}
		$('#uploadimage-image').attr('src', '');
		$('#uploadimage-dialog').magnificPopup('close');
		$('#uploadimage-dialog .status').text('Opening Select File Dialog...');
		$('#uploadimage-dialog .loading').show();
		$('#uploadimage_file').val('');
	}

	$('input[type="file"]').on('change', function() {
		var self = this;
		$('#uploadimage-dialog .status').text('Loading Selected Image...');
		window.setTimeout( function() {
			readURL(self, '#uploadimage-image');
		}, 500);
	});

	document.body.onfocus = function() {
		// happens if cancel is selected
		// cleanUp();
	};

	function readURL(input, target) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				$(target).attr('src', e.target.result);
				var aspectRatio = $("#uploadimage_aspect_ratio").val();
				$('.crop-ratio').text(aspectRatio + ':1');
				$('#uploadimage-dialog .status').text('Crop Image to Aspect Ratio');
				$('#uploadimage-dialog .loading').hide();
			};
			reader.readAsDataURL(input.files[0]);
		}
	}

	$('#uploadimage-image').on('load', function() {
		var self = this;
		var width = this.naturalWidth;
		var height = this.naturalHeight;
		var aspectRatio = parseFloat($("#uploadimage_aspect_ratio").val());
		if (isNaN(aspectRatio)) {
			aspectRatio = 1.0;
		}
		$('.crop-status .x').show();
		console.info('height', height, 'width', width, 'aspectRatio', aspectRatio);
		console.info($('#uploadimage_aspect_ratio').val());
		cropper = new Cropper(this, {
			aspectRatio: aspectRatio,
			crop: function(e) {
				$('.crop-status .width').html(parseInt(e.detail.width) + 'px')
					$('.crop-status .height').html(parseInt(e.detail.height) + 'px')
				// 	console.log(parseInt(e.detail.x));
				// console.log(parseInt(e.detail.y));
				// console.log(parseInt(e.detail.width));
				// console.log(parseInt(e.detail.height));
				// console.log(e.detail.rotate);
				// console.log(e.detail.scaleX);
				// console.log(e.detail.scaleY);
			}
		});
	});
});

</script>
