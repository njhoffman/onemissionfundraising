<?php
	$banner_url = "";
	if (!empty($field_map['banner_image']['value'])) {
		$banner_url = wp_get_attachment_url($field_map['banner_image']['value']);
	}
	$logo_url = "";
	if (!empty($field_map['logo_image']['value'])) {
		$logo_url = wp_get_attachment_url($field_map['logo_image']['value']);
	}
	$yt_video = isset($field_map['image_or_youtube_banner']) && isset($field_map['image_or_youtube_banner']['value'])
		&& $field_map['image_or_youtube_banner']['value'] === 'yt_video';
	$banner_image = isset($field_map['image_or_youtube_banner']) && isset($field_map['image_or_youtube_banner']['value'])
		&& $field_map['image_or_youtube_banner']['value'] === 'image';
	$yt_link = isset($field_map['banner_youtube_video_id']) && isset($field_map['banner_youtube_video_id']['value'])
	  	? $field_map['banner_youtube_video_id']['value'] : '';

	$social_url = "";
	if (!empty($field_map['yoast_wpseo_opengraph-image']['value'])) {
		$social_url = $field_map['yoast_wpseo_opengraph-image']['value'];
	} else if (!empty($field_map['_yoast_wpseo_opengraph-image']['value'])) {
		$social_url = $field_map['_yoast_wpseo_opengraph-image']['value'];
	}


?>


<div class="row">
	<div class="col-sm-8">
		<?php
			$field_error = array_pop($field_map['logo_image']['errors']);
		?>

		<h2 class="h3">Add Profile Image <span class="required">*</span></h2>
		<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
		<p>Your profile image is displayed on your Fund page as well as on the search results on the website.  It’s the first image supporters will see, so make sure it connects well with you and your fundraising story.  <em><strong>Ideal image dimensions are 280 pixels wide by 280 pixels tall.</strong></em></p>
		<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
			<button class="add-image" data-aspect_ratio="1" data-target_id="logo_image" data-target_file="logo_file">
				<?php
					if (empty($logo_url)) { echo "Add Image"; }
					else { echo "Change Image"; }
				?>
			</button>
			<br />
			<input type="file" style="display: none" id="logo_file" />
			<?php if (empty($logo_url)): ?>
			<input type="hidden" name="logo_image" value="" />
			<?php endif; ?>
			<div class="image-container">
				<img name="logo_image" id="logo_image" src="<?php echo $logo_url; ?>"
					style="<?php echo empty($logo_url) ? 'display: none' : 'display: block' ?>" />
			</div>
		</div>
		<br />

		<hr>
			<?php
				$field_error = array_pop($field_map['image_or_youtube_banner']['errors']);
			?>

		<div>
			<h2 class="h3">Add Banner Image or Video</h2>
			<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
			<p>
				Adding a banner or video is optional.
				But it can add a lot of life to your story.
				Your Fund page will support a banner image or a video. <br><em><strong>Ideal banner image dimensions are 900 pixels wide by 300 pixels tall.</strong></em>
			</p>
			<div class="form-group banner-display">
				<input type="radio" name="image_or_youtube_banner" id="image_banner" value="image"
					<?php if ($banner_image) { echo 'checked'; } ?>
				/>
				<label for="image_banner">Image</label>
				<input type="radio" name="image_or_youtube_banner" id="youtube_banner" value="yt_video"
					<?php if ($yt_video == true) { echo 'checked'; }?>
				/>
				<label for="youtube_banner">YouTube Video</label>
			</div>
			<?php
				$field_error = array_pop($field_map['banner_image']['errors']);
			?>
			<?php
						$field_error = array_pop($field_map['banner_youtube_video_id']['errors']);
			?>
			<div class="form-group youtube-link <?php echo ($yt_video == true ? '' : 'hidden') ?> <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
				<label for="youtube">YouTube Link</label>
				<br />
				<span class="error-message"></span>
				<span class="success-message"></span>
				<input type="url" class="form-control" id="youtube" name="banner_youtube_video_id" placeholder="https://www.youtube.com/?=2343as3s"
				value="<?php echo $yt_link;?>"
				>
			</div>


			<div class="video-wrapper">
				<iframe id="ytplayer" style="<?php echo (! $yt_video ? 'display: none' : '' )?>" type="text/html" width="420" height="240"
					src="<?php echo ($yt_video ? $yt_link : ""); ?>"
					frameborder="0">
				</iframe>
			</div>


			<div class="form-group banner-image <?php echo ($yt_video ? 'hidden' : '') ?> <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
				<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
				<br />
					<button class="add-image" data-aspect_ratio="3" data-target_id="banner_image" data-target_file="banner_file">
					<?php
						if (empty($banner_url)) { echo "Add Image"; }
						else { echo "Change Image"; }
					?>
					</button>
				<br />
				<input type="file" style="display: none" id="banner_file" />
				<div class="image-container">
					<img name="banner_image" id="banner_image" class="img-responsive" src="<?php echo $banner_url; ?>"
						style="<?php echo empty($banner_url) ? 'display: none' : 'display: block' ?>" />
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-4 text-center">
		<p class="space-0">Example locations of images/videos on your fund page:</p>
		<img src="/content/themes/fh_boilerplate-child/img/admin_help/onemission-preview-thumb.png" />
	</div>
</div>

		<hr>
		<?php
			$field_error = array_pop($field_map['_yoast_wpseo_opengraph-image']['errors']);
		?>
		<h2 class="h3">Add Social Media Sharing Image </h2>
		<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
		<p>Successful Funds do a great job of sharing on social media.  It’s optional, but we recommend adding a social sharing image below. This image can be the same as an image you added above (although different dimensions), or something different.  Keep in mind, THIS is the image that will be seen when you and your supporters share your Fund on social media.</p>
		<p><em><strong>The ideal social media image is 1200 pixels wide by 630 pixels tall.</strong></em>  And, if you have the capability, consider adding text to your image that invites supporters to visit and support your Fund.</p>
		<p>Need help with images?  Please contact your Fund Ambassador.</p>
		<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
			<button class="add-image" data-aspect_ratio="1.9" data-target_id="_yoast_wpseo_opengraph-image" data-target_file="social_file">
				<?php
					if (empty($social_url)) { echo "Add Image"; }
					else { echo "Change Image"; }
				?>
			</button>
			<br />
			<input type="file" style="display: none" id="social_file" />
			<?php if (empty($social_url)): ?>
			<input type="hidden" name="_yoast_wpseo_opengraph-image" value="" />
			<?php endif; ?>
			<div class="image-container">
				<img name="_yoast_wpseo_opengraph-image" id="_yoast_wpseo_opengraph-image" src="<?php echo $social_url; ?>"
					style="<?php echo empty($social_url) ? 'display: none' : 'display: block' ?>" />
			</div>
		</div>

			<div class="space-top-5">
		<p class="space-0 image-warning">
				By uploading any image to the One Mission website you explicitly agree that you own all rights
				to this image or can prove full permission to reproduce it under free license, public domain, or fair usage agreements.
			</p>
	</div>

<div class="button-footer">
	<div class="button-left">
		<button type="submit" name="save_later" class="btn btn-lg btn-default <?php echo $completion_status == 'approved' ? 'hidden' : '' ?>">
			Save For Later
		</button>
	</div>
	<div class="button-center">
		<p class="submit-status">
			<span class="submit-text">Submitting...</span>
			<span class="progress-percent"></span></p>
		<div class="progressbar"></div>
	</div>
	<div class="button-right">
		<button type="submit" name="save_continue" class="btn btn-lg btn-success">
			<?php echo ($completion_status == 'approved' ? 'Update' : 'Save &amp; Continue'); ?>
		</button>
	</div>
</div>

	</div>


<?php require_once(__DIR__ . "/dialogs/upload-image.php"); ?>

<script>

jQuery(document).ready(function($) {

	console.info("WHATSUP DOG");
	$("#youtube").on('change', function() {
		var youtube_re = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;
		var video = $(this).val().match(youtube_re);
		if (video && video[1]) {
			$('.youtube-link .success-message').html('Captured ID: ' + video[1]);
			var newUrl = "https://www.youtube.com/embed/" + video[1];
			// $('#youtube').val(newUrl);
			$("#ytplayer").show().attr("src", newUrl);
		} else {
			$('.youtube-link .error-message').html('Could not parse ID from: ' + $(this).val());
			$('#ytplayer').hide();
			$(this).val('');
		}
	});

	$('input[name="image_or_youtube_banner"]').on('change', function() {
		if ($(this).val() === 'image') {
			$('.banner-image').addClass('selected').removeClass('hidden');
			$('.youtube-link').removeClass('selected').addClass('hidden');
		} else {
			$('.banner-image').removeClass('selected').addClass('hidden');
			$('.youtube-link').addClass('selected').removeClass('hidden');
			if ($('#youtube').val() && $('#youtube').val().length > 0) {
				$('#youtube').trigger('change');
			}
		}
	});

	$('button.add-image').on('click', function(e) {
		e.preventDefault();
		$('#uploadimage_file').trigger('click');
		var targetId = $(this).data('target_id');
		$('#uploadimage_target_id').val(targetId);
		var aspectRatio = $(this).data('aspect_ratio');
		$('#uploadimage_aspect_ratio').val(aspectRatio);
		var targetFile = $(this).data('target_file');
		$('#uploadimage_target_file').val(targetFile);
		console.info('add image', targetId, aspectRatio, targetFile);
	});

	$('input[type="file"]').on('change', function() {
		var self = this;
		console.info('file changed, opening popup');

		// load popup, dialog code in templates/dialogs/upload-image
		$.magnificPopup.open({
			items: {
				type: 'inline',
				src: '#uploadimage-dialog',
			},
			closeOnBgClick: false,
			preloader: false,
			callbacks: {
				beforeOpen: function() {
				}
			}
		});


		// $('#uploadimage-dialog .status').text('Loading Selected Image...');
		// window.setTimeout( function() {
		// 	readURL(self, '#uploadimage-image');
		// }, 500);
	});

});
</script>



