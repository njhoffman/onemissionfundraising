<?php

$field_map = Array(
	'fundraiser_title' => Array(
		'required' => true,
		'page' => 1,
		'locked' => true
	),
	'fundraiser_name' => Array(
		'required' => false,
		'page' => 1,
		'locked' => true
	),
	'fundraiser_city' => Array(
		'required' => false,
		'page' => 1
	),
	'fundraiser_state' => Array(
		'required' => false,
		'page' => 1
	),
	'fundraiser_country' => Array(
		'required' => true,
		'page' => 1
	),
	'fundraiser_type' => Array(
		'required' => true,
		'page' => 1,
		'locked' => true
	),
	'fundraiser_start_date' => Array(
		'required' => true,
		'page' => 2,
		'locked' => true
	),
	'fundraiser_end_date' => Array(
		'required' => true,
		'page' => 2,
		'locked' => true
	),
	'fundraiser_goal_amount' => Array(
		'required' => true,
		'page' => 2,
		'locked' => true
	),
	'fundraiser_description' => Array(
		'required' => true,
		'page' => 3
	),
	'logo_image' => Array(
		'required' => true,
		'page' => 4,
	),
	'banner_image' => Array(
		'required' => false,
		'page' => 4,
	),
	'image_or_youtube_banner' => Array(
		'required' =>  false,
		'page' => 4,
	),
	'banner_youtube_video_id' => Array(
		'required' => false,
		'page' => 4,
	),
	'yoast_wpseo_opengraph-image' => Array(
		'required' => false,
		'page' => 4,
	),
	'_yoast_wpseo_opengraph-image' => Array(
		'required' => false,
		'page' => 4,
	),
	'thank_you_message' => Array(
		'required' => false,
		'page' => 5
	),
	'payee_check_name' => Array(
		'required' => false,
		'page' => 6,
		'locked' => true
	),
	'payee_name' => Array(
		'required' => true,
		'page' => 6,
		'locked' => true
	),
	'payee_organization' => Array(
		'required' => false,
		'page' => 6,
		'locked' => true
	),
	'payee_address' => Array(
		'required' => true,
		'page' => 6,
		'locked' => true
	),
	'payee_city' => Array(
		'required' => true,
		'page' => 6,
		'locked' => true
	),
	'payee_state' => Array(
		'required' => true,
		'page' => 6,
		'locked' => true
	),
	'payee_zip' => Array(
		'required' => true,
		'page' => 6,
		'locked' => true
	),
	'payee_phone' => Array(
		'required' => true,
		'page' => 6,
		'locked' => true
	),
	'leader_organization' => Array(
		'required' => false,
		'page' => 6,
		'locked' => true
	),
	'leader_address' => Array(
		'required' => true,
		'page' => 6,
		'locked' => true
	),
	'leader_city' => Array(
		'required' => true,
		'page' => 6,
		'locked' => true
	),
	'leader_state' => Array(
		'required' => true,
		'page' => 6,
		'locked' => true
	),
	'leader_zip' => Array(
		'required' => true,
		'page' => 6,
		'locked' => true
	),
	'leader_phone' => Array(
		'required' => true,
		'page' => 6,
		'locked' => true
	),
	'enable_501c' => Array(
		'required' => false,
		'page' => 6,
		'locked' => true
	),
	'document_501c' => Array(
		'required' => false,
		'page' => 6
	),
	'fundraiser_website' =>  Array(
		'required' => false,
		'page' => 7
	),
	'fundraiser_facebook' =>  Array(
		'required' => false,
		'page' => 7
	),
	'fundraiser_twitter' =>  Array(
		'required' => false,
		'page' => 7
	),
	'fundraiser_instagram' => Array(
		'required' => false,
		'page' => 7
	),
	// 'support_email_notifications' => Array(
	// 	'required' => false,
	// 	'page' => 8
	// ),
	'mvp' => Array(
		'required' => true,
		'page' => 8
	),
	'custom_apparel' => Array(
		'required' => true,
		'page' => 8
	),
	'bulk_free_shipping' => Array(
		'required' => true,
		'page' => 8
	)
);
?>
