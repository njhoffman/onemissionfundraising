<div class="row social-websites">
	<div class="col-sm-7">
		<h2 class="h3">Your Social Links</h2>
		<p>Supporters will want to follow your story.  Provide links to your blog or website, Facebook, Twitter, and/or Instagram where you’ll be promoting your Fund.  Links to these site will be displayed on your Fund page.</p>
		<p><em><small>Be sure to include the full website address including “http://” or “https://”.</small></em></p>
		<?php
				$field_error = array_pop($field_map['fundraiser_website']['errors']);
		?>
		<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
			<label for="website_address">Blog or Website</label>
			<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
			<input type="text" class="form-control" id="website_address" name="fundraiser_website"
				value="<?php echo (isset($field_map['fundraiser_website']['value'])
					? $field_map['fundraiser_website']['value'] : ''); ?>"
			/>
			<p><em><small>E.g. https://www.onemission.fund</small></em></p>
		</div>
		<?php
				$field_error = array_pop($field_map['fundraiser_facebook']['errors']);
		?>
		<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
			<label for="facebook_address">Facebook Address</label>
			<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
			<input type="text" class="form-control" id="facebook_address" name="fundraiser_facebook"
				value="<?php echo (isset($field_map['fundraiser_facebook']['value'])
					? $field_map['fundraiser_facebook']['value'] : ''); ?>"
			/>
			<p><em><small>E.g. https://www.facebook.com/onemissionfundraising</small></em></p>
		</div>
		<?php
				$field_error = array_pop($field_map['fundraiser_twitter']['errors']);
		?>
		<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
			<label for="twitter_address">Twitter Address</label>
			<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
			<input type="text" class="form-control" id="twitter_address" name="fundraiser_twitter"
				value="<?php echo (isset($field_map['fundraiser_twitter']['value'])
					? $field_map['fundraiser_twitter']['value'] : ''); ?>"
			/>
			<p><em><small>E.g. https://twitter.com/OneMission_Love</small></em></p>
		</div>
		<?php
				$field_error = array_pop($field_map['fundraiser_instagram']['errors']);
		?>
		<div class="form-group <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
			<label for="instagram ">Instagram Address</label>
			<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
			<input type="text" class="form-control" id="instagram_address" name="fundraiser_instagram"
				value="<?php echo (isset($field_map['fundraiser_instagram']['value'])
					? $field_map['fundraiser_instagram']['value'] : ''); ?>"
			/>
			<p><em><small>E.g. https://instagram.com/onemission_love</small></em></p>
		</div>

	</div>
	<div class="col-sm-5">
		<div class="inset inset-help">
			<h4>TIPS</h4>
			<hr>
			<p>
				Links to these sites will be displayed on your Fund page.
			</p>
		</div>
	</div>
</div>

<div class="button-footer">
	<div class="button-left">
		<button type="submit" name="save_later" class="btn btn-lg btn-default <?php echo $completion_status == 'approved' ? 'hidden' : '' ?>">
			Save For Later
		</button>
	</div>
	<div class="button-center">
		<p class="submit-status">
			<span class="submit-text">Submitting...</span>
			<span class="progress-percent"></span></p>
		<div class="progressbar"></div>
	</div>
	<div class="button-right">
		<button type="submit" name="save_continue" class="btn btn-lg btn-success">
			<?php echo ($completion_status == 'approved' ? 'Update' : 'Save &amp; Continue'); ?>
		</button>
	</div>
</div>


<script type="text/javascript">
jQuery(document).ready(function($) {

	// don't need this currently, suggest buttons would prefill text boxes

<?php
	$fundraiser_title = "";
	echo 'var fundTitle = "' . preg_replace("/[^A-Za-z0-9\- ]/", "", (strtolower(str_replace(' ', '-', $fundraiser_title)))) . '";' . "\n";
?>
	$('.suggest').on('click', function(e) {
		e.preventDefault();
		switch (this.name.replace('suggest_', '')) {
			case "website":
				$('input[name="fundraiser_website"]').val('http://' + fundTitle + '.com');
				break;
			case "facebook":
				$('input[name="fundraiser_facebook"]').val('http://www.facebook.com/' + fundTitle);
				break;
			case"twitter":
				$('input[name="fundraiser_twitter"]').val('http://www.twitter.com/' + fundTitle);
				break;
			case "instagram":
				$('input[name="fundraiser_instagram]').val('http://www.instagram.com/' + fundTitle);
				break;
		}
	});
});

</script>

