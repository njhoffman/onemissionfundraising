<?php
	// $start_date = !empty($field_map['fundraiser_start_date']['value'])
	// 	? date('Y-m-d', strtotime($field_map['fundraiser_start_date']['value'])) : '';
	// $end_date = !empty($field_map['fundraiser_end_date']['value'])
	// 	? date('Y-m-d', strtotime($field_map['fundraiser_end_date']['value'])) : '';
	$start_date = !empty($field_map['fundraiser_start_date']['value'])
		? $field_map['fundraiser_start_date']['value'] : '';
	$end_date = !empty($field_map['fundraiser_end_date']['value'])
		? $field_map['fundraiser_end_date']['value'] : '';
	$goal_amount = !empty($field_map['fundraiser_goal_amount']['value'])
	   ? trim($field_map['fundraiser_goal_amount']['value']) : '';
?>


<div class="row">
	<div class="col-sm-6 col-md-7">
		<h2 class="h3">Set a Goal</h2>
		<p>Your fundraising goal is a vital part of your story. Make sure your goal is <strong>MEANINGFUL, REASONABLE and ACHIEVABLE</strong> within the timeframe that you specify below.   Set a goal that you are committed to and confident that you can reach.</p>
<?php
		$field_error = array_pop($field_map['fundraiser_goal_amount']['errors']);
		$field_locked = $field_map['fundraiser_goal_amount']['locked'] == true ? 'disabled' : '';
?>
		<div class="form-group space-3 <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
			<label class="h4" for="amount">What is your fundraising goal?<span class="required">*</span></label>
			<div class="amount-wrap">
				<span class="money-sign">$</span>
				<input type="number" step="1"  name="fundraiser_goal_amount"
					class="form-control amount" <?php echo $field_locked; ?>
					<?php
						echo !empty($goal_amount) ? 'value="' . $goal_amount . '"' : '';
					?>
				/>
				<span class="lock-control dashicons dashicons-lock <?php echo $field_locked; ?>" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
			</div>
			<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
		</div>
		<br>
		<h2 class="h3">Fund Length</h2>
		<p>Set a timeline that connects with your story and most importantly creates urgency.  Create urgency by making your timeline as short as possible.  We recommend keeping a fund open for a maximum of 90 days.</p>
<?php
		$field_error = array_pop($field_map['fundraiser_start_date']['errors']);
		$field_locked = $field_map['fundraiser_start_date']['locked'] == true ? 'disabled' : '';
?>
		<div class="form-group space-1 start_date <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
			<label class="h4" for="start_date">Start Date<span class="required">*</span></label>
			<input type="text" id="start_date" name="fundraiser_start_date"
				class="form-control datepicker" <?php echo $field_locked; ?>
				<?php
					echo !empty($start_date) ? 'value="' . $start_date . '"' : '';
				?>
			/>
			<span class="lock-control dashicons dashicons-lock input-lock <?php echo $field_locked; ?>" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
			<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
		</div>
<?php
		$field_error = array_pop($field_map['fundraiser_end_date']['errors']);
		$field_locked = $field_map['fundraiser_end_date']['locked'] == true ? 'disabled' : '';
?>
		<div class="form-group space-3 end_date <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
			<label class="h4" for="end_date">End Date<span class="required">*</span></label>
			<input type="text" id="end_date" name="fundraiser_end_date"
				class="form-control datepicker" <?php echo $field_locked; ?>
				<?php
					echo !empty($end_date) ? 'value="' . $end_date . '"' : '';
				?>
			/>
			<span class="lock-control dashicons dashicons-lock input-lock <?php echo $field_locked; ?>" data-toggle="tooltip" title="This field is locked. Please contact your Fund Ambassador if you'd like to make a change."></span>
			<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
		</div>
		<div class="form-group">
			<i>
			<?php
					if (!empty($start_date) && empty($end_date)) {
						echo "Fundraising will continue indefinitely starting on: " . $start_date;
					} elseif(!empty($start_date)) {
						$start_date_obj = date_create($start_date);
						$end_date_obj = date_create($end_date);
						$interval =  date_diff($start_date_obj, $end_date_obj);
						//echo "<br />Fundraising will run for " . $interval->format("%a Days") . " from " . $start_date . " to " . $end_date;
						echo "<br /><span class='text-success'>Ok, your Fund will be open for <strong>" . $interval->format("%a days") . "</strong>. Let's do this!</span>";
					}
			?>
			</i>
		</div>
	</div>
	<div class="col-sm-6 col-md-5">
		<div class="inset inset-help">
			<h4>TIPS</h4>
			<hr>
			<p>If you need to raise a large amount of money, consider breaking it up into smaller chunks and re-starting your fund once you’ve hit your goal (success leads to more success).</p>
			<hr>
			<p>Remember, once your successful fund ends you can always relaunch your campaign and increase your goal.</p>
			<br>
			<h4>HELP CENTER ARTICLES</h4>
			<hr>
			<p>- <a href="https://onemission.fund/help-center/setting-your-goal/" target="_blank">Setting Your Goal</a>
			<br>- <a href="https://onemission.fund/help-center/setting-your-timeline/" target="_blank">Setting Your Timeline</a></p>
		</div>
	</div>
</div>

<div class="button-footer">
	<div class="button-left">
		<button type="submit" name="save_later" class="btn btn-lg btn-default <?php echo $completion_status == 'approved' ? 'hidden' : '' ?>">
			Save For Later
		</button>
	</div>
	<div class="button-center">
		<p class="submit-status">
			<span class="submit-text">Submitting...</span>
			<span class="progress-percent"></span></p>
		<div class="progressbar"></div>
	</div>
	<div class="button-right">
		<button type="submit" name="save_continue" class="btn btn-lg btn-success">
			<?php echo ($completion_status == 'approved' ? 'Update' : 'Save &amp; Continue'); ?>
		</button>
	</div>
</div>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" />

<script>
jQuery(document).ready(function($) {
	// $('.datepicker').each(function(idx, el) {
	// 	var ret = $(el).datepicker();
   //
	// });
});
</script>



