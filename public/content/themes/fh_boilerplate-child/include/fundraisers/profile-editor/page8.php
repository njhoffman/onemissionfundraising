<?php

$bulk_free_shipping = isset($field_map['bulk_free_shipping']) && isset($field_map['bulk_free_shipping']['value'])
	? $field_map['bulk_free_shipping']['value'] : '';
$custom_apparel = isset($field_map['custom_apparel']) && isset($field_map['custom_apparel']['value'])
	? $field_map['custom_apparel']['value'] : '';
$mvp = isset($field_map['mvp']) && isset($field_map['mvp']['value'])
	? $field_map['mvp']['value'] : '';
?>

<div class="row">
	<div class="col-sm-12">
		<h2 class="h3">Game Changers</h2>
		<p>Game Changers are things that you can do within your Fund to make it even more successful. <em>All of these things are optional, but they can make a huge difference.</em></p>
		<hr>
		<br>
		<?php $field_error = array_pop($field_map['custom_apparel']['errors']); ?>
		<img class="gamechanger-img" src="/content/themes/fh_boilerplate-child/img/Custom-Products.jpg" />
		<p class="lead space-1"><strong>Custom Apparel, Mugs, Art, or Jewelry</strong></p>
		<p class="space-1">Would you like to add custom apparel, custom coffee mugs, art prints, cards, or jewelry?  If interested, select “yes” below and your fund ambassador will help you get the ball rolling.</p>
		<br>
		<div class="form-group space-3 game-changer-radios <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
			<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
			<label class="radio-inline">
				<input type="radio" name="custom_apparel" value="yes"
					<?php echo ($custom_apparel == 'yes' ?  ' checked ' : ''); ?>
						/>Yes
			</label>
			<label class="radio-inline">
				<input type="radio" name="custom_apparel" value="no"
					<?php echo ($custom_apparel == 'no' ?  ' checked ' : ''); ?>
						/>No
			</label>
		</div>
		<br>
		<hr style="clear:both;">
		<br>
		<?php $field_error = array_pop($field_map['mvp']['errors']); ?>
		<img class="gamechanger-img" src="/content/themes/fh_boilerplate-child/img/MVPs.jpg" />
		<p class="lead space-1"><strong>MVPs (Most Valuable Promoters)</strong></p>
		<p class="space-1">
			It’s very hard to be successful alone.  You need friends, family members, etc to help you along the way.  We have special deals available for your MVPs.  <a href="https://onemission.fund/help-center/most-valuable-promoters/" target="_blank">Learn More &raquo;</a><strong>
		</p>
		<br>
		<div class="form-group space-3 game-changer-radios <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
			<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
			<label class="radio-inline">
				<input type="radio" name="mvp" value="yes"
					<?php echo ($mvp == 'yes' ?  ' checked ' : ''); ?>
						/>Yes
			</label>
			<label class="radio-inline">
				<input type="radio" name="mvp" value="no"
					<?php echo ($mvp == 'no' ?  ' checked ' : ''); ?>
						/>No
			</label>
		</div>
	<br>
		<hr style="clear:both;">
		<br>
		<?php $field_error = array_pop($field_map['bulk_free_shipping']['errors']); ?>
		<img class="gamechanger-img" src="/content/themes/fh_boilerplate-child/img/Bulk-Free-Shipping.jpg" />
		<p class="lead space-1"><strong>Bulk Free Shipping/Local Pickup Codes</strong></p>
		<p class="space-1">
			Do you have local supporters and/or do your supporters congregate together frequently?  Is there a big event coming up to support your cause?  If so, offering a bulk free shipping code can be a great motivator.  <a href="https://onemission.fund/help-center/bulk-free-shipping/" target="_blank">Learn More &raquo;</a>?
		</p>
		<br>
		<div class="form-group space-3 game-changer-radios <?php echo (!empty($field_error) ?  'field-error' : ''); ?>">
			<?php echo (!empty($field_error) ? '<span class="error-message">' . $field_error . '</span>' : ''); ?>
			<label class="radio-inline">
				<input type="radio" name="bulk_free_shipping" value="yes"
					<?php echo ($bulk_free_shipping == 'yes' ?  ' checked ' : ''); ?>
						/>Yes
			</label>
			<label class="radio-inline">
				<input type="radio" name="bulk_free_shipping" value="no"
					<?php echo ($bulk_free_shipping == 'no' ?  ' checked ' : ''); ?>
						/>No
			</label>
		</div>
	</div>
</div>

<div class="button-footer">
	<div class="button-left">
		<button type="submit" name="save_later" class="btn btn-lg btn-default <?php echo $completion_status == 'approved' ? 'hidden' : '' ?>">
			Save For Later
		</button>
	</div>
	<div class="button-center">
		<p class="submit-status">
			<span class="submit-text">Submitting...</span>
			<span class="progress-percent"></span></p>
		<div class="progressbar"></div>
	</div>
	<div class="button-right">
		<button type="submit" name="save_continue" class="btn btn-lg btn-success">
			<?php echo ($completion_status == 'approved' ? 'Update' : 'Save &amp; Continue'); ?>
		</button>
	</div>
</div>

