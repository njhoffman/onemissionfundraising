<?php
//
//  Set custom user fields
//
function _fh_custom_user_fields( $contactmethods ) {
    
    unset( $contactmethods['aim'] );
    unset( $contactmethods['yim'] );
    unset( $contactmethods['jabber'] );

    if( is_super_admin() ) {

        global $social_icons;
    
        // Iterate over $social_icons configuration array and set contact methods
        foreach( $social_icons as $icon_class => $icon_data ) {
    
            if( $icon_class != 'rss' ) {
    
                // Dash throws off WordPress contact method - underscore seems fine.            
                $icon_class = preg_replace( '/-/', '_', $icon_class );
    
                $contactmethods[ $icon_class ] = $icon_data['network'] . ' URL';
            }
        }

    } else {

        unset( $contactmethods['url'] );
        unset( $contactmethods['googleplus'] );
        unset( $contactmethods['facebook'] );
        unset( $contactmethods['twitter'] );
    }

    return $contactmethods;
}
?>