<?php

//
//  Does the cart have a subscription in it?
//
function _om_cart_has_subscription() {
	global $woocommerce;
	$cart = $woocommerce->cart->get_cart();
	if( ! empty( $cart ) ) {
		foreach( $cart as $cart_item_key => $item ) {
			if( $item['data'] instanceof WC_Product_Subscription ) {
				return true;
			}
		}
	}
	return false;
}

//
//  Does the passed items array have a subscription in it?
//
function _om_items_has_subscription( $items ) {
	if( ! empty( $items ) ) {
		foreach( $items as $item_key => $item ) {
			if( isset( $item['product_id'] ) && $item['product_id'] ) {
				$product_id = $item['product_id'];
				if( WC_Subscriptions_Product::is_subscription( $product_id ) ) {
					return true;
				}
			}
		}
	}
	return false;
}

//
//  Return the donation percentage for a group of items
//
function _om_get_percentage_from_items( $items ) {
	if( ! empty( $items ) ) {
		foreach( $items as $item_key => $item ) {
			if( isset( $item['product_id'] ) && $item['product_id'] ) {
				// Get percentage (stored as integer)
				$donation_percentage = get_post_meta( $item['product_id'], 'donation_percentage', true );
				// Update the parent always with the donation percentage
				if( $donation_percentage ) {
					return $donation_percentage;
				}
			}
		}
	}
	return false;
}

function _om_woo_get_subscriptions_cart_total( ) {
	global $woocommerce;
	$cart_contents_total = 0;

	// Force WooCommerce to calculate totals earlier
	$woocommerce->cart->calculate_totals();

	foreach ($woocommerce->cart->recurring_carts as $rc_key => $rc) {
		$cart_contents_total += $rc->cart_contents_total;
	}

	// Check for negative (fully discounted cart)
	if( $cart_contents_total < 0 ) {
		$cart_contents_total = 0;
	}

	return floatval( $cart_contents_total );
}

//
//  Get the subscription donation price total
//
function _om_get_subscriptions_donation_total() {

	global $woocommerce;

	$cart               = $woocommerce->cart->get_cart();
	$with_taxes         = OM_DONATION_CALC_AFTER_TAXES;
	$donatable_subtotal = _om_woo_get_subscriptions_cart_total( $with_taxes, false );

	return $donatable_subtotal;
	// if( $donatable_subtotal && ! empty( $cart ) ) {
	//
	//     foreach( $cart as $cart_item_key => $item ) {
	//
	//         if( $item['data'] instanceof WC_Product_Subscription ) {
	//
	//             $subscription = $item['data'];
	//
	//             if( isset( $subscription->product_custom_fields['donation_percentage'] ) ) {
	//
	//                 $donation_percentage = (int) $subscription->product_custom_fields['donation_percentage'][0];
	//                 return (float) number_format( ( $donation_percentage / 100 ) * $donatable_subtotal, 2 );
	//             }
	//         }
	//     }
	// }
	//
	// return 0;
}


//
//  Get the subscription donation price message (for use in checkout)
//
function _om_get_subscription_donation_message() {
	global $woocommerce;
	$cart = $woocommerce->cart->get_cart();
	if( ! empty( $cart ) ) {
		foreach( $cart as $cart_item_key => $item ) {
			if( $item['data'] instanceof WC_Product_Subscription ) {
				$subscription        = $item['data'];
				$price               = (int) $subscription->subscription_price;
				$donation_percentage = (int) $subscription->product_custom_fields['donation_percentage'][0];

				// Synchronized payment
				if( (int) $subscription->subscription_payment_sync_date ) {
					return '$' . number_format( ( $donation_percentage / 100 ) * $price, 2 ) . ' will be donated every billing period!';
				} else {
					return '$' . number_format( ( $donation_percentage / 100 ) * $price, 2 ) . ' will be donated every billing period starting now!';
				}
			}
		}
	}
	return '';
}

function _om_subscription_created_fix($sub, $order, $recurring_cart) {
	_om_log("Subscription created: $sub->ID - parent: " .  $sub->get_parent_id());
	// it is a parent order so we need to set Total Donation Amount to $105 (needs to be initially set to child order inherits value, *sigh*
	if ($sub->get_parent_id()) {
		update_post_meta( $sub->get_parent_id(), 'Total Donation Amount', '0' );
	}
}
add_action('woocommerce_checkout_subscription_created', '_om_subscription_created_fix', 10, 3);
