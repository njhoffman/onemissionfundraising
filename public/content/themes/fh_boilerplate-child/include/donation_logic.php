<?php

//
//	Utility function to get a non-formatted cart total price
//	- Based on "get_cart_total()" from WooCommerce core (http://goo.gl/cpT565)
//
function _om_woo_get_cart_total_float( $with_taxes = false, $with_discounts_applied = false, $with_shipping = false ) {
	global $woocommerce;
	$cart_contents_total = 0;

	// Force WooCommerce to calculate totals earlier
	$woocommerce->cart->calculate_totals();

	// If shipping included
	if( $with_shipping ) {
		$cart_contents_total += $woocommerce->cart->shipping_total;
	}
	// If prices include tax OR not taxes included
	if ( $woocommerce->cart->prices_include_tax || ! $with_taxes ) {
		$cart_contents_total += $woocommerce->cart->cart_contents_total;
	} else {
		$cart_contents_total += $woocommerce->cart->cart_contents_total + $woocommerce->cart->tax_total;
	}
	// Apply discount if applicable
	if( $with_discounts_applied ) {

		$discount = 0;
		if( empty($woocommerce->cart->discount_cart )) {
			if( ! empty( $woocommerce->cart->coupons ) ) {
				foreach( $woocommerce->cart->coupons as $coupon ) {
					if($coupon->discount_type == "percent"){
						$discount += ($coupon->coupon_amount/100) * $cart_contents_total;
					}else{
						$discount += $coupon->coupon_amount;
					}
				}
			}
		}

		//if( isset( $woocommerce->cart->discount_total ) && $woocommerce->cart->discount_total ) {
		// $discount += $woocommerce->cart->discount_total;
		//}
		if(empty($woocommerce->cart->discount_cart) && $discount > 0){
			$cart_contents_total = $cart_contents_total - $discount;
		}

	}
	// Check for negative (fully discounted cart)
	if( $cart_contents_total < 0 ) {

		$cart_contents_total = 0;
	}

    return floatval( $cart_contents_total );
}

//
//  Get total donation amount
//
function _om_get_total_donation() {

    // Add in check for subscription products
    // - Note: Subscriptions are handled separate from normal products
    if( _om_cart_has_subscription() ) {
		return _om_get_subscriptions_donation_total();
    }

    $custom_donation = _fh_donation_exsits();
    $total_donation  = 0;

    if( $custom_donation ) {
        // Increment total donation by custom donation
        $total_donation += $custom_donation;
    }

    // Set $total_donation to percentage of $donatable_subtotal
    $total_donation += OM_CART_DONATION_PERCENTAGE * _om_get_donatable_subtotal();

    return $total_donation;
}

//
//  Get total donatable subtotal (what gets multiplied by OM_CART_DONATION_PERCENTAGE)
//
function _om_get_donatable_subtotal() {

    $custom_donation    = _fh_donation_exsits();
    $with_taxes         = OM_DONATION_CALC_AFTER_TAXES;
    $donatable_subtotal = _om_woo_get_cart_total_float( $with_taxes, true );
    $donatable_subtotal = ( $donatable_subtotal < 0 ) ? 0 : $donatable_subtotal;
    if( $custom_donation ) {

        // Remove custom donation from donatable subtotal
        $donatable_subtotal = $donatable_subtotal - $custom_donation;
    }

    return $donatable_subtotal;
}

//
//  Add in coupon total to coupon labels
//
function _om_coupon_filter( $coupon ) {

    global $woocommerce;

    if ( is_string( $coupon ) ) {

        $coupon = new WC_Coupon( $coupon );
    }

    $code = str_replace( 'coupon: ', '', $coupon->code );
    $discount = number_format( $woocommerce->cart->discount_total, 2 );

    $out  = 'Discount: ' . esc_html( $code ) . '<br>';
    $out .= '<em style="font-weight: normal;">$' . $discount . '</em>';

    return $out;
}

add_filter( 'woocommerce_cart_totals_coupon_label', '_om_coupon_filter' );

//
//  Append cart totals table
//
function _om_append_cart_totals_table() {
?>
    <div class="donation_totals">
<?php
    // End totals table and put space before donations table
    $donation_subtotal = _om_get_donatable_subtotal();
    if( $donation_subtotal ):
?>
      <h2>Donation Totals</h2>
      <table cellspacing="0">
<?php
      if( OM_SHOW_DONATABLE_SUBTOTAL_ON_CHECKOUT ):
?>
      <tr>
        <th>Donate-able Subtotal<br />
          <em style="font-weight: normal;">
            After custom donation and coupons; before tax &amp; shipping
          </em>
        </th>
        <td>
          $<?php echo number_format( _om_get_donatable_subtotal(), 2 ) ?>
        </td>
      </tr>
<?php
      endif;
      if( OM_SHOW_DONATION_PERCENT_ON_CHECKOUT ):
?>
        <tr>
          <th>Product Donation Percent</th>
          <td><?php echo OM_CART_DONATION_PERCENTAGE * 100 ?>%</td>
        </tr>
<?php
      endif;
?>
      <tr>
        <th>Total Donation Amount</th>
        <td class="donation-amount">
          $<?php echo number_format( _om_get_total_donation(), 2 )?>
        </td>
      </tr>
    </table>
<?php
    endif;
?>
    <div class="wc-proceed-to-checkout">
      <?php do_action( 'woocommerce_proceed_to_checkout' ); ?>
    </div>
  </div>
<?php
}

add_action( 'woocommerce_cart_collaterals', '_om_append_cart_totals_table' );

//
//  Prepend Order Total on checkout page
//
function _om_prepend_total_checkout() {

    if( _om_get_donatable_subtotal() ) {

        echo '<tr>';
        echo '<th>Donation Amount</th>';
        echo '<td class="text-right">$' . number_format( _om_get_total_donation(), 2 ) . '</td>';
        echo '</tr>';
    }
}
add_action( 'woocommerce_review_order_before_order_total', '_om_prepend_total_checkout' );

//
//  Display message for overall item donation as percentage
//  - called from woocommerce/single-product/price.php in child theme
//
function _om_product_donation_price_append() {

    // Override custom field
    $override = get_field( 'donation_blurb' );

    if( $override && ! empty( $override ) ) {

        ?>
        <div id="product_donation_amount">
            <p><?php echo stripcslashes($override); ?></p>
        </div>

        <hr class="small">
        <?php

        return;
    }

    // Grab fundraiser name and GUID from cookie
    $fundraiser_name = ( isset( $_COOKIE['fundraiser_name'] ) && $_COOKIE['fundraiser_name'] ) ?
        $_COOKIE['fundraiser_name'] :
        false;

    $fundraiser_guid = ( isset( $_COOKIE['fundraiser_guid'] ) && $_COOKIE['fundraiser_guid'] ) ?
        $_COOKIE['fundraiser_guid'] :
        false;

    // Overall product donation amount
    $donation_percentage = OM_CART_DONATION_PERCENTAGE * 100 . '%';

    // Get the group name for adding to message
    $group_add = ( $fundraiser_name && $fundraiser_guid ) ?
        ' to <br><strong><a href="' . $fundraiser_guid . '">' . $fundraiser_name . '</a></strong>' :
        ' to <br><strong><a href="/about-us/one-mission-love-fund" target="_blank">the One Mission Love Fund</a></strong>';
    ?>

    <div id="product_donation_amount">
        <p><strong><?php echo $donation_percentage ?></strong> from the proceeds of this item will be donated<?php echo stripcslashes($group_add); ?>!</p>
    </div>

    <hr class="small">

<?php
}

// Checkout page change shipping address
function _om_checkout_shipping_change_button($checkout) {
?>

  <span class="change_shipping_wrapper">
    <a class="popup-with-form" title="Change Address" href="#change-shipping-form">
      <button id="om_change_shipping" class="btn btn-info">
        <i class="fa fa-edit"></i>
        <?php esc_attr_e( 'change', 'woocommerce' ); ?>
      </button>
    </a>
  </span>
<?php
}

// add_filter('woocommerce_checkout_shipping', '_om_checkout_shipping_change_button', 10, 2);

function _om_checkout_shipping_change_form($checkout) {
  global $woocommerce;
?>
  <form id="change-shipping-form" class="dialog change-shipping-form white-popup-block mfp-hide" style="max-width:450px;">
    <h2 class="text-center">Change Shipping</h2>
<?php

  foreach ($checkout->checkout_fields['shipping'] as $key => $field) {
    // $name = $woocommerce->customer->get_shipping_first_name();
    woocommerce_form_field($key, $field, $checkout->get_value($key));
  }
?>
    <div class="buttons row">
      <div class="col-sm-6">
        <button class="btn btn-lg btn-block btn-default cancel">Cancel</button>
      </div>
      <div class="col-sm-6">
        <button class="btn btn-lg btn-block btn-success apply" id="change_shipping_apply">Apply</button>
      </div>
    </div>
  </form>
<?php
}
add_action('woocommerce_before_checkout_form', '_om_checkout_shipping_change_form');

function _om_checkout_shipping($arg1, $arg2) {
  _dbg("CHECKOUT SHIPPING");
}
add_action('woocommerce_checkout_shipping', '_om_checkout_shipping', 10, 2);

//
//  Fires on payment completion
//  - Saves down following fields to order meta
//      * Fundraiser ID
//      * Fundraiser User ID
//      * Fundraiser Name
//      * Fundraiser GUID
//      * Total Donation Amount
//
//      * Total (Gross): Donation Sum of Donations from Products + Cash Donation + Subscription Donation
//      * Total (Net) Donation: Total Gross Donation minus Calculated Transaction Fee
//      * Product Purchases: Sum of all product sales (need to subtract discounts/deals)
//      * Donations from Products: 40% of the prooduct sales
//      * Cash Donation:
//      * Subscription Donation
//      * Subscription Fee: Passed from subscription project
//      * Shipping
//      * Tax: Indicates whether supporter gave cash-only donation or bought products, pass to hubspot to remarket to cash-only donations
//      * Order Total
//      * Tip
//      * Credit Card Fee for Tip: 2.9% of the Tip Amount
//      * Calculated Transaction Fee: 2.9% plus $.30 per the whole transaction amount, plus the subscription fee, minus the credit card fee for tip

function _om_payment_complete( $order_id ) {
	global $HubSpotApi;

	$product_purchases = 0;
	$subscription_donation = 0;
	$cash_donation = 0;
	$fund_id = 0;
	$fund_guid = 'https://onemission.fund';
	$fund_name = 'One Mission Love Fund';

  // Fundraiser ID
  if( isset( $_COOKIE['shopping_for_id'] ) && $_COOKIE['shopping_for_id'] ) {
    // Shopping for fundraiser
    $fund_id = $_COOKIE['shopping_for_id'];
    update_post_meta( $order_id, 'Fundraiser ID', $_COOKIE['shopping_for_id'] );
  } else {
    // Shopping for Love Fund
    update_post_meta( $order_id, 'Fundraiser ID', '0' );
  }

  // Fundraiser User ID
  if( isset( $_COOKIE['fundraiser_user_id'] ) && $_COOKIE['fundraiser_user_id'] ) {
    // Shopping for fundraiser
    update_post_meta( $order_id, 'Fundraiser User ID', $_COOKIE['fundraiser_user_id'] );
  } else {
    // Shopping for Love Fund
    update_post_meta( $order_id, 'Fundraiser User ID', '0' );
  }

  // Fundraiser Name
  if( isset( $_COOKIE['fundraiser_name'] ) && $_COOKIE['fundraiser_name'] ) {
    // Shopping for fundraiser
    $fund_name = $_COOKIE['fundraiser_name'];
    update_post_meta( $order_id, 'Fundraiser Name', $_COOKIE['fundraiser_name'] );
  } else {
    // Shopping for Love Fund
    update_post_meta( $order_id, 'Fundraiser Name', 'One Mission Love Fund' );
  }

  // Fundraiser Permalink
  if( isset( $_COOKIE['fundraiser_guid'] ) && $_COOKIE['fundraiser_guid'] && ! empty( $_COOKIE['fundraiser_guid'] ) ) {
    // Shopping for fundraiser
    update_post_meta( $order_id, 'Fundraiser GUID', $_COOKIE['fundraiser_guid'] );
    $fund_guid = $_COOKIE['fundraiser_guid'];
  } else {
    // Shopping for Love Fund
    update_post_meta( $order_id, 'Fundraiser GUID', 'https://onemission.fund' );
  }

  /* change shipping address update */
  if(isset($_REQUEST['shipping_first_name'] )) {
    update_post_meta( $order_id, '_shipping_first_name', $_REQUEST['shipping_first_name'] );
  }

  if(isset($_REQUEST['shipping_last_name'] )) {
    update_post_meta( $order_id, '_shipping_last_name', $_REQUEST['shipping_last_name'] );
  }

  if(isset($_REQUEST['shipping_address_1'] )) {
    update_post_meta( $order_id, '_shipping_address_1', $_REQUEST['shipping_address_1'] );
  }

  if(isset($_REQUEST['shipping_address_2'] )) {
    update_post_meta( $order_id, '_shipping_address_2', $_REQUEST['shipping_address_2'] );
  }

  if(isset($_REQUEST['shipping_city'] )) {
    update_post_meta( $order_id, '_shipping_city', $_REQUEST['shipping_city'] );
  }

  if(isset($_REQUEST['shipping_state'] )) {
    update_post_meta( $order_id, '_shipping_state', $_REQUEST['shipping_state'] );
  }

  if(isset($_REQUEST['shipping_postcode'] )) {
    update_post_meta( $order_id, '_shipping_postcode', $_REQUEST['shipping_postcode'] );
  }

  function updateOrder($id, $field, $value, $cast_float) {
    if (isset($value)) {
      if ($cast_float) {
        $value = number_format((float) $value, 2);
      }
      update_post_meta($id, $field, $value);
    }
  }

  //
  //  Donation % if exists (used for subscription products only!!!)
  //
  $order = new WC_Order( $order_id );
  $items = $order->get_items();

  if( _om_items_has_subscription( $items ) ) {
    $subscription_donation = _om_get_subscriptions_donation_total();
    $donation_percentage = _om_get_percentage_from_items( $items );

    updateOrder($order_id, 'Donation Percentage', $donation_percentage);
    updateOrder($order_id, 'Subscription Donation', $subscription_donation);

    // Set a flag for generic handling of subscription orders from the post meta side of things
      updateOrder($order_id, 'OMF Subscription Order', 'true');
  }

  // Total (Gross) Donation Amount
  $total_donation_amt = _om_get_total_donation();
  $shipping = $order->shipping_total;

  // TODO: update hubspot with flag  if cash only (no-tax)
  $total_tax = $order->total_tax;

  // Calculate cash donations (custom donations)
  foreach($items as $item) {
    if ($item->get_name() == 'Custom Donation') {
      if (isset($item['item_meta']['_line_total'])) {
        $cash_donation += floatval($item['item_meta']['_line_total'][0]);
      } else if (isset($item['total'])) {
        $cash_donation += floatval($item['total']);
      } else {
        $total = $fields['_order_total'][0];
        // if no item meta found and only 1 order, assume cash donation is entire order
        $cash_donation += floatval($total);
      }
    }
  }

  foreach( $order->get_items('fee') as $item_id => $item_fee ){
    $fee_name = $item_fee->get_name();
    $fee_total = $item_fee->get_total();
    $fee_total_tax = $item_fee->get_total_tax();
  }

  $order_total = $order->total - $fee_total;
	$product_total = _om_get_donatable_subtotal();
	$product_donations = 0.4 * $product_total;
  // Transaction Fee = (Cash Donation + Product Purchases + Tax + Shipping) * 0.029 + 0.30
  $calculated_transaction_fee = ($cash_donation + $product_total + $total_tax + $shipping) * 0.029 + 0.3; // - $fee_total_tax;
  $total_net_donation = floatval($total_donation_amt - $calculated_transaction_fee);

  updateOrder($order_id, 'Tip Amount', $fee_total, true);
  updateOrder($order_id, 'Tip Credit Card Fee', (0.029 * $fee_total), true);
  updateOrder($order_id, 'Tax', $total_tax, true);
  updateOrder($order_id, 'Total Donation Amount', $total_donation_amt, true);
  updateOrder($order_id, 'Order Total', $order_total, true);
  updateOrder($order_id, 'Shipping', $order->shipping_total, true);
	updateOrder($order_id, 'Product Purchases', $product_total, true);
	updateOrder($order_id, 'Product Donation', $product_donations, true);
	updateOrder($order_id, 'Cash Donation', $cash_donation, true);
	updateOrder($order_id, 'Subscription Donation', $subscription_donation, true);
  updateOrder($order_id, 'Total Net Donation', $total_net_donation, true);
	updateOrder($order_id, 'Transaction Fee', $calculated_transaction_fee, true);

  $user_id = get_current_user_id();
	$user = get_userdata($user_id);
	$user_meta = get_user_meta($user_id);
	$order_meta = get_post_meta($order_id);

	// fire hubspot API field updates
	$email = isset($user) && isset($user->user_email) ? $user->user_email : $_POST['billing_email'];
	$supported_email_updates = $_POST['supported_email_updates'] == "1" ? "true" : "false";
	$hubspot_fields = array(
		array('property' => 'supported_fundraiser_name', 'value' => $fund_name),
		array('property' => 'supported_fundraiser_guid', 'value' => $fund_guid),
		array('property' => 'supported_fundraiser_id', 'value' => $fund_id),
		array('property' => 'supported_order_total', 'value' => number_format((float) $order_total, 2)),
		array('property' => 'supported_product_donation', 'value' => number_format((float) $product_donations, 2)),
		array('property' => 'supported_subscription_donation', 'value' => number_format((float) $subscription_donation, 2)),
		array('property' => 'supported_cash_donation', 'value' => number_format((float) $cash_donation, 2)),
		array('property' => 'supported_tip', 'value' => number_format((float) $fee_total, 2)),
		array('property' => 'supported_email_updates', 'value' => $supported_email_updates)
	);
	$optional_fields = array(
		'billing_first_name' => 'firstname',
		'billing_last_name' => 'lastname',
		'billing_company' => 'company',
		'billing_phone' => 'phone',
		'billing_postcode' => 'zip',
		'billing_address_1' => 'address',
		'billing_city' => 'city',
		'billing_state' => 'state'
	);

	foreach($optional_fields as $of => $hsfield) {
		if (is_callable(array($order, "get_{$of}"))) {
			$hsval = $order->{"get_{$of}"}();
			if (!empty($hsval)) {
				array_push($hubspot_fields, array('property' => $hsfield, 'value' => $hsval));
			}
		}
	}

	$hs_update = $HubSpotApi->create_or_update_contact_by_email($email, $hubspot_fields);
	return;
}

// add_action( 'woocommerce_payment_complete', '_om_payment_complete' );
add_action( 'woocommerce_checkout_update_order_meta', '_om_payment_complete' );

/////////////////////////////////////////////////////////////////////////////////////
//
//  Custom donation logic
//
/////////////////////////////////////////////////////////////////////////////////////

//
//  Loops through items in cart, check if product exists with FH_CUSTOM_DONATION_ID
//  - IF exists returns float of custom donation amount
//  - ELSE return false
//
function _fh_donation_exsits() {

    global $woocommerce;

    $total_donation = 0;

    if( sizeof( $woocommerce->cart->get_cart() ) > 0 ) {

        foreach( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {

            $_product   = $values['data'];
            $product_id = $_product->get_id();

            if( $product_id == FH_CUSTOM_DONATION_ID ) {

                // Product exists, return donation value as float
                return floatval( $woocommerce->session->fh_donation );
            }
        }
    }

    return false;
}

//
//  Calculate a rounded up donation value "round-up" cart feature
//
function _fh_round_donation( $total, $value = 10 ) {

    $donation = ( ceil( $total / $value ) * $value ) - $total;

    return array(
        'donation' =>         $donation,
        'rounded_up_total' => ( $donation + $total )
    );
}

//
//  Adds "add donation" form to end of cart
//
function _fh_donation_after_cart_table() {

    global $woocommerce;

    $num_coupons = count( $woocommerce->cart->applied_coupons );

    if( ! _fh_donation_exsits() ) {

        unset( $woocommerce->session->fh_donation );
    }

    function _om_donate_coupon_row($rud) {
?>
      <div class="donation_wrap">
        <label>Add an additional donation:</label>

        <div class="input-group">
          <span class="input-group-addon"><?php echo FH_DONATE_LABEL_UNIT; ?></span>
          <input type="text" id="fh_donation_field" name="fh_donation" value="" class="form-control text-right" placeholder="0.00" />
          <span class="input-group-btn">
            <input type="text" name="fh_donate_hidden" value="1" class="hidden">
            <button id="fh_donate_btn" class="btn btn-success">
              <i class="fa fa-plus"></i>
              <i class="fa fa-heart"></i>
              <span class="sr-only"> Add donation!</span>
            </button>
          </span>
        </div>

        <p class="no_margin">
          <strong>
            <a id="fh_in_cart_donate_round_up" data-roundup="<?php echo number_format( $rud['donation'], 2 ); ?>" href="#">
              Donate $<?php echo number_format( $rud['donation'], 2 ); ?>
            </a>
          </strong> to bring your order to an even <strong><br />
            $<?php echo number_format( $rud['rounded_up_total'], 2 ); ?>
          </strong>!
        </p>
        <script>
          // Logic for round-up population button
          jQuery(function () {
            jQuery( '#fh_in_cart_donate_round_up' ).on( 'click', function (e) {
              e.preventDefault();
              jQuery( '#fh_donation_field' ).val( jQuery( this ).attr( 'data-roundup' ) );
            });
          });
        </script>
      </div>
<?php
    }
?>
    <tr id="donate_coupon_row">
      <td colspan="3" class="donate_coupon_col hidden">
<?php
    /* hiding td for for now, doesn't seem to work */

    // Calculate the rounding up to nearest $10 increment donation ( $32.73 -> $7.27 )
    $round_up_donate = _fh_round_donation( $woocommerce->cart->total );
    if( ! _fh_donation_exsits() && $round_up_donate['donation']) {
      _om_donate_coupon_row($round_up_donate);
    }
?>
      </td>
      <td colspan="3" class="donate_coupon_col hidden">
<?php
      if( ! _fh_donation_exsits() && $round_up_donate['donation']) {
        _om_donate_coupon_row($round_up_donate);
      }
?>
      </td>
      <td class="actions visible-sm visible-md visible-lg" colspan="6">
      <?php
        if ( wc_coupons_enabled() ) {
      ?>
        <div class="coupon">
          <label for="coupon_code"><?php esc_html_e( 'coupon:', 'woocommerce' ); ?></label>
          <input type="text" name="coupon_code" class="input-text" value="" placeholder="<?php esc_attr_e( 'coupon code', 'woocommerce' ); ?>" />
          <button type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'apply coupon', 'woocommerce' ); ?>">
            <?php esc_attr_e( 'apply coupon', 'woocommerce' ); ?>
          </button>
          <?php do_action( 'woocommerce_cart_coupon' ); ?>
        </div>
      <?php
        }
      ?>
      </td>

    </tr>
    <script>
      // reload cart immediately on quantity change
      jQuery('div.woocommerce').on('change', '.qty', function(){
        jQuery("[name='update_cart']").prop("disabled", false);
        jQuery("[name='update_cart']").trigger("click");
      });

     // stupid hack to get donation total to update
     var subtotal = parseInt(jQuery('.product-subtotal .woocommerce-Price-amount').text().replace('$', '') * 100);
     jQuery('div.woocommerce').bind('DOMSubtreeModified', function(){
       var newSubtotal = parseInt(jQuery('.product-subtotal .woocommerce-Price-amount').text().replace('$', '') * 100);
       if (newSubtotal !== subtotal) {

        var newDonation = ((newSubtotal * 0.4) / 100).toFixed(2);
        console.log("NEW DONATION", newDonation);
        jQuery(".donation_totals .donation-amount").text('$' + newDonation);
        subtotal = newSubtotal;
       }
        // console.log(jQuery('.product-subtotal .woocommerce-Price-amount').text());
      });
    </script>
<?php
}
add_action( 'woocommerce_cart_contents', '_fh_donation_after_cart_table' );


//
//  Runs at init hook to look for $_POST['fh_donation'] and adds product to cart
//  if applicable
//
function _fh_process_donation() {

    global $woocommerce;

    $donation = isset( $_POST['fh_donation'] ) && !empty( $_POST['fh_donation'] )
        ? floatval( $_POST['fh_donation'] ) :
        false;

    if( $donation && isset( $_POST['fh_donate_hidden'] ) && $_POST['fh_donate_hidden'] ) {

        // add item to basket
        $found = false;

        // add to session
        if( $donation >= 0 ) {

            $woocommerce->session->fh_donation = $donation;

            // Check if product already in cart
            if( sizeof( $woocommerce->cart->get_cart() ) > 0 ) {

                foreach( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {

                    $_product = $values['data'];

                    if( $_product->id == FH_CUSTOM_DONATION_ID ) {

                        $found = true;
                    }
                }

                // if product not found, add it
                if( ! $found ) {

                    $woocommerce->cart->add_to_cart( FH_CUSTOM_DONATION_ID );
                }

            } else {

                // if no products in cart, add it
                $woocommerce->cart->add_to_cart( FH_CUSTOM_DONATION_ID );
            }
        }
    }
}

add_action( 'init', '_fh_process_donation' );

//
//  Hooked woocommerce_get_price that looks for FH_CUSTOM_DONATION_ID
//  and responds with "fh_donation" stored in session
//
function _fh_woo_get_price_donation( $price, $product ) {

    global $woocommerce;

    if( $product->id == FH_CUSTOM_DONATION_ID ) {

        return isset( $woocommerce->session->fh_donation ) ?
            floatval( $woocommerce->session->fh_donation ) :
            0;
    }

    return $price;
}

add_filter( 'woocommerce_get_price', '_fh_woo_get_price_donation', 10, 2 );


/////////////////////////////////////////////////////////////////////////////////////
//
//  Coupons & custom donation logic
//
/////////////////////////////////////////////////////////////////////////////////////


//  Hide coupon code form on cart/checkout page if custom donation product is in cart

function _fh_hide_coupon_field_on_cart_and_checkout( $enabled ) {

    global $woocommerce;

    if ( is_cart() || is_checkout() ) {

        if( _fh_donation_exsits() ) {

            // Remove all coupons
            $woocommerce->cart->remove_coupons();

            $enabled = false;
        }
    }

    return $enabled;
}

add_filter( 'woocommerce_coupons_enabled', '_fh_hide_coupon_field_on_cart_and_checkout' );

//
//  Show "coupons disabled" message after cart if custom donation product exists
//
function _fh_coupons_disabled_custom_donation_msg() {

    if( _fh_donation_exsits() && 1 == 0 ) {
        ?>
        <tr id="fh_donation_row">
            <td colspan="6">
                <div class="donation_wrap">Coupons &amp; Gift Certificates disabled with custom donation!</div>
            </td>
        </tr>
        <?php
    }
}

add_action( 'woocommerce_cart_contents', '_fh_coupons_disabled_custom_donation_msg' );

//
//  Rewrite error message if custom donation is being added to the cart
//  and the coupon is being removed
//
function _fh_coupon_custom_donation_error( $error ) {

    $error_message = 'Sorry for the inconvenience. Gift Cards cannot be used for transactions that include Cash Donations. Please remove the Cash Donation portion of your Cart and re-enter the Gift Card number.';
    $search_for_1  = 'is invalid - it has now been removed from your order';

    if( _fh_donation_exsits() && strpos( $error, $search_for_1 ) !== false ) {

        $error = $error_message;
    }

    $search_for_2 = 'Sorry, this coupon is not applicable to your cart contents.';

    if( _fh_donation_exsits() && strpos( $error, $search_for_2 ) !== false ) {

        $error = $error_message;
    }

    return $error;
}

add_filter( 'woocommerce_add_error', '_fh_coupon_custom_donation_error' );
