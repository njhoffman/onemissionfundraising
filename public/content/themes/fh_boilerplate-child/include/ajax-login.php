<?php

/**
 * AJAX login & create account
 *
 */

// TODO: take log messages out and use actions in config/logger

// includes client js file, populates loginObj
function _om_ajax_login_init(){


	wp_register_script('login-script', get_template_directory_uri() . '-child/js/dialogs/login.js', array('jquery') );
	wp_enqueue_script('login-script');

	wp_localize_script( 'login-script', 'loginObj', array(
		'ajaxUrl' => admin_url( 'admin-ajax.php' ),
		'redirectUrl' => home_url(),
		'loadingMessage' => __('Validating, please wait...')
	));

	// Enable the user with no privileges to run ajax_login() in AJAX
	add_action( 'wp_ajax_nopriv_ajax_login', '_om_ajax_login' );
}

// Execute the action only if the user isn't logged in
if (!is_user_logged_in()) {
	add_action('init', '_om_ajax_login_init');
}

function _om_ajax_check_required($required_fields) {
}
// collect data from post, check if nonce is valid, perform user login
function _om_ajax_login(){
	global $HubSpotApi;

	$return_obj = array();
	$create_account = isset($_POST["create"]) ? true : false;

	// First check the nonce, if it fails the function will break
	// why are the nonce's working
	if ($create_account) {
		// check_ajax_referer( 'ajax-signup-nonce', 'security-signup' );
	} else {
	   // check_ajax_referer( 'ajax-signin-nonce', 'security-signin' );
	}

	// check required fields
	$required_fields = array(
		"username" => "Email",
		"password" => "Password"
	);
	if ($create_account) {
		$required_fields = array_merge($required_fields, array(
			"first_name" => "First Name",
			"last_name" => "Last Name",
		));
	}
	$required_missing = array();
	foreach ($required_fields as $key => $value) {
		if ( empty($_POST[$key]) ) {
			array_push($required_missing, $value);
		}
	}
	if (count($required_missing) > 0) {
		$return_obj = array(
			'loggedIn' => false,
			'message' => __('Following fields are required: ' . implode($required_missing, ", "))
		);
		wp_send_json_error($return_obj);
	}

	// Nonce is checked, get the POST data and sign user on
	$info = array();
	$info['user_login'] = sanitize_text_field($_POST['username']);
	$info['user_email'] = sanitize_text_field($_POST['username']);
	// sanitize strips '+' which is useful for debugging
	$info['user_password'] = sanitize_text_field($_POST['password']);
	$info['user_pass'] = sanitize_text_field($_POST['password']);
	$info['remember'] = true;
	if ($create_account) {
		$info['user_login_confirm'] = sanitize_text_field($_POST['confirm_username']);
		$info['first_name'] = sanitize_text_field($_POST['first_name']);
		$info['last_name'] = sanitize_text_field($_POST['last_name']);
	}


	$user_signon = false;

	// TODO: ugly ugly, refactor
	if ( $create_account ) {
		if ( isset($_POST["confirm_password"]) && $_POST["password"] !== $_POST["confirm_password"] ) {
			$return_obj = array(
				'loggedIn' => false,
				'message' => __('Passwords do not match.')
			);
		} else if ( isset($_POST["confirm_username"]) && $_POST["username"] !== $_POST["confirm_username"] ) {
			$return_obj = array(
				'loggedIn' => false,
				'message' => __('Emails do not match.')
			);
		} else if (!filter_var($_POST['username'], FILTER_VALIDATE_EMAIL)) {
			$return_obj = array(
				'loggedIn' => false,
				'message' => __('Username must be an email address.')
			);
		} else if (strlen($_POST['password']) < '6') {
			$return_obj = array(
				'loggedIn' => false,
				'message' => __('Password must have at least six characters.')
			);
		} else {
			$user_insert = wp_insert_user($info);
			if (is_wp_error($user_insert) ){
				$error_msg = $user_insert->get_error_message();
				_om_warn("Create account fail");
				$return_obj = array(
					'loggedIn' => false,
					'message' => __($error_msg)
				);
			} else {
				_om_log("Create account success: " . $info['user_login']);
				// update custom user fields
				update_user_meta( $user_insert, 'fund_creation_status', 'Pre-Fund');
				update_user_meta( $user_insert, 'fund_creation_progress', array(
					1 => false,
					2 => false,
					3 => false,
					4 => false,
					5 => false,
					6 => false,
					7 => false,
					8 => false,
					9 => false
				));

				$return_obj = array(
					'loggedIn' => true,
					'redirectUrl' => home_url(),
					'message' => __('Account created, logging in...'),
					'userInfo' => $info
				);

				// IMPORTANT! 2nd parameter must be true for live site
				$user_signon = wp_signon($info);
				// $user_signon = wp_signon($info, false);
			}
		}
	} else {
		// IMPORTANT! 2nd parameter must be true for live site
		// $user_signon = wp_signon( $info, false );
		$user_signon = wp_signon($info);
		if (is_wp_error($user_signon) ){
			_om_log('Login fail: ' . $info['user_login']);
			$return_obj = array(
				'loggedIn' => false,
				'message' => __('Wrong username or password.')
			);
		} else {
			_om_log('Login success: ' . $info['user_login']);
			$return_obj = array(
				'loggedIn' => true,
				'message' => __('Login successful, redirecting...'),
				'redirectUrl' => admin_url('/')
			);
		}
	}

	if ($return_obj['loggedIn'] != true) {
		wp_send_json_error($return_obj);
	} else {
		wp_send_json_success($return_obj);
	}
}
?>
