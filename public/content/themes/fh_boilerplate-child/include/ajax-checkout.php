<?php

/**
 * AJAX login & create account
 *
 */

// TODO: take log messages out and use actions in config/logger

// includes client js file, populates loginObj
function _om_ajax_change_address_init(){


	wp_register_script('change-address-script', get_template_directory_uri() . '-child/js/dialogs/change_address.js', array('jquery') );
	wp_enqueue_script('change-address-script');

	wp_localize_script( 'change-address-script', 'changeAddressObj', array(
		'ajaxUrl' => admin_url( 'admin-ajax.php' ),
		'redirectUrl' => home_url(),
		'loadingMessage' => __('Validating, please wait...')
	));

	// Enable the user with no privileges to run ajax_login() in AJAX
	// add_action( 'wp_ajax_nopriv_ajax_login', '_om_ajax_login' );
}

add_action('init', '_om_ajax_change_address_init');

?>
