<?php

function _om_add_assign_checkout_fields_menu_item() {
	if( is_super_admin() ) {
		add_submenu_page('custom-reports-admin-page.php', 'Assign Checkout Fields', 'Assign Checkout Fields', 'administrator', 'assign-checkout-fields', '_om_assign_checkout_fields');

		if (isset($_GET['page']) && $_GET['page'] == 'assign-checkout-fields' ) {
			wp_register_script('assign-checkout-fields', get_template_directory_uri() . '-child/js/assign_checkout_fields.js', array('jquery'));
			wp_enqueue_script('assign-checkout-fields');
			wp_localize_script('assign-checkout-fields', 'assignCheckoutObj', array(
				'ajaxUrl' => admin_url( 'admin-ajax.php')
			));
		}
	}
}
add_action('admin_menu', '_om_add_assign_checkout_fields_menu_item');

function _om_ajax_assign_checkout_fields($arg1) {
  $page = 1;
  $batch_number = 10;
  $only_unassigned = false;
  if (isset($_POST['page'])) {
    $page = $_POST['page'];
  }
  if (isset($_POST['batchNumber'])) {
    $batch_number = $_POST['batchNumber'];
  }
  if (isset($_POST['onlyUnassigned']) && $_POST['onlyUnassigned'] === "checked") {
    $only_unassigned = true;
  }
	if (false) {
		wp_send_json_error(array('message' => 'No fundraiser ids to lookup'));
	} else {
    $items = _om_get_all_orders($page, $batch_number, $only_unassigned);
		wp_send_json_success(array('items' => $items));
	}
}
add_action('wp_ajax_ajax_assign_checkout_fields', '_om_ajax_assign_checkout_fields');

function _om_get_all_orders($page, $batch_number, $only_unassigned) {
  global $HubSpotApi;

  // _dbg("Page: $page \t Batch: $batch_number");
  // $query_status = array( 'wc-processing', 'wc-completed', 'wc-on-hold', 'wc-cancelled', 'wc-pending' );
	$query_status = array( 'wc-completed', 'wc-processing' );
	$order_args = array(
    'posts_per_page' => $batch_number,
    'post_type' => 'shop_order',
    'post_status' => $query_status,
    'orderby' => 'post_date',
    'order' => 'ASC',
    'paged' => $page
  );

  if ($only_unassigned) {
    $order_args['meta_query'] = array(
      array(
        'key' => 'Order Total',
        'compare' => 'NOT EXISTS',
        'value' => ''
      )
    );
  }

	$orders = new WP_Query($order_args);
	$order_n = 0;

  $order_list = [];

  function updateOrder($id, $field, $value, $record, &$changed, &$added, &$existing) {
    if (isset($record[$field]) && isset($record[$field][0]) && (string)$record[$field][0] != (string)$value) {
      _dbg("Changed $field: " . $record[$field][0] . " - " . $value);
      update_post_meta($id, $field, $value);
      array_push($changed, $field);
    } else if (isset($value) && !isset($record[$field])) {
      update_post_meta($id, $field, $value);
      array_push($added, $field);
    } else {
      array_push($existing, $field);
    }
  }

	while ($orders->have_posts()) {
    $added = [];
    $changed = [];
    $existing = [];
    $product_total = 0;
    $product_donation = 0;
    $subscription_donation = 0;
    $cash_donation = 0;
    $fee_total = 0;
    $fee_total_tax = 0;
    $tip_cc_fee = 0;

    $refund_shipping = 0;
    $refund_total_tax = 0;
    $refund_total = 0;

		$orders->the_post();
		$order_post = $orders->post;
    $order_id = get_the_ID();
		$order_fields = get_post_custom($order_id);
    $fund_name = $order_fields['Fundraiser Name'][0];
    $total_donation = $order_fields['Total Donation Amount'][0];
    $fund_id = isset($order_fields['Fundraiser ID']) ? $order_fields['Fundraiser ID'][0] : '';
    $fund_guid = isset($order_fields['Fundraiser GUID']) ? $order_fields['Fundraiser GUID'][0] : '';
    $fund_user_id = isset($order_fields['Fundraiser User ID']) ? $order_fields['Fundraiser User ID'][0] : '';
    $stripe_fee = isset($order_fields['Stripe Fee']) ?
      $order_fields['Stripe Fee'][0] : '---';
    $net_revenue_from_stripe = isset($order_fields['Net Revenue From Stripe']) ?
      $order_fields['Net Revenue From Stripe'][0] : '---';
    $paid_date = $order_post->post_date;
    // $order = new WC_Order( $order_post->ID );
    $order = wc_get_order($order_id);
    $items = $order->get_items();

    foreach($items as $item) {
      if ($item['name'] == 'Custom Donation') {
        if (isset($item['item_meta']['_line_total'])) {
          $cash_donation += floatval($item['item_meta']['_line_total'][0]);
        } else if (isset($item['total']) && floatval($item['total']) > 0) {
          $cash_donation += floatval($item['total']);
        } else {
          $total = $order_fields['_order_total'][0];
          // if no item meta found and only 1 order, assume cash donation is entire order
          $cash_donation += floatval($total);
        }
      }
    }
    if ($order_id === 4997) {
      _dbg("4997: $cash_donation");
      _dbg($order_fields);
      // _dbg($items);
    }
    foreach( $order->get_items('fee') as $item_id => $item_fee ){
      $fee_name = $item_fee->get_name();
      $fee_total = $item_fee->get_total();
      $fee_total_tax = $item_fee->get_total_tax();
      $tip_cc_fee = 0.029 * $fee_total;
    }

    foreach ($order->get_refunds() as $refund) {
      $refund_shipping += $refund->get_shipping_total();
      $refund_total_tax += $refund->get_total_tax();
      $refund_total += $refund->get_total();
    }

    $tax_total = $order->get_total_tax() + $refund_total_tax;
    $shipping_total = $order->get_shipping_total() + $refund_shipping;
    $order_total = $order->get_total() + $refund_total - $fee_total;
    // Calculate cash donations (custom donations)
    $product_purchases = $order->get_total() - $fee_total - $cash_donation - $shipping_total - $tax_total + $refund_total;
    if ($product_purchases < 0) {
      $product_purchases = 0;
    }
    $product_donation = 0.4 * $product_purchases;
    // Transaction Fee = (Cash Donation + Product Purchases + Tax + Shipping) * 0.029 + 0.30
    $transaction_fee = ($cash_donation + $product_purchases + $tax_total + $shipping_total) * 0.029 + 0.3; // - $fee_total_tax;
    // $transaction_fee = $stripe_fee !== '---' ? $stripe_fee - $tip_cc_fee : 0 - $tip_cc_fee;
    $total_net_donation = $total_donation - $transaction_fee;

    if(_om_items_has_subscription( $items ) ) {
      // doesn't work, just assign product total
      $subscription_donation = $order_total;
      $product_donation = 0;
      $product_purchases = 0;
      $donation_percentage = _om_get_percentage_from_items( $items );
    }

    updateOrder($order_id, 'Total Net Donation', number_format((float) $total_net_donation, 2), $order_fields, $changed, $added, $existing);
    updateOrder($order_id, 'Transaction Fee', number_format((float) $transaction_fee, 2), $order_fields, $changed, $added, $existing);
    updateOrder($order_id, 'Product Purchases', number_format((float) $product_purchases, 2), $order_fields, $changed, $added, $existing);
    updateOrder($order_id, 'Product Donation', number_format((float) $product_donation, 2), $order_fields, $changed, $added, $existing);
    updateOrder($order_id, 'Subscription Donation', number_format((float) $subscription_donation, 2), $order_fields, $changed, $added, $existing);
    updateOrder($order_id, 'Cash Donation', number_format((float) $cash_donation, 2), $order_fields, $changed, $added, $existing);
    updateOrder($order_id, 'Shipping', number_format((float) $shipping_total, 2), $order_fields, $changed, $added, $existing);
    updateOrder($order_id, 'Tax', number_format((float) $tax_total, 2), $order_fields, $changed, $added, $existing);
    updateOrder($order_id, 'Order Total', number_format((float) $order_total, 2), $order_fields, $changed, $added, $existing);

    $hubspot_fields = array(
      array('property' => 'supported_fundraiser_name', 'value' => $fund_name),
      array('property' => 'supported_fundraiser_guid', 'value' => $fund_guid),
      array('property' => 'supported_fundraiser_id', 'value' => $fund_id),
      array('property' => 'supported_order_total', 'value' => number_format((float) $order_total, 2)),
      array('property' => 'supported_product_donation', 'value' => number_format((float) $product_donation, 2)),
      array('property' => 'supported_subscription_donation', 'value' => number_format((float) $subscription_donation, 2)),
      array('property' => 'supported_cash_donation', 'value' => number_format((float) $cash_donation, 2))
    );

    $hs_errors = '';
    $hs_update = false;
    $email = $order_fields['_billing_email'][0];
    if (strtotime($paid_date) > strtotime("-1 year", time())) {
      // $fund_user = get_userdata($fund_user_id);
      $hs_update = $HubSpotApi->create_or_update_contact_by_email($email, $hubspot_fields);
    } else {
      _dbg("Skipping hubspot: " . $order->get_ID());
    }

    if (!isset($hs_update) || isset($hs_update->errors)) {
      $hs_errors = 'HubSpot Error for email: ' . $email . "\n" . json_encode($hs_update->errors);
      _dbg($hs_errors);
    } else if ($hs_update) {
      _dbg("Updated Hubspot: " . $order->get_ID() . " - $email");
      // _dbg($hs_update);
    }

    array_push($order_list, array(
      'ID' => $order->get_ID(),
      'fundraiser_guid' => $fund_guid,
      'fundraiser_user_id' => $fund_user_id,
      'fundraiser_name' => $fund_name,
      'paid_date' => $paid_date,
      'stripe_fee' => $stripe_fee,
      'net_revenue_from_stripe' => $net_revenue_from_stripe,
      'total' => $order_total,
      'shipping_total' => $shipping_total,
      'total_tax' => $tax_total,
      'total_donation' => $total_donation,
      'total_net_donation' => $total_net_donation,
      'product_purchases' => $product_purchases,
      'product_donation' => $product_donation,
      'cash_donation' => $cash_donation,
      'subscription_donation' => $subscription_donation,
      'tip_amount' => $fee_total,
      'tip_cc_fee' => $tip_cc_fee,
      'refund_shipping' => $refund_shipping,
      'refund_total_tax' => $refund_total_tax,
      'refund_total' => $refund_total,
      'transaction_fee' => $transaction_fee,
      '_added_fields' => $added,
      '_changed_fields' => $changed,
      '_existing_fields' => $existing,
      '_hubspot_success' => isset($hs_update) && isset($hs_update->vid),
      '_hubspot_new' => isset($hs_update) && isset($hs_update->isNew) && $hs_update->isNew != '',
      '_hubspot_email' => $email,
      '_hubspot_errors' => $hs_errors
    ));
	}
  return $order_list;
}

function _om_get_checkout_field_count() {
  global $wpdb;
  $results = $wpdb->get_results(
    "SELECT count(*) FROM {$wpdb->prefix}postmeta WHERE meta_key = 'Product Purchases'",
    OBJECT
  );
  $orders = wp_count_posts('shop_order');

  $total_orders = $orders->{'wc-pending'} + $orders->{'wc-processing'} + $orders->{'wc-on-hold'} +
          $orders->{'wc-completed'} + $orders->{'wc-cancelled'} +
          $orders->{'wc-refunded'} + $orders->{'wc-failed'};
  $total_remaining = $total_orders - $results[0]->{'count(*)'} - $orders->{'wc-failed'} -
    $orders->{'wc-on-hold'} - $orders->{'wc-cancelled'} - $orders->{'wc-pending'} - $orders->{'wc-refunded'};

  $html_out = '' .
    '<table id="checkout_fields_totals">' .
      '<tr>' .
        '<th>Pending</th>' .
        '<td>' . $orders->{'wc-pending'} .'</td>' .
        '<th>Processing</th>' .
        '<td>' . $orders->{'wc-processing'} .'</td>' .
      '</tr>' .
      '<tr>' .
        '<th>On Hold</th>' .
        '<td>' . $orders->{'wc-on-hold'} .'</td>' .
        '<th>Refunded </th>' .
        '<td>' . $orders->{'wc-refunded'} .'</td>' .
      '</tr>' .
      '<tr>' .
        '<th>Canceled</th>' .
        '<td>' . $orders->{'wc-cancelled'} .'</td>' .
        '<th>Failed</th>' .
        '<td>' . $orders->{'wc-failed'} .'</td>' .
      '</tr>' .
      '<tr>' .
        '<th>Completed</th>' .
        '<td>' . $orders->{'wc-completed'}.'</td>' .
        '<th>Total</th>' .
        '<td>' . $total_orders . '</td>' .
      '</tr>' .
      '<tr>' .
        '<th>Processed</th>' .
        '<td class="total-processed">' . $results[0]->{'count(*)'} . '</td>' .
        '<th>Left to Process</th>' .
        '<td class="total-remaining">' . $total_remaining . '</td>' .
      '</tr>' .
    '</table>';

  echo $html_out;
}

function _om_assign_checkout_fields() {
?>

<style>
  #checkout_fields_totals {
    width: 400px;
    margin: 20px auto;
    text-align: center;
    white-space: nowrap;
  }
  #checkout_fields_results table {
    width: 1600px;
    margin-left: auto;
    margin-right: auto;
    text-align: center;
    white-space: nowrap;
    table-layout: fixed;
    border-collapse: collapse;
  }

  #checkout_fields_results table thead {
    border-bottom: solid 3px black;
  }
  #checkout_fields_results table tbody {
    display: block;
    overflow: auto;
    height: 800px;
    width: 1600px;
  }

  #checkout_fields_results table tbody tr:nth-child(even) {
    background-color: #ddd;
  }

  #checkout_fields_results table th {
    width: 80px;
    vertical-align: bottom;
  }
  #checkout_fields_results table td {
    width: 80px;
  }

  #checkout_fields_results table th.tiny ,
  #checkout_fields_results table td.tiny {
    width: 40px;
  }

  #checkout_fields_results table td.hubspot-update {
    font-weight: bold;
    font-size: 1.2em;
    cursor: pointer;
  }

  #checkout_fields_results table td.hubspot-update.success:before {
    color: blue;
    content: '●';
  }

  #checkout_fields_results table td.hubspot-update.added:before {
    color: green;
    content: '+ ● +';
  }

  #checkout_fields_results table td.hubspot-update.failure:before {
    color: red;
    content: '‼';
  }

  #checkout_fields_results table td.new {
    font-weight: bold;
    color: blue;
  }

  #checkout_fields_results table td.existing {
    color: black;
  }

  #checkout_fields_results table td.added {
    color: green;
  }

  #checkout_fields_results table td.changed {
    color: red;
  }
  .progress-wrapper {
    text-align: center;
  }

  #next_checkout_fields {
    display: block;
    font-weight: bold;
    padding: 6px 12px;
    text-transform: uppercase;
    font-size: 1.2em;
    margin: 10px auto;
    text-align: center;
  }

  .total-processed .changed,
  .total-remaining .changed {
    font-weight: bold;
    color: green;
  }

  .pages-text {
  }
  .pages-text .current-page {
    display: inline-block;
    background: #ffffee;
    padding: 5px 10px;
    border: solid 1px #bbbbaa;
  }

  .pages-text .current-page {
    display: none;
  }

  .pages-text .per-page {
    display: inline-block;
  }
  .pages-text .per-page input {
    width: 60px;
    text-align: right;
  }

	.progress-text {
		margin: 0;
	}

	.progressbar {
		width: 350px;
		height: 30px;
	}

  #checkout_fields_form label {
    text-align: center;
    display: block;
    margin-bottom: 10px;
  }

</style>


	<div class="wrap">
	<form id="checkout_fields_form">
		<h2>Assign Checkout Fields</h2>
    <?php _om_get_checkout_field_count(); ?>
    <label> <input type="checkbox" name="only-unassigned" />Only Load Unassigned</label>
    <div class="progress-wrapper">
      <p class="progress-text"></p>
      <progress class="progressbar" value="0" max="100"></progress>
      <button id="next_checkout_fields" class="page-title-action ajax-action">
        Start
      </button>
      <div class="pages-text">
        <div class="current-page">Page 0</div>
        <div class="per-page">Orders/Page
          <input type="number" name="per-page" value="500" />
        </div>
      </div>
    </div>
    <div id="checkout_fields_results">
    </div>

	</form>

<?php
}


