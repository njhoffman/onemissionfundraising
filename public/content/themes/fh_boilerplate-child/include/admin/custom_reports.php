<?php

function _om_add_custom_reports_menu_item() {
	if( is_super_admin() ) {
		add_menu_page('Custom Reports', 'Custom Reports', 'administrator', 'custom-reports-admin-page.php', 'custom_reports_admin_page', "dashicons-format-aside");
		add_submenu_page('custom-reports-admin-page.php', 'Fundraiser Goals', 'Fundraiser Goals', 'administrator', 'custom-reports-fundraisers', '_om_custom_reports_fundraisers');

		// dont load scripts unless we are on the right page as it loops ajax calls
		if (isset($_GET['page']) && $_GET['page'] == 'custom-reports-fundraisers' ) {
			wp_register_script('fundraiser-report-goals', get_template_directory_uri() . '-child/js/fundraiser_report.js', array('jquery'));
			wp_enqueue_script('fundraiser-report-goals');
			wp_localize_script('fundraiser-report-goals', 'fundraiserObj', array(
				'ajaxUrl' => admin_url( 'admin-ajax.php')
			));
		}
	}
}
add_action('admin_menu', '_om_add_custom_reports_menu_item');

function _om_ajax_fundraiser_report_ids() {

	$fundraisers = _om_fundraiser_report_ids();
	$fundraiserIds = [];
	while ( $fundraisers->have_posts() ) {
		$fundraisers->the_post();
		$post = $fundraisers->post;
		array_push($fundraiserIds, $post->ID);
	}

	wp_send_json_success(array('fundraisers' => $fundraiserIds ));
}
add_action('wp_ajax_ajax_fundraiser_report_ids', '_om_ajax_fundraiser_report_ids');

function _om_fundraiser_report_ids() {
	$args = array(
			'post_type' => 'page',
			'post_parent' => OM_FUNDRAISER_PAGE_PARENT_ID,
			'post_status' => array( 'publish' ),
			'posts_per_page' => -1,
			'orderby' => 'post_title',
			'order' => 'ASC'
		);
	return new WP_Query( $args );
}

function _om_ajax_fundraiser_report_items() {
	$fundraiserIds = $_POST['ids'];
	$fundraiserData = [];
	if (empty($fundraiserIds)) {
		wp_send_json_error(array('message' => 'No fundraiser ids to lookup'));
	} else {
		foreach ($fundraiserIds as $fundraiserId) {
			array_push($fundraiserData, _om_fundraiser_get_item($fundraiserId));
		}
		wp_send_json_success(array('items' => $fundraiserData));
	}
}
add_action('wp_ajax_ajax_fundraiser_report_items', '_om_ajax_fundraiser_report_items');


function _om_fundraiser_get_item($id) {
	$start_date = false;
	$end_date = false;

	if (isset($_POST['filter'])) {
		foreach ($_POST['filter'] as $filter) {
			if (!empty ($filter['value'])) {
				switch ($filter['name']) {
					case 'start_date':
						$start_date = $filter['value'];
						break;
					case 'end_date':
						$end_date = $filter['value'];
						break;
					case 'start_time':
						$start_date = $start_date . ' ' . $filter['value'];
						break;
					case 'end_time':
						$end_date = $end_date . ' ' . $filter['value'];
						break;
				}
			}
		}
	}
	$start_date = strtotime($start_date);
	$end_date = strtotime($end_date);

	$post = get_post($id);
	$fields = get_fields( $post->ID );

	$fundraiser_id = $post->ID;
	$fundraiser_title = $post->post_title;
	$goal_amount = $fields["fundraiser_goal_amount"];
	$continuous = $fields["fundraiser_timed_or_continuous"] === "continuous" ? true : false;
	$goal_status_adjustment = isset($fields["goal_status_adjustment"]) ? $fields["goal_status_adjustment"] : 0;
	$goal_status_total = _om_get_gsa_total($goal_status_adjustment);
	$goal_status_items = [];
	if ($goal_status_adjustment) {
		$gsas = array_filter(explode('|', $goal_status_adjustment));
		foreach($gsas as $gsa) {
			$gsa_fields = explode('::', $gsa);
			array_push($goal_status_items, $gsa_fields);
		}
	}
	$query_status = array( 'wc-processing', 'wc-completed' );

	$order_args = array(
		'posts_per_page' => -1,
		'post_type' =>      'shop_order',
		'post_status' => $query_status,
		'meta_query' => array(
			array(
				'key' =>   'Fundraiser ID',
				'value' => $post->ID
			)
		)
	);

	$orders = new WP_Query($order_args);
	$order_total = 0;
	$objects = [];
	$order_n = 0;
	while ($orders->have_posts()) {
		$orders->the_post();
		$order_post = $orders->post;
		$order_fields = get_post_custom($order_post->ID);
		// _dbg($order_post->post_date . " " . $order_post->post_modified . " " . $order_fields['_paid_date'][0] . "\n\n");
		// $paid_date = strtotime($order_fields['_paid_date'][0]);
		$paid_date = strtotime($order_post->post_date);
		if ($start_date && $paid_date < $start_date) {
			// _dbg("Skipping start: " . date("d-m-Y H:i:s", $start_date) .  " paid: " . date("d-m-Y H:i:s", $paid_date));
			continue;
		}
		if ($end_date && $paid_date > $end_date) {
			// _dbg("Skipping end: " + date("d-m-Y H:i:s", $end_date) .  " paid: " . date("d-m-Y H:i:s", $paid_date));
			continue;
		}
		if (isset($order_fields['Total Donation Amount'])) {
			$order_total += floatval( $order_fields['Total Donation Amount'][0] );
		}
		$order_n++;
		array_push($objects, $order_fields);
	}

	$raised_amount = round(floatval($goal_status_total + $order_total), 2);
	$goal_amount = $goal_amount > 0 ? $goal_amount: 0;

	$fundraiserItem = array(
		"Fundraiser ID" => $fundraiser_id,
		"Fundraiser Title" => $fundraiser_title,
		"Orders" => $order_n,
		"GSA" => $goal_status_total,
		"GSA Items" => $goal_status_items,
		"Goal Amount" => $goal_amount,
		"Total Raised" => $raised_amount
		// "objects" => $objects
	);
	return $fundraiserItem;
}

function _om_admin_init() {
	if (isset($_GET['fundraiser-csv']) == 'true') {
		// $fundraiserData = _om_fundraiser_report();
		$fundraiserData = [];
		$content = _om_arr_to_csv($fundraiserData);
		header("Content-type: application/x-msdownload",true,200);
		header("Content-Disposition: attachment; filename=FundraiserGoals.csv");
		header("Pragma: no-cache");
		header("Expires: 0");
		echo $content;
		exit(0);
	}
}
add_action('admin_init', '_om_admin_init');

function _om_template_redirect() {
	if ($_SERVER["REQUEST_URI"] === '/wp/wp-admin/downloads/fundraiser-csv') {
			// $fundraiserData = _om_fundraiser_report();
			$fundraiserData = [];
			$content = _om_arr_to_csv($fundraiserData);
			header("Content-type: application/x-msdownload",true,200);
			header("Content-Disposition: attachment; filename=FundraiserGoals.csv");
			header("Pragma: no-cache");
			header("Expires: 0");
			echo $content;
			exit(0);
	}
}
add_action( 'template_redirect', '_om_template_redirect' );

function _om_arr_to_csv($data) {
	// 	$fh = fopen('php://output', 'w') or die("Can't open php://output");
	$fh = fopen('php://temp', 'rw'); // use memory instead

	// write out the headers
	$arrKeys = array_keys(current($data));
	fputcsv($fh, $arrKeys);

	// write out the data
	foreach ( $data as $row ) {
		fputcsv($fh, $row);
	}
	rewind($fh);
	$content = stream_get_contents($fh);
	fclose($fh);
	return $content;
}

function _om_custom_reports_fundraisers() {
?>

	<style>
		button {
			font-weight: bold;
			padding: 5px 8px;
		}
		.date-filter {
			margin: 20px;
		}
		.date-filter input {
			width: 145px;
		}
		.date-filter button {
			vertical-align: bottom;
		}
		.csv-export {
		}
		.csv-export button {
			font-size: 1.2em !important;
			padding: 7px 10px;
		}
		table.gsa {
			text-align: center;
		}
	a.csv {
		display: none;
	}
	.progress-text {
		margin: 0;
	}
	.progressbar {
		width: 250px;
		height: 20px;
	}
	.status-wrapper {
		margin-left: 20px;
		margin: 10px 20px;
	}
	.status-wrapper label {
		display: inline-block;
		width: 150px;
		font-weight: bold;
	}
	</style>


	<div class="wrap">
	<form id="fundraiser_goal_form">

		<h2>Fundraiser Goal Export</h2>
		<div class="date-filter">
			<strong>Date Filter</strong>
			<br />
			<label>Start Date
				<input type="date" name="start_date" id="start_date" />
			</label>
			<label>End Date
				<input type="date" name="end_date" id="end_date" />
			</label>
			<br />
			<label>Start Time
				<input type="time" name="start_time" id="start_time" />
			</label>
			<label>End Time
				<input type="time" name="end_time" id="end_time" />
			</label>
			<button class="page-title-action clear">Clear</button>
		</div>
		<div class="load-status">
			<div class="progress-wrapper">
				<p class="progress-text">Loading fundraiser ids...</p>
				<progress class="progressbar" value="0" max="100"></progress>
				<button class="page-title-action ajax-action">Stop</button>
				<a class="page-title-action csv">CSV Export</a>
			</div>
			<div class="status-wrapper">
				<label>Loaded Records:</label>
				<span class="loaded-records"></span><br />
				<label>Total Records:</label>
				<span class="total-records"></span><br />
				<label>Total Query Time:</label>
				<span class="query-time"></span>
			</div>
		</div>

		<table class="gsa">
			<tr>
				<th>#</th>
				<th>Fundraiser ID</th>
				<th>Fundraiser Title</th>
				<th>Orders</th>
				<th>GSA</th>
				<th>Goal Amount</th>
				<th>Total Raised</th>
			</tr>
		</table>
	</form>

<?php
}


