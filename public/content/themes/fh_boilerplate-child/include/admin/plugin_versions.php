<?php

function _om_add_plugin_versions_menu_item() {
	if( is_super_admin() ) {
		add_submenu_page('custom-reports-admin-page.php', 'Plugin Version Manager', 'Plugin Version Manager', 'administrator', 'manage-plugin-versions', '_om_plugin_versions');

		if (isset($_GET['page']) && $_GET['page'] == 'manage-plugin-versions' ) {
      wp_register_script('magnific-popup', get_template_directory_uri() . '-child/js/vendor/jquery.magnific-popup.min.js');
			wp_register_script('manage-plugin-versions', get_template_directory_uri() . '-child/js/manage_plugin_versions.js', array('jquery', 'magnific-popup'));
			wp_enqueue_script('manage-plugin-versions');
			wp_localize_script('manage-plugin-versions', 'managePluginsObj', array(
				'ajaxUrl' => admin_url( 'admin-ajax.php')
			));
		}
	}
}
// set the arguments to get latest info from repository via API ##

add_action('admin_menu', '_om_add_plugin_versions_menu_item');

if (!function_exists('plugins_api')) {
  require_once(ABSPATH . 'wp-admin/includes/plugin-install.php');
}

function _om_get_plugin_api_data($plugin_slug) {
  $args = array(
    'slug' => $plugin_slug,
    'fields' => array('version' => true )
  );

  $call_api = plugins_api('plugin_information', $args );
  $ret_data = array(
    'version'       => '--',
    'last_updated'  => '--',
    'download_link' => '--',
    'requires'      => '--',
    'requires_php'  => '--',
    'tested'        => '--',
    'all_versions'  => '--'
  );

  if (is_wp_error($call_api)) {
    $api_error = $call_api->get_error_message();
    _dbg("** Plugin Error: $plugin_slug \n\t", $api_error);
  } else if (isset($call_api->code) && $call_api->code === 'rest_forbidden') {
    _dbg("** Plugin 401 Error: $plugin_slug => $call_api->message");
  } else if (!isset($call_api->name)) {
    _dbg("** Plugin Unknown Error: $plugin_slug => $call_api->message");
  } else {
    // name, slug, version, author, author_profile, requires, tested, requires_php, compatibility, rating, ratings, num_ratings, support_threads
    // downloaded, last_updated, added, homepage, sections[description], [faq], [changelog], [screenshots], download_link, screenshots, tags, versions, donate_link
    _dbg("Plugin Fetch Success: $plugin_slug v$call_api->version");
    if (! is_wp_error($call_api) && isset($call_api->version)) {
      $ret_data = array(
        'version'       => $call_api->version,
        'last_updated'  => isset($call_api->last_updated) ? $call_api->last_updated : '--',
        'download_link' => isset($call_api->download_link) ? $call_api->download_link : '--',
        'requires'      => isset($call_api->requires) ? $call_api->requires : '--',
        'requires_php'  => isset($call_api->requires_php) ? $call_api->requires_php : '--',
        'tested'        => isset($call_api->tested) ? $call_api->tested : '--',
        'all_versions'  => isset($call_api->versions) ? $call_api->versions : '--'
      );
    }
  }
  return $ret_data;
}

function _om_plugin_versions() {
  global $wp_version;
  global $wpdb;

  // communicate with wordpress plugin API: https://code.tutsplus.com/tutorials/communicating-with-the-wordpressorg-plugin-api--wp-33069
  // get most recent wordpress version
  $url = 'https://api.wordpress.org/core/version-check/1.7/';
  $response = wp_remote_get($url);

  $wp_version_obj = json_decode($response['body']);
  $wp_latest = $wp_version_obj->offers[0];
  // version, download, locale, packages[full], current, version, php_version, mysql_version, new_bundled, partial_version, new_files

	// existing plugin data
  $plugins = get_plugins();
  $table_out = '';

  $deprecated = array(
    'wpmandrill',
    'nav-menu-roles',
    'gravityformsuserregistration',
    'gravityformsmailchimp'
  );

  $custom_ignore = array(
    'switch-username',
    'onemission-admin-fundraiser'
  );

  $lookup_swap = array(
    'acf' => 'advanced-custom-fields',
    'capsman-enhanced' => 'capability-manager-enhanced'
  );

  $notes = array(
    'acf-field-date-time-picker' => 'Control used for special form fields like fundraiser start / end time',
    'capsman' => 'Allows role creation and fine tuned user permissions under "Users -> Capabilities" menu.',
    'custom-post-type-ui' => 'Used for Help Center Articles (Post Types and Taxonomies).  Accessed through "CPT UI" panel.',
    'disable-wordpress-updates' => 'This should always be active to prevent breaking changes being applied from updates',
    'google-captcha' => 'Prevents SPAM comment entries.  Pro version available but not needed.',
    'gravityforms' => 'Used throughout site for form creation, added to pages with [gravity form] syntax in html.',
    'gravityformsmailchimp' => 'Part of Basic Mail Chimp License Package.  Question: Is this being used?',
    'gravityformsuserregistration' => 'Part of Elite Mail Chimp License Package.  Can this be removed? Account creation only happens with create account modal dialog.',
    'nav-menu-roles' => 'Does not appear to be used, theme uses NavWalker class to implement twitter like nav menus https://github.com/wp-bootstrap/wp-bootstrap-navwalker',
    'query-monitor' => 'Development tool to analyze database query performance.',
    'raw-html' => 'Uses [raw] or <--raw--> tags. Is this being used anywhere?',
    'statsd' => 'Client interface to submit statsD metric information for logging at https://stats.onemission.fund',
    'user-switching' => 'Allows fast user switching in admin area for super users.',
    'widget-context' => 'Allows widgets to be conditionally shown based on content section, post type, url, etc.',
    'wordpress-social-login' => 'Facilitates account creation / logging in through social media.  Currently buttons hidden on signup / signin page due to Facebook CORS issues but backend functionality already finished, should disable until buttons brought back.',
    'wp-crontrol' => 'Development tool to analyze queued cron tasks.',
    'wp-super-cache' => '**Currently not configured ** Caches content for faster user end experience.',
    'wp-optimize' => 'Development tool for automatic database cleaning.',
    'wpmandrill' => 'Sends emails through Mandrill API (https://mandrillapp.com/api/docs/).  Can this be removed?',
    'wordpress-seo' => 'XML Sitemaps, breadcrumbs, canonical URLs to avoid duplicate content, title and meta descriptiuon templating, google snippet previews'
  );

  // assign manual look up slugs
  $manual_versions = array();

  $woocommerce_slugs = array(
    'woocommerce-checkout-add-ons',
    'woocommerce-customer-order-csv-export',
    'woocommerce-subscriptions',
    'woocommerce-shipping-fedex',
    'woocommerce-shipping-usps',
    'woocommerce-shipment-tracking',
    'woocommerce-smart-coupons',
  );
  $wc_RE = '/- version (\d+.\d+.\d+)/';

  foreach ($woocommerce_slugs as $wcslug) {
    $wc_url = "http://dzv365zjfbd8v.cloudfront.net/changelogs/$wcslug/changelog.txt";
    $wc_response = wp_remote_get($wc_url);
    preg_match($wc_RE, $wc_response['body'], $wc_matches);
    $manual_versions[$wcslug] = $wc_matches[1];
  }

  // $php_version = phpversion();
  $php_version = PHP_MAJOR_VERSION . "." . PHP_MINOR_VERSION . "." . PHP_RELEASE_VERSION;
  $mysql_version = $wpdb->db_version();
  // get latest php version from changelog
  // wget 'https://php.net/ChangeLog-5.php' -qO -|grep h3|sed 's/<[^<>]*>//g'
  $php_url = 'https://php.net/ChangeLog-5.php';
  $php_response = wp_remote_get($php_url);
  // _dbg("PHP RESPONSE", $php_response);
  preg_match('/\{\{\{ (\d+.\d+.\d+)/', $php_response['body'], $php_matches);
  $php_latest_version = $php_matches[1];

  $totals = array('curr' => 0, 'unknown' => 0, 'behind' => 0, 'all' => 0);

  foreach($plugins as $plugin) {
    $slug = $plugin['TextDomain'];
    if (array_key_exists($slug, $lookup_swap)) {
      $slug = $lookup_swap[$slug];
    }
    $totals['all']++;
    if (!in_array($slug, $custom_ignore)) {

      if (array_key_exists($slug, $manual_versions)) {
        $latest = array(
          'version' => $manual_versions[$slug],
          'all_versions' => '--',
          'last_updated' => '--',
          'requires' => '--',
          'tested' => '--'
        );
      } else {
        $latest = _om_get_plugin_api_data($slug);
      }

      // assign class based on how far behind
      $version_class = '';
      if ($latest['version'] === $plugin['Version']) {
        $version_class = 'current';
        $totals['curr']++;
      } else if ($latest['version'] == '--') {
        $version_class = 'unknown';
        $totals['unknown']++;
      } else {
        $latest_version_parts = explode('.', $latest['version']);
        $current_version_parts = explode('.', $plugin['Version']);
        $version_class = 'behind-major';
        if ($latest_version_parts[1] === $current_version_parts[1]) {
          $version_class = 'behind-patch';
        } else if ($latest_version_parts[0] === $current_version_parts[0]) {
          $version_class = 'behind-minor';
        }
        $totals['behind']++;
      }

      if (in_array($slug, $deprecated)) {
        $version_class = 'deprecated';
      }

      $note = '';
      if (array_key_exists($slug, $notes)) {
        $note = "<a href='javascript:alert(\"" . $notes[$slug] . "\")'>Note</a>";
      }

      $latest_version = $latest['version'];
      if ($latest['all_versions'] !== '--') {
        $latest_version = "<a class='download' href='#' " .
          "data-current-version='" . $plugin['Version'] .  "' " .
          "data-latest-version='" . $latest['version'] .  "' " .
          "data-name='" . $plugin['Name'] .  "' " .
          "data-versions='" . json_encode($latest['all_versions']) . "'>" .
          $latest['version'] . "</a>";
      }


      $last_updated = $latest['last_updated'] == '--'
        ? '--'
        : date('m-d-Y', strtotime($latest['last_updated']));

      $table_out .= "" .
        "<tr>" .
          "<td class='name'>" . $plugin['Name'] . "</td>" .
          "<td class='current_version " . $version_class . "'>" . $plugin['Version'] . "</td>" .
          "<td class='latest_version'>" . $latest_version . "</td>" .
          "<td class='last_updated'>" . $last_updated . "</td>" .
          "<td class='requires_version'>" . $latest['requires'] . "</td>" .
          "<td class='tested_version'>" . $latest['tested'] . "</td>" .
          "<td class='uri'><a target='_blank' href='" . $plugin['PluginURI'] . "'>Link</a></td>" .
          "<td class='description' title='" . strip_tags($plugin['Description']) . "'>" . $plugin['Description'] . "</td>" .
          "<td class='notes'>" . $note. "</td>" .
        "</tr>";
    }
  }

?>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>-child/css/magnific-popup.css" />
<style>

  .dialog {
    margin-left: auto;
    margin-right: auto;
    background: white;
    padding: 20px;
    position: relative;
    width: max-content;
  }

  .dialog button.close {
    margin-left: auto !important;
    margin-right: auto !important;
    margin-top: 10px;
    display: block;
  }

  .dialog p.status {
    display: none;
    text-align: center;
    font-weight: bold;
  }

  .dialog .plugin-versions {
    max-height: 400px;
    overflow-y: auto;
    width: max-content;
  }

  .dialog .plugin-versions table {
    border-collapse: collapse;
  }

  .dialog .plugin-versions table td {
    padding: 2px 5px;
  }

  .dialog .plugin-versions .latest {
    background-color: #b2e0b2;
  }

  .dialog .plugin-versions .current {
    background-color: orange;
  }

  .wrap h2 {
    text-align: center;
  }

  #plugin_versions {
    width: 1100px;
  }

  .totals_wrapper {
    margin-left: auto;
    margin-right: auto;
    overflow: auto;
    width: fit-content;
  }

  table#plugin_totals,
  table#system_versions {
    border: solid 1px #ccc;
    width: 200px;
    margin: 25px;
    float: left;
  }

  table#plugin_totals .total ,
  table#system_versions .version {
    text-align: center;
  }

  table#plugin_totals tr:nth-child(even),
  table#system_versions tr:nth-child(even) {
    background-color: #ddd;
  }

  #plugin_versions table#plugin_list {
    margin-left: auto;
    margin-right: auto;
    border-collapse: collapse;
    white-space: nowrap;
    table-layout: fixed;
    width: 1000px;
  }

  #plugin_versions table#plugin_list tbody tr:nth-child(even) {
    background-color: #ddd;
  }

  #plugin_versions table#plugin_list thead {
    border-bottom: solid 3px black;
  }

  #plugin_versions table#plugin_list tbody {
    overflow: auto;
  }

  #plugin_versions table#plugin_list tr {
    height: 2.0em;
  }

  #plugin_versions table#plugin_list td {
    overflow: hidden;
    text-overflow: ellipsis;
  }

  #plugin_versions table#plugin_list .description {
    width: 300px;
  }

  #plugin_versions table#plugin_list .notes,
  #plugin_versions table#plugin_list .uri {
    width: 50px;
    text-align: center;
  }
  #plugin_versions table#plugin_list .last_updated {
    width: 100px;
    text-align: center;
  }

  #plugin_versions table#plugin_list .requires_version,
  #plugin_versions table#plugin_list .tested_version,
  #plugin_versions table#plugin_list .latest_version,
  #plugin_versions table#plugin_list .current_version {
    width: 75px;
    text-align: center;
  }

  #plugin_versions table#plugin_list td.current_version {
    font-weight: bold;
    color: white;
  }

  #plugin_versions #legend {
    border: solid 1px #ccc;
    margin-left: 5em;
    font-size: 0.85em;
    margin-top: -10em;
  }

  #plugin_versions #legend .color {
    width: 15px;
    border-radius: 2px;
  }

  #plugin_versions td.unknown {
    background-color: darkgray;
  }
  #plugin_versions td.behind-major {
    background-color: red;
  }
  #plugin_versions td.behind-minor {
    background-color: #CC8800;
  }
  #plugin_versions td.behind-patch {
    background-color: #00AA44;
  }
  #plugin_versions td.current {
    background-color: #007700;
    }
  #plugin_versions td.deprecated {
    /* background-color: #00AA44; */
    background-color: #007700;
  }

</style>

	<div class="wrap">
    <!-- plugin versions dialog-->
    <div id="plugin-versions-dialog" class="dialog white-popup-block mfp-hide">
      <h4 class="text-center subheader"></h4>
      <div class="plugin-versions">
      </div>
      <div class="buttons clearfix wrap">
        <button class="page-title-action ajax-action btn-lg btn-block btn-primary close">
          CLOSE
        </button>
      </div>
    </div>


    <h2 style="max-width: 1100px">Manage Plugin Versions</h2>
    <form id="plugin_versions">
      <div class="totals_wrapper">
        <table id="system_versions">
          <tr>
            <th colspan="3">System</th>
          </tr>
          <tr>
            <th></th>
            <th>Current</th>
            <th>Latest</th>
          </tr>
          <tr>
            <td>Wordpress</td>
            <td class="version"><?php echo $wp_version; ?></td>
            <td class="version"><?php echo $wp_latest->version; ?></td>
          </tr>
          <tr>
            <td>PHP</td>
            <td class="version"><?php echo $php_version; ?></td>
            <td class="version"><?php echo $php_latest_version; ?></td>
          </tr>
          <tr>
            <td>MySQL</td>
            <td class="version"><?php echo $mysql_version; ?></td>
            <td class="version">8.0.13</td>
          </tr>
        </table>

        <table id="plugin_totals">
          <tr>
            <th colspan="2">Plugins</th>
          </tr>
          <tr>
            <td>Current</td>
            <td class="total"><?php echo $totals['curr']; ?></td>
          </tr>
          <tr>
            <td>Behind</td>
            <td class="total"><?php echo $totals['behind']; ?></td>
          </tr>
          <tr>
            <td>Unknown</td>
            <td class="total"><?php echo $totals['unknown']; ?></td>
          </tr>
          <tr>
            <td>Total</td>
            <td class="total"><?php echo $totals['all']; ?></td>
          </tr>
        </table>
      </div>
      <table id="legend">
        <tr>
          <th colspan="2">Legend</th>
        </tr>
        <tr>
          <td class="color current"></td>
          <td>Current</td>
        </tr>
        <tr>
          <td class="color behind-patch"></td>
          <td>Behind Patch</td>
        </tr>
        <tr>
          <td class="color behind-minor"></td>
          <td>Behind Minor</td>
        </tr>
        <tr>
          <td class="color behind-major"></td>
          <td>Behind Major</td>
        </tr>
      </table>
      <table id="plugin_list">
        <thead>
          <th class="name">Name</th>
          <th class="current_version">Current <br />Version</th>
          <th class="latest_version">Latest <br />Version</th>
          <th class="last_updated">Last<br />Updated</th>
          <th class="requires_version">Requires<br />WP Version</th>
          <th class="tested_version">Tested<br />WP Version</th>
          <th class="uri">Plugin<br />Link</th>
          <th class="description">Description</th>
          <th class="notes">Site<br />Notes</th>
        </thead>
        <tbody>
        <?php echo $table_out; ?>
        </tbody>
      </table>
    </form>
<?php
}


