<?php

global $wpdb;

function add_dev_import_menu_item() {
	add_options_page('Developer Sites Sync', 'Dev Site Sync', 'administrator', 'devsite-admin-page.php', 'devsite_admin_page');
}
add_action('admin_menu', 'add_dev_import_menu_item');
// admin menu items for dev import

function devsite_admin_page() {
	// TODO: store credentials somewhere safer (probably intiialize global db objects in bootstrap)
	global $wpdb;
	$dev_db = new wpdb('om_site_usr', 'YOEebwNHmPgmQrhb9BAH', 'one_mission_dev', 'localhost');
	$dev_db->woocommerce_order_items = 'wp_woocommerce_order_items';
	$dev_user_count = $dev_db->get_var( "SELECT COUNT(*) FROM $wpdb->users" );
	$dev_post_count = $dev_db->get_var( "SELECT COUNT(*) FROM $wpdb->posts" );
	$dev_comments_count = $dev_db->get_var( "SELECT COUNT(*) FROM $wpdb->comments" );
	$dev_orders_count = $dev_db->get_var( "SELECT COUNT(*) FROM $dev_db->woocommerce_order_items" );

	$prod_db = new wpdb('om_site_usr', 'YOEebwNHmPgmQrhb9BAH', 'one_mission_wp', 'localhost');
	$prod_db->woocommerce_order_items = 'wp_woocommerce_order_items';
	$prod_user_count = $prod_db->get_var( "SELECT COUNT(*) FROM $wpdb->users" );
	$prod_post_count = $prod_db->get_var( "SELECT COUNT(*) FROM $wpdb->posts" );
	$prod_comments_count = $prod_db->get_var( "SELECT COUNT(*) FROM $wpdb->comments" );
	$prod_orders_count = $prod_db->get_var( "SELECT COUNT(*) FROM $prod_db->woocommerce_order_items" );


	?>
<style>
	#dev-site h2 {
		margin-bottom: 30px;
	}
	#dev-site table {
		text-align: center;
		border-collapse: collapse;
	}
	#dev-site table td:first-child {
		text-align: left;
	}
	#dev-site table th,
	#dev-site table td {
		padding: 3px 8px;
	}
	#dev-site table th {
	text-decoration: underline;
	}
	#dev-site table td {
		border-bottom: dashed 1px #aaa;
	}
</style>
	<div class="wrap" id="dev-site">
		<h2>Dev Site Synchronization Tool</h2>
		<table id="dev-site-table">
			<thead>
				<tr>
					<th></th>
					<th>Dev Item Count</th>
					<th>Live Item Count</th>
					<th>Last Sync</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Users</td>
					<td><?php echo $dev_user_count; ?></td>
					<td><?php echo $prod_user_count; ?></td>
					<td>9/30/16 9:33 PM</td>
					<td><button type="button">Sync</button></td>
				</tr>
				<tr>
					<td>Posts / Products</td>
					<td><?php echo $dev_post_count; ?></td>
					<td><?php echo $prod_post_count; ?></td>
					<td>9/30/16 9:33 PM</td>
					<td><button type="button">Sync</button></td>
				</tr>
				<tr>
					<td>Comments</td>
					<td><?php echo $dev_comments_count; ?></td>
					<td><?php echo $prod_comments_count; ?></td>
					<td>9/30/16 9:33 PM</td>
					<td><button type="button">Sync</button></td>
				</tr>
				<tr>
					<td>Orders</td>
					<td><?php echo $dev_orders_count; ?></td>
					<td><?php echo $prod_orders_count; ?></td>
					<td>9/30/16 9:33 PM</td>
					<td><button type="button">Sync</button></td>
				</tr>
				<tr>
					<td>Uploads</td>
					<td>0</td>
					<td>0</td>
					<td>9/30/16 9:33 PM</td>
					<td><button type="button">Sync</button></td>
				</tr>
			</tbody>
		</table>
	</div>
	<?php
}

