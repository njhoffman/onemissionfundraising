<?php
function _om_admin_remove_menu_items() {
	if( is_super_admin() ) {
		remove_menu_page( 'yst_ga_dashboard' );      // Yoast google analytics
		remove_menu_page( 'plugins.php' );                //Plugins
		// remove_menu_page( 'upload.php' );                // Media
		remove_menu_page( 'themes.php' );                 //Appearance
		// remove_menu_page( 'options-general.php' );        //Settings
		remove_menu_page( 'i4t3-logs' );        // 404 logs
		remove_menu_page( 'wpseo_dashboard' );        // SEO
		remove_menu_page( 'google-captcha.php' );        // Google captcha
		// ?? remove_menu_page( 'ajax-search-lite' );        // Ajax search lite settings

		// remove_menu_page( 'index.php' );                  //Dashboard
		// remove_menu_page( 'jetpack' );                    //Jetpack*
		// remove_menu_page( 'edit.php' );                   //Posts
		// remove_menu_page( 'edit.php?post_type=page' );    //Pages
		// remove_menu_page( 'edit-comments.php' );          //Comments
		// remove_menu_page( 'users.php' );                  //Users
		// remove_menu_page( 'tools.php' );                  //Tools
	}
}
add_action('admin_menu', '_om_admin_remove_menu_items');

// custom post type settings
remove_action( 'admin_menu', 'cptui_plugin_menu' );

// add header to indicate active database

function _om_admin_header_database() {
  global $wpdb;

  if (is_super_admin()) {
    $style = 'display: inline-block; color: white; padding: 3px 8px; margin-top: 5px; font-weight: bold; border-radius: 0px 10px 10px 0px;';
    if ($wpdb->dbname === "one_mission_dev") {
      echo "<span style='$style background-color: blue'>Dev Database</span>";
    } else {
      echo "<span style='$style background-color: red'>Live Database</span>";
    }
  }
}

add_action('in_admin_header', '_om_admin_header_database');
