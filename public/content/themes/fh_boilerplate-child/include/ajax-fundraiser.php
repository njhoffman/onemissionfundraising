<?php

/**
 * AJAX Start Fundraiser
 *
 */

// includes client js file
function _om_ajax_fundraiser_init(){
	wp_register_script( 'jquery-ui', get_template_directory_uri() . '-child/js/vendor/jquery-ui.min.js', array('jquery') );
	wp_enqueue_script( 'jquery-ui' );

	wp_register_script('fundraiser-dialog-script', get_template_directory_uri() . '-child/js/dialogs/fundraiser.js', array('jquery') );
    wp_enqueue_script('fundraiser-dialog-script');
    wp_register_script('fundraiser-admin-script', get_template_directory_uri() . '-child/js/fundraiser_admin.js', array('jquery', 'jquery-ui') );
    wp_enqueue_script('fundraiser-admin-script');

	 wp_localize_script( 'fundraiser-dialog-script', 'fundraiserObj', array(
        'ajaxUrl' => admin_url( 'admin-ajax.php' )
    ));

	 wp_localize_script( 'fundraiser-admin-script', 'fundraiserObj', array(
        'ajaxUrl' => admin_url( 'admin-ajax.php' )
    ));

	 add_action( 'wp_ajax_ajax_create_fundraiser', '_om_ajax_create_fundraiser');
	 add_action( 'wp_ajax_ajax_update_fundraiser', '_om_ajax_update_fundraiser');
	 add_action( 'wp_ajax_ajax_submit_fundraiser', '_om_ajax_submit_fundraiser');
	 add_action( 'wp_ajax_ajax_update_post_image', '_om_ajax_update_post_image');
}

if (is_user_logged_in()) {
	add_action('init', '_om_ajax_fundraiser_init');
}

function _om_ajax_update_post_image() {

	if (!function_exists('wp_generate_attachment_metadata')){
		require_once(ABSPATH . "wp-admin" . '/includes/image.php');
		require_once(ABSPATH . "wp-admin" . '/includes/file.php');
		require_once(ABSPATH . "wp-admin" . '/includes/media.php');
	}

	if (isset($_FILES['post_image'])) {
		$post_id = $_POST['post_id'];
		$attach_id = media_handle_upload( 'post_image',  $post_id );
		$full_path = get_attached_file($attach_id);
		$attach_data = wp_generate_attachment_metadata($attach_id, $full_path);
		wp_update_attachment_metadata($attach_id, $attach_data);
		if (is_wp_error($attach_id)) {
			_om_warn('Upload post image error: ' . $attach_id->get_error_messages());
		} else if ($attach_id > 0) {
			$file_url = wp_get_attachment_url($attach_id);
			_om_log('Upload post image success: ' . $attach_id . ' ' . $file_url);
			$ret = set_post_thumbnail($post_id, $attach_id);
		}
	}
}

function _om_ajax_create_fundraiser() {
	global $HubSpotApi;

	 // check required fields
	 $required_fields = array(
		 "name"    => "Title",
		 "type"    => "Type",
		 "country" => "Country"
	 );
	 $required_missing = array();
	 foreach ($required_fields as $key => $value) {
		 if ( empty($_POST[$key]) ) {
			 array_push($required_missing, $value);
		 }
	 }
	 if (count($required_missing) > 0) {
		 echo json_encode(array(
			 'loggedIn' => false,
			 'message' => __('Following fields are required: ' . implode($required_missing, ", "))
		 ));
		 die();
	 }

	 $user_id = get_current_user_id();
	 $user = new WP_User($user_id);
	 $user->remove_role('administrator');
	 $user->remove_role('customer');
	 $user->add_role('fundraiser');
	 // $user->add_role('administrator');


	 // create fundraiser and associate with user
	 $user_meta = get_user_meta( $user_id );
	 $fundraiser_title = stripcslashes($_POST["name"]);
	 $fundraiser_type = $_POST["type"];
	 $fundraiser_city = $_POST["city"];
	 $fundraiser_state = $_POST["state"];
	 $fundraiser_country = $_POST["country"];

	 // Create pages and associate with user
	 $profile_page = array(
		 'post_title' => $fundraiser_title,
		 'post_content' => '',
		 'post_status' => 'draft',
		 'post_type' => 'page',
		 'comment_status' => 'open',
		 'post_author' => $user_id,
		 'post_parent' => OM_FUNDRAISER_PAGE_PARENT_ID,
		 'page_template' => OM_FUNDRAISER_PAGE_TEMPLATE
	 );

	 $profile_page_id = wp_insert_post( $profile_page, TRUE );

	 // Add in association for fundraiser profile ID within meta field for user
	 update_user_meta( $user_id, 'fundraiser_profile_id', $profile_page_id );

	 update_field("fundraiser_type", $fundraiser_type, $profile_page_id);
	 update_field("fundraiser_city", $fundraiser_city, $profile_page_id);
	 update_field("fundraiser_state", $fundraiser_state, $profile_page_id);
	 update_field("fundraiser_country", $fundraiser_country, $profile_page_id);
	 update_field("fundraiser_timed_or_continuous", "timed", $profile_page_id);
	 update_field("show_fundraiser_status", "1", $profile_page_id);

	 $fund_creation_progress = get_user_meta($user_id, 'fund_creation_progress')[0];
	 $fund_creation_progress[1] = true;
	 update_user_meta( $user_id, 'fund_creation_status', 'In Progress');
	 update_user_meta($user_id, 'fund_creation_progress', $fund_creation_progress);

	 // update hubspot contact fields
	 $hubspot_fields = array(
		 array('property' => 'fund_creation_status', 'value' => 'In Progress'),
		 array('property' => 'fund_creation_progress', 'value' => '1'),
		 array('property' => 'fund_name', 'value' => $fundraiser_title),
		 array('property' => 'fundraiser_city', 'value' => $fundraiser_city),
		 array('property' => 'fundraiser_state', 'value' => $fundraiser_state),
		 array('property' => 'fundraiser_country', 'value' => $fundraiser_country),
		 array('property' => 'fundraiser_type', 'value' => str_replace('&', ';', $fundraiser_type))
	 );

	 $user_info = get_userdata($user_id);
	 $hs_update = $HubSpotApi->update_contact_by_email($user_info->user_email, $hubspot_fields);

	 // Add the "is_featured" meta value as false
	 // - Needs to be there for fundraiser profile browsing
	 // - For some reason this needs to be an ACF key
	 // - As per http://bit.ly/1t18kyW
	 update_field( 'field_535e97b700bd8', 0, $profile_page_id );

	// send email notification
	$to = get_field('fundraiser_created_email', 'options');
	$subject = "New fundraiser created.";
	$message = "<a href='" . admin_url('post.php?post=' . $profile_page_id . '&action=edit')
		. "'>Click here</a> to view the fundraiser created, page ID: " . $profile_page_id;
	$message .= "<br /><br /><h3>Fundraiser Fields</h3>";

	$message .= "<strong>Type: </strong>" . $fundraiser_type . '<br />';
	$message .= "<strong>City: </strong>" . $fundraiser_city . '<br />';
	$message .= "<strong>State: </strong>" . $fundraiser_state . '<br />';
	$message .= "<strong>Country: </strong>" . $fundraiser_country . '<br />';
	$message .= "<br /><br /><h3>User Fields</h3>";

	$ignored_fields = [
		'use_ssl',
		'show_admin_bar_front',
		'wp_capabilities',
		'admin_color',
		'rich_editing',
		'session_tokens',
		'dismissed_wp_pointers',
		'comment_shortcuts',
		'manageedit-shop_ordercolumnshidden',
	];

	foreach ($user_meta as $key => $value) {
		if (!in_array($key, $ignored_fields) && strpos($key, '_') != 0) {
			$pretty_key = ucfirst(str_replace('_', ' ', $key));
			$message .= "<strong>" . $pretty_key . "</strong>: " . $value[0] . "<br />";
		}
	}
	wp_mail($to, $subject, $message);
	_om_log('Fundraiser created: ' . $profile_page_id);

	wp_send_json_success(array('role' => implode($user->roles)));
}

function _om_ajax_submit_fundraiser() {
	global $HubSpotApi;
	$user_id = get_current_user_id();
	$profile_id = get_user_meta( $user_id, 'fundraiser_profile_id' )[0];
	$fundraiser_post = get_page($profile_id);
	$fundraiser_meta = get_post_meta($fundraiser_post->ID);
	update_field("pending_approval", date('m/d/Y h:m a'), $profile_id);
	wp_update_post( array(
		'ID' => $profile_id,
		'post_status' => 'pending'
	));
	$pending_approval = get_field("pending_approval", $profile_id);
	update_user_meta( $user_id, 'fund_creation_status', 'Pending');

	// send email notification
	$to = get_field('fundraiser_submitted_email', 'options');
	$subject = "Fundraiser submitted for approval.";
	$message = "<a href='" . admin_url('post.php?post=' . $profile_id . '&action=edit')
		. "'>Click here</a> to view the fundraiser for submitted page ID: " . $profile_id;
	$message .= "<br /><br /><h3>Fundraiser Fields</h3>";

	$ignored_fields = [
		"pinged",
		"post_content",
		"post_excerpt",
		"comment_status",
		"ping_status",
		"post_password",
		"to_ping",
		"post_content_filtered",
		"menu_order",
		"post_mime_type",
		"comment_count"
		];

	foreach ($fundraiser_post as $key => $value) {
		if (!in_array($key, $ignored_fields) && strpos($key, '_') != 0) {
			$pretty_key = ucfirst(str_replace('_', ' ', $key));
			$message .= "<strong>" . $pretty_key . "</strong>: " . $value . "<br />";
		}
	}
	$message .= "<br /><br /><h3>Custom Fields</h3>";
	foreach ($fundraiser_meta as $key => $value) {
		if (!in_array($key, $ignored_fields) && strpos($key, '_') != 0) {
			$pretty_key = ucfirst(str_replace('_', ' ', $key));
			$message .= "<strong>" . $pretty_key . "</strong>: " . $value[0] . "<br />";
		}
	}
	wp_mail($to, $subject, $message);

	// submit fundraiser status update through hubspot
	$user_info = get_userdata($user_id);
	$hs_fields = array();
	array_push($hs_fields, array( 'property' => 'fund_creation_progress', 'value' => '9' ));
	array_push($hs_fields, array( 'property' => 'fund_creation_status', 'value' => 'Completed' ));
	$hs_update = $HubSpotApi->update_contact_by_email($user_info->user_email, $hs_fields);

	_om_log('Fundraiser submitted: ' . $profile_id);

	wp_send_json_success(
		array(
			'pendingApproval' => $pending_approval
		)
	);
}

function _om_ajax_update_fundraiser() {
	global $HubSpotApi;
	$user_id = get_current_user_id();
	$profile_id = get_user_meta( $user_id, 'fundraiser_profile_id' )[0];
	$fundraiser_post = get_page($profile_id);
	$updated_fields = "";
	$current_step = $_POST['current_step'];
	$hubspot_fields = array();

	// good intentions, not really worth it, still need to maintain ugh
	$fieldValidations = Array(
		'fundraiser_start_date'          => Array('date'),
		'fundraiser_end_date'            => Array('date'),
		'fundraiser_goal_amount'         => Array('numeric'),
		'fundraiser_website'             => Array('string'),
		'fundraiser_facebook'            => Array('string'),
		'fundraiser_twitter'             => Array('string'),
		'fundraiser_instagram'           => Array('string'),
		'fundraiser_description'         => Array('string'),
		'thank_you_message'              => Array('string'),
		'for_profit'                     => Array(),
		'enable_501c'                    => Array(),
		'document_501c'                  => Array(),
		'support_email_notifications'    => Array(),
		'image_or_youtube_banner'        => Array(),
		'banner_youtube_video_id'        => Array(),
		'leader_organization'            => Array(),
		'leader_address'                 => Array(),
		'leader_city'                    => Array(),
		'leader_state'                   => Array(),
		'_yoast_wpseo_opengraph-image'   => Array(),
		'logo_image'                     => Array(),
		'leader_zip'                     => Array(),
		'leader_phone'                   => Array(),
		'payee_check_name'               => Array(),
		'payee_name'                     => Array(),
		'payee_organization'             => Array(),
		'payee_address'                  => Array(),
		'payee_city'                     => Array(),
		'payee_state'                    => Array(),
		'payee_zip'                      => Array(),
		'payee_phone'                    => Array(),
		'fundraiser_type'                => Array(),
		'fundraiser_city'                => Array(),
		'fundraiser_state'               => Array(),
		'fundraiser_country'             => Array(),
		'fundraiser_timed_or_continuous' => Array(),
		'mvp'                            => Array(),
		'bulk_free_shipping'             => Array(),
		'custom_apparel'                 => Array()
	);

	$failed = [];

	foreach ($fieldValidations as $key => $validations) {
		if (isset($_POST[$key])) {
			// custom validations

			if ($key == 'fundraiser_start_date' || $key == 'fundraiser_end_date') {
				if (!empty($_POST[$key]) && new DateTime('yesterday') >= new DateTime($_POST[$key])) {
					array_push($failed, Array(
						'key' => $key,
						'message' => 'Date must be in the future',
					));
					continue;
				}
			}

			if ($key == 'fundraiser_end_date' && !empty($_POST['fundraiser_end_date'])) {
				$start_date = isset($_POST['fundraiser_start_date']) ? new DateTime($_POST['fundraiser_start_date']) : null;
				if (!empty($start_date) && $start_date > new DateTime($_POST[$key])) {
					array_push($failed, Array(
						'key' => $key,
						'message' => 'End date must be after start date',
					));
					continue;
				}

				if (!empty($start_date) && $start_date->add(new DateInterval('P365D')) < new DateTime($_POST[$key])) {
					array_push($failed, Array(
						'key' => $key,
						'message' => 'End date must be within 365 days of start date',
					));
					continue;
				}

			}

			if ($key === 'fundraiser_description') {
				if (strlen(utf8_decode(trim(strip_tags($_POST[$key])))) < 200) {
					array_push($failed, Array(
						'key' => $key,
						'message' => 'Description must be longer than 200 characters',
					));
					continue;
				}
			}

			 update_field($key, $_POST[$key], $fundraiser_post->ID);
			 update_post_meta($fundraiser_post->ID, $key, $_POST[$key]);
			 // update hubspot field data
			 $hubspot_key = $key;
			 $hubspot_val = $_POST[$key];
			 if ($key === 'fundraiser_type') {
				 $hubspot_val = str_replace('&', ';', $hubspot_val);
			 } else if ($key === 'fundraiser_timed_or_continuous') {
				 $hubspot_key = 'continuous';
				 $hubspot_val = false;
			 } else if ($key === 'fundraiser_start_date' || $key === 'fundraiser_end_date') {
				 $hubspot_val = strtotime($hubspot_val) * 1000;
			 }

			 if (!in_array($key, $HubSpotApi->field_skip)) {
				 array_push($HubSpotApi->field_map, array( "property" => $hubspot_key, "value" => $hubspot_val ));
			 }
			 $updated_fields .= $key . ' (' . strlen($_POST[$key])  . ' chars), ';
		}
	}

	 if (count($failed) > 0) {
		 echo json_encode(
			 array('errors' => __($failed))
		 );
		 die();
	 }

	// non-meta tag fields, requires special handling
	if (isset($_POST['fundraiser_title'])) {
		$fundraiser_title = stripcslashes($_POST['fundraiser_title']);
		$fundraiser_name = isset($_POST['fundraiser_name']) ? stripcslashes($_POST['fundraiser_name']) : $fundraiser_title;
		wp_update_post( array(
			'ID' => $fundraiser_post->ID,
			'post_title' => $fundraiser_title,
			'post_name' => $fundraiser_name
		));
		$fundraiser_url = get_permalink($fundraiser_post);
		array_push($hubspot_fields, array( "property" => "fund_name", "value" => $fundraiser_title));
		array_push($hubspot_fields, array( "property" => "fund_id", "value" => $fundraiser_post->ID));
		array_push($hubspot_fields, array( "property" => "fund_permalink", "value" => $fundraiser_url));
		array_push($hubspot_fields, array( "property" => "shop_for_link", "value" => $fundraiser_url));
		$updated_fields .= 'fundraiser_title (' . strlen($fundraiser_title) . ' chars), ';
	} else if (isset($_POST['fundraiser_name'])) {
		$fundraiser_title = isset($_POST['fundraiser_title'])
			? stripcslashes($_POST['fundraiser_title'])
			: stripcslashes($_POST['fundraiser_name']);
		wp_update_post( array(
			'ID' => $fundraiser_post->ID,
			'post_name' => $fundraiser_title
		));
		$fundraiser_url = get_permalink($fundraiser_post);
		array_push($hubspot_fields, array( "property" => "fund_name", "value" => $fundraiser_title));
		array_push($hubspot_fields, array( "property" => "fund_id", "value" => $fundraiser_post->ID));
		array_push($hubspot_fields, array( "property" => "fund_permalink", "value" => $fundraiser_url));
		array_push($hubspot_fields, array( "property" => "shop_for_link", "value" => $fundraiser_url));
		$updated_fields .= 'fundraiser_title (' . strlen($fundraiser_title) . ' chars), ';
	}

	if (!function_exists('wp_generate_attachment_metadata')){
		require_once(ABSPATH . "wp-admin" . '/includes/image.php');
		require_once(ABSPATH . "wp-admin" . '/includes/file.php');
		require_once(ABSPATH . "wp-admin" . '/includes/media.php');
	}

	if (isset($_FILES['banner_image'])) {
		$attach_id = media_handle_upload( 'banner_image',  $fundraiser_post->ID );
		if (is_wp_error($attach_id)) {
			_om_warn('Upload banner image error: ' . $attach_id->get_error_messages());
		} else if ($attach_id > 0) {
			$file_url = wp_get_attachment_url($attach_id);
			_om_log('Upload banner image success: '. $attach_id . ' ' . $file_url);
			update_post_meta($fundraiser_post->ID, 'banner_image', $attach_id);
		}
	}

	if (isset($_FILES['logo_image'])) {
		$attach_id = media_handle_upload( 'logo_image',  $fundraiser_post->ID );
		if (is_wp_error($attach_id)) {
			_om_warn('Upload logo image error: ' . $attach_id->get_error_messages());
		} else if ($attach_id > 0) {
			$file_url = wp_get_attachment_url($attach_id);
			_om_log('Upload logo image success: ' .  $attach_id . ' ' . $file_url);
			update_post_meta($fundraiser_post->ID, 'logo_image', $attach_id);
			$updated_fields .= 'logo_image (url: ' . $file_url . '), ';
		}
	}

	if (isset($_FILES['_yoast_wpseo_opengraph-image'])) {
		$attach_id = media_handle_upload( '_yoast_wpseo_opengraph-image',  $fundraiser_post->ID );
		if (is_wp_error($attach_id)) {
			_om_warn('Upload social image error: ' . $attach_id->get_error_messages());
		} else if ($attach_id > 0) {
			$file_url = wp_get_attachment_url($attach_id);
			_om_log('Uploaded social image success: ' . $attach_id . ' ' . $file_url);
			$res = update_post_meta($fundraiser_post->ID, 'yoast_wpseo_opengraph-image', $file_url);
			$updated_fields .= 'yoast_wpseo_opengraph-image (url: ' . $file_url . '), ';
		}
	}

	if (isset($_FILES['document_501c'])) {
		$attach_id = media_handle_upload( 'document_501c',  $fundraiser_post->ID );
		if (is_wp_error($attach_id)) {
			_om_warn('Upload 501c document error: ' . $attach_id->get_error_messages());
		} else if ($attach_id > 0) {
			$file_url = wp_get_attachment_url($attach_id);
			_om_log('Uploaded 501c document success: ' . $attach_id . ' ' . $file_url);
			update_post_meta($fundraiser_post->ID, 'document_501c', $attach_id);
			$updated_fields .= 'document_501c (url: ' . $file_url . '), ';
		}
	}


	$redirect_url = admin_url('?page=fundraiser-profile-editor');
	if (isset($_POST['submit_action']) && $_POST['submit_action'] == 'save_later') {
		$redirect_url = get_home_url();
	}

	// update custom user fundraiser status fields
	$fund_creation_progress = get_user_meta($user_id, 'fund_creation_progress')[0];
	$fund_creation_progress[$current_step] = true;
	update_user_meta($user_id, 'fund_creation_progress', $fund_creation_progress);

	// update hubspot contact fields
	$user_info = get_userdata($user_id);
	$hubspot_fund_progress = "";
	foreach ($fund_creation_progress as $fcp_key => $fcp_val) {
		if ($fcp_val === true && !empty($fcp_key)) {
			$hubspot_fund_progress .= "$fcp_key;";
		}
	}
	array_push($hubspot_fields, array( 'property' => 'fund_creation_progress', 'value' => $hubspot_fund_progress));
	$hs_update = $HubSpotApi->update_contact_by_email($user_info->user_email, $hubspot_fields);

	_om_log("fields " . $updated_fields . "|Updated fields success");
	wp_send_json_success(
		array(
			'updatedFields' => $updated_fields,
			'redirectUrl' => $redirect_url
		)
	);
}

?>
