<?php

// wordpress custom reports fundraisers / gsa monthly export

// global $wpdb;
// echo implode( ' ', $wpdb->tables() ) . '\n';

if( php_sapi_name() !== 'cli' ) {
     die("Meant to be run from command line");
 }

 function find_wordpress_base_path() {
     $dir = dirname(__FILE__);
     do {
         //it is possible to check for other files here
         if( file_exists($dir."/wp-config.php") ) {
             return $dir;
         }
     } while( $dir = realpath("$dir/..") );
     return null;
 }

 define( 'BASE_PATH', find_wordpress_base_path() . "/" );
 define('WP_USE_THEMES', false);
 global $wp, $wp_query, $wp_the_query, $wp_rewrite, $wp_did_header;
 require(BASE_PATH . 'wp/wp-load.php');

 add_filter( 'mandrill_payload', 'add_csv_as_attachment2' );
 function add_csv_as_attachment2( $message ) {
	 // TODO: queries and results parse differently when executing from command line, why and fuck you again wordpress
	 // $fundraiserData = _om_fundraiser_report();


	 // array_push( $message["attachments"], array(
		//  'type' => 'text/plain',
		//  'name' => 'FundraiserGoals.csv',
		//  'content' => $csvContents
	 // ) );
	 // // echo var_dump($message);
	 return $message;
 }

 $last_month_dt = strtotime(date('Y-m') . "last day of previous month");
 $last_month = date('F', $last_month_dt);
 $last_month_query = date('Y-m-d', $last_month_dt);
 $link = WP_SITEURL . "/wp-admin/admin.php?page=custom-reports-fundraisers.php" .
	 "&fundraiser-csv=true" .
	 "&end=" . $last_month_query;


 $body = "<p><a href='" . $link . "'>" .
	 "Click here</a> to access the " . $last_month . " fundraiser report.</p>";

 wp_mail( ["njhoffman1982@gmail.com", "john@onemissionfundraising.com"], $last_month . " Fundraiser Report", $body)


// // ob_end_clean() ?
?>
