<?php

	$by_type   = ( isset( $_GET['by-type'] ) ) ? sanitize_text_field( $_GET['by-type'] ) : false;
	$by_search = ( isset( $_GET['by-search'] ) ) ? sanitize_text_field( $_GET['by-search'] ) : false;

	// if search term provided, disregard type filtering - njh 11/17
	if ($by_search) {
		$by_type = false;
	}

	// Grab ACF fields for custom content blocks
	$acf_fields = get_fields();

	//
	//	Replace title block with browsing interface
	//
	function custom_title() {

		global $fundraiser_types;
		global $by_type;
		global $by_search;
		global $acf_fields;



		?>


			<div class="full-section clearfix space-8">

				<!-- if NOT by type and NOT by search -->
				<?php if ( ! $by_type && ! $by_search ) { ?>
				<div class="box bg-fade darkBlue" style="background-image: url('https://onemission.fund/content/themes/fh_boilerplate-child/img/om-testimonial-bg.webp');">
					<div class="box-content no-padding container">
						<div class="space-2">

							<?php
								echo '<div class="space-top-7"><h1 class="text-uppercase text-center text-white noborders space-0">Find a Fund</h1>';
								echo '<p class="lead text-center"><em>Who\'s world do you want to change?</em></p></div>';
							?>

							<div class="space-top-5 clearfix">
								<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 space-5 text-center">
									<form id="fundraiser_browse_tools" role="form" method="get" action="<?php echo get_permalink() ?>">
										<div class="row">
											<div class="col-sm-6 space-3">
												<div class="form-group space-3">
													<label for="fundraiser_browse_search" class="h4 text-white text-uppercase space-3 hidden-xs">Search By Name</label>
													<?php

														$value = ( $by_search ) ? $by_search : '';
													?>
													<input name="by-search" id="fundraiser_browse_search" type="text" class="form-control" placeholder="Search funds" value="<?php echo $value; ?>" />
												</div>
												<button type="submit" class="btn btn-outline btn-outline-white btn-lg hidden-xs">Search</button>
											</div>
											<div class="col-sm-6 space-3">
												<div class="form-group space-3">
													<label for="fundraiser_browse_select" class="h4 text-white text-uppercase space-3 hidden-xs">Browse By Type</label>
													<select name="by-type" id="fundraiser_browse_select" class="form-control">
														<option value="">Show All Fundraiser Types</option>
														<?php

															foreach ( $fundraiser_types as $key => $value ) {

																$selected = ( $by_type && $key == $by_type ) ? ' selected' : '';

																echo '<option value="' . $key . '"' . $selected . '>' . $value . '</option>';

															}
														?>
													</select>
												</div>
												<!-- first button is hidden on mobile and second is visible on mobile, for better UX -->
												<button type="submit" class="btn btn-outline btn-outline-white btn-lg hidden-xs">Browse</button>
												<button type="submit" class="btn btn-outline btn-outline-white btn-lg visible-xs">Search</button>
											</div>
										</div>
									</form>
								</div>
								<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 space-3">
									<h3 class="h4 text-white text-uppercase text-center space-3">Featured Fund</h3>
										<?php

                        $featured_fundraiser_id     = (int) get_field( 'featured_fundraiser_id', 'option' );
                        $featured_fundraiser_title  = get_the_title( $featured_fundraiser_id );
                        $featured_fundraiser_uri    = get_page_uri( $featured_fundraiser_id );
                        $featured_fundraiser_desc   = get_field( 'featured_fundraiser_description', 'option' );
                        $featured_fundraiser_fields = get_fields( $featured_fundraiser_id );
                        $featured_fundraiser_logo   = '';

                        // Fundraiser Logo
                        if( !empty( $featured_fundraiser_fields['logo_image']['sizes']['shop_single'] ) ) {

                            $featured_fundraiser_logo = '<a href="/' . $featured_fundraiser_uri . '"><img class="featured-img" src="' . $featured_fundraiser_fields['logo_image']['sizes']['thumbnail'] . '"></a>';
                        }

                        echo '<h5><strong><a class="text-white" href="/' . $featured_fundraiser_uri . '">' . $featured_fundraiser_title . '</a></strong></h5>';
                        echo $featured_fundraiser_logo;
                        echo '<p class="space-top-1 small">' . $featured_fundraiser_desc . ' <em><a href="/products/?shopfor=' . $featured_fundraiser_id . '">Shop to Support &rarr;</a></em></p>';

                        // Shop to support link
                        echo '<div class="text-center"></div>';
                    ?>
								</div>
							</div>


						</div>
					</div>
				</div>

				<? } ?>
				<!-- end if NOT by type and NOT by search -->

				<!-- if by type OR by search -->
				<?php if ( $by_type || $by_search ) { ?>

				<div class="fundraiser-browse-tools-banner">
				<form id="fundraiser_browse_tools" role="form" method="get" action="<?php echo get_permalink() ?>">
					<div class="row">
						<div class="col-lg-4 col-lg-offset-2 col-md-5 col-md-offset-1 col-sm-6 text-center">
							<div class="form-group">
								<?php
									$value = ( $by_search ) ? $by_search : '';
								?>
								<input name="by-search" id="fundraiser_browse_search" type="text" class="form-control" placeholder="Search funds" value="<?php echo $value; ?>" />
								<button type="submit" class="btn btn-outline btn-outline-white hidden-xs">Search</button>
							</div>
						</div>
						<div class="col-lg-4 col-md-5 col-sm-6 text-center">
							<div class="form-group">
								<select name="by-type" id="fundraiser_browse_select" class="form-control">
									<option value="">Show All Fundraiser Types</option>
									<?php

										foreach ( $fundraiser_types as $key => $value ) {

											$selected = ( $by_type && $key == $by_type ) ? ' selected' : '';

											echo '<option value="' . $key . '"' . $selected . '>' . $value . '</option>';

										}
									?>
								</select>
								<!-- first button is hidden on mobile and second is visible on mobile, for better UX -->
								<button type="submit" class="btn btn-outline btn-outline-white hidden-xs">Browse</button>
								<button type="submit" class="btn btn-outline btn-outline-white visible-xs">Search</button>
							</div>
						</div>
					</div>
				</form>
			</div>

			<?php } ?>
			<!-- end by type OR by search -->

		</div>

			<?php
				global $by_type;
				global $by_search;

				$action   = ( $by_type ) ? '' : 'Find';
				$action   = ( $by_search ) ? 'Searching for "' . $by_search . '" in' : $action;
				$selected = ( $by_type ) ? $fundraiser_types[ $by_type ] . '' : 'All Funds';

				// if by type OR by search
				if ( $by_type || $by_search ) {
					echo '<h1 class="text-uppercase text-center noborders space-0">' . $action . ' ' . $selected . '</h1>';
					echo '<p class="lead text-center space-6"><em>Who\'s world do you want to change?</em></p>';
				}
				// if neither
				if ( ! $by_type && ! $by_search ) {
					echo '<h2 class="text-uppercase text-center noborders space-6">All Funds</h2>';
				}
			 ?>


		<?php
	}

	add_action( '_fh_the_title', 'custom_title' );


	function custom_content() {

		global $by_type;
		global $by_search;
		global $acf_fields;

		// Setup args for custom query
		$args = array(
			'post_type' => 'page',
			'post_parent' => OM_FUNDRAISER_PAGE_PARENT_ID,
			'post_status' => array( 'publish' ),
			'posts_per_page' => 30,
			'paged' => (get_query_var('paged')) ? get_query_var('paged') : 1,

			// 'meta_query' => array(
			// 	array(
			// 		'key' => 'is_featured',
			// 		'orderby' => 'meta_value',
			// 		'order' => 'DESC'
			// 	)
			// )

			// 'orderby' => 'meta_value',
			// 'meta_key' => 'is_featured',
			// 'order' => 'DESC'
		);

		// Search query
		if( $by_search ) {

			$args['s'] = $by_search;
		}

		// Type query
		if( $by_type ) {

			$args['meta_query'] = array(
				array(
					'key' =>   'fundraiser_type',
          'compare' => 'LIKE',
					'value' => $by_type
				),
				array(
					'key' => 'is_featured',
					'orderby' => 'meta_value',
					'order' => 'DESC'
				),
				// array(
				// 	'key' => 'fundraiser_start_date',
				// 	'orderby' => 'meta_value',
				// 	'order' => 'DESC'
				// )
			);
		}

		$profiles_query = new WP_Query( $args );

		if ( $profiles_query->have_posts() ) {

			$i = 0;

			echo '<div id="fundraiser_poloroids_wrap" class="container"><div class="row">';

			while ( $profiles_query->have_posts() ) {

				$profiles_query->the_post();

				$post                          = $profiles_query->post;
				$fields                        = get_fields( $post->ID );
				$shop_link                     = '/products/?shopfor=' . $post->ID;
				$fundraiser_container_class_id = 'fundraiser_id_' . $post->ID;
				$fundraiser_url                = get_permalink( $post->ID );

				// Check if fundraiser is closed
				if (isset($fields["fundraiser_start_date"]) && isset($fields["fundraiser_end_date"])) {
					$closed = (
						_om_is_fundraiser_closed( $fields['fundraiser_timed_or_continuous'], $fields['fundraiser_start_date'], $fields['fundraiser_end_date'] )
					);
				} else {
					$closed = false;
				}

				// Calculate first/last column classes
				$extra_class = ( ! ( $i % 3 ) || $i == 0 ) ? ' first' : '';
				$extra_class = ( ! ( ( $i + 1 ) % 3 ) ) ? ' last' : $extra_class;

				$html_out = '<div class="col-sm-6 col-md-4 col-lg-3"><div class="fundraiser_poloroid">';
				$html_out .= '<div class="text-center">';

				if( !empty( $fields['logo_image']['sizes']['shop_single'] ) ) {

					$html_out .= '<a href="' . $fundraiser_url . '" class=""><img class="fundraisers_search" src="' . $fields['logo_image']['sizes']['thumbnail'] . '"></a>';

				} else {

					$html_out .= '<a href="' . $fundraiser_url . '" class=""><img class="fundraisers_search" src="' . get_stylesheet_directory_uri() . '/img/no_fundraiser_logo.png"></a>';
				}

				$html_out .= '</div>';

				// Header classes
				$header_str_length = strlen( $post->post_title );

				if( $header_str_length < 20 ) {

					$header_class = '';

				} elseif( 40 > $header_str_length && $header_str_length >= 20 ) {

					$header_class = 'small_1';

				} elseif( 56 > $header_str_length && $header_str_length >= 40 ) {

					$header_class = 'small_2';

				} elseif( $header_str_length >= 56 ) {

					$header_class = 'small_3';
				}


				$html_out .= '<h3 class="' . $header_class . '"><strong><a href="' . $fundraiser_url . '">' . $post->post_title . '</a></strong></h3>';
				$html_out .= _om_limit_words( strip_tags( $fields['fundraiser_description'] ), 20 ) . '...';
				$html_out .= '<div id="poloroid_options">';

				// $btn_text = ( $closed ) ? 'View Profile' : 'Support This Fundraiser';
				// $html_out .= '<a class="btn btn-success btn-md" href="' . $fundraiser_url . '"><i class="fa fa-heart"></i>&nbsp; ' . $btn_text . '</a>';

				$html_out .= '<a class="btn btn-info btn-sm" href="' . $fundraiser_url . '#tab_fundraiser_profile">View Profile</a>';

				if( ! $closed ) {

					$html_out .= '&nbsp;<a class="btn btn-success btn-sm" href="' . $fundraiser_url . '?shopfor=' . $post->ID . '#tab_fundraiser_support">Support!</a>';

					// $html_out .= '<a class="btn btn-success btn-xs" href="' . $shop_link . '">Shop to Support!<!-- &nbsp;<i class="fa fa-shopping-cart"></i> --></a>';
					// $html_out .= '<a class="btn btn-info btn-xs" href="' . $fundraiser_url . '?shopfor=' . $post->ID . '#how-to-support-donate">Donate!<!-- &nbsp;<i class="fa fa-dollar"></i> --></a>';
				}

				$html_out .= '</div>';
				$html_out .= '</div></div>';

				echo $html_out;

				$i++;
			}

			echo '</div></div>';

		} else {

			echo '<p class="h3 text-center">No fundraisers were found for this type!</p>';
		}

		// Reset Post Data
		wp_reset_postdata();

		_fh_paginate( $profiles_query );

		?>

		 	<div class="mini-separator space-top-6"></div>

		 	<div class="space-top-6 space-6 text-center">

		<?php	if( isset( $acf_fields[ $by_type ] ) && ! empty( $acf_fields[ $by_type ] ) ): ?>

			<p class="lead text-center"><strong><?php echo nl2br( $acf_fields[ $by_type ] ); ?></strong></p>

		<?php else: ?>

			<p class="lead text-center"><strong>Are you passionate about a cause?</strong></p>

		<?php endif; ?>

			<a href="#signup-form" class="btn btn-lg btn-success popup-with-form">Start a Fund</a>

		</div>

		<?php

	}

	add_action( '_fh_the_content', 'custom_content' );

	$holy_grail_class = 'full nobreadcrumbs';
	include( get_template_directory() . '/templates/holy_grail_master.php' );

?>
