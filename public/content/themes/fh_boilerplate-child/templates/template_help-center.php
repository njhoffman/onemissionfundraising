<?php
	/*
		Template Name: Help Center
	*/

	// Setup Holy Grail class
	$holy_grail_class = 'one_full';

	// Load post into global $loc_post context
	$loc_post = Timber::get_post();

	//	Disable Breadcrumbs
	add_action( '_fh_the_crumbs', '__return_false' );

	// Disable on-page social sharing
	add_action( '_fh_on_page_social_sharing', '__return_false' );

	// Remove default title, sidebars, and content
	add_action( '_fh_the_title', '__return_false' );

	// Remove main content action and add custom profile main content
	remove_action( '_fh_the_content', '_fh_content' );
	add_action( '_fh_the_content', '_om_main_content' );

?>

<?php
	//
	//	MAIN CONTENT
	//
	function _om_main_content() {

		global $loc_post;

	// Get featured image
	$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
?>



	<div class="help-center">

		<div class="page-section space-6" style="background-image: url('https://images.pexels.com/photos/132721/nature-landscape-sky-clouds-132721.jpeg?w=1260&h=750&auto=compress&cs=tinysrgb'); background-color: #efefef;">
		<!-- <div class="page-section space-6" style="background-image: url('<?php echo($feat_image) ?>'); background-color: #efefef;"> -->
			<h1 class="space-top-8">One Mission Help Center</h1>

			<div class="row space-8">
				<div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
					<?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
				</div>
			</div>
		</div>

		<div class="container space-10">
			<div class="row">
				<div class="col-sm-8 col-lg-9">
					<div class="help-center-content">

						<h2>Help Topics</h2>
						<hr>



						<div class="row">
<?php

$tax = 'help_center_categories';

$terms = get_terms( $tax, [
  'hide_empty' => false, // do not hide empty terms
  'orderby' => 'id'
]);

foreach( $terms as $term ) {
	echo '<div class="col-sm-6 space-2">';
    echo '<h4><a href="'. get_term_link( $term ) .'">'. $term->name .'</a></h4>';
    echo '<p>'. $term->description .'</p>';
    echo '</div>';
} ?>

</div>



					</div>
				</div>
				<div class="col-sm-4 col-lg-3">
					<div class="inset inset-gray space-4">
						<h5>Support</h5>
						<p>Need additional help or just want to work with a really nice human? <br>Please <a href="/contact-us/">contact us</a>.</p>
					</div>

					<?php

						 global $wp_query;
						 $postid = $wp_query->post->ID;
						 echo get_post_meta($postid, 'help_center_sidebar', true);
						 wp_reset_query();
					?>

				</div>
			</div>

			<?php the_content(); ?>
		</div>

	</div>


<?php
// end MAIN CONTENT
}

	//	Grab the main template
	include( get_template_directory() . '/templates/holy_grail_master.php' );



?>
