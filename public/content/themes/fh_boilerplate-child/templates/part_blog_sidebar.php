<div class="blog-sidebar col-sm-4 col-lg-3 col-sm-pull-8 col-lg-pull-9">
	<img class="space-2" src="https://onemission.fund/content/uploads/2018/02/one-mission-founders.png" class="img-responsive" />

	<p class="space-2">We are John and Amanda Rhomberg, who, on a plane ride home from Haiti in 2013, penned on a napkin our vision for what is now One Mission. Our mission is simple: to help world-changers fundraise for GOOD. So… we created a platform to do just that.</p>

	<center><a class="btn btn-muted btn-sm" href="/about-us/" target="_blank">Watch Our Story <span class="fa fa-arrow-right"></span></a></center>

	<br>
	<br>

	<h4 class="text-center">Categories</h4>
	<ul class="blog-sidebar-list">
	    <?php wp_list_categories( array(
	        'orderby'    => 'name',
			  'title_li' => '',
			  // hide fundraiser-news category
			  'exclude' => array('124')
	    ) ); ?>
	</ul>

	<br>

	<h4 class="text-center">Featured Posts</h4>
	<ul class="blog-sidebar-list">
	    <?php
	    $feat_posts = wp_get_recent_posts(array(
	        'numberposts' => 4,
	        'include' => array("47315", "29264", "5372", "4014")
	    ));
	    foreach($feat_posts as $post) : ?>
	        <li>
	            <a href="<?php echo get_permalink($post['ID']) ?>"><?php echo $post['post_title'] ?></a>
	        </li>
	    <?php endforeach; wp_reset_query(); ?>
	</ul>

	<br>
	<br>

	<div class="blog-sidebar-callout text-center space-3" style="background-image: url('https://onemission.fund/content/uploads/2018/02/find-a-fund-blog-sidebar.png');">
		<h4>World-Changers Doing Good</h4>
		<a href="/support/" target="_blank" class="btn btn-dark">Find a Fund</a>
	</div>

	<div class="blog-sidebar-callout text-center space-3" style="background-image: url('https://onemission.fund/content/uploads/2018/02/shop-now-blog-sidebar.png');">
		<h4>Start Shopping and Donate 40%</h4>
		<a href="/products/" target="_blank" class="btn btn-dark">Shop Now</a>
	</div>

	<div class="blog-sidebar-callout text-center space-3" style="background-image: url('https://onemission.fund/content/uploads/2018/02/start-a-fund-blog-sidebar.png');">
		<h4>Have a Great Cause?</h4>
		<a href="/fundraise/" target="_blank" class="btn btn-dark">Start a Fund</a>
	</div>

</div>
