<?php

	if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );
?><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>

		<!-- HEY! Stop looking up my skirt ;) -->

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="p:domain_verify" content="6468221bbb6b3838acf3cf3533d13571"/>

		<title><?php wp_title( '|' ); ?></title>

		<!-- Responsive Viewport -->
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- Pingback -->
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<link rel="stylesheet" href="/content/themes/fh_boilerplate-child/css/style.css" />

		<?php wp_head(); ?>
		<!-- Facebook Pixel Code -->
		<script>
			!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
			n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
			document,'script','//connect.facebook.net/en_US/fbevents.js');

			fbq('init', '1055361767817310');
			fbq('track', "PageView");
		</script>
			<noscript>
				<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1055361767817310&ev=PageView&noscript=1" />
			</noscript>
			<!-- End Facebook Pixel Code -->
	</head>

	<body <?php body_class(); ?>>

	<!-- Anchor for "Back to Top" buttons -->
	<a id="_fh_top"></a>

	<!--[if lt IE 7]>
		<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
