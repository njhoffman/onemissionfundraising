<?php
	/*
		Template Name: Fundraise With Us
	*/

	// Setup Holy Grail class
	$holy_grail_class = 'one_full';

	// Load post into global $loc_post context
	$loc_post = Timber::get_post();

	//	Disable Breadcrumbs
	add_action( '_fh_the_crumbs', '__return_false' );

	// Disable on-page social sharing
	add_action( '_fh_on_page_social_sharing', '__return_false' );

	// Remove default title, sidebars, and content
	add_action( '_fh_the_title', '__return_false' );

	// Remove main content action and add custom profile main content
	remove_action( '_fh_the_content', '_fh_content' );
	add_action( '_fh_the_content', '_om_main_content' );

?>

<?php
	//
	//	MAIN CONTENT
	//
	function _om_main_content() {

		global $loc_post;

	// Get featured image
	$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
?>

	<?php the_content(); ?>

			


<?php // gravity_form( 23, false, false, false, '', false ); ?>

<?php
// end MAIN CONTENT
} 

	//	Grab the main template
	include( get_template_directory() . '/templates/holy_grail_master.php' );



?>
