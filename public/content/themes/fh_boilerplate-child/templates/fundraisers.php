<?php

	/*
		Template Name: Fundraisers
	*/

	$by_type   = ( isset( $_GET['by-type'] ) ) ? sanitize_text_field( $_GET['by-type'] ) : false;
	$by_search = ( isset( $_GET['by-search'] ) ) ? sanitize_text_field( $_GET['by-search'] ) : false;

	// Grab ACF fields for custom content blocks
	$acf_fields = get_fields();

	//
	//	Replace title block with browsing interface
	//
	function custom_title() {

		global $fundraiser_types;
		global $by_type;
		global $by_search;

		$action   = ( $by_type ) ? 'Browsing' : 'Find';
		$action   = ( $by_search ) ? 'Searching for "' . $by_search . '" in' : $action;
		$selected = ( $by_type ) ? $fundraiser_types[ $by_type ] . ' ' : '';

		echo '<h1>' . $action . ' ' . $selected . 'Fundraisers</h1>';
	}

	add_action( '_fh_the_title', 'custom_title' );
	add_action( '_fh_sidebar_right_prepend', 'custom_sidebar_prepend' );

	function custom_sidebar_prepend() {

		global $fundraiser_types;
		global $by_type;
		global $by_search;
		global $acf_fields;

		?>
		<div class="segment well">

			<ul><li>

				<form id="fundraiser_browse_tools" role="form" method="get" action="<?php echo get_permalink() ?>">

					<div class="form-group">
						<label for="fundraiser_browse_select">Fundraiser Type:</label>
						<select name="by-type" id="fundraiser_browse_select" class="form-control input-sm">
							<option value="">Show All Fundraiser Types</option>
							<?php

								foreach ( $fundraiser_types as $key => $value ) {

									$selected = ( $by_type && $key == $by_type ) ? ' selected' : '';

									echo '<option value="' . $key . '"' . $selected . '>' . $value . '</option>';

								}
							?>
						</select>
					</div>
					<div class="form-group">
						<label for="fundraiser_browse_search">Fundraiser Name:</label>
						<?php

							$value = ( $by_search ) ? $by_search : '';
						?>
						<input name="by-search" id="fundraiser_browse_search" type="text" class="form-control input-sm" placeholder="search fundraisers" value="<?php echo $value; ?>" />
					</div>

					<button type="submit" class="btn btn-info btn-md btn-block">Find Fundraisers</button>

				</form>

			</li></ul>

		</div>

		<div class="segment well">

			<ul><li>

				<?php if( isset( $acf_fields[ $by_type ] ) && ! empty( $acf_fields[ $by_type ] ) ): ?>

					<p><?php echo nl2br( $acf_fields[ $by_type ] ); ?></p>

				<?php endif; ?>

				<p><a href="#signup-form" class="btn btn-block btn-md btn-success popup-with-form">Start a Fundraiser!</a></p>

			</li></ul>


		</div>
		<?php
	}

	function custom_content() {

		global $by_type;
		global $by_search;

		// Setup args for custom query
		$args = array(
			'post_type' => 'page',
			'post_parent' => OM_FUNDRAISER_PAGE_PARENT_ID,
			'post_status' => array( 'publish' ),
			'posts_per_page' => 30,
			'paged' => (get_query_var('paged')) ? get_query_var('paged') : 1,

			// 'meta_query' => array(
			// 	array(
			// 		'key' => 'is_featured',
			// 		'orderby' => 'meta_value',
			// 		'order' => 'DESC'
			// 	)
			// )

			// 'orderby' => 'meta_value',
			// 'meta_key' => 'is_featured',
			// 'order' => 'DESC'
		);

		// Search query
		if( $by_search ) {

			$args['s'] = $by_search;
		}

		// Type query
		if( $by_type ) {

			$args['meta_query'] = array(
				array(
					'key' =>   'fundraiser_type',
					'value' => $by_type
				),
				array(
					'key' => 'is_featured',
					'orderby' => 'meta_value',
					'order' => 'DESC'
				),
				// array(
				// 	'key' => 'fundraiser_start_date',
				// 	'orderby' => 'meta_value',
				// 	'order' => 'DESC'
				// )
			);
		}

		$profiles_query = new WP_Query( $args );

		if ( $profiles_query->have_posts() ) {

			$i = 0;

			echo '<div id="fundraiser_poloroids_wrap">';

			while ( $profiles_query->have_posts() ) {

				$profiles_query->the_post();

				$post                          = $profiles_query->post;
				$fields                        = get_fields( $post->ID );
				$shop_link                     = '/products/?shopfor=' . $post->ID;
				$fundraiser_container_class_id = 'fundraiser_id_' . $post->ID;
				$fundraiser_url                = get_permalink( $post->ID );

				// Check if fundraiser is closed
				$closed = (
					_om_is_fundraiser_closed( $fields['fundraiser_timed_or_continuous'], $fields['fundraiser_start_date'], $fields['fundraiser_end_date'] )
				);

				// Calculate first/last column classes
				$extra_class = ( ! ( $i % 3 ) || $i == 0 ) ? ' first' : '';
				$extra_class = ( ! ( ( $i + 1 ) % 3 ) ) ? ' last' : $extra_class;

				$html_out = '<div class="fundraiser_poloroid' . $extra_class . '">';
				$html_out .= '<div class="text-center">';

				if( !empty( $fields['logo_image']['sizes']['shop_single'] ) ) {

					$html_out .= '<a href="' . $fundraiser_url . '" class=""><img class="fundraisers_search" src="' . $fields['logo_image']['sizes']['thumbnail'] . '"></a>';

				} else {

					$html_out .= '<a href="' . $fundraiser_url . '" class=""><img class="fundraisers_search" src="' . get_stylesheet_directory_uri() . '/img/no_fundraiser_logo.png"></a>';
				}

				$html_out .= '</div>';

				// Header classes
				$header_str_length = strlen( $post->post_title );

				if( $header_str_length < 20 ) {

					$header_class = '';

				} elseif( 40 > $header_str_length && $header_str_length >= 20 ) {

					$header_class = 'small_1';

				} elseif( 56 > $header_str_length && $header_str_length >= 40 ) {

					$header_class = 'small_2';

				} elseif( $header_str_length >= 56 ) {

					$header_class = 'small_3';
				}


				$html_out .= '<h3 class="' . $header_class . '"><strong><a href="' . $fundraiser_url . '">' . $post->post_title . '</a></strong></h3>';
				$html_out .= _om_limit_words( strip_tags( $fields['fundraiser_description'] ), 25 ) . '...';
				$html_out .= '<div id="poloroid_options">';

				// $btn_text = ( $closed ) ? 'View Profile' : 'Support This Fundraiser';
				// $html_out .= '<a class="btn btn-success btn-md" href="' . $fundraiser_url . '"><i class="fa fa-heart"></i>&nbsp; ' . $btn_text . '</a>';

				$html_out .= '<a class="btn btn-info btn-sm" href="' . $fundraiser_url . '#tab_fundraiser_profile">View Profile</a>';

				if( ! $closed ) {

					$html_out .= '&nbsp;<a class="btn btn-success btn-sm" href="' . $fundraiser_url . '?shopfor=' . $post->ID . '#tab_fundraiser_support">Support!</a>';

					// $html_out .= '<a class="btn btn-success btn-xs" href="' . $shop_link . '">Shop to Support!<!-- &nbsp;<i class="fa fa-shopping-cart"></i> --></a>';
					// $html_out .= '<a class="btn btn-info btn-xs" href="' . $fundraiser_url . '?shopfor=' . $post->ID . '#how-to-support-donate">Donate!<!-- &nbsp;<i class="fa fa-dollar"></i> --></a>';
				}

				$html_out .= '</div>';
				$html_out .= '</div>';

				echo $html_out;

				$i++;
			}

			echo '</div>';

		} else {

			echo '<p class="h3 text-center">No fundraisers were found for this type!</p>';
		}

		// Reset Post Data
		wp_reset_postdata();

		_fh_paginate( $profiles_query );

	}

	add_action( '_fh_the_content', 'custom_content' );

	$holy_grail_class = 'one_full';
	include( get_template_directory() . '/templates/holy_grail_master.php' );

?>
