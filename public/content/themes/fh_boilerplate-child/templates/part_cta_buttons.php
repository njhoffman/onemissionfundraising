<h3 class="text-center text-uppercase text-white">How would you like to change the world today?</h3>

<div class="row">
	<div class="col-sm-4 space-2">
		<a href="/support/" class="btn btn-lg btn-yellow btn-block">Find a Fund</a>
	</div>
	<div class="col-sm-4 space-2">
		<a href="/fundraise/" class="btn btn-lg btn-success btn-block">Fundraise with Us</a>
	</div>
	<div class="col-sm-4 space-2">
		<a href="/products/" class="btn btn-lg btn-primary btn-block">Shop Now</a>
	</div>
</div>
