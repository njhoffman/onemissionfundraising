<?php
	/*
		Template Name: Fundraiser Profile Template
	*/


	// Setup Holy Grail class
	$holy_grail_class = 'two_left_1';

	// Load post into global $loc_post context
	$loc_post = Timber::get_post();

	// Get the shopping link
	$shop_link = '/products/?shopfor=' . $loc_post->ID;

	//
	//	Simply Scroll
	//
	wp_register_script( 'simplyscroll_js', get_stylesheet_directory_uri() . '/js/vendor/jquery_simplyscroll/jquery.simplyscroll.js', array( 'jquery' ) );
	wp_enqueue_script( 'simplyscroll_js' );

	wp_register_style( 'simplyscroll_css', get_stylesheet_directory_uri() . '/js/vendor/jquery_simplyscroll/jquery.simplyscroll.css' );
	wp_enqueue_style( 'simplyscroll_css' );

	//
	//	Readmore
	//
	wp_register_script( 'readmore_js', get_stylesheet_directory_uri() . '/js/vendor/readmore/readmore.min.js', array( 'jquery' ) );
	wp_enqueue_script( 'readmore_js' );

	//
	//	Fundraiser JS
	//
	wp_register_script( 'fundraiser_js', get_stylesheet_directory_uri() . '/js/fundraiser_page.js', array( 'jquery', 'simplyscroll_js' ) );

	$localize_inject = array(
		'fundraiser_id' => $loc_post->ID
	);

	wp_localize_script( 'fundraiser_js', 'child_vars', $localize_inject );
	wp_enqueue_script( 'fundraiser_js' );

	$warn_overlay = false;
	if (isset($_GET['warn'])) {
		$warn_overlay = true;
	}

	// BUG: Fix blank title on preview bug (not sure why this is happening)
	if( empty( $loc_post->post_title ) ) {

		$loc_post->post_title = get_the_title( $loc_post->ID );
	}

	// Inject the last viewed profile (used to link-back from product browsing)
	$inject_into_cookie = array( 'last_fundraiser_viewed_guid' => get_permalink( $loc_post->ID ) );
    Utils::inject_cookie( $inject_into_cookie );

	// Grab ACF Fields
	// fundraisers not saved through regular admin editor don't have matching _field_name entries
	// ACF fields doesn't return them correctly, must load metadata
	// more info -> https://support.advancedcustomfields.com/forums/topic/get_field-doesnt-return-a-value-when-fields-are-created-with-update_field/
	$loc_fields = get_fields( $loc_post->ID );
	if (! isset($loc_fields['logo_image'])) {
		$loc_fields = get_post_meta( $loc_post->ID );
		foreach ($loc_fields as $key => $field) {
			if (is_array($field)) {
				$loc_fields[$key] = $field[0];
			}
		}
		$logo_image = wp_get_attachment_url($loc_fields['logo_image']);
		$banner_image = wp_get_attachment_url($loc_fields['banner_image']);
		$loc_fields['logo_image'] = array('sizes' => array('om-profile-280' => $logo_image ) );
		$loc_fields['banner_image'] = array('sizes' => array('om-profile-900' => $banner_image ) );
	}

	//	Check for a closed fundraiser
	//
	$closed = false;

	//
	//	Timed fundraiser - check if fundraiser closed
	//
	if( $loc_post->fundraiser_timed_or_continuous == 'timed' ) {

		$unix_start_date   = strtotime( $loc_post->fundraiser_start_date );
		$unix_end_date     = strtotime( $loc_post->fundraiser_end_date );
		$unix_now          = time();

		// Logic to determine if fundraiser is closed
		$closed = _om_is_fundraiser_closed( $loc_post->fundraiser_timed_or_continuous, $loc_post->fundraiser_start_date, $loc_post->fundraiser_end_date );
	}

	//
	//	Track viewed fundraiser if fundraiser open for love fund notification display in cart
	//	- $loc_post->post_title
	//	- $loc_fields['logo_image']['sizes']['om-profile-280']
	//
	if( ! $closed ) {

		_om_track_viewed_fundraiser(
			$loc_post->ID,
			$loc_post->post_title,
			get_permalink( $loc_post->ID ),
			_om_limit_words( strip_tags( $loc_fields['fundraiser_description'] ), 25 ) . '...',
			$loc_fields['logo_image']['sizes']['om-profile-280']
		);
	}

	// Get the user meta information (for displaying group leader info)
	$user_meta = get_user_meta( $loc_post->post_author );

	// Remove default title, sidebars, and content
	add_action( '_fh_the_title', '__return_false' );
	add_action( '_fh_sidebar_left_content', '_om_profile_sidebar_content' );

	// Remove main content action and add custom profile main content
	remove_action( '_fh_the_content', '_fh_content' );
	add_action( '_fh_the_content', '_om_profile_main_content' );

	// Disable on-page social sharing
	add_action( '_fh_on_page_social_sharing', '__return_false' );

	// Look up orders via $loc_post->post_author (Fundraiser User ID)
	$query_status = array( 'wc-processing', 'wc-completed' );

	// Setup args for custom orders query
	$order_args = array(
		'posts_per_page' => -1,
		'post_type' =>      'shop_order',
		'post_status' =>    $query_status,
		'orderby' =>        'post_date',
		'order' =>          'DESC',
		'meta_query' => array(
			array(
				'key' =>   'Fundraiser User ID',
				'value' => $loc_post->post_author
			)
		)
	);

	$fundraiser_goal_total_from_orders = 0;
	$sponsors_limit                    = 100;
	$sponsors_html                     = '';

	// Run orders query
	$orders = new WP_Query( $order_args );

	if( !empty( $orders->posts ) ) {

		$i = 0;

		// Itterate over orders and tally up total donation amount
		// - Also generate HTML for the recent donators
		foreach( $orders->posts as $order ) {

			// Grab information for each order
			$order_id         = $order->ID;
			$fields           = get_post_custom( $order->ID );
			$billing_fname    = $fields['_billing_first_name'][0];
			$billing_linitial = substr( $fields['_billing_last_name'][0], 0, 1 );
			$billing_company  = $fields['_billing_company'][0];
			$order_date       = $order->post_date;
			$order_timestamp  = strtotime( $order_date );
			$order_human_date = date( 'F j, Y', $order_timestamp );

			// Generate HTML for each sponsor if less than $sponsors_limit
			if( $i < $sponsors_limit ) {

				// Public donation attribution
				if(
					isset( $fields['Public Donation Attribution'] ) &&
					isset( $fields['Public Donation Attribution'][0] ) &&
					! empty( $fields['Public Donation Attribution'][0] )
				) {

					$sponsor_html = $fields['Public Donation Attribution'][0];

				} else {

					if( isset( $states_lookup[ $fields['_billing_state'][0] ] ) ) {

						$sponsor_html = 'Donation from ' . $states_lookup[ $fields['_billing_state'][0] ];

					} else {

						// Shouldn't excecute
						$sponsor_html = 'Donation from ' . $fields['_billing_state'][0];
					}
				}

				// Donation date
				$sponsor_html .= '<br>' . $order_human_date;

				$sponsors_html .= '<li>' . $sponsor_html . '</li>';
			}

			// Generate total fundraiser status from "Total Donation Amount" field
			$fundraiser_goal_total_from_orders += str_replace(",", "", $fields['Total Donation Amount'][0]);

			$i++;
		}
	}


	//
	//	Run adjustment if need be (goal_status_adjustment)
	//
    if(
        isset( $loc_fields['goal_status_adjustment'] ) &&
        ! empty( $loc_fields['goal_status_adjustment'] ) &&
        $loc_fields['goal_status_adjustment'] &&
        is_numeric( (float) $loc_fields['goal_status_adjustment'] )
    ) {
		 $gsa_total = _om_get_gsa_total($loc_fields['goal_status_adjustment']);
		 $fundraiser_goal_total_from_orders += (float) $gsa_total;
    }


	//
	//	GRAB CURRENT SHOPPING FOR AND CHECK FOR SAME SHOP-FOR
	//
	$is_currently_shopping_for = false;

	if( isset( $_COOKIE['shopping_for_id'] ) && is_numeric( $_COOKIE['shopping_for_id'] ) && $_COOKIE['shopping_for_id'] ) {

		$current_profile_id = (int) $loc_post->ID;
		$shopping_for_id    = (int) $_COOKIE['shopping_for_id'];

		// Check if shopping for and profile are the same
		$is_currently_shopping_for = ( $current_profile_id === $shopping_for_id ) ? true : false;
	}


	//
	//	DISABLE BREADCRUMBS
	//
	add_action( '_fh_the_crumbs', '__return_false' );





	//
	//	MAIN CONTENT
	//
	function _om_profile_main_content() {

		global $loc_post;
		global $loc_fields;
		global $fundraiser_types;
		global $user_meta;
		global $shop_link;
		global $woocommerce;
		global $fundraiser_goal_total_from_orders;
		global $is_currently_shopping_for;
		global $closed;

		//
		//	Grab news posts for fundraisers
		//
		$news_html = '';

		$args = array(
			'author' => $loc_post->post_author,
			'nopaging' => true
			// hide fundraiser_news category (only for internal use)
			// 'category__not_in' => array(124)
		);

		function limit_posts( $limit, $query ) {
			// Set posts limit to 10
			return 'LIMIT 0, 40';
		}

		add_filter( 'post_limits', 'limit_posts', 10, 2 );

		$the_query = new WP_Query( $args );

		if ( $the_query->have_posts() ) {

			$news_html .= '<div class="" id="fundraiser_news">';

			while ( $the_query->have_posts() ) {

				$the_query->the_post();
				$news_html .= _fh_display_post_excerpt( $the_query->post, false );
			}

			// Get the fundraiser's news link (by author)
			// $more_news_link = get_author_posts_url( $loc_post->post_author );

			$news_html .= '</div>';

		}

		// Reset Post Data
		wp_reset_postdata();

		//
		//	Pre-profile status
		//
		echo '<div id="profile_status">';

		//
		//	Title
		//
		echo '<h1>' . $loc_post->post_title . '</h1>';

		//
		//	Check if currently shopping for
		//	- Don't duplicate shopping for notification if you are currently shopping for the
		//	  same fundraiser
		//
		if( ! $is_currently_shopping_for ) {


			//
			//	Enable/disable fundraiser status
			//
			$show_monetary_status = true;

			if( isset( $loc_post->show_fundraiser_status ) ) {

				$show_monetary_status = ( $loc_post->show_fundraiser_status === '1' ) ?
					true :
					false;
			}

			if( $show_monetary_status ) {

				$clean_goal_amount = str_replace(',', '', $loc_post->fundraiser_goal_amount );
				$clean_goal_status = str_replace(',', '', $fundraiser_goal_total_from_orders );

				$hide_progress_bar = false;

				if( floatval( $clean_goal_amount ) ) {

					$current_percent = floatval( $clean_goal_status ) / floatval( $clean_goal_amount );

	                if( $current_percent >= 1 ) {

	                    $css_bar_percent      = 100;
	                    $progress_bar_precent = 100;

	                } else {

	                    $css_bar_percent      = $current_percent * 100;
	                    $progress_bar_precent = number_format( ( $current_percent * 100 ) );
	                    $progress_bar_precent = ( (float) $progress_bar_precent > 100 ) ? 100 : $progress_bar_precent;
	                }

				} else {

					$hide_progress_bar = true;
				}

				if ($closed) {
					$hide_progress_bar = true;
				}

				// Progress bar
				if( $show_monetary_status && ! $hide_progress_bar ) {

		            // Progress bar
		            echo '<div id="profile_progress_bar_wrap">';
		            echo '<div class="progress progress-striped active">';
		            echo '<div id="profile_progress_bar" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="' . $progress_bar_precent . '" aria-valuemin="0" aria-valuemax="100" style="width: ' . $css_bar_percent . '%;">';
		            echo $progress_bar_precent . '%';
		            echo '</div>';
		            echo '</div>';
		            echo '<span class="left_text">$' . number_format( $clean_goal_status, 2 ) . '<br>Raised</span>';
		            echo '<span class="right_text">$' . number_format( $clean_goal_amount, 2 ) . '<br>Goal!</span>';
		            echo '</div>';

				} elseif( $show_monetary_status && floatval( $clean_goal_status ) ) {

					echo '<p class="h4"><strong>$' . number_format( $clean_goal_status, 2 ) . ' Raised!</strong></p>';

				} elseif( $show_monetary_status ) {

					echo '<p>No donations have been raised!</p>';
				}
			}

			//
			//	Timed fundraiser
			//
			if( $loc_post->fundraiser_timed_or_continuous == 'timed' ) {

				$unix_start_date   = strtotime( $loc_post->fundraiser_start_date );
				$unix_end_date     = strtotime( $loc_post->fundraiser_end_date );
				$unix_now          = time();
				$human_date_format = 'M j, Y';
				$human_start_date  = date( $human_date_format, $unix_start_date );
				$human_end_date    = date( $human_date_format, $unix_end_date );

				echo '<p class="start_end">';

				// Start Date
				if( ! empty( $human_start_date ) ) {

					echo 'Start: ' . $human_start_date . ' - ';
				}

				// End Date
				if( ! empty( $human_end_date ) ) {

					echo 'End: ' . $human_end_date;
				}

				echo '</p>';
			}

		}

		// End status container:
		echo '</div>';

		wc_print_notices();

		//
		//	Tab navigation
		//
		?>

		<div id="profile_tab_content">

			<?php if( $closed ): ?>

			<div role="tabpanel" class="less-is-more profile-description_less-is-more" id="fundraiser_profile">

			<?php else: ?>

			<div role="tabpanel" class="less-is-more profile-description_less-is-more" id="fundraiser_profile">

			<?php endif; ?>

				<?php
				//
				//	Banner
				//	- Image / YouTube Video
				//
				if( $loc_post->image_or_youtube_banner == 'image' && ! empty( $loc_fields['banner_image'] ) ) {

					// Banner Image
					echo '<p class="text-center"><img src="' . $loc_fields['banner_image']['sizes']['om-profile-900'] . '" title="' . $loc_post->post_title . '" alt="' . $loc_post->post_title . '"></p>';

				} elseif( ! empty( $loc_post->banner_youtube_video_id ) ) {

					if( strpos( $loc_post->banner_youtube_video_id, 'youtube.com' ) ) {

						// If user incorrectly supplied a URL of a youtube vid then get the v parameter
						parse_str( parse_url( $loc_post->banner_youtube_video_id, PHP_URL_QUERY ), $url_vars );
						$yt_video_id = $url_vars['v'];

					} else {

						$yt_video_id = $loc_post->banner_youtube_video_id;
					}

					// Banner YouTube Video

					echo '<div class="arve-wrapper arve-normal-wrapper arve-youtube-wrapper"><div class="arve-embed-container">';
					echo '<iframe id="yt_player_iframe" class="arve-inner" type="text/html" width="900" height="506" src="//www.youtube.com/embed/' . $yt_video_id . '" frameborder="0"></iframe>';
					echo '</div></div>';

				}


				//
				//	Description
				//	- NOTE: When using $loc_post->fundraiser_description, <p> tags are missing;
				//			use $loc_fields which utilizes get_fields instead
				//
				echo '<div id="profile_desc" class="less-is-more">'. $loc_fields['fundraiser_description'] . '</div>';

				//
				//	Group Information
				//
				$col_a_arr = array();
				$col_b_arr = array();

				//////////////////// Build Column A //////////////////////

				// City/state
				if( !empty( $loc_post->fundraiser_city_state ) ) {

					array_push( $col_a_arr, '<strong>Making a Difference In:</strong> ' . $loc_post->fundraiser_city_state );
				}

				// Group leader fname/lname
				if( ! empty( $user_meta['first_name'][0] ) && ! empty( $user_meta['last_name'][0] ) ) {

					$user_full_name = $user_meta['first_name'][0] . ' ' . $user_meta['last_name'][0];
					array_push( $col_a_arr, '<strong>Fund Leader:</strong> ' . $user_full_name );
				}

				// Fundraiser Type
				$post_fundraisers = '';
				foreach (explode('&', $loc_post->fundraiser_type) as $ftype) {
					$post_fundraisers .=  (!empty($post_fundraisers) ? ', ' : '') . $fundraiser_types[$ftype];
				}
				array_push( $col_a_arr, '<strong>Fund Type:</strong> ' . $post_fundraisers );

				// Joined date
				// $unformatted = strtotime( $loc_post->post_date );
				// array_push( $col_a_arr, '<strong>Signed Up:</strong> ' . date( 'F j, Y', $unformatted ) );

				// Last-updated date
				// $unformatted = strtotime( $loc_post->post_modified );
				// array_push( $col_a_arr, '<strong>Profile Last Updated:</strong> ' . date( 'F j, Y, g:i a', $unformatted ) );


				//////////////////// Build Column B //////////////////////

				if( ! empty( $loc_post->fundraiser_website ) ) {

					array_push( $col_b_arr, '<tr><td><i class="fa fa-compass"></i></td><td><a href="' . $loc_post->fundraiser_website . '" target="_blank">' . $loc_post->post_title . '\'s Official Website</a></td></tr>' );
				}

				if( ! empty( $loc_post->fundraiser_facebook ) ) {

					array_push( $col_b_arr, '<tr><td><i class="fa fa-facebook"></i></td><td><a href="' . $loc_post->fundraiser_facebook . '" target="_blank">' . $loc_post->post_title . '\'s Facebook Page</a></td></tr>' );
				}

				if( ! empty( $loc_post->fundraiser_twitter ) ) {

					array_push( $col_b_arr, '<tr><td><i class="fa fa-twitter"></i></td><td><a href="' . $loc_post->fundraiser_twitter . '" target="_blank">' . $loc_post->post_title . '\'s Twitter Feed</a></td></tr>' );
				}

				if( ! empty( $loc_post->fundraiser_pinterest ) ) {

					array_push( $col_b_arr, '<tr><td><i class="fa fa-pinterest"></i></td><td><a href="' . $loc_post->fundraiser_pinterest . '" target="_blank">' . $loc_post->post_title . ' on Pinterest</a></td></tr>' );
				}


				//
				//	Display Group Information Columns
				//

				$cols_class = ( ! empty( $col_b_arr ) ) ? 'col-sm-6' : 'col-sm-12';

				?>
				<div class="row">
					<?php if( ! empty( $col_a_arr ) ): ?>
					<div class="<?php echo $cols_class; ?>">
						<div class="well">
							<p>
								<?php echo implode( $col_a_arr, '<br>' ); ?>
							</p>
						</div>
					</div>
					<?php endif; ?>
					<?php if( ! empty( $col_b_arr ) ): ?>
					<div class="<?php echo $cols_class; ?>">
						<div class="well">
							<table id="fundraiser_info_table">
								<?php echo implode( $col_b_arr ); ?>
							</table>
						</div>
					</div>
					<?php endif; ?>
				</div>

			<!-- End profile tab div: -->
			</div>

			<div class="section-header">
				<div class="row">
					<div class="col-md-12">
						<div class="donation-block text-center bg_image" style="background-image: url('https://onemissionfundraising.com/content/uploads/2016/01/One-Mission-Products.jpg');">
							<div class="overlay">
								<div class="col-md-6 col-md-offset-3 col-sm-12">
									<h2>Purchase with Purpose</h2>
									<p class="lead">40% of every product purchased is donated</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Custom Fundraiser Products -->
			<?php if( ! $closed ): ?>

				<?php

					$group_product_category = get_field( 'group_product_category' );

					if( $group_product_category && isset( $group_product_category->count ) && $group_product_category->count ):
				?>

				<div id="fundraiser_favorite_products" class="text-center woocommerce-page">

					<h3 class="text-uppercase">Favorite Products</h3>

					<!-- <p class="h4"><em>40% of everything you buy is donated!</em></p><br> -->

					<div class="less-is-more fav-products_less-is-more">
					<?php

						echo do_shortcode( '[product_category category="' . $group_product_category->slug . '" columns="4" per_page="30" orderby="menu_order" ]' );
					?>
					</div>

				</div>


				<?php endif; ?>

			<?php endif; ?>



			<?php if( ! $closed ): ?>

			<div role="tabpanel" id="fundraiser_support">

				<!-- How it works -->
				<div id="profile_ctas">

					<?php

						// Check if we're using a somber tone (ACF field)
						$user_somber = ( isset( $loc_fields['use_somber_copy'] ) && $loc_fields['use_somber_copy'] ) ?
							true : false;
					?>

					<?php if( $user_somber ): ?>

						<!-- <h3>Product Categories</h3> -->

					<?php else: ?>

						<!-- <h3>Product Categories</h3> -->
						<!-- <p class="h4"><em>(That's right, 40% of everything you spend gets donated to this fundraiser!)</em></p> -->

					<?php endif; ?>

					<!-- Shopping categories -->
					<div id="fundraiser_profile_shopping_cats text-center" class="grid_content row">
							<div class="grid_item col-md-3 col-sm-6 col-xs-6">
								<a class="overlay-effect" href="/product-category/apparel/apparel-by-message/?shopfor=<?php echo $loc_post->ID; ?>">
									<div class="overlay" style="background-image: url('/content/themes/fh_boilerplate-child/img/product_categories/new/apparel.jpg');">
										<div class="table">
											<div class="yellow">
												<h4>Shop Apparel</h4>
											</div>
										</div>
									</div>
								</a>
							</div>
							<div class="grid_item col-md-3 col-sm-6 col-xs-6">
								<a class="overlay-effect" href="/product-category/art-prints/?shopfor=<?php echo $loc_post->ID; ?>">
									<div class="overlay" style="background-image: url('/content/themes/fh_boilerplate-child/img/product_categories/new/art_prints.jpg');">
										<div class="table">
											<div class="green">
												<h4>Shop Art</h4>
											</div>
										</div>
									</div>
								</a>
							</div>
							<div class="grid_item col-md-3 col-sm-4 col-xs-6">
								<a class="overlay-effect" href="/product-category/art-prints/note-cards/?shopfor=<?php echo $loc_post->ID; ?>">
									<div class="overlay" style="background-image: url('/content/themes/fh_boilerplate-child/img/product_categories/new/note_cards.jpg');">
										<div class="table">
											<div class="purple">
												<h4>Shop Cards</h4>
											</div>
										</div>
									</div>
								</a>
							</div>
							<div class="grid_item col-md-3 col-sm-4 col-xs-6">
								<a class="overlay-effect" href="/product-category/jewelry/?shopfor=<?php echo $loc_post->ID; ?>">
									<div class="overlay" style="background-image: url('/content/themes/fh_boilerplate-child/img/product_categories/new/sterling_silver.jpg');">
										<div class="table">
											<div class="blue">
												<h4>Shop Jewelry</h4>
											</div>
										</div>
									</div>
								</a>
							</div>
							<div class="grid_item col-md-3 col-sm-4 col-xs-6">
								<a class="overlay-effect" href="/product-category/candles-and-wax-melts/?shopfor=<?php echo $loc_post->ID; ?>">
									<div class="overlay" style="background-image: url('/content/themes/fh_boilerplate-child/img/product_categories/new/candles_wax_melts.jpg');">
										<div class="table">
											<div class="darkBlue">
												<h4>Shop Candles &amp; Melts</h4>
											</div>
										</div>
									</div>
								</a>
							</div>
							<div class="grid_item col-md-3 col-sm-4 col-xs-6">
								<a class="overlay-effect" href="/product-category/coffee-mugs/?shopfor=<?php echo $loc_post->ID; ?>">
									<div class="overlay" style="background-image: url('/content/themes/fh_boilerplate-child/img/product_categories/new/coffee_mugs.jpg');">
										<div class="table">
											<div class="tan">
												<h4>Shop Coffee Mugs</h4>
											</div>
										</div>
									</div>
								</a>
							</div>
							<div class="grid_item col-md-3 col-sm-4 col-xs-6">
								<a class="overlay-effect" href="/custom-canvas/?shopfor=<?php echo $loc_post->ID; ?>">
									<div class="overlay" style="background-image: url('/content/themes/fh_boilerplate-child/img/product_categories/new/canvas.jpg');">
										<div class="table">
											<div class="pink">
												<h4>Shop Custom Canvas</h4>
											</div>
										</div>
									</div>
								</a>
							</div>
							<div class="grid_item col-md-3 col-sm-4 col-xs-6">
								<a class="overlay-effect" href="/product-category/haitian-made/?shopfor=<?php echo $loc_post->ID; ?>">
									<div class="overlay" style="background-image: url('/content/themes/fh_boilerplate-child/img/product_categories/new/haitian_made.jpg');">
										<div class="table">
											<div class="yellow">
												<h4>Shop Haitian-Made</h4>
											</div>
										</div>
									</div>
								</a>
							</div>
					</div>


					<?php /* if( ! count( $woocommerce->cart->applied_coupons ) ): */ ?>

						<a id="how-to-support-donate"></a>

						<br>

						<?php if( $user_somber ): ?>

							<!-- <h2 class="font_walter">Donate Cash Now</h2> -->

						<?php else: ?>

							<!-- <h3>Donate Now</h3> -->
							<!-- <p class="h4"><em>(So maybe shopping isn't your thing. Donate straight cash, it's all good)</em></p> -->

						<?php endif; ?>

						<!-- Instant donation form -->
						<div class="row">
							<div class="col-md-12">
								<div id="fundraiser_profile_cash_now" class="donation-block text-center bg_image" style="background-image: url('/content/themes/fh_boilerplate-child/img/coins.jpg');">
									<div class="overlay">
										<div class="col-sm-8 col-sm-offset-2">
											<h2>Donate Now</h2>
											<form id="profile_donation_frm" action="/cart/?shopfor=<?php echo $loc_post->ID; ?>" method="post">
												<div class="input-group">
													<span class="input-group-addon"><?php echo FH_DONATE_LABEL_UNIT; ?></span>
													<input type="text" id="fh_donation_field" name="fh_donation" value="" class="form-control text-right" placeholder="0.00" />
													<span class="input-group-btn">
														<!-- <input type="submit" name="fh_donate_btn" class="btn btn-success" value="Add Donation" /> -->
														<input type="text" name="fh_donate_hidden" value="1" class="hidden">
														<!-- <button id="fh_donate_btn" class="btn btn-danger"><i class="fa fa-plus"></i> <i class="fa fa-heart"></i><span class="sr-only"> Add donation!</span></button> -->
														<button id="fh_donate_btn" class="btn btn-success">Donate<span class="sr-only"> Donate</span></button>
													</span>
												</div>

											</form>
											<p>No platform fees! Credit card processing fees of 2.9% +$0.30 per transaction are passed along to the fund.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php

							//
							//	Minimum donation logic
							//

							// Only ensure $10 minimum donation when cart is empty
							if( sizeof( $woocommerce->cart->cart_contents ) == 0 ):
						?>
						<script>

							jQuery(function () {

								jQuery( '#fh_donate_btn' ).on( 'click', function (e) {

									e.preventDefault();

									var message = '';
									var donation_amt = jQuery( '#fh_donation_field' )
										.val()
										.replace( /\s+/g, "" );

									// Validate input
									if( ! isNaN( parseFloat( donation_amt ) ) && isFinite( donation_amt ) ) {

										// Check number of decimals
										var filtered_donation = parseFloat( donation_amt ).toFixed( 2 );

										if( filtered_donation >= 10 ) {

											jQuery( profile_donation_frm.submit() );

										} else {

											// $10 donation limit
											jQuery( '.donate_warn' ).hide();
											message = '<p class="donate_warn error">Whoops! There is a $10 minimum limit!</p>';
											jQuery( message ).insertAfter( '#fundraiser_profile_cash_now form' );
										}

									} else {

										// Non-numeric warning
										jQuery( '.donate_warn' ).hide();
										message = '<p class="donate_warn error">Whoops! Please enter a dollar amount!</p>';
										jQuery( message ).insertAfter( '#fundraiser_profile_cash_now form' );
									}
								});
							});
						</script>

						<?php endif; ?>

					<?php /* endif; */ ?>

				</div>

			</div>

			<?php endif; ?>



		<!-- End tab content div: -->
		</div>

		<hr>

		<?php echo $news_html; ?>


	<?php
		//
		//	Fundraiser Comments
		//
		echo '<h2 class="text-center">' . $loc_post->post_title . ' Discussion:</h2>';
		comments_template();

		// Social sharing block full

			$share_link = ( ! $closed ) ?
				get_permalink( $loc_post->ID ) . '?shopfor=' . $loc_post->ID :
				get_permalink( $loc_post->ID );
			$mail_link = "mailto:?subject=Check out this fundraiser for $loc_post->post_title&body=Check out the fundraiser here: $share_link";
		?>

		<div id="fundraiser_profile_share" class="text-center how_support segment well hidden-xs">

			<div class="row">
				<div class="col-md-6">
					<h4>Share This Fundraiser!</h4>

					<div class="icon_buttons">
						<a class="btn-facebook"
							href="http://www.facebook.com/sharer.php?u=<?php echo urlencode($share_link) .  '&title=' . urlencode( $loc_post->post_title ); ?>"
							class="btn facebook" rel="nofollow" target="_blank" title="Share on Facebook">
							<span class="fa fa-facebook"></span>
						</a>
						<a class="btn-twitter"
							href="http://twitter.com/home?status=<?php echo $share_link ?>+%28<?php echo urlencode( $loc_post->post_title ); ?>%29"
							class="btn twitter" rel="nofollow" target="_blank" title="Share on Twitter">
							<span class="fa fa-twitter"></span>
						</a>
						<a class="btn-google" href="<?php echo $mail_link; ?>" class="btn google" rel="nofollow" title="Email a Friend">
							<span class="fa fa-envelope"></span>
						</a>
					</div>
				</div>

				<div class="col-md-6">

					<h4>Send this link to all your friends!</h4>

					<textarea onclick="this.select();" class="form-control" rows="3"><?php echo $share_link; ?></textarea>

				</div>

			</div>

		</div>

		<?php

	}



	//
	//	SIDEBAR CONTENT
	//
	function _om_profile_sidebar_content() {

		global $loc_post;
		global $loc_fields;
		global $args;
		global $shop_link;
		global $sponsors_html;
		global $fundraiser_goal_total_from_orders;
		global $is_currently_shopping_for;
		global $closed;

		//
		//	Sidebar Logo
		//
		if( ! empty( $loc_fields['logo_image'] ) && ! $is_currently_shopping_for ) {

			// Banner Image
			echo '<div class="text-center">';
			echo '<img src="' . $loc_fields['logo_image']['sizes']['om-profile-280'] . '" title="' . $loc_post->post_title . '" alt="' . $loc_post->post_title . '">';
			echo '</div>';
			echo '<br>';
		}

		?>


		<!--
			Support Button
		-->

		<?php

			$support_link = ( ! $closed ) ?
				get_permalink( $loc_post->ID ) . '?shopfor=' . $loc_post->ID :
				get_permalink( $loc_post->ID );
		?>
		<?php if( !$is_currently_shopping_for && !$closed): ?>
		<a class="btn btn-lg btn-success btn-block" href="<?php echo $support_link; ?>">Support This Fund</a>
		<?php endif ?>
		<br>

		<!-- Social sharing block -->
		<?php
			$share_link = ( ! $closed ) ?
				get_permalink( $loc_post->ID ) . '?shopfor=' . $loc_post->ID :
				get_permalink( $loc_post->ID );
			$mail_link = "mailto:?subject=Check out this fundraiser for $loc_post->post_title&body=Check out the fundraiser here: $share_link";
		?>

		<div id="fundraiser_profile_share" class="text-center how_support segment well">

			<h4>Share This Fundraiser!</h4>

			<div class="icon_buttons">
				<a class="btn-facebook"
					href="http://www.facebook.com/sharer.php?u=<?php echo $share_link ?>&t=<?php echo urlencode( $loc_post->post_title ); ?>"
					class="btn facebook"
					rel="nofollow"
					target="_blank"
					title="Share on Facebook">
					<span class="fa fa-facebook"></span>
				</a>
				<a class="btn-twitter"
					href="http://twitter.com/home?status=<?php echo $share_link ?>+%28<?php echo urlencode( $loc_post->post_title ); ?>%29"
					class="btn twitter"
					rel="nofollow"
					target="_blank"
					title="Share on Twitter">
					<span class="fa fa-twitter"></span>
				</a>
				<a class="btn-google"
					class="btn google"
					rel="nofollow"
					title="Email a Friend"
					href="<?php echo $mail_link ?>"><span class="fa fa-envelope"></span></a>

			</div>

			<p class="link_label">Send this link to all your friends!</p>

			<textarea onclick="this.select();" class="form-control" rows="4"><?php echo $share_link; ?></textarea>

		</div>


		<!--
			Recent donators
		-->
		<?php if( ! empty( $sponsors_html ) ): ?>
		<div class="segment well">
			<ul><li>
				<h4 class="text-center">Supporter List</h4>

				<div id="donations_list_container">
					<ul id="donations_scroller">
						<?php echo $sponsors_html; ?>
					</ul>
				</div>

			</li></ul>
		</div>
		<?php endif; ?>

		<?php
	}


	//	Grab the main template
	include( get_template_directory() . '/templates/holy_grail_master.php' );
?>
