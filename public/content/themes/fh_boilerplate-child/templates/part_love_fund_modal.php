<div id="love_fund_modal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">You are shoppping to support the One Mission Love Fund!</h3>
            </div>
            <div class="modal-body">

                <p>If you wish to benefit a specific fundraiser, please select from the list below of recently viewed fundraisers or click the "Find a fundraiser to support!" button.</p>

                <?php

                    if( $fundraisers_from_cookie !== false && is_array( $fundraisers_from_cookie ) && ! empty( $fundraisers_from_cookie ) ) {

                        echo '<div id="love_fund_notice">';
                        echo '<h3 class="text-center">Your Recently Viewed Fundraisers:</h3>';

                        foreach( $fundraisers_from_cookie as $fundraiser ) {

                            $current_url = str_replace( '?clearshopfor=1', '', Utils::get_current_url() );
                            $shop_for = $current_url . '?shopfor=' . $fundraiser['id'];

                            echo '<div class="row">';

                            echo '<div class="col-sm-4">';
                            echo '<img src="' . $fundraiser['logo'] . '" alt="' . $fundraiser['name'] . '" title="' . $fundraiser['name'] . '">';
                            echo '</div>';

                            echo '<div class="col-sm-8">';
                            echo '<span class="title">' . $fundraiser['name'] . '</span>';
                            echo '<p class="description">' . $fundraiser['desc'] . '</p>';
                            echo '<a href="' . $shop_for . '" class="btn btn-md btn-success"><i class="fa fa-heart"></i>&nbsp; Support this fundraiser and check-out &raquo;</a>';
                            echo '</div>';

                            echo '</div>';
                            echo '<hr>';
                        }

                        echo '</div>';

                    } else {

                        ?>

                            <div class="text-center">
                                <a href="/support/" class="btn btn-success btn-block"><i class="fa fa-star"></i>&nbsp; Find a fundraiser to support!</a>
                            </div>

                            <hr>

                        <?php
                    }
                ?>

                <p>If you still want to support the One Mission Love Fund, that's great. Thank you! Please click the button below.</p>
            </div>
            <div class="modal-footer">
                <button id="shop_love_fund" type="button" class="btn btn-info btn-block" data-dismiss="modal"><i class="fa fa-shopping-cart"></i>&nbsp; Shop for the One Mission Love Fund! &raquo;</button>
            </div>
        </div>
    </div>
</div>
