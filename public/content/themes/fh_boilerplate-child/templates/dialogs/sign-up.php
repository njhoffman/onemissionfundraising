<?php
	$facebook_url = "https://onemission.fund/wp/wp-login.php?" .
		"action=wordpress_social_authenticate&mode=login&provider=Facebook" .
		"&redirect_to=https%3A%2F%2Fonemission.fund%3A80%2F";
	$google_url = "https://onemission.fund/wp/wp-login.php?" .
		"action=wordpress_social_authenticate&mode=login&provider=Google" .
		"&redirect_to=https%3A%2F%2Fonemission.fund%3A80%2F";
?>
<form id="signup-form" class="dialog login-form white-popup-block mfp-hide" style="max-width:450px;">
	<input type="hidden" name="create" value="true" />
	<input type="hidden" name="create_fundraiser" value="false" />
	<h4 class="text-center subheader">Start a Fund</h4>
	<h2 class="text-center">Create An Account</h2>
	<p class="text-center"><i>One Mission makes it easy to be a World Changer through our unique platform, combining product fundraising with crowdfunding.</i></p>
	<div class="form-group row">
		<div class="col-xs-6">
			<input type="text" class="form-control first-name" id="signup-first-name" name="first_name" placeholder="First Name">
		</div>
		<div class="col-xs-6" style="padding-left:0;">
			<input type="text" class="form-control last-name" id="signup-last-name" name="last_name" placeholder="Last Name">
		</div>
	</div>
	<div class="form-group">
		<input type="text" class="form-control username" id="signup-email" name="username" placeholder="Email">
	</div>
	<div class="form-group">
		<input type="text" class="form-control confirm-username" id="signup-confirm-username" name="confirm_username" placeholder="Confirm Email">
	</div>
	<div class="form-group">
		<input type="password" class="form-control password" id="signup-password" name="password" placeholder="Password">
	</div>
	<p class="status"></p>
	<button type="submit" class="btn btn-lg btn-block btn-success create-account">Create Account</button>
	<p class="text-center" style="margin: 10px 0px">
		Already have a One Mission account?
		<a class="popup-with-form" href="#signin-form">Sign in.</a>
	</p>
	<div style="display: none" class="divider"><span>or</span></div>
	<div class="wp-social-login-provider-list">
		<a href="<?php echo $facebook_url; ?>"
			style="display: none"
			rel="nofollow"
			data-provider="Facebook"
			class="btn btn-lg btn-block btn-outline btn-outline-blue space-2 social-login wp-social-login-provider wp-social-login-provider-facebook">
			<i class="fa fa-facebook"></i> &nbsp; Sign Up with Facebook
		</a>
		<a href="<?php echo $google_url; ?>"
			style="display: none"
			rel="nofollow"
			data-provider="Google"
			class="btn btn-lg btn-block btn-outline btn-outline-red space-2 social-login wp-social-login-provider wp-social-login-provider-google">
			<i class="fa fa-google-plus"></i> &nbsp; Sign Up with Google
		</a>
	</div>
	<?php wp_nonce_field( 'ajax-signup-nonce', 'security-signup' ); ?>
</form>
