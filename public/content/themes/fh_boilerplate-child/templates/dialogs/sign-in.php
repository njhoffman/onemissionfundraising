<?php
	$facebook_url = "https://onemission.fund/wp/wp-login.php?" .
		"action=wordpress_social_authenticate&mode=login&provider=Facebook" .
		"&redirect_to=https%3A%2F%2onemission.fund%3A80%2F";
	$google_url = "https://onemission.fund/wp/wp-login.php?" .
		"action=wordpress_social_authenticate&mode=login&provider=Google" .
		"&redirect_to=https%3A%2F%2Fonemission.fund%3A80%2F";
?>
<form id="signin-form" class="dialog login-form white-popup-block mfp-hide" style="max-width:450px;">

	<h2 class="text-center">Login</h2>

	<div class="form-group">
		<input type="text" class="form-control username" id="signin-email" name="username" placeholder="Email">
	</div>
	<div class="form-group">
		<input type="password" class="form-control password" id="signin-password" name="password" placeholder="Password">
		<p class="text-center lost"><a href="<?php echo wp_lostpassword_url(); ?>">Lost your password?</a></p>
	</div>
	<p class="status"></p>
	<button type="submit" class="btn btn-lg btn-block btn-success sign-in">Login</button>

	<div style="display: none" class="divider"><span>or</span></div>

		<a href="<?php echo $facebook_url; ?>"
			style="display: none"
			rel="nofollow"
			target="_blank"
			data-provider="Facebook"
			class="btn btn-lg btn-block btn-outline btn-outline-blue space-2 social-login wp-social-login-provider wp-social-login-provider-facebook">
			<i class="fa fa-facebook"></i> &nbsp; Login with Facebook
		</a>

		<a href="<?php echo $google_url; ?>"
			style="display: none"
			target="_blank"
			rel="nofollow"
			data-provider="Google"
			class="btn btn-lg btn-block btn-outline btn-outline-red space-2 social-login wp-social-login-provider wp-social-login-provider-google">
			<i class="fa fa-google-plus"></i> &nbsp; Login with Google
		</a>


	<p class="text-center">Don't have a One Mission account yet? <br />
	<a href="#signup-form" class="popup-with-form">Sign up.</a>
	<?php wp_nonce_field( 'ajax-signin-nonce', 'security-signin' ); ?>
</form>
