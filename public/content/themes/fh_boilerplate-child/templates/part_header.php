<?php

    if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

    // Grab current URL for usage within clear link
    $clear_url = home_url( $wp->request ) . '?clearshopfor=1';
?>

<!-- Top Navigation: -->
<div id="om_affix_wrap">
    <div id="om_affix" data-spy="affix" data-offset-top="0" data-offset-bottom="200">

        <div class="body_wrap">

            <nav id="nav_header" class="navbar navbar-inverse" role="navigation">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-main-nav-collapse-1">MENU</button>
                    <a class="navbar-brand" href="<?php echo home_url(); ?>">
                        <h1><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo_nav.png" title="<?php bloginfo( 'name' ); ?>" alt="<?php bloginfo( 'name' ); ?>" /></h1>
                    </a>
                </div>

					<?php
						// Add account logic for nav links
						$account_manage_link    = '';
						$fundraiser_manage_link = '';
						$fundraiser_start_link = '';
						$fundraiser_setup_link = '';
						$fundraiser_start_link =
							'<li class="menu-item start-fund">' .
								'<a href="#signup-form" class="popup-with-form" title="Start Fund" name="start_fund_link">' .
								'START FUND</a></li>';
						$login_logout_link      = '<li class="menu-item"><a class="popup-with-form" title="Login" href="#signin-form">Login</a></li>';

						$signup_link = '';


						if( is_user_logged_in() ) {

							$signup_link = '';
							$fundraiser_start_link = '<li class="menu-item start-fund"><a href="#startfund-form" class="popup-with-form" title="Start Fund">START FUND</a></li>';
							$fundraiser_setup_link = '<li class="menu-item setup-fund hidden"><a href="/wp/wp-admin/?page=fundraiser-profile-editor" ' .
								' title="Setup Fund">SETUP FUND</a><a class="popup-with-form hidden" href="#setupfund-form"></a></li>';
							$login_logout_link   = '<li class="menu-item"><a title="Logout" href="' . wp_logout_url() . '">Logout</a></li>';

							// If user is a fundraiser
							if( _om_check_user_role( 'fundraiser' ) ) {
								$fundraiser_start_link = '<li class="menu-item start-fund hidden"><a href="#startfund-form" class="popup-with-form" title="Start Fund">START FUND</a></li>';

								$user_id = get_current_user_id();
								$profile_id = get_user_meta( $user_id, 'fundraiser_profile_id' )[0];
								$fundraiser_post = get_post($profile_id);
								if (isset($fundraiser_post) && ($fundraiser_post->post_status == 'publish' || $fundraiser_post->post_status == 'private')) {
									$fundraiser_manage_link = '<li class="menu-item setup-fund"><a href="' . admin_url() . '" title="Manage Fund">MANAGE FUND</a></li>';
								} else {
									$fundraiser_setup_link = '<li class="menu-item setup-fund"><a href="' . admin_url('?page=fundraiser-profile-editor') .
										'" title="Setup Fund">SETUP FUND</a><a class="popup-with-form hidden" href="#setupfund-form"</a></li>';
								}
							// if user is an administrator
							} else if (is_super_admin()) {
								$fundraiser_start_link = '<li class="menu-item admin"><a href="' . admin_url() . '" title="Admin">ADMIN AREA</a></li>';
								$account_manage_link = '';
							// everyone else
							} else {
								$account_manage_link = '<li class="menu-item"><a title="My Account" href="/my-account/">Account</a></li>';
							}
						}
					?>
                </li>

                <!-- Non-responsive menu -->
                <?php ob_start(); ?>

                <ul id="om_non_responsive_nav" class="nav navbar-nav">
                    <li class="menu-item drawer_li"><a class="drawer_link" data-target="#drawer_fundraisers" href="#">Find a Fundraiser &nbsp;<i class="fa fa-caret-down"></i></a></li>
                    <li class="menu-item drawer_li"><a class="drawer_link" data-target="#drawer_shopping" href="#">Shop with Purpose &nbsp;<i class="fa fa-caret-down"></i></a></li>
                    <li class="menu-item drawer_li"><a class="drawer_link" data-target="#drawer_fundraise_with_us" href="#">Fundraise with Us &nbsp;<i class="fa fa-caret-down"></i></a></li>
                    <li class="menu-item"><a href="/about-us">About Us</a></li>

                </ul>
                <ul id="om_non_responsive_nav_right" class="nav navbar-nav navbar-right">
                    <?php echo $account_manage_link; ?>
                    <?php echo $fundraiser_setup_link; ?>
                    <?php echo $fundraiser_start_link; ?>
                    <?php echo $fundraiser_manage_link; ?>
                    <?php echo $login_logout_link; ?>
                    <?php echo $signup_link; ?>
                    <li class="menu-item"><a id="om_nav_search_trigger" href="#"><span class="fa fa-search"></span></a></li>
                </ul>

                <?php $nav_non_responsive_items = ob_get_clean(); ?>

					<!-- Reveal Dialogs -->
					 <?php require_once(__DIR__ . "/dialogs/sign-up.php"); ?>
					 <?php require_once(__DIR__ . "/dialogs/sign-in.php"); ?>
					 <?php require_once(__DIR__ . "/dialogs/start-fund.php"); ?>

                <!-- Search form -->
                <?php ob_start(); ?>

                    <form id="om_nav_search" class="navbar-form navbar-right" role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>" >
                        <div class="input-group">
                            <label for="head_search_input" class="sr-only">Search Website:</label>
                            <input id="head_search_input" type="text" class="form-control" value="<?php echo get_search_query(); ?>" name="s" placeholder="search website" />

                            <span class="input-group-btn">
                                <button type="submit" class="btn">
                                    <span class="fa fa-search"></span>
                                </button>
                            </span>
                        </div>
                    </form>

                <?php $nav_search_form = ob_get_clean(); ?>

                <!-- Responsive Navigation -->
                <?php

                    //
                    //  Append a WooCommerce simple shopping cart button if applicable
                    //
                    $woo_cart = ( function_exists( '_fh_woo_simple_cart_link_html' ) ) ?
                        '<div id="navbar_cart" class="navbar-right">' . _fh_woo_simple_cart_link_html() . '</div>' :
                        '';

                    wp_nav_menu(array(
                        'menu'            => 'main-nav',
                        'theme_location'  => 'main-nav',
                        'depth'           => 2,
                        'container'       => 'div',
                        'container_class' => 'collapse navbar-collapse menu-header',
                        'container_id'    => 'bs-main-nav-collapse-1',
                        'menu_class'      => 'menu nav navbar-nav',
                        'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                        'walker'          => new wp_bootstrap_navwalker(),
                        'items_wrap'      => $nav_non_responsive_items . '<ul id="%1$s" class="%2$s">%3$s' . $login_logout_link . '</ul>' . $nav_search_form . $woo_cart
                    ));
                ?>
            </nav>
        </div>
    </div>
</div>

<!-- Navigation Drawers (sub navigation) -->
<div id="sub_nav">
    <div id="drawer_wrap" class="body_wrap">

        <!-- Fundraisers Drawer -->
        <div id="drawer_fundraisers" class="sub_nav_drawer">

            <!-- <a href="#" class="close_drawer" title="Close Drawer"><i class="fa fa-chevron-circle-up"></i></a> -->

            <div class="row">

                <div class="col-sm-4 text-center">

                    <h4><strong>Find a Fundraiser to Support</strong></h4>
                    <hr>

                    <ul class="list-unstyled">
                        <li><a href="/support/">All Fundraisers</a></li>
                        <li><a href="/support/?by-type=churches-youth-groups">Churches &amp; Youth Ministry</a></li>
                        <li><a href="/support/?by-type=schools-fine-arts-athletics">Schools, Fine Arts, &amp; Athletics</a></li>
                        <li><a href="/support/?by-type=non-profit-organizations">Non-Profit Organizations</a></li>
                        <li><a href="/support/?by-type=mission-groups-missionaries">Mission Trips &amp; Groups</a></li>
                        <li><a href="/support/?by-type=medical-causes">Medical Causes</a></li>
                        <li><a href="/support/?by-type=adoptive-parents">Adoption</a></li>
                        <li><a href="/support/?by-type=community-benefits">Community Benefit</a></li>
                        <li><a href="/support/?by-type=other-groups-individuals">Other Groups &amp; Individuals</a></li>
                    </ul>

                </div>
                 <div class="col-sm-3 text-center">

                    <h4><strong>Learn More</strong></h4>
                    <hr>
                    <ul class="list-unstyled">
                        <li><a href="/about-us/how-it-works/">How it Works</a></li>
                        <li><a href="/about-us/">About Us</a></li>
                        <li><a href="/fundraise/">Fundraise with Us</a></li>
                    </ul>
                    <div style="max-width:170px; margin:auto;">
                        <a href="/about-us/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/about-video-img.png" title="Watch a video about OneMission" /></a>
                        <h4 class="shopping_nav_link"><a href="/about-us/">Watch Video</a></h4>
                    </div>

                </div>
                <div class="col-sm-5">

                    <h4 class="text-center"><strong>Featured Fundraiser</strong></h4>
                    <hr>

                    <!-- featured_fundraiser_id, featured_fundraiser_description -->

                    <?php

                        $featured_fundraiser_id     = (int) get_field( 'featured_fundraiser_id', 'option' );
                        $featured_fundraiser_title  = get_the_title( $featured_fundraiser_id );
                        $featured_fundraiser_uri    = get_page_uri( $featured_fundraiser_id );
                        $featured_fundraiser_desc   = get_field( 'featured_fundraiser_description', 'option' );
                        $featured_fundraiser_fields = get_fields( $featured_fundraiser_id );
                        $featured_fundraiser_logo   = '';

                        // Fundraiser Logo
                        if( !empty( $featured_fundraiser_fields['logo_image']['sizes']['shop_single'] ) ) {

                            $featured_fundraiser_logo = '<a href="/' . $featured_fundraiser_uri . '" class="pull-right"><img id="drawer_featured_logo" src="' . $featured_fundraiser_fields['logo_image']['sizes']['thumbnail'] . '"></a>';
                        }

                        echo $featured_fundraiser_logo;
                        echo '<h4><strong><a href="/' . $featured_fundraiser_uri . '">' . $featured_fundraiser_title . '</a></strong></h4>';
                        echo '<p>' . $featured_fundraiser_desc . '</p>';

                        // Shop to support link
                        echo '<div class="text-center"><a href="/products/?shopfor=' . $featured_fundraiser_id . '">Shop to Support ' . $featured_fundraiser_title . ' &raquo;</a></div>';
                    ?>

                </div>
            </div>
        </div>

        <!-- Shopping Drawer -->
        <div id="drawer_shopping" class="sub_nav_drawer">

            <!-- <a href="#" class="close_drawer" title="Close Drawer"><i class="fa fa-chevron-circle-up"></i></a> -->

            <div class="row">

                <div class="col-sm-2">
                    <a href="/product-category/apparel/apparel-by-message/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/product_categories/new/apparel.jpg" title="<?php bloginfo( 'name' ); ?> - Apparel" alt="<?php bloginfo( 'name' ); ?> - Apparel" /></a>
                    <h4 class="shopping_nav_link"><a href="/product-category/apparel/apparel-by-message/">Apparel</a></h4>
                    <ul class="list-unstyled text-center">
                        <li><a href="/product-category/apparel/apparel-by-message/">Shop by message</a></li>
                        <li><a href="/product-category/apparel/unisex/">Unisex</a></li>
                        <li><a href="/product-category/apparel/women/">Women</a></li>
                        <li><a href="/product-category/apparel/kids/">Kids</a></li>
                    </ul>
                </div>

                <div class="col-sm-2">
                    <a href="/product-category/art-prints/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/product_categories/new/art_prints.jpg" title="<?php bloginfo( 'name' ); ?> - Art" alt="<?php bloginfo( 'name' ); ?> - Art" /></a>
                    <h4 class="shopping_nav_link"><a href="/product-category/art-prints/">Art &amp; Note Cards</a></h4>
                    <ul class="list-unstyled text-center">
                        <li><a href="/product-category/art-prints/">Art Prints</a></li>
                        <li><a href="/product-category/note-cards/">Note Cards</a></li>
                    </ul>
                </div>

                <div class="col-sm-2">
                    <a href="/product-category/haitian-made/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/product_categories/new/haitian_made.jpg" title="<?php bloginfo( 'name' ); ?> - Haitian Made" alt="<?php bloginfo( 'name' ); ?> - Haitian Made" /></a>
                    <h4 class="shopping_nav_link"><a href="/product-category/haitian-made/">Haitian Made</a></h4>
                    <ul class="list-unstyled text-center">
                        <li><a href="/product-category/haitian-made/haitian-metal-art/">Haitian Metal Art</a></li>
                        <li><a href="/product-category/haitian-made/haitian-bags-pouches/">Haitian Bags &amp; Pouches</a></li>
                        <li><a href="/product-category/haitian-made/haitian-made-jewelry/">Haitian Made Jewelry</a></li>
                    </ul>
                </div>

                <div class="col-sm-2">
                    <a href="/product-category/jewelry/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/product_categories/new/sterling_silver.jpg" title="<?php bloginfo( 'name' ); ?> - Jewelry" alt="<?php bloginfo( 'name' ); ?> - Jewelry" /></a>
                    <h4 class="shopping_nav_link"><a href="/product-category/jewelry/">Jewelry</a></h4>
                    <ul class="list-unstyled text-center">
                        <li><a href="/product-category/jewelry/leather-cuffs/">Leather Cuffs</a></li>
                        <li><a href="/product-category/jewelry/silver-cuffs/">Sterling Silver Cuffs</a></li>
                        <li><a href="/product-category/jewelry/bar-necklaces/">Sterling Silver Necklaces</a></li>
                    </ul>
                </div>

                <div class="col-sm-2">
                    <a href="/product-category/candles-and-wax-melts/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/product_categories/new/candles_wax_melts.jpg" title="<?php bloginfo( 'name' ); ?> - Candles &amp; Tarts" alt="<?php bloginfo( 'name' ); ?> - Candles &amp; Tarts" /></a>
                    <h4 class="shopping_nav_link"><a href="/product-category/candles-and-wax-melts/">Candles &amp; Melts</a></h4>
                    <ul class="list-unstyled text-center">
                        <li><a href="/product-category/candles-and-wax-melts/soy-candles/">Soy Candles</a></li>
                        <li><a href="/product-category/candles-and-wax-melts/wax-melts/">Soy Wax Melts</a></li>
                    </ul>
                </div>

                <div class="col-sm-2">
                    <a href="/product-category/coffee-mugs/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/product_categories/new/coffee_mugs.jpg" title="<?php bloginfo( 'name' ); ?> - Coffee Mugs" alt="<?php bloginfo( 'name' ); ?> - Coffee Mugs" /></a>
                    <h4 class="shopping_nav_link"><a href="/product-category/coffee-mugs/">Coffee Mugs</a></h4>
                </div>

                <div class="col-sm-2">
                    <a href="/custom-canvas/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/product_categories/new/canvas.jpg" title="<?php bloginfo( 'name' ); ?> - Custom Canvas" alt="<?php bloginfo( 'name' ); ?> - Custom Canvas" /></a>
                    <h4 class="shopping_nav_link"><a href="/custom-canvas/">Custom Canvas</a></h4>
                </div>

                <div class="pull-right clearfix">
                    <br>
                    <br>
                    <a class="btn btn-lg btn-purple" href="/product-category/gift-cards/"><i class="fa fa-gift"></i>&nbsp; <strong>Gift Cards!</strong></a>
                </div>

            </div>
        </div>

        <!-- Fundraise with us drawer -->
        <div id="drawer_fundraise_with_us" class="sub_nav_drawer">

            <!-- <a href="#" class="close_drawer" title="Close Drawer"><i class="fa fa-chevron-circle-up"></i></a> -->

            <div class="row">

                <div class="col-sm-3 text-center">

                    <h4><strong>Fundraising Programs</strong></h4>
                    <hr>
                    <ul class="list-unstyled">
                        <li><a href="/fundraise/">All Programs</a></li>
                        <li><a href="/fundraise/online-store/">Online Fundraising Store</a></li>
                        <li><a href="/fundraise/custom-apparel/">Custom Apparel</a></li>
                        <li><a href="/fundraise/change-challenge/">Change Challenge</a></li>
                        <li><a href="/fundraise/candle-fundraiser/">Candle Fundraiser</a></li>
                        <li><a href="/fundraise/photo-canvas-fundraiser/">Photo Canvas Fundraiser</a></li>
                        <li><a href="/fundraise/customized-fundraiser/">Custom Fundraiser</a></li>
                    </ul>
                    <a href="/fundraise/">Learn More &raquo;</a>

                </div>

                <div class="col-sm-3 text-center">

                    <h4><strong>Learn More</strong></h4>
                    <hr>
                    <a href="/fundraise/">Learn about all the fundraising options available with One Mission &raquo;</a>
                    <a style="display:block; margin-top:10px;" href="/fundraise/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/learn-about-all-fundraising-options-btn.png" title="Learn About All Fundraising Options" /></a>

                </div>

                <div class="col-sm-3 text-center">

                    <h4><strong>Start an Online Fundraiser</strong></h4>
                    <hr>
                    <a href="/fundraise/online-store/">Create a free online fundraising store. Learn more &raquo;</a>
                    <a style="display:block; margin-top:10px;" class="popup-with-form" href="#signup-form"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/create-fundraise-store-btn.png" title="Create an Online Fundraising Store" /></a>

                </div>



                <div class="col-sm-3 text-center">

                    <h4><strong>Ideas for:</strong></h4>
                    <hr>

                    <ul class="list-unstyled">
                        <li><a href="/fundraise/youth-ministry/">Churches &amp; Youth Groups</a></li>
                        <li><a href="/fundraise/schools/">Schools, Fine Arts, &amp; Athletics</a></li>
                        <li><a href="/fundraise/non-profit/">Non-Profit Organizations</a></li>
                        <li><a href="/fundraise/mission-trip/">Mission Trips &amp; Groups</a></li>
                        <li><a href="/fundraise/medical/">Medical Causes</a></li>
                        <li><a href="/fundraise/adoption/">Adoption</a></li>
                        <li><a href="/fundraise/community/">Community Benefit</a></li>
                        <li><a href="/fundraise/memorial/">Memorial Funds</a></li>
                    </ul>

                </div>
            </div>
        </div>


    </div>
</div>

<!-- /Top Navigation -->

<div id="over_body_wrap">
<div class="body_wrap">

    <!-- Shopping for/cart CTA -->
    <?php

    global $woocommerce;

    $support_html = '';
    $cart_count   = (int) $woocommerce->cart->cart_contents_count;
    $woo_actions  = '';


    if(
        $donate_amount = _om_get_total_donation() ||
        $cart_count ||
        ( isset( $_COOKIE['shopping_for_id'] ) && $_COOKIE['shopping_for_id'] )
    ) {

        if( isset( $_COOKIE['shopping_for_id'] ) && is_numeric( $_COOKIE['shopping_for_id'] ) && $_COOKIE['shopping_for_id'] ) {

            // Shopping to support a group

			  $support_post = Timber::get_post($_COOKIE['shopping_for_id']);

			  $closed = false;

			  if( $support_post->fundraiser_timed_or_continuous == 'timed' ) {

				  $unix_start_date   = strtotime( $support_post->fundraiser_start_date );
				  $unix_end_date     = strtotime( $support_post->fundraiser_end_date );
				  $unix_now          = time();

				  $closed = _om_is_fundraiser_closed( $support_post->fundraiser_timed_or_continuous, $support_post->fundraiser_start_date, $support_post->fundraiser_end_date );
				}
            //
            //  If $_GET['key'] is set then re-calculate shopping for resetting total
            //  donation amount shown in header
            //
            if( isset( $_GET['key'] ) && ! empty( $_GET['key'] ) ) {

                _om_set_shop_for( $_COOKIE['shopping_for_id'] );
            }

            if( ! empty( $_COOKIE['fundraiser_profile_logo'] ) ) {

                $support_html .= '<div id="supporting_logo"><a href="' . $_COOKIE['fundraiser_guid'] . '"><img src="' . $_COOKIE['fundraiser_profile_logo'] . '" title="' . $_COOKIE['fundraiser_name'] . '" alt="' . $_COOKIE['fundraiser_name'] . '"></a></div>';
            }

            // Progress bar generation
            $clean_goal_amount = floatval( $_COOKIE['fundraiser_goal_total'] );
            $clean_goal_status = floatval( $_COOKIE['fundraiser_goal_total_from_orders'] );

				$hide_progress_bar = false;

				if(! floatval( $clean_goal_amount ) || $closed) {
					$hide_progress_bar = true;
				}

				$h_s = ( $clean_goal_status ) ? 'h3' : 'h2';

				$fundraiser_name = stripcslashes($_COOKIE['fundraiser_name']);
        $support_html .= '<' . $h_s . '>Supporting <a href="' . $_COOKIE['fundraiser_guid'] . '">' . $fundraiser_name . '</a></' . $h_s . '>';
        $support_html .= '<div id="cta_clear"><a href="' . $clear_url . '">(clear shopping for)</a></div>';


        // Ensure there's a goal amount
        if( isset($clean_goal_amount) && isset($clean_goal_status) ) {

					$current_percent = $clean_goal_amount > 0 ? floatval( $clean_goal_status ) / floatval( $clean_goal_amount ) : 0;

					if( $current_percent >= 1 ) {

						$css_bar_percent      = 100;
						$progress_bar_precent = 100;

					} else {

						$css_bar_percent      = $current_percent * 100;
						$progress_bar_precent = number_format( ( $current_percent * 100 ) );
						$progress_bar_precent = ( (float) $progress_bar_precent > 100 ) ? 100 : $progress_bar_precent;
					}

					if (! $hide_progress_bar) {
						// Add progress bar
						$support_html .= '<div id="cta_progress_bar_wrap">';
						$support_html .= '<div class="progress progress-striped active">';
						$support_html .= '<div id="cta_progress_bar" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="' . $progress_bar_precent . '" aria-valuemin="0" aria-valuemax="100" style="width: ' . $css_bar_percent . '%;">';
						$support_html .= $progress_bar_precent . '%';
						$support_html .= '</div>';
						$support_html .= '</div>';
						$support_html .= '<span class="left_text">$' . number_format( $clean_goal_status, 2 ) . '<br>Raised</span>';
						$support_html .= '<span class="right_text">$' . number_format( $clean_goal_amount, 2 ) . '<br>Goal!</span>';
						$support_html .= '</div>';

					 } elseif( floatval( $clean_goal_status ) ) {

						 $support_html .= '<p class="h4"><strong>$' . number_format( $clean_goal_status, 2 ) . ' Raised!</strong></p>';

					 } else {

						 $support_html .= '<p>No donations have been raised!</p>';
					 }

					 //
					 //	Timed fundraiser
					 //
					 if( $support_post->fundraiser_timed_or_continuous == 'timed' ) {

						 $unix_start_date   = strtotime( $support_post->fundraiser_start_date );
						 $unix_end_date     = strtotime( $support_post->fundraiser_end_date );
						 $unix_now          = time();
						 $human_date_format = 'M j, Y';
						 $human_start_date  = date( $human_date_format, $unix_start_date );
						 $human_end_date    = date( $human_date_format, $unix_end_date );

						 $support_html .= '<p class="start_end">';

						 // Start Date
						 if( ! empty( $human_start_date ) ) {
							 $support_html .= 'Start: ' . $human_start_date . ' - ';
						 }

						 // End Date
						 if( ! empty( $human_end_date ) ) {
							 $support_html .= 'End: ' . $human_end_date;
						 }

						 $support_html .= '</p>';
					 }
				}

        } else {

            // Shopping to support the love fund
            $support_html .= '<div id="supporting_logo"><img src="' . get_stylesheet_directory_uri() . '/img/logo_status_love_fund.jpg" title="One Mission Love Fund" alt="One Mission Love Fund"></div>';
            $support_html .= '<h2>Shopping for the <a href="/about-us/one-mission-love-fund/">One Mission Love Fund</a>!</h2>';
            $support_html .= '<a class="btn btn-info" href="/support/">Find a fundraiser to support!</a>';
        }
    }

    $curr_page = basename(str_replace('?'. $_SERVER['QUERY_STRING'], "", $_SERVER['REQUEST_URI']));
    if ($curr_page === 'cart') {
      // dont show any buttons on the cart page
      $woo_actions .= '';
    } elseif ( $cart_count ) {

        $cart_count_s  = ( $cart_count > 1 ) ? 's' : '';
        $cart_total    = _om_woo_get_cart_total_float( true, true, true );
        $cart_url      = $woocommerce->cart->get_cart_url();
        $checkout_url  = $woocommerce->cart->get_checkout_url();
        $donate_amount = _om_get_total_donation();

        // Cart Ribbon
        $woo_actions .= '<div id="cart_ribbon">';
        $woo_actions .= '<a href="' . $cart_url . '">';
        $woo_actions .= '<div class="cart_title">Cart &nbsp;<i class="fa fa-shopping-cart"></i></div>';
        $woo_actions .= '<div class="cart_count">' . $cart_count . ' Item' . $cart_count_s . '</div>';
        $woo_actions .= '<div class="cart_total">$' . number_format( $cart_total, 2 ) . '</div>';
        $woo_actions .= '</a>';
        $woo_actions .= '</div>';

        // Checkout Ribbon
        // $woo_actions .= '<div id="checkout_ribbon">';
        // $woo_actions .= '<a href="' . $checkout_url . '">';
        // $woo_actions .= '<div class="checkout_title">Checkout Now!</div>';
        // $woo_actions .= '<div class="checkout_heart"><i class="fa fa-heart"></i></div>';
        // $woo_actions .= '</a>';
        // $woo_actions .= '</div>';

        // Donation Amount Notification
        if( $donate_amount ) {

            $woo_actions .= '<div id="cta_donation_amt">';
            $woo_actions .= '$' . number_format( $donate_amount, 2 ) . ' will be donated!';
            $woo_actions .= '</div>';
        }

    } elseif ( ! empty( $support_html ) ) {

        $woo_actions .= '<div id="go_shopping_btn_new">';
        $woo_actions .= '<a class="btn btn-lg btn-success" href="/products/"><i class="fa fa-shopping-cart"></i> Shop Now</a>';
        $woo_actions .= '</div>';
    }

    if( ! empty( $woo_actions ) || ! empty( $support_html ) ) {

        echo '<div id="cta_sub_header">';

        echo $woo_actions;
        echo $support_html;

        echo '</div>';
    }


    //
    //  Global notice/warning
    //
    global $om_global_warning;

    if( ! empty( $om_global_warning ) ) {

        echo '<div id="global_warn" class="alert alert-info" role="alert">';
        echo '<p>' . $om_global_warning . '</p>';
        echo '</div>';
    }
?>
