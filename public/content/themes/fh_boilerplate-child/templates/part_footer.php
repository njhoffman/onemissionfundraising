	<?php do_action( '_fh_footer_prepend' ); ?>

	<noscript class="alert alert-danger">
        <strong>Uh oh!</strong> Looks like you have JavaScript disabled. To use all of this site's features you must enable JavaScript! This is just a friendly warning =)
    </noscript>

	<footer class="inverse_a">
		<div class="container">

		<?php do_action( '_fh_footer_inside_prepend' ); ?>

		<?php

			if( FH_FOOTER_WIDGETS_NUM ) {

				echo '<div id="foot_widgets" class="row">' . "\n";

				for( $i = 1; $i <= FH_FOOTER_WIDGETS_NUM; $i++ ) {

					echo '<div class="col-sm-' . FH_FOOTER_WIDGETS_BS_COL . '">' . "\n";
					echo '<div class="' . FH_FOOTER_WIDGETS_CLASSES . '">' . "\n";

					echo _fh_get_sidebar_html( FH_FOOTER_WIDGETS_ID . '_' . $i );

					echo '</div>' . "\n";
					echo '</div>' . "\n";
				}

				echo '</div>' . "\n";
			}

		?>

		<div id="footer_positioned">

			<img id="foot_fox" src="<?php echo get_stylesheet_directory_uri(); ?>/img/characters/fox_be_kind.png" alt="Be Kind!" title="Be Kind!">

			<div id="foot_socials">
	          	<?php _fh_social_icons(); ?>
	        </div>

        </div>

		<div class="bottom text-center">
			<p>&copy; <?php echo date('Y'); ?> <a href="<?php echo home_url(); ?>"><?php bloginfo( 'name' ); ?></a>. All rights reserved.</p>
		</div>

		<?php do_action( '_fh_footer_inside_append' ); ?>

		</div>
	</footer>

</div> <!-- Ends .body_wrap -->
</div> <!-- Ends #over_body_wrap -->

<?php do_action( '_fh_footer_append' ); ?>