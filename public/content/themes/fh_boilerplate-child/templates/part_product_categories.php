<div class="grid_content row">
	<div class="grid_item col-md-6 col-sm-6 col-xs-6">
		<a class="overlay-effect" href="/product-category/apparel/apparel-by-message/" alt="Shop for Apparel" title="Shop for Apparel">
			<div class="overlay" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/product_categories/new/apparel.jpg');">
				<div class="table">
					<div class="yellow">
						<h4>Shop Apparel</h4>
					</div>
				</div>
			</div>
		</a>
	</div>
	<div class="grid_item col-md-6 col-sm-6 col-xs-6">
		<a class="overlay-effect" href="/product-category/art-prints/" alt="Shop for Art Prints" title="Shop for Art Prints">
			<div class="overlay" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/product_categories/new/art_prints.jpg');">
				<div class="table">
					<div class="green">
						<h4>Shop Art</h4>
					</div>
				</div>
			</div>
		</a>
	</div>
	<div class="grid_item col-md-4 col-sm-4 col-xs-6">
		<a class="overlay-effect" href="/product-category/art-prints/note-cards/" alt="Shop for Note Cards" title="Shop for Note Cards">
			<div class="overlay" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/product_categories/new/note_cards.jpg');">
				<div class="table">
					<div class="purple">
						<h4>Shop Cards</h4>
					</div>
				</div>
			</div>
		</a>
	</div>
	<div class="grid_item col-md-4 col-sm-4 col-xs-6">
		<a class="overlay-effect" href="/product-category/jewelry/" alt="Shop for Jewelry" title="Shop for Jewelry">
			<div class="overlay" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/product_categories/new/sterling_silver.jpg');">
				<div class="table">
					<div class="blue">
						<h4>Shop Jewelry</h4>
					</div>
				</div>
			</div>
		</a>
	</div>
	<div class="grid_item col-md-4 col-sm-4 col-xs-6">
		<a class="overlay-effect" href="/product-category/candles-and-wax-melts/" alt="Shop for Candles" title="Shop for Candles">
			<div class="overlay" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/product_categories/new/candles_wax_melts.jpg');">
				<div class="table">
					<div class="darkBlue">
						<h4>Shop Candles &amp; Melts</h4>
					</div>
				</div>
			</div>
		</a>
	</div>
	<div class="grid_item col-md-4 col-sm-4 col-xs-6">
		<a class="overlay-effect" href="/product-category/coffee-mugs/" alt="Shop for Coffee Mugs" title="Shop for Coffee Mugs">
			<div class="overlay" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/product_categories/new/coffee_mugs.jpg');">
				<div class="table">
					<div class="tan">
						<h4>Shop Coffee Mugs</h4>
					</div>
				</div>
			</div>
		</a>
	</div>
	<div class="grid_item col-md-4 col-sm-4 col-xs-6">
		<a class="overlay-effect" href="/custom-canvas/" alt="Shop for Custom Canvas" title="Shop for Custom Canvas">
			<div class="overlay" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/product_categories/new/canvas.jpg');">
				<div class="table">
					<div class="pink">
						<h4>Shop Custom Canvas</h4>
					</div>
				</div>
			</div>
		</a>
	</div>
	<div class="grid_item col-md-4 col-sm-4 col-xs-6">
		<a class="overlay-effect" href="/product-category/haitian-made/" alt="Shop for Haitian Art" title="Shop for Haitian Art">
			<div class="overlay" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/product_categories/new/haitian_made.jpg');">
				<div class="table">
					<div class="yellow">
						<h4>Shop Haitian-Made</h4>
					</div>
				</div>
			</div>
		</a>
	</div>
</div>
