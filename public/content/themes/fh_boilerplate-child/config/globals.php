<?php

//
//  Global definitions
//
DEFINE( 'OM_FUNDRAISER_PAGE_PARENT_ID', 1341 );
DEFINE( 'OM_FUNDRAISER_FEATURED_ITEMS_PARENT_ID',686 );
DEFINE( 'OM_FUNDRAISER_FEATURED_CATEGORIES_ID', 687 );
DEFINE( 'OM_HOME_PAGE_PARENT_ID', 344 );
DEFINE( 'OM_FUNDRAISER_NEWS_CATEGORY', 124 );
DEFINE( 'OM_FUNDRAISER_PAGE_TEMPLATE', 'templates/template_profile.php' );

// Set to the id of your previously created donation product
// define( 'FH_CUSTOM_DONATION_ID', 1961 );
define( 'FH_CUSTOM_DONATION_ID', 2974 );

// Setup what unit (originally &pound;) to display on donation form
define( 'FH_DONATE_LABEL_UNIT', '$' );

// Parent category ID for fundraiser orgs - used to filter out product categories from browsing
// define( 'ORG_PRODUCTS_PARENT_CAT_ID', 125 );
define( 'ORG_PRODUCTS_PARENT_CAT_ID', 141 );

// Login/Logout page ID
define( 'OM_LOGOUT_PAGE_ID', 604 );

// Page ID for front-end FAQ
define( 'FAQ_PAGE', 2874 );

// Post ID for Custom Canvas product
define( 'OM_CUSTOM_CANVAS_POST_ID', 4515 );

// Global definition for % donation off of cart (ie: 40% of cart is donated)
define( 'OM_CART_DONATION_PERCENTAGE', .40 );

// Donation amount before taxes or after
define( 'OM_DONATION_CALC_AFTER_TAXES', false );

// Show donatable subtotal on checkout
define( 'OM_SHOW_DONATABLE_SUBTOTAL_ON_CHECKOUT', false );

// Show donation percent on checkout
define( 'OM_SHOW_DONATION_PERCENT_ON_CHECKOUT', true );

//
//  Set a global warning variable
//  - Used by fundraiser is closed message on shopfor
//
global $om_global_warning;
