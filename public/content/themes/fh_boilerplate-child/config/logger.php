<?php
require_once( 'colors.php' );

// output custom information to debug.log
if ( ! function_exists('_dbg')) {
   function _dbg()  {
		$args = func_get_args();
		$num_args = func_num_args();
		$out = "";
		$json_out = "";
		$json_options =  0;
		// $json_options = JSON_PRETTY_PRINT;
		for ($i = 0; $i < $num_args; $i++) {
			if ( is_array( $args[$i] ) || is_object( $args[$i] ) ) {
				$out .= "\n" . print_r($args[$i], true) . "\n";
				$json_out .=  "\n" . json_encode($args[$i], $json_options);
			} else {
				$out .= $args[$i];
				$json_out .= "\n" . $args[$i];
			}
		}
		$path = get_theme_root() . '/../out.log';
		file_put_contents($path, "$out\n", FILE_APPEND);

		$json_path = get_theme_root() . '/../out.json';
		file_put_contents($json_path, "$json_out", FILE_APPEND);
   }
}

function _om_write_log( $level, $msg ) {
	$colors = new Colors();
	$path = get_theme_root() . '/../actions.log';
	if (!empty($_SERVER['HTTP_REFERER'])) {
		$ref = $_SERVER['HTTP_REFERER'];
	} else {
		$ref = "none";
	}
	if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	$uri = $_SERVER['REQUEST_URI'];
	$ua = $_SERVER['HTTP_USER_AGENT'];
	$method = $_SERVER['REQUEST_METHOD'];
	$user = wp_get_current_user();

  $params = "";
  $param_n = 0;
  foreach ($_REQUEST as $reqKey => $reqValue) {
    $params .= "\t" . ($param_n > 0 ? "\t" : "") . "$reqKey: $reqValue\n";
    $param_n++;
  }

	$username = $user->ID == 0 ? "none" : $user->user_login;
	$file_line = strtoupper($level) .
		":  |user:" .
		"|ip: $ip" .
		"|uri: $uri" .
		"|method: $method" .
		"|ua: $ua" .
		"|ref: $ref|";

	$log_line = strtoupper($level) .
		":  |user: " . $colors->color($username, "cyan") .
		"|ip: $ip" .
    "|ua: $ua" .
    "|$params";

	_dbg(/* strtoupper($level) .  ": */ "$msg\n\t" . implode("\n\t", array_slice(explode('|', $log_line), 1)) );
	file_put_contents($path, $file_line . "$msg\n", FILE_APPEND);
}

function _om_info( $msg )  {
	_om_write_log('info', $msg);
}

function _om_log( $msg )  {
	_om_write_log('log', $msg);
}

function _om_warn( $msg )  {
	_om_write_log('warn', $msg);
}


// log all actions
function aal_handler() {
	static $list = array ();
	$exclude = array ( 'gettext', 'gettext_with_context' );
	$action = current_filter();
	$args   = func_get_args();
	if (! in_array( $action, $exclude )) {
		$list[] = $action;
	}
	// shutdown is the last action
	if ('shutdown' == $action) {
		// print '<pre>' . implode( "\n", $list ) . '</pre>';
		_dbg(implode( "\n => ", $list));
	}
}
// add_action( 'all', 'aal_handler', 99999, 99 );

// log time of each request
$logged_req_time = 0;
function _om_request_log_start() {
	global $logged_req_time;
	$logged_req_time = microtime(true);
}
add_action('init', '_om_request_log_start');

function _om_request_log_end() {
	$colors = new Colors();
	global $logged_req_time;
	global $statsd;
	if (empty($logged_req_time)) {
		return;
	}
	$user = wp_get_current_user();
	$logged_req = $_SERVER['REQUEST_URI'];
	$logged_req_time = microtime(true) - $logged_req_time;
	$name = "req.";

	if (wp_doing_ajax()) {
		if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'heartbeat') {
			return;
		}
		$logged_req .= isset($_REQUEST['action']) ? '?action=' . $_REQUEST['action'] : '';
		$name .= "ajax.";
	} else {
		$name .= strtolower($_SERVER['REQUEST_METHOD']) . ".";
	}

	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		if ( in_array( 'administrator', $user->roles ) ) {
			$name .= "admin";
		} else if ( in_array( 'fundraiser', $user->roles ) ) {
			$name .= "fundraiser";
		} else {
			$name .= "normal";
		}
	} else {
		$name .= "normal";
	}

	$statsd->timing($name, $logged_req_time * 1000);
	$req_time = round($logged_req_time, 2);
	$req_method = $_SERVER['REQUEST_METHOD'];
	if ($req_method === 'GET') {
		$req_method = $colors->color($req_method, 'green');
	} else if ($req_method === 'POST') {
		$req_method = $colors->color($req_method, 'purple');
	} else {
		$req_method = $colors->color($req_method, 'white');
	}
	_om_log("$req_method $logged_req ($req_time s)");

}
add_action( 'shutdown', '_om_request_log_end', 99999, 99 );

// woocommerce subscription hooks
function _log_scheduled_subscription($sub_id) {
	_om_log("Scheduled subscription: ",  $sub_id);
}
function _log_subscription_payment_complete($sub) {
	_om_log("Subscription payment complete: $sub->ID");
	// _dbg("Subscription parent id: ". $sub->get_parent_id());
	// _dbg("Subscription order id: ". $sub->order->id);
}
function _log_subscription_renewal_payment_complete($sub, $last_order) {
	_om_log("Subscription renewal payment complete: $sub->ID");
	// _dbg("Sub", $sub);
	// _dbg("Last order", $last_order);
}
function _log_subscription_payment_failed($sub, $new_status) {
	_om_log("Subscription payment failed: $new_status");
	// _dbg("Sub", $sub);
}
function _log_subscription_renewal_payment_failed($sub) {
	_om_log("Subscription renewal payment failed: ", $sub->ID);
	// _dbg("Sub", $sub);
}
function _log_subscription_status_updated($sub, $new_status, $old_status) {
	_om_log("Subscription status updated: $sub->ID - $new_status from $old_status");
	// _dbg("Sub", $sub);
}
function _log_subscription_created($sub, $order, $recurring_cart) {
	_om_log("Subscription created: $sub->ID - parent: " .  $sub->get_parent_id());
}
function _log_subscription_created_for_order($order) {
	_om_log("Subscription created for order: $order->ID");
	// _dbg("Order", $order);
}
add_action('woocommerce_scheduled_subcription_payment', '_log_scheduled_subscription', 10, 1);
add_action('woocommerce_subscription_payment_complete', '_log_subscription_payment_complete', 10, 1);
add_action('woocommerce_subscription_renewal_payment_complete', '_log_subscription_renewal_payment_complete', 10, 2);
add_action('woocommerce_subscription_payment_failed', '_log_subscription_payment_failed', 10, 2);
add_action('woocommerce_subscription_renewal_payment_failed', '_log_subscription_renewal_payment_failed', 10, 1);
add_action('woocommerce_subscription_status_updated', '_log_subscription_status_updated', 10, 3);
add_action('woocommerce_checkout_subscription_created', '_log_subscription_created', 10, 3);
add_action('subscriptions_created_for_order', '_log_subscription_created_for_order', 10, 1);

// woocommerce action hooks
function _log_order_start() { _om_log('Starting order'); }
function _log_order_completed($order_id) { _om_log("Completed order: $order_id"); }
function _log_order_processing($order_id) { _om_log("Processing order: $order_id"); }
function _log_order_pending($order_id) { _om_log("Pending order: $order_id"); }
function _log_order_on_hold($order_id) { _om_log("On-hold order: $order_id"); }
function _log_order_cancelled($order_id) { _om_log("Cancelled order: $order_id"); }
function _log_order_failed($order_id) { _om_log("Failed order: $order_id"); }
add_action('woocommerce_before_checkout_process', '_log_order_start' , 10, 1 );
add_action('woocommerce_order_status_completed', '_log_order_completed' , 10, 1 );
add_action('woocommerce_order_status_processing', '_log_order_processing' , 10, 1 );
add_action('woocommerce_order_status_pending', '_log_order_pending' , 10, 1 );
add_action('woocommerce_order_status_on-hold', '_log_order_on_hold' , 10, 1 );
add_action('woocommerce_order_status_cancelled', '_log_order_cancelled' , 10, 1 );
add_action('woocommerce_order_status_failed', '_log_order_failed' , 10, 1 );

function _log_add_to_cart($cart_key, $product_id, $quantity, $variation_id) {
	_om_log("Add to cart: product_id $product_id variation_id  $variation_id x $quantity");
}
add_action('woocommerce_add_to_cart', '_log_add_to_cart', 10, 4);
