// handles login and register dialogs

jQuery(document).ready(function($) {
  // Perform AJAX login on form submit
  console.log('loaded dialogs-change_address.js');

  $('#change-shipping-form .cancel').on('click', function(e) {
    e.preventDefault();
    $('#change-shipping-form').magnificPopup('close');
  });

  $("#change_shipping_apply").on('click', function(e){
    e.preventDefault();

    const fields = $('#change-shipping-form').serializeArray();
    const formValues = {};
    fields.forEach(function(field) {
      formValues[field.name] = field.value;
    });

    $('.shipping_details li.shipping_name').html(formValues.shipping_first_name + ' ' + formValues.shipping_last_name);
    $('.shipping_details li.shipping_address_1').html(formValues.shipping_address_1);
    $('.shipping_details li.shipping_address_2').html(formValues.shipping_address_2);
    $('.shipping_details li.shipping_city').html(formValues.shipping_city + ', ' + formValues.shipping_state + '  ' + formValues.shipping_postcode);

    fields.forEach(field => {
      if (field.value.trim() !== '') {
        $('.shipping_changed_values').append('<input type="hidden" name="' + field.name + '" value="' + field.value + '" />');
      }
    });

    $('#change-shipping-form').magnificPopup('close');

  });
});
