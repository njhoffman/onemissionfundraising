// handles login and register dialogs

jQuery(document).ready(function($) {
  console.log('loaded dialogs-login.js');

  // Perform AJAX login on form submit
  $(".login-form").on('submit', function(e){
    e.preventDefault();
    var formId = "#" + e.currentTarget.id;
    var $button = $(formId + ' button.create-account');
    var buttonText = $button.html();
    $button.text(loginObj.loadingMessage).attr("disabled", "true");
    var createFundraiser = $(formId + ' input[name="create_fundraiser"]').val();

    var submitObj = {
      'action':           'ajax_login', //calls wp_ajax_nopriv_ajaxlogin
      'first_name':       $(formId + ' .first-name').val(),
      'last_name':        $(formId + ' .last-name').val(),
      'username':         $(formId + ' .username').val(),
      'confirm_username': $(formId + ' .confirm-username').val(),
      'password':         $(formId + ' .password').val(),
      'create':           $(formId + ' input[name="create"]').val(),
      'security-signin':         $(formId + ' #security-signin').val(),
      'security-signup':         $(formId + ' #security-signup').val()
    };

    $.ajax({
      type: 'POST',
        dataType: 'json',
        url: loginObj.ajaxUrl,
        data: submitObj ,
        success: function(response) {
          $(formId + ' p.status').show().text(response.data.message);
          if (response.success) {
            $(formId + ' p.status').removeClass("fail").addClass("success");
            var redirectUrl = response.data && response.data.redirectUrl ?
              response.data.redirectUrl : loginObj.redirectUrl;
            if (true || createFundraiser) {
              redirectUrl += '/#start-fund';
              console.info('createFundraiser, redirecting: ' + redirectUrl);
            }
            var oldPath = window.location.pathname;
            window.location.href = response.data.redirectUrl;
            if (oldPath == '/') {
              window.location.reload();
            }
            document.location.href = redirectUrl;
          } else {
            $button.html(buttonText).attr("disabled", false);
            $(formId + ' p.status').removeClass("success").addClass("fail");
          }
        }
    });
  });
});
