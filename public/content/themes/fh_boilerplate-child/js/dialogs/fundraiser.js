jQuery(document).ready(function($) {

  console.log('loaded dialogs-fundraiser.js');

  var redirectUrl = "/wp/wp-admin/?page=fundraiser-profile-editor&step=1&initial=1";

  $('#startfund-form').on('submit', function(e) {
    e.preventDefault();
    var formId = "#startfund-form";
    var $button = $(formId + ' button.start-fund');
    var buttonText = $button.html();
    $button.text("Submitting, please wait ...").attr("disabled", "true");

    var submitObj = {
      'action':     'ajax_create_fundraiser', // calls wp_ajax_create_fundraiser
      'name':       $(formId + ' .name').val(),
      'city':       $(formId + ' .city').val(),
      'state':      $(formId + ' .state').val(),
      'country':    $(formId + ' .country').val(),
    };

    submitObj.type = $(formId + ' [name="fundraiser_type"]')
      .filter(function(idx, cb) {
        return cb.checked;
      }).map(function(idx, cb) {
        return cb.value;
      }).toArray()
      .join('&');

    $.ajax({
      type: 'POST',
        dataType: 'json',
        url: fundraiserObj.ajaxUrl,
        data: submitObj ,
        success: function(data) {
          $button.html(buttonText).attr("disabled", false);
          if (!data.success) {
            $(formId + ' p.status').show().addClass("fail").removeClass("success").text(data.message);
          } else {
            // redirect to admin area and show setup fund dialog
            $(".menu-item.start-fund").hide();
            window.location.href = redirectUrl;
          }
        }
    });
  });

  // TODO: this is used in both the dialog and profile editor
  $('input[name="fundraiser_type"]').on('change', function(e) {
    var checked = $('input[name="fundraiser_type"]')
      .filter(function(idx, cb) {
        return cb.checked;
      });
    if (checked.length > 1) {
      $('input[name="fundraiser_type"]').filter(function(idx, cb) {
        return !cb.checked;
      }).attr('disabled', true);
    } else {
      $('input[name="fundraiser_type"]').attr('disabled', false);
    }
  });
  $('input[name="fundraiser_type"]').trigger('change');

  $('input[name="country_select"]').on('change', function(e) {
    if (e.target.value === 'United States') {
      $('.othercountry-fields').find('input').attr('type', 'hidden').val('United States');
      $('.othercountry-fields').find('.required').hide();
      $('.unitedstates-fields').show();
    } else {
      $('.othercountry-fields').find('input').attr('type', 'text').val('');
      $('.othercountry-fields').find('.required').show();
      $('.unitedstates-fields').hide();
    }
  });

  $('.help-icon').on('mouseenter', function(e) {
    $('.help-text').hide();
    $(this).siblings('.help-text').fadeIn(500);
  });

  $('.help-icon').on('mouseleave', function(e) {
    var self = this;
    window.setTimeout(function() {
      if (! $(self).is(":hover")) {
        $(self).siblings('.help-text').fadeOut(500);
      }
    }, 500);
  });

});
