
	//
	//	Grab current URL hash for hash lookup + prefix for hashes
	//
	var hash   = document.location.hash;
	var prefix = 'tab_';

	jQuery( function() {

		//
		//	Checks for add to cart buttons and adds in the ?shopfor={fundraiser_id} on fundraiser page
		//
		if( jQuery( '#fundraiser_favorite_products .product a' ).length ) {

			var href;

			jQuery( '#fundraiser_favorite_products .product a' ).each( function() {

				// Loop through and add shop for to each add to cart
				href = jQuery( this ).attr( 'href' );

				// Check to ensure shopfor is not already set
				if( href.indexOf( 'shopfor' ) === -1 ) {

					var append = ( href.indexOf( '?' ) !== -1 ) ? '&' : '?';

					href += append + 'shopfor=' + child_vars.fundraiser_id;
					jQuery( this ).attr( 'href', href );
				}
			});
		}

		//
		//  Readmore
		//
		jQuery('#profile_desc').readmore({
			speed: 150,
			moreLink: '<div class="less-is-more-link"><a href="#" class="btn btn-primary">Read more <i class="fa fa-level-down"></i></a></div>',
  			lessLink: '<div class="less-is-more-link"><a href="#" class="btn btn-primary">Read less <i class="fa fa-level-up"></i></a></div>',
  			collapsedHeight: 300
		});
		jQuery('.fav-products_less-is-more').readmore({
			speed: 150,
			moreLink: '<div class="less-is-more-link"><a href="#" class="btn btn-primary">View more favorites <i class="fa fa-level-down"></i></a></div>',
  			lessLink: '<div class="less-is-more-link"><a href="#" class="btn btn-primary">View less favorites <i class="fa fa-level-up"></i></a></div>',
  			collapsedHeight: 400
		});
		jQuery('.article_excerpt.less-is-more').readmore({
			speed: 150,
			moreLink: '<div class="less-is-more-link"><a href="#" class="btn btn-primary">READ MORE<i class="fa fa-level-down"></i></a></div>',
  			lessLink: '<div class="less-is-more-link"><a href="#" class="btn btn-primary">READ LESS<i class="fa fa-level-up"></i></a></div>',
  			collapsedHeight: 200
		});

		//
		//	Setup jQuery scroller for donations
		//
		if( jQuery( '#donations_scroller' ).height() >= 140 ) {

			setTimeout( initiate_scroll, 2000 );
		}

		//
		//	Add target _blank to all news links
		//
		if( jQuery( '#fundraiser_news a' ).length ) {

			jQuery( '#fundraiser_news a' ).attr( 'target', '_blank' );
		};

		//
		//	If hash is found in current URL switch to that tab
		//
		if( hash ) {

			jQuery( '.nav-tabs a[href=' + hash.replace( prefix, '' ) + ']' ).tab( 'show' );
		}

		//
		//	Change hash for page-reload/linking
		//
		jQuery( '.nav-tabs a' ).on( 'shown.bs.tab', function (e) {

			window.location.hash = e.target.hash.replace( '#', '#' + prefix );
		});
	});

	//
	//	Initializes scroller
	//
	function initiate_scroll() {

		jQuery( '#donations_scroller' ).simplyScroll({
			auto:        true,
			frameRate:   30,
			orientation: 'vertical',
			speed:       1
		})
	}
