var interval_func;

//
//	Generates a UUID for use as progress-id
//
function generate_uuid() {

	var uuid = '';

    for( var i = 0; i < 32; i++ ) {

        uuid += Math.floor( Math.random() * 16 ).toString( 16 );
    }

    return uuid;
}

//
//  Progress function for polling upload progress
//
function poll_progress( uuid, progress_bar ) {

    jQuery.getJSON( '/progress', { 'X-Progress-ID': uuid }, function ( data ) {

        var percent_complete = 0;

        // If status is uploading then calculate percent and set progress bar
        if ( data.state == 'uploading' ) {

            percent_complete = Math.round( ( data.received / data.size ) * 100 );
            progress_bar.css( { width: percent_complete + '%' } );
            progress_bar.text( percent_complete + '%' );
        }

        // If the upload is done then set the width of the progress bar to 100%
        if ( data.state == 'done' || percent_complete == 100 ) {

            progress_bar.css({ width: '100%' });
            clearInterval( interval_func );
        }
    });
}

//
//  Document ready
//
jQuery( function() {

    var progress_bar = jQuery( '#custom_canvas_upload_progress' );

    jQuery( document ).ready( function () {

        // statbarWidth = jQuery('#statbar_box').width();
        
        // Attach event to the upload form submission
        jQuery( '#frm_custom_canvas' ).on( 'submit', function ( e ) {

            if( jQuery( '#img' ).val() ) {

                var uuid = generate_uuid();

                jQuery( '.progress' ).show();
                jQuery( '#custom_canvas_error' ).hide();
                jQuery( '.alert-info' ).text( 'Please wait while your image is uploaded...' );

                jQuery( this ).find( 'button' ).attr( 'disabled', 'disabled' );

                // Add X-Progress-ID uuid to action for the upload ID
                jQuery( '#frm_custom_canvas' ).attr( 'action', '/custom-canvas/?X-Progress-ID=' + uuid );

                // jQuery(this).disabled = false;
                
                interval_func = setInterval( function () {

                    poll_progress( uuid, progress_bar );

                }, 1000 );
            
            } else {

                e.preventDefault();

                var error = '<div class="alert alert-danger">';
                error    += 'Please select a file for upload!';
                error    += '</div>';

                jQuery( '#custom_canvas_error' ).html( error );
            }

        });
    });
});