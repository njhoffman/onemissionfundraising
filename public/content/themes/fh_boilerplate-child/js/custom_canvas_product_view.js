var images_container  = '#content .images';
var oneinch           = 2.3; // % for 1 inch of size
var dropdown_select   = '#pa_canvas-size';
var canvas_preview_id = 'canvas_preview';

// console.log( cc_vars );

jQuery( window ).load( function() {

	// Filter canvas sizes that don't meet crieteria
	jQuery( jQuery( dropdown_select ).find( 'option' ) ).each( function( index ) {

		var option_value = jQuery( this ).val();

		if( option_value ) {

			var tmp_arr   = option_value.split( 'x' );
			var width_in  = parseInt( tmp_arr[0] );
			var height_in = parseInt( tmp_arr[1] );
			// var shown     = false;

			// console.log( option_value + ' width_in=' + width_in + ' height_in=' + height_in );

			if( 
				( cc_vars.cc_orientation == 'square' && width_in == height_in ) ||
				( cc_vars.cc_orientation == 'portrait' && width_in <= height_in ) ||
				( cc_vars.cc_orientation == 'landscape' && width_in >= height_in )
			) {

				jQuery( this ).show();

			} else {

				jQuery( this ).hide();
			}
		}
	});


	// Add custom canvas class for couch background & sizing
	jQuery( images_container ).addClass( 'custom_canvas_bg' );

	// Clear images div html
	jQuery( images_container ).html( '<div id="' + canvas_preview_id + '"></div>' );

	// Change canvas size
	jQuery( dropdown_select ).change( function() {

		var tmp_arr   = jQuery( this ).val().split( 'x' );
		var width_in  = tmp_arr[0];
		var height_in = tmp_arr[1];
		var width_percent  = width_in * oneinch;
		var height_percent = height_in * oneinch;

		// console.log( cc_vars.cc_preview_image );

		jQuery( '#' + canvas_preview_id ).css({
			'width': width_percent + '%',
			'height': height_percent + '%',
			'margin-left': '-' + ( width_percent / 2 ) + '%',
			'top': ( 35 - height_percent / 2 ) + '%',
			'background-image': 'url(\'' + cc_vars.cc_preview_image + '\')',
			'background-repeat': 'no-repeat',
			'background-position': 'center'
		});
	});

	switch( cc_vars.cc_orientation ) {

		case 'square':
			jQuery( dropdown_select ).val( '12x12' ).change();
			break;
		case 'portrait':
			jQuery( dropdown_select ).val( '16x20' ).change();
			break;
		case 'landscape':
			jQuery( dropdown_select ).val( '20x16' ).change();
			break;
	}

	resizePreview();
});

jQuery( window ).on( 'resize', function() {

	resizePreview();
});

function resizePreview() {

	var preview_height = jQuery( '.custom_canvas_bg' ).width();

	jQuery( '.custom_canvas_bg' ).css({
		'height': ( preview_height ) + 'px'
	});
}