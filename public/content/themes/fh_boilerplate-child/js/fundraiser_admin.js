
jQuery(document).ready(function($) {
	function get_tinymce_content(){
		if (tinyMCE && tinyMCE.activeEditor) {
			return tinyMCE.activeEditor.getContent();
		}
	}

	function dataURItoBlob(dataURI) {
		var byteString = atob(dataURI.split(',')[1]);
		var mimeType = dataURI.split(',')[0].split(':')[1].split(';')[0];
		var ab = new ArrayBuffer(byteString.length);
		var ia = new Uint8Array(ab);
		for (var i = 0; i < byteString.length; i++) {
			ia[i] = byteString.charCodeAt(i);
		}
		return new Blob([ab], { type: mimeType });
	}

	function disableButtons() {
		if (saveContinueButton) {
			saveContinueButton.attr('disabled', true);
		}
		if (saveLaterButton) {
			saveLaterButton.attr('disabled', true);
		}
	}

	function enableButtons() {
		if (saveContinueButton) {
			saveContinueButton.attr('disabled', false);
		}
		if (saveLaterButton) {
			saveLaterButton.attr('disabled', false);
		}
	}

	function mainAjaxCall(submitObj) {
		disableButtons();
		if (submitStatus) {
			submitStatus.show();
		}
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: fundraiserObj.ajaxUrl,
			data: submitObj ,
			success: function(response) {
				enableButtons();
				if (response.errors && response.errors.length > 0) {
					$('.error-message').remove();
					$('.form-group').removeClass('field-error');
					$('.submit-text').text('');
					response.errors.forEach(function(err) {
						$('[name="' + err.key + '"]')
							.parents('.form-group')
							.addClass('field-error')
							.find('label')
							.after('<span class="error-message">' + err.message + '</span>');
					});
				}
				if (response.data && response.data.redirectUrl) {
					changedFields = {};
					oldCharCount = null;
					console.info('redirecting to: ', response.data.redirectUrl);
					var oldPath = window.location.pathname;
					window.location.href = response.data.redirectUrl;
					if (oldPath == '/') {
						window.location.reload();
					}
				}
			},
			error: function(error){
				enableButtons();
				console.error(error);
				debugger;
			}
		});
	}

	function submitForm(e) {
		e.preventDefault();
		var submitFields = $(this).serializeArray();
		var submitObj = {
			'action': 'ajax_update_fundraiser', // calls wp_ajax_update_fundraiser
			'submit_action': submitAction
		};

		var ftypeVal = false;
		// unchecked checkboxes don't get serialized, have to grab them manually
		$('input[type="checkbox"]').each(function(i, cb) {
			if (cb.name !=='fundraiser_type') {
				if (cb.checked === false) {
					submitFields.push({ name: cb.name, value: 'off' });
				} else {
					submitFields.push({ name: cb.name, value: 'on' });
				}
			} else {
				// special handling for fundraiser types
				if (cb.checked) {
					if (!ftypeVal) {
						ftypeVal = cb.value;
					} else {
						ftypeVal += "&" + cb.value;
					}
				}
			}
		});
		if (ftypeVal) {
			submitFields.push({ name: 'fundraiser_type', value: ftypeVal });
		}

		//  radio buttons special processing
		var rbNames = [];
		$(formId + ' input[type="radio"]').each(function(i, rb) {
			if (rbNames.indexOf(rb.name) === -1) {
				rbNames.push(rb.name);
			}
			if (rb.checked) {
				submitFields.push({ name: rb.name, value: rb.value });
			}
		});

		var dateSet = false;
		submitFields.forEach(function(sf) {
			var val = sf.value;
			if (sf.name === "fundraiser_description") {
				val = get_tinymce_content();
			}
			if (sf.name === "fundraiser_start_date" || sf.name === "fundraiser_end_date") {
				if (val === "" && !dateSet) {
					dateSet = null;
				} else {
					dateSet = true;
				}
			}
			submitObj[sf.name] = val;
		});

		rbNames.forEach(function(rbName) {
			// if no radio buttons selected for field name, set to empty value
			if (! submitObj.hasOwnProperty(rbName)) {
				submitObj[rbName] = '';
			}
		});

		if (dateSet) {
			submitObj['fundraiser_timed_or_continuous'] = 'timed';
		} else if (dateSet === null) {
			submitObj['fundraiser_timed_or_continuous'] = 'continuous';
		}


		if ($('.same-as-leader-checkbox').length > 0 && $('.same-as-leader-checkbox')[0].checked) {
			Object.keys(submitObj).forEach(function(key) {
				if (/leader_/.test(key)) {
					submitObj[key.replace(/leader_/,'payee_')] = submitObj[key];
				}
			});
			submitObj['payee_name'] = $('input[name="leader_name"]').val();
		}

		var fd = false;
		$('input[type="file"]').each(function(i, fileInput) {
			if ($(fileInput).attr('targetid')) {
				var dataURL = $($(fileInput).attr('targetid')).attr('src');
				var fieldName = $($(fileInput).attr('targetid')).attr('name');

				// remove empty values required to make 'required' validation error work
				if (fieldName === 'logo_image') { delete submitObj['logo_image']; }

				var blob = dataURItoBlob(dataURL);
				if (!fd) {
					fd = new FormData();
					fd.append('action', 'ajax_update_fundraiser');
				}
				var fileName = fieldName + '.' + blob.valueOf().type.slice(-3);
				fd.append(fieldName, blob, fileName);
			} else if (fileInput.files[0]) {
				if (!fd) {
					fd = new FormData();
					fd.append('action', 'ajax_update_fundraiser');
				}
				if (fileInput.name === 'document_501c') { console.info('deleting it'); delete submitObj['document_501c']; }
				fd.append(fileInput.name, fileInput.files[0]);
			}
		});

		// if file uploads exist, process those before main form submission
		if (fd) {
			console.info("processing files", fd);
			var progressBar = $('.progressbar').length > 0
				? $('.progressbar').progressbar()
				: false;

			var progressPercent = $('.progress-percent').length > 0
				? $('.progress-percent')
				: false;


			if (submitStatus) { submitStatus.show().find('.submit-text').text('Uploading file ...'); }
			if (progressBar) { progressBar.show(); }
			if (progressPercent) { progressPercent.text('0%') }

			disableButtons();

			$.ajax({
				type: 'POST',
				url: fundraiserObj.ajaxUrl,
				dataType: 'json',
				data: fd,
				processData: false,
				contentType: false,
				xhr: function() {
					var xhr = new window.XMLHttpRequest();
					xhr.upload.addEventListener('progress', function(evt) {
						if (evt.lengthComputable) {
							var percentComplete = evt.loaded / evt.total;
							percentComplete = parseInt(percentComplete * 100);
							console.info(percentComplete);
							if (progressBar) {
								progressBar.progressbar("value", percentComplete);
							}
							if (progressPercent) {
								progressPercent.html(percentComplete + '%');
								if (submitStatus && percentComplete === 100) {
									changedFields = {};
									submitStatus.text('Upload complete, submitting ...');
								}
							}
						}
					}, false);
					return xhr;
				},
				success: function(response) {
					changedFields = {};
					$('input[targetid]').removeAttr('targetid');
					if (response.errors && response.errors.length > 0) {
						enableButtons();
						$('.error-message').remove();
						$('.form-group').removeClass('field-error');
						response.errors.forEach(function(err) {
							$('[name="' + err.key + '"]')
							.parents('.form-group')
							.addClass('field-error')
							.find('label')
							.after('<span class="error-message">' + err.message + '</span>');
						});
					}
					mainAjaxCall(submitObj);
				},
				error: function(error){
					enableButtons();
					console.error(error);
					debugger;
				}
			});
		} else {
			mainAjaxCall(submitObj);
		}
	}

	function changeField(e) {
		if ($(this).attr('type') == 'radio') {
			return;
		}
		var name =  $(this).attr('name');
		var newVal = $(this).val();
		var origVal = e.currentTarget.defaultValue;
		var title = $(this).parent().siblings('label').length > 0 ?
			$(this).parent().siblings('label').text : name;
		if (!changedFields[name]) {
			if (newVal.trim() != origVal.trim()) {
				changedFields[name] = { new: newVal, old: origVal, title: title };
			}
		} else {
			if (newVal.trim() != origVal.trim()) {
				changedFields[name] = { new: newVal, old: origVal, title: title };
			} else {
				delete changedFields[name];
			}
		}
	}

	var formId = "#profile-editor";
	var changedFields = {};

	// field modification monitor and warn on exit

	// initialize any datepickers
	$('.datepicker').each(function(idx, el) {
		 $(el).datepicker().on('changeDate', changeField);
	});

	var fieldControls = formId + ' input, ' + formId + ' textarea, ' + formId + ' select';
	$(fieldControls).on('change', changeField);

	var oldFtype = '';
	$(formId + ' input[name="fundraiser_type"]:checked').each(function(i, el) {
		oldFtype = oldFtype.length === 0 ? $(el).val() : oldFtype + '&' + $(el).val()
	});

	// fundraiser checkboxes special handling
	$(formId + ' input[name="fundraiser_type"]').on('change', function(e) {
		var fundType = '';
		$(formId + ' input[name="fundraiser_type"]:checked').each(function(i, el) {
			fundType = fundType.length === 0 ? $(el).val() : fundType + '&' + $(el).val()
		});
		if (oldFtype != fundType) {
			changedFields['fundraiser_type'] = { new: fundType, old: oldFtype, title: 'Fundraiser Type' };
		} else {
			delete changedFields['fundraiser_type'];
		}
	});

	// character counting for tinymce boxes
	var oldCharCount = null;
	if ($('.character-count').length > 0) {
		oldCharCount = $('.character-count').data('currcount');
	}

	// radio buttons
	$(formId + ' input[type="radio"]').on('click', function(e) {
		changedFields[$(this).attr('name')] = {};
	});



	$(window).on('beforeunload', function(e) {
		// character count special handling
		delete changedFields['tinymce'];
		var newCharCount = parseInt($('.character-count').attr('data-currcount'));
		if (oldCharCount !== null && (newCharCount != oldCharCount)) {
			changedFields['tinymce'] = { old: oldCharCount, new: newCharCount };
		}

		// image uploader special handling
		$(formId + ' input[type="file"][targetid]').each(function(i, el) {
			changedFields[$(el).attr('targetid')] = {};
		});

		if (Object.keys(changedFields).length > 0) {
			console.info('displaying exit warning for changed fields:', changedFields);
			var confirmationMessage = 'You have unsaved changes on this form from fields: ' +
				Object.keys(changedFields) + '.  Are you sure you want to leave?';
			return confirmationMessage;
		}
	});

	// form submission
	var submitAction = "";

	var submitStatus = $('.submit-status').length > 0
		? $('.submit-status')
		: false;

	var saveContinueButton = $('button[name="save_continue"]').length > 0 ?
		$('button[name="save_continue"]')
		: false

	var saveLaterButton = $('button[name="save_later"]').length > 0 ?
		$('button[name="save_later"]')
		: false;

	$('button[type="submit"]').click(function(e) {
		submitAction = e.target.name;
	});


	$(formId).on('submit', submitForm);

	$('button[name="launch"]').click(function(e) {
		e.preventDefault();
		var submitObj = {
			'action': 'ajax_submit_fundraiser'
		};
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: fundraiserObj.ajaxUrl,
			data: submitObj ,
			success: function(response) {
				window.location.reload();
			}
		});
	});

	console.info('fundraiser_admin loaded');
});
