
jQuery(document).ready(function($) {
  var batchNumber = 50;
  var pageNumber = 500;
  var currPage = 0;
  var currIdx = 1;
  var isProcessing = false;
  var tableHeaders = [
    '<table class="fixed_headers">' ,
      '<thead>',
        '<tr>',
          '<th class="tiny">HS</th>',
          '<th>Order ID</th>',
          '<th>Fund <br />User ID</th>',
          '<th>Fund Name</th>',
          '<th>Date</th>',
          '<th>Stripe Fee</th>',
          '<th>Net Revenue<br />From Stripe</th>',
          '<th>Refund <br />Shipping</th>',
          '<th>Refund <br />Tax</th>',
          '<th>Refund <br />Total</th>',
          '<th>Total <br />Donation<br />Amount</th>',
          '<th>Total <br />Net <br /> Donation</th>',
          '<th>Product <br />Purchases</th>',
          '<th>Product <br />Donation</th>',
          '<th>Cash <br />Donation</th>',
          '<th>Subscription <br />Donation</th>',
          '<th>Shipping</th>',
          '<th>Tax</th>',
          '<th>Order <br />Total</th>',
          // '<th>Tip <br />Amount</th>',
          // '<th>Credit <br />Card Fee <br />for Tip</th>',
          '<th>Calculated <br />Transaction <br />Fee</th>',
        '</tr>',
      '</thead>'
  ].join('\n');
  var htmlOut = tableHeaders;

  $('#next_checkout_fields').click(function(e) {
    e.preventDefault();
    pageNumber = $('input[name="per-page"]').val();
    $('input[name="per-page"]').attr('disabled', 'disabled');
    $('.current-page').css('display', 'inline-block');
    if (isProcessing) {
      $('#next_checkout_fields').text('Next ' + pageNumber);
      $('#checkout_fields_results').html(htmlOut);
      isProcessing = false;
    } else {
      isProcessing = true;
      htmlOut = tableHeaders;
      $('#checkout_fields_results').html(
        '<i style="text-align: center; display: block">Scanning ' + pageNumber + ' orders ...</i>'
      );
      $('.progressbar').val(0);
      currIdx = pageNumber * currPage;
      currPage++;
      getCheckoutFields();
    }
  });

  function getClass(field, item) {
    return "new " + (item._existing_fields.indexOf(field) !== -1 ? "existing" : "") +
      (item._added_fields.indexOf(field) !== -1 ? "added" : "") +
      (item._changed_fields.indexOf(field) !== -1 ? "changed" : "")
  }

  function parseItem(item) {
    var fund_name = item.funderaiser_name && item.fundraiser_name.length > 18 ?
      item.fundraiser_name.slice(0, 15) + '...' : item.fundraiser_name;
    var stripe_fee = isNaN(item.stripe_fee) ? item.stripe_fee : Number(item.stripe_fee).toFixed(2);
    var net_revenue_from_stripe = isNaN(item.net_revenue_from_stripe) ? item.net_revenue_from_stripe : Number(item.net_revenue_from_stripe).toFixed(2);
    var refund_shipping = isNaN(item.refund_shipping) ? item.refund_shipping : Number(item.refund_shipping).toFixed(2);
    var refund_total_tax = isNaN(item.refund_total_tax) ? item.refund_total_tax : Number(item.refund_total_tax).toFixed(2);
    var refund_total = isNaN(item.refund_total) ? item.refund_total : Number(item.refund_total).toFixed(2);
    var total_donation = isNaN(item.total_donation) ? item.total_donation : Number(item.total_donation).toFixed(2);
    var total_net_donation = isNaN(item.total_net_donation) ? item.total_net_donation : Number(item.total_net_donation).toFixed(2);
    var product_purchases = isNaN(item.product_purchases) ? item.product_purchases : Number(item.product_purchases).toFixed(2);
    var product_donation = isNaN(item.product_donation) ? item.product_donation : Number(item.product_donation).toFixed(2);
    var cash_donation = isNaN(item.cash_donation) ? item.cash_donation : Number(item.cash_donation).toFixed(2);
    var subscription_donation = isNaN(item.subscription_donation) ? item.subscription_donation : Number(item.subscription_donation).toFixed(2);
    var shipping_total = isNaN(item.shipping_total) ? item.shipping_total : Number(item.shipping_total).toFixed(2);
    var total_tax = isNaN(item.total_tax) ? item.total_tax : Number(item.total_tax).toFixed(2);
    var total = isNaN(item.total) ? item.total : Number(item.total).toFixed(2);
    var tip_amount = isNaN(item.tip_amount) ? item.tip_amount : Number(item.tip_amount).toFixed(2);
    var tip_cc_fee = isNaN(item.tip_cc_fee) ? item.tip_cc_fee : Number(item.tip_cc_fee).toFixed(2);
    var transaction_fee = isNaN(item.transaction_fee) ? item.transaction_fee : Number(item.transaction_fee).toFixed(2);


    htmlOut += [
      '<tr>',
      '<td class="tiny hubspot-update ' + (item._hubspot_success ? 'success ' : 'failure ') +
          (item._hubspot_new ? 'new ' : '') +
          '" title="' + (item._hubspot_errors.length > 0 ? item._hubspot_errors.replace(/"/g, "") : item._hubspot_email) + '"></td>',
        '<td>' + item.ID + '</td>',
        '<td>' + item.fundraiser_user_id + '</td>',
        '<td>' + fund_name + '</td>',
        '<td>' + new Date(item.paid_date).toLocaleDateString() + '</td>',
        '<td>' + stripe_fee + '</td>',
        '<td>' + net_revenue_from_stripe + '</td>',
        '<td>' + refund_shipping + '</td>',
        '<td>' + refund_total_tax + '</td>',
        '<td>' + refund_total + '</td>',
        '<td>' + total_donation + '</td>',
        '<td class="' + getClass('Total Net Donation', item) + '">' + total_net_donation + '</td>',
        '<td class="' + getClass('Product Purchases', item) + '">' + product_purchases + '</td>',
        '<td class="' + getClass('Product Donation', item) + '">' + product_donation + '</td>',
        '<td class="' + getClass('Cash Donation', item) + '">' + cash_donation + '</td>',
        '<td class="' + getClass('Subscription Donation', item) + '">' + subscription_donation + '</td>',
        '<td class="' + getClass('Shipping Total', item) + '">' + shipping_total + '</td>',
        '<td class="' + getClass('Tax Total', item) + '">' + total_tax + '</td>',
        '<td class="' + getClass('Order Total', item) + '">' + total + '</td>',
        // '<td class="new">' + tip_amount + '</td>',
        // '<td class="new">' + tip_cc_fee + '</td>',
        '<td class="' + getClass('Transaction Fee', item) + '">' +  transaction_fee + '</td>',
      '</tr>'].join('\n');
  }

  var processed = parseInt($('.total-processed').text());
  var remaining = parseInt($('.total-remaining').text());
  var procChanged = false;

  function getCheckoutFields() {
    console.info('getCheckoutFields', currIdx, currPage);
    if (!isProcessing) {
      return;
    }
    $('#next_checkout_fields').text('Stop');

    var ajaxObj = {
      action: 'ajax_assign_checkout_fields',
      page: (currIdx / batchNumber),
      batchNumber: batchNumber,
      onlyUnassigned: $('input[name="only-unassigned"]').attr('checked')
    };

    $.ajax({
      type: 'POST',
      dataType: 'json',
      url: assignCheckoutObj.ajaxUrl,
      data: ajaxObj,
      success: function(response) {
        currIdx += response.data.items.length;
        response.data.items.forEach(parseItem);
        var pct = 1 - (parseInt((currPage * pageNumber) - currIdx) / pageNumber);
        $('.progressbar').val(pct * 100);

        response.data.items.forEach(function(item) {
          if (item._added_fields.length > 0) {
            remaining--;
            processed++;
            procChanged = true;
          }
        });

        if (currIdx >= (currPage * pageNumber)) {
          $('.progress-text').text('Scanned ' + currIdx + ' Total Orders');
          $('#next_checkout_fields').text('Next ' + pageNumber);
          $('#checkout_fields_results').html(htmlOut);
          $('.current-page').html('<strong>Page ' + currPage + ' </strong> (' + (pageNumber * (currPage - 1) + 1) + ' - ' + (pageNumber * currPage) + ')');
          $('.total-processed').html('<span class="' + (procChanged ? 'changed' : '') + '">' + processed + "</span>");
          $('.total-remaining').html('<span class="' + (procChanged ? 'changed' : '') + '">' + remaining + "</span>");
          isProcessing = false;
        } else {
          getCheckoutFields();
          $('.progress-text').text('Scanning ' + currIdx + ' orders out of ' + (currPage * pageNumber));
        }
      },
      error: function(error){
        console.error(error);
      }
    });
  }
  console.log("Loaded: assign_checkout_fields.js");
});
