jQuery(document).ready(function($) {
  // version download dialog

  $('a.download').on('click', function(e) {
    e.preventDefault();
    var versions = $(this).data('versions');
    var currentVersion = $(this).data('current-version');
    var latestVersion = $(this).data('latest-version');
    var pluginName = $(this).data('name');
    // var versions_html = '<table class="download-links"><tr><th>Version</th><th>Link</th></tr>';
    var versions_html = '<table class="download-links">';

    Object.keys(versions)
      .reverse()
      .forEach(function(key) {
        var rowClass = key === latestVersion ? 'latest' : key === currentVersion ? 'current' : '';
        versions_html += '<tr class="' + rowClass + '"><th>' + key + '</th>' +
          '<td><a target="_blank" href="' + versions[key] + '">' +
          versions[key] + '</a></td></tr>';
      });

    versions_html += '</table>';
    jQuery.magnificPopup.open({
      items: {
        type: 'inline',
        src: '#plugin-versions-dialog',
      },
      closeOnBgClick: true,
      preloader: false,
      callbacks: {
        beforeOpen: function() {
          $('#plugin-versions-dialog .plugin-versions').html(versions_html);
          $('#plugin-versions-dialog .subheader').text('Download links for ' + pluginName);
        }
      }
    });
  });

  $('#plugin-versions-dialog .close').on('click', function(e) {
    e.preventDefault();
    $('#plugin-versions-dialog').magnificPopup('close');
  });

  console.log("Loaded: manage_plugin_versions.js");
});
