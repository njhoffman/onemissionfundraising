//
//	Application JavaScript Here!
//
jQuery( function() {


	// DOM/Window ready
	jQuery('.unslider').unslider({
		autoplay: true,
		arrows: false,
		infinite: true,
		speed: 300,
		delay: 10000
	});

	jQuery('.youtube').magnificPopup({ type: 'iframe', iframe: { markup: '<div class="mfp-iframe-scaler">'+ '<div class="mfp-close"></div>'+ '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>'+ '</div>', patterns: { youtube: { index: 'youtube.com/', id: 'v=', src: '//www.youtube.com/embed/%id%?rel=0' } }, srcAction: 'iframe_src', } });

	jQuery('.popup-with-form').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#name',

		// When element is focused, some mobile browsers in some cases zoom in
		// It looks not nice, so we disable it:
		callbacks: {
			beforeOpen: function() {
				if(jQuery(window).width() < 700) {
					this.st.focus = false;
				} else {
					this.st.focus = '#name';
				}
			}
		}
	});

	jQuery('.popup-with-form').on('click', function(e) {
		// set hidden flag to indicate they want to start a fund
		if (jQuery(this).attr("name") === "start_fund_link") {
			jQuery("#signup-form input[name='create_fundraiser']").val(true);
		} else {
			jQuery("#signup-form input[name='create_fundraiser']").val(false);
		}
	});

	if (location.hash === "#start-fund") {
		// start-fund hash is a flag to open the start fund dialog right away
		jQuery('.menu-item.start-fund .popup-with-form').trigger('click');
	}

	if( jQuery( '.fundraiser_short' ).length ) {

		var description_height_max = 95;

		jQuery.each( jQuery( '.fundraiser_short .description' ), function () {

			console.log( jQuery( this ).height() );
			console.log( jQuery( this ).attr( 'data-short-id' ) );

			var description_height = jQuery( this ).height();

			if( description_height >= description_height_max ) {

				var fundraiser_short_id = jQuery( this ).attr( 'data-short-id' );
				var read_more_link_id = fundraiser_short_id + '_more';

				// Set CSS to hide overflow description
				jQuery( this ).css({
					'height': description_height_max,
					'overflow-y': 'hidden',
					'margin-bottom': 5
				});

				jQuery( '<a id="' + read_more_link_id + '" href="#">read full description</a>' ).insertAfter( this );

				jQuery( '#' + read_more_link_id ).on( 'click', function(e) {

					e.preventDefault();

					jQuery( '.' + fundraiser_short_id + ' .description' ).css({ 'height': 'auto' });
					jQuery( this ).hide();
				});
			}
		});
	}

	//
	//	Drawer Navigation (non-responsive)
	//
	jQuery( '.drawer_link' ).on( 'click', function(e) {

		e.preventDefault();

		if( jQuery( this ).parent().hasClass( 'active' ) ) {

			// Remove active class
			jQuery( this ).parent().removeClass( 'active' );

			// Change caret direction
			jQuery( this ).find( 'i' ).removeClass( 'fa-caret-up' );
			jQuery( this ).find( 'i' ).addClass( 'fa-caret-down' );

			// If active currently then close the drawer
			close_drawer();

		} else {

			// Remove active classes
			jQuery( '.drawer_link' ).parent().removeClass( 'active' );

			// Add navigation active class
			jQuery( this ).parent().addClass( 'active' );

			// Reset nav carets
			reset_nav_carets();

			// Change caret direction
			jQuery( this ).find( 'i' ).removeClass( 'fa-caret-down' );
			jQuery( this ).find( 'i' ).addClass( 'fa-caret-up' );

			// Toggle drawer with appropriate content
			toggle_drawer( jQuery( this ).attr( 'data-target' ) );
		}

	});

	//
	//	Drawer Navigation close
	//
	jQuery( '.close_drawer' ).on( 'click', function(e) {

		e.preventDefault();

		// Close drawer
		close_drawer();
	});

	//
	//	Drawer Navigation close on content click
	//
	jQuery( '#over_body_wrap' ).on( 'click', function(e) {

		// Close drawer
		close_drawer();
	});

	//
	//  Search field in nav
	//
	var searchNavTrigger = jQuery('#om_nav_search_trigger');
	var searchNav = jQuery('#om_nav_search');

	jQuery(searchNavTrigger).on('click', this, function() {
		searchNav.slideToggle('fast');
	});

	//
	//	Immediately show modal for love fund notification
	//
	if( ! jQuery.cookie( 'seen_love_fund_notification' ) ) {

		jQuery( '#love_fund_modal' ).modal();
	}

	//
	//	When shop for love fund selected then record so modal doesn't show again
	//
	jQuery( '#shop_love_fund' ).on( 'click', function ( e ) {

		setSeenGenFundWarning();
	});

});

//
//	Toggle visibility of #sub_nav
//
function toggle_drawer( sub_content_div ) {

	// Hide all of the drawers
	jQuery( '.sub_nav_drawer' ).hide();

	// Show appropriate drawer
	jQuery( sub_content_div ).show();

	// If drawer is hidden then slideDown
	if( ! jQuery( '#sub_nav' ).is( ':visible' ) ) {

		jQuery( '#sub_nav' ).slideDown();
	}
}

//
//	Close drawer
//
function close_drawer() {

	// Remove active classes
	jQuery( '.drawer_link' ).parent().removeClass( 'active' );

	// Reset navigation carets
	reset_nav_carets();

	// If drawer is visible then slideUp
	if( jQuery( '#sub_nav' ).is( ':visible' ) ) {

		jQuery( '#sub_nav' ).slideUp();
	}
}

//
//	Reset nav carets
//
function reset_nav_carets() {

	// Reset all carets to down position
	jQuery( '.drawer_link' ).find( 'i' ).removeClass( 'fa-caret-up' );
	jQuery( '.drawer_link' ).find( 'i' ).addClass( 'fa-caret-down' );
}

//
//	Set has seen notification
//	- expires in 2 days
//
function setSeenGenFundWarning() {

    jQuery.cookie( 'seen_love_fund_notification', true, { expires: 2, path: '/' } );

    return;
}
