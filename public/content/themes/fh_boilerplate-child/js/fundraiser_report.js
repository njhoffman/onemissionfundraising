jQuery(document).ready(function($) {

	var batchNumber = 10;

	var start_date_re = /(&startdate=(.*?))(&|$)/;
	var end_date_re   = /(&enddate=(.*?))(&|$)/;
	var start_time_re = /(&starttime=(.*?))(&|$)/;
	var end_time_re   = /(&endtime=(.*?))(&|$)/;

	var loc = window.location.href;

	var dt = new Date();
	var lmStart = new Date(dt.getFullYear(), dt.getMonth() - 1, 1);
	var lmEnd = new Date(dt.setDate(0));
	$('#start_date').val(
		loc.match(start_date_re) && loc.match(start_date_re).length >= 3
			? loc.match(start_date_re)[2]
			: lmStart.getFullYear() + '-' +
				(lmStart.getMonth() < 10 ? '0' : '') + lmStart.getMonth() + '-' +
				(lmStart.getDate() < 10 ? '0' : '') + lmStart.getDate()
	);
	$('#end_date').val(
		loc.match(end_date_re) && loc.match(end_date_re).length >= 3
			? loc.match(end_date_re)[2]
			: lmEnd.getFullYear() + '-' +
				(lmEnd.getMonth() < 10 ? '0' : '') + lmEnd.getMonth() + '-' +
				(lmEnd.getDate() < 10 ? '0': '') + lmEnd.getDate()
	);
	$('#start_time').val(
		loc.match(start_time_re) && loc.match(start_time_re).length >= 3
			? loc.match(start_time_re)[2]
			: "00:00"
	);
	$('#end_time').val(
		loc.match(end_time_re) && loc.match(end_time_re).length >= 3
			? loc.match(end_time_re)[2]
			: "23:59"
	);

	var submittingItems = false;
	var startTime = false;

	$('button.clear').click(function(e) {
		e.preventDefault();
		$('#start_date').val('');
		$('#end_date').val('');
		$('#start_time').val('');
		$('#end_time').val('');
	});

	$('button.ajax-action').click(function(e) {
		e.preventDefault();
		if (submittingItems) {
			submittingItems = false;
			startTime = false;
			n = 0;
			$('.progress-text').text('Stopped');
			$('button.ajax-action').text('Reload');
			$('a.csv').show();
		} else {
			getFundraiserIds();
			$('.progressbar').val(0);
			$('button.ajax-action').text('Stop');
			$('a.csv').hide();
			$('table tr').not(':first-child').remove();
		}
	});


	debugger;
	getFundraiserIds();

	function numberWithCommas(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	var n = 1;
	function appendItems(items) {
		var html_out = "";
		items.forEach(function(item) {
			var gsa_total = "$0";
			var gsa_items = "";
			if (item['GSA Items'] && item['GSA Items'].length > 0) {
				var total_sign = item['GSA'].toString()[0] == '-' ? '-' : '&nbsp;';
				gsa_total = total_sign + " $" + numberWithCommas(item['GSA'].toString().replace(/^[\-]/, ""));
				item['GSA Items'].forEach(function(gsa) {
					if (Array.isArray(gsa) && gsa.length > 1) {
						var sign = gsa[1].toString().trim()[0] == "-" ? "- " : "+";
						var amt = numberWithCommas(gsa[1].toString().trim().replace(/^[\- ]/, ""));
						var dt = new Date(parseInt(gsa[0]) * 1000).toLocaleDateString();
						gsa_items += sign + " $" + amt + Array(15 - amt.length).join(' ') +
							dt + Array(15 - dt.length).join(' ') + gsa[2] + "\n";
					} else {
						var sign = gsa[0].toString().trim()[0] == "-" ? "- " : "+";
						var amt = numberWithCommas(gsa[0].toString().trim().replace(/^[\- ]/, ""));
						gsa_items += sign + " $" + amt + "\n";
					}
				});

			}
			html_out +=
				"<tr>" +
					"<td>" + n + "</td>" +
					"<td>" + item["Fundraiser ID"] + "</td>" +
					"<td>" + item["Fundraiser Title"] + "</td>" +
					"<td>" + item["Orders"] + "</td>" +
					"<td>" +
					(gsa_items.length > 0
							? "<a title='" + gsa_items + "'>" + gsa_total + "</a>"
							: gsa_total
					) + "</td>" +
					"<td>$" + numberWithCommas(item["Goal Amount"]) + "</td>" +
					"<td>$" + numberWithCommas(item["Total Raised"]) + "</td>" +
				"</tr>";
			n++;
		});
		$('table.gsa').append(html_out);
	}

	function getFundraiserItems(fundraiserData) {
		if (!submittingItems)
			return;
		if (!startTime)
			startTime = new Date().getTime();
		$('.query-time').text( Math.round((new Date().getTime() - startTime) / 1000) + " second(s)");
		var currIds = fundraiserData.ids.slice(fundraiserData.currIdx, fundraiserData.currIdx + batchNumber);
		var submitFields = $('#fundraiser_goal_form').serializeArray();
		var ajaxObj = {
			action: 'ajax_fundraiser_report_items',
			ids:     currIds,
			currIdx: fundraiserData.currIdx,
			filter:  submitFields
		}
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: fundraiserObj.ajaxUrl,
			data: ajaxObj,
			success: function(response) {
				if (!submittingItems) return;
				$('.loaded-records').text(parseInt(fundraiserData.currIdx) + 1);
				if (fundraiserData.currIdx + 1 === fundraiserData.ids.length) {
					$('.progress-text').text('Finished Loading ' + (parseInt(fundraiserData.currIdx) + 1) + ' Records');
					$('a.csv').show();
					startTime = false;
					submittingItems = false;
					n = 0;
				} else {
					$('.progress-text').text('Loaded ' + (parseInt(fundraiserData.currIdx) + 1)
													 + ' items out of ' + fundraiserData.ids.length);
				}
				var pct = parseInt(fundraiserData.currIdx) / parseInt(fundraiserData.ids.length) * 100;
				$('.progressbar').val(pct);

				if (response.data.items && response.data.items.length > 0) {
					appendItems(response.data.items);
				}

				if (fundraiserData.currIdx + batchNumber === fundraiserData.ids.length)  {
					fundraiserData.currIdx = fundraiserData.ids.length;
					$('button.ajax-action').text('Reload');
					getFundraiserItems(fundraiserData);
				} else if (fundraiserData.currIdx + batchNumber < fundraiserData.ids.length) {
					fundraiserData.currIdx += batchNumber;
					getFundraiserItems(fundraiserData);
				}
			},
			error: function(error){
				console.error(error);
			}
		});
	}

	function getFundraiserIds(submitFields) {
		var fundraiserData = {
			currIdx: 0,
			ids: [],
			items: []
		};
		var ajaxObj = {
			action: 'ajax_fundraiser_report_ids'
		};
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: fundraiserObj.ajaxUrl,
			data: ajaxObj,
			success: function(response) {
				console.info(response);
				fundraiserData.ids = response.data.fundraisers;
				$('.total-records').text(fundraiserData.ids.length);
				$('.progress-text').text('Loading ' + fundraiserData.ids.length + ' items...');
				submittingItems = true;
				getFundraiserItems(fundraiserData);
			},
			error: function(error){
				console.error(error);
			}
		});
	}

	function exportTableToCSV($table, filename) {
			// var $rows = $table.find('tr:has(td)');
			var $rows = $table.find('tr');
			// Temporary delimiter characters unlikely to be typed by keyboard
			// This is to avoid accidentally splitting the actual contents
			var tmpColDelim = String.fromCharCode(11); // vertical tab character
			var tmpRowDelim = String.fromCharCode(0); // null character

			// actual delimiter characters for CSV format
			var colDelim = '","';
			var rowDelim = '"\r\n"';

			// Grab text from table into CSV formatted string
			var csv = '"' + $rows.map(function (i, row) {
				var $row = $(row);
				var $cols = $row.find('td,th');

				return $cols.map(function (j, col) {
					var $col = $(col);
					var text = $col.text();
					return text.replace('"', '""'); // escape double quotes
				}).get().join(tmpColDelim);
			}).get().join(tmpRowDelim)
			.split(tmpRowDelim).join(rowDelim)
			.split(tmpColDelim).join(colDelim) + '"';

		// Data URI
		var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

		$(this)
		.attr({
			'download': filename,
			'href':     csvData,
			'target':   '_blank'
		});
	}

	// This must be a hyperlink
	$("a.csv").on('click', function (event) {
		// CSV
		exportTableToCSV.apply(this, [$('table.gsa'), 'fundraiser_report.csv']);

		// IF CSV, don't do event.preventDefault() or return false
		// We actually need this to be a typical hyperlink
	});

});
