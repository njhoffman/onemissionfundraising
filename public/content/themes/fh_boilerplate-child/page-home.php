<?php
	$by_type   = ( isset( $_GET['by-type'] ) ) ? sanitize_text_field( $_GET['by-type'] ) : false;
	$by_search = ( isset( $_GET['by-search'] ) ) ? sanitize_text_field( $_GET['by-search'] ) : false;
	$all_fundraisers = ( isset( $_GET['all'] ) ) ? true : false;

	if ($all_fundraisers !== false || $by_type !== false || $by_search !== false):
?>

<?php

	// Grab ACF fields for custom content blocks
	$acf_fields = get_fields();

	//
	//	Replace title block with browsing interface
	//
	function custom_title() {

		global $fundraiser_types;
		global $by_type;
		global $by_search;

		$action   = ( $by_type ) ? 'Browsing' : 'Find';
		$action   = ( $by_search ) ? 'Searching for "' . $by_search . '" in' : $action;
		$selected = ( $by_type ) ? $fundraiser_types[ $by_type ] . ' ' : '';

		echo '<h1>' . $action . ' ' . $selected . 'Fundraisers</h1>';
	}

	add_action( '_fh_the_title', 'custom_title' );
	add_action( '_fh_sidebar_right_prepend', 'custom_sidebar_prepend' );

	function custom_sidebar_prepend() {

		global $fundraiser_types;
		global $by_type;
		global $by_search;
		global $acf_fields;

		?>
		<div class="segment well">

			<ul><li>

				<form id="fundraiser_browse_tools" role="form" method="get" action="<?php echo get_permalink() ?>">

					<div class="form-group">
						<label for="fundraiser_browse_select">Fundraiser Type:</label>
						<select name="by-type" id="fundraiser_browse_select" class="form-control input-sm">
							<option value="">Show All Fundraiser Types</option>
							<?php

								foreach ( $fundraiser_types as $key => $value ) {

									$selected = ( $by_type && $key == $by_type ) ? ' selected' : '';

									echo '<option value="' . $key . '"' . $selected . '>' . $value . '</option>';

								}
							?>
						</select>
					</div>
					<div class="form-group">
						<label for="fundraiser_browse_search">Fundraiser Name:</label>
						<?php

							$value = ( $by_search ) ? $by_search : '';
						?>
						<input name="by-search" id="fundraiser_browse_search" type="text" class="form-control input-sm" placeholder="search fundraisers" value="<?php echo $value; ?>" />
					</div>

					<button type="submit" class="btn btn-info btn-md btn-block">Find Fundraisers</button>

				</form>

			</li></ul>

		</div>

		<div class="segment well">

			<ul><li>

				<?php if( isset( $acf_fields[ $by_type ] ) && ! empty( $acf_fields[ $by_type ] ) ): ?>

					<p><?php echo nl2br( $acf_fields[ $by_type ] ); ?></p>

				<?php endif; ?>

				<p><a href="#signup-form" class="btn btn-block btn-md btn-success popup-with-form">Start a Fund!</a></p>

			</li></ul>


		</div>
		<?php
	}

	function custom_content() {

		global $by_type;
		global $by_search;

		// Setup args for custom query
		$args = array(
			'post_type' => 'page',
			'post_parent' => OM_FUNDRAISER_PAGE_PARENT_ID,
			'post_status' => array( 'publish' ),
			'posts_per_page' => 30,
			'paged' => (get_query_var('paged')) ? get_query_var('paged') : 1,

			// 'meta_query' => array(
			// 	array(
			// 		'key' => 'is_featured',
			// 		'orderby' => 'meta_value',
			// 		'order' => 'DESC'
			// 	)
			// )

			// 'orderby' => 'meta_value',
			// 'meta_key' => 'is_featured',
			// 'order' => 'DESC'
		);

		// Search query
		if( $by_search ) {

			$args['s'] = $by_search;
		}

		// Type query
		if( $by_type ) {

			$args['meta_query'] = array(
				array(
					'key' =>   'fundraiser_type',
					'value' => $by_type
				),
				array(
					'key' => 'is_featured',
					'orderby' => 'meta_value',
					'order' => 'DESC'
				),
				// array(
				// 	'key' => 'fundraiser_start_date',
				// 	'orderby' => 'meta_value',
				// 	'order' => 'DESC'
				// )
			);
		}

		$profiles_query = new WP_Query( $args );

		if ( $profiles_query->have_posts() ) {

			$i = 0;

			echo '<div id="fundraiser_poloroids_wrap">';

			while ( $profiles_query->have_posts() ) {

				$profiles_query->the_post();

				$post                          = $profiles_query->post;
				$fields                        = get_fields( $post->ID );
				$shop_link                     = '/products/?shopfor=' . $post->ID;
				$fundraiser_container_class_id = 'fundraiser_id_' . $post->ID;
				$fundraiser_url                = get_permalink( $post->ID );

				// Check if fundraiser is closed
				if (isset($fields["fundraiser_start_date"]) && isset($fields["fundraiser_end_date"])) {
					$closed = (
						_om_is_fundraiser_closed( $fields['fundraiser_timed_or_continuous'], $fields['fundraiser_start_date'], $fields['fundraiser_end_date'] )
					);
				} else {
					$closed = false;
				}

				// Calculate first/last column classes
				$extra_class = ( ! ( $i % 3 ) || $i == 0 ) ? ' first' : '';
				$extra_class = ( ! ( ( $i + 1 ) % 3 ) ) ? ' last' : $extra_class;

				$html_out = '<div class="fundraiser_poloroid' . $extra_class . '">';
				$html_out .= '<div class="text-center">';

				if( !empty( $fields['logo_image']['sizes']['shop_single'] ) ) {

					$html_out .= '<a href="' . $fundraiser_url . '" class=""><img class="fundraisers_search" src="' . $fields['logo_image']['sizes']['thumbnail'] . '"></a>';

				} else {

					$html_out .= '<a href="' . $fundraiser_url . '" class=""><img class="fundraisers_search" src="' . get_stylesheet_directory_uri() . '/img/no_fundraiser_logo.webp"></a>';
				}

				$html_out .= '</div>';

				// Header classes
				$header_str_length = strlen( $post->post_title );

				if( $header_str_length < 20 ) {

					$header_class = '';

				} elseif( 40 > $header_str_length && $header_str_length >= 20 ) {

					$header_class = 'small_1';

				} elseif( 56 > $header_str_length && $header_str_length >= 40 ) {

					$header_class = 'small_2';

				} elseif( $header_str_length >= 56 ) {

					$header_class = 'small_3';
				}


				$html_out .= '<h3 class="' . $header_class . '"><strong><a href="' . $fundraiser_url . '">' . $post->post_title . '</a></strong></h3>';
				$html_out .= _om_limit_words( strip_tags( $fields['fundraiser_description'] ), 25 ) . '...';
				$html_out .= '<div id="poloroid_options">';

				// $btn_text = ( $closed ) ? 'View Profile' : 'Support This Fundraiser';
				// $html_out .= '<a class="btn btn-success btn-md" href="' . $fundraiser_url . '"><i class="fa fa-heart"></i>&nbsp; ' . $btn_text . '</a>';

				$html_out .= '<a class="btn btn-info btn-sm" href="' . $fundraiser_url . '#tab_fundraiser_profile">View Profile</a>';

				if( ! $closed ) {

					$html_out .= '&nbsp;<a class="btn btn-success btn-sm" href="' . $fundraiser_url . '?shopfor=' . $post->ID . '#tab_fundraiser_support">Support!</a>';

					// $html_out .= '<a class="btn btn-success btn-xs" href="' . $shop_link . '">Shop to Support!<!-- &nbsp;<i class="fa fa-shopping-cart"></i> --></a>';
					// $html_out .= '<a class="btn btn-info btn-xs" href="' . $fundraiser_url . '?shopfor=' . $post->ID . '#how-to-support-donate">Donate!<!-- &nbsp;<i class="fa fa-dollar"></i> --></a>';
				}

				$html_out .= '</div>';
				$html_out .= '</div>';

				echo $html_out;

				$i++;
			}

			echo '</div>';

		} else {

			echo '<p class="h3 text-center">No fundraisers were found for this type!</p>';
		}

		// Reset Post Data
		wp_reset_postdata();

		_fh_paginate( $profiles_query );

	}

	add_action( '_fh_the_content', 'custom_content' );

	$holy_grail_class = 'two_right_1';
	include( get_template_directory() . '/templates/holy_grail_master.php' );

?>

<?php
	else:
?>

<?php


	//
	//	Drop title, breadcrumbs
	//
	add_action( '_fh_the_title', '__return_false' );
	add_action( '_fh_the_crumbs', '__return_false' );

	add_action( '_fh_the_content', function() {


		?>

		<div class="hero-full space-8">
			<div class="unslider">
				<ul>
					<li>
						<div class="hero-content" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/banners/om_home_img.webp');">
							<div class="hero-content-inner">
								<h1>Change the World. <br>One Mission at a Time.</h1>
							</div>
						</div>
					</li>
					<li>
						<div class="hero-content" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/banners/banner-2.webp');">
							<div class="hero-content-inner">
								<h1>Purchase with Purpose</h1>
								<h3 class="space-5">40% of every purchase is donated</h3>
								<div class="mini-separator light"></div>
								<a href="/products/" class="btn btn-xl btn-primary space-top-5">Shop Now</a>
							</div>
						</div>
					</li>
					<li>
						<div class="hero-content" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/banners/banner-3.webp');">
							<div class="hero-content-inner">
								<h1 class="space-0">WORLD CHANGER</h1>
								<p class="lead"><em>– (wurld cheyn-jer) –</em></p>
								<h4><strong>Noun:</strong> Someone who sees a need and decides to DO SOMETHING about it.</h4>
								<p class="lead">(Making a difference for even one person totally counts!)</p>
								<a href="/fundraise/" class="btn btn-xl btn-success space-top-2">Fundraise with Us</a>
							</div>
						</div>
					</li>
					<!-- <li>
						<div class="hero-content" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/banners/home-brave.jpg'); background-position: 50% top;">
							<div class="hero-content-inner">
								<h1>Land of the Free, Home of the Brave</h1>
								<a href="/product-category/apparel-by-message/home-of-the-brave/" class="btn btn-xl btn-primary space-top-5">Get this Tee</a>
							</div>
						</div>
					</li> -->
					<li>
						<div class="hero-content" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/banners/change-challenge.webp'); background-position: 50% bottom;">
							<div class="hero-content-inner">
								<h1 style="display: inline-block; background: rgba(0,0,0,0.7); padding: 10px 20px !important;">Raise Funds for your Mission Offline <br>with Change Challenges</h1>
								<div class="mini-separator light"></div>
								<a href="https://onemission.fund/fundraise/change-challenge/" class="btn btn-xl btn-primary space-top-5">Learn More</a>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>


		<div class="full-section clearfix">
			<div class="space-8 clearfix">

				<h2 class="text-uppercase text-center space-6">Why One Mission?</h2>

				<div class="mini-separator"></div>

				<div class="container space-3 space-top-6 intro-section">
					<div class="row clearfix text-center">
						<div class="col-md-10 col-md-offset-1 space-3">
							<p class="lead">One Mission is a <strong>unique fundraising site</strong> combining high-impact product fundraising and crowdfunding. We are dedicated to helping world-changers do good.</p>
						</div>
					</div>
					<div class="row clearfix">
						<div class="col-sm-5 space-4">
							<a href="https://www.youtube.com/watch?v=HhYAkVmdVBs" class="youtube">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/watch-video.webp" class="img-responsive">
							</a>
						</div>

						<div class="col-sm-7 why-list">
							<p class="lead" style="font-size: 20px; font-weight: bold;">And that's why...</p>
							<ul class="dashed">
								<li>We create beautiful, responsible products that generate repeat support</li>
								<li>We donate 40% of every product purchase</li>
								<li>We don't charge crowdfunding platform fees</li>
								<li>We provide personal assistance every step of the way</li>
							</ul>

						</div>
					</div>
					<div class="row clearfix space-top-3 space-4">
						<div class="col-md-12">
							<p class="lead text-center">Whether you are starting a <strong style="display:inline-block;">One Mission Fund</strong> or supporting a great cause through purchases or donations, One Mission makes it easy to be a world-changer.</p>
						</div>
					</div>
				</div>

				<div class="mini-separator"></div>

			</div>
		</div>


		<div class="full-section clearfix">
			<div id="world-changer-boxes" class="space-8 clearfix">

				<div class="container">
					<h2 class="text-uppercase text-center space-6">How do you want to change the world?</h2>
				</div>

				<div class="box col-sm-4 bg-img bg-fade yellow" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/home/support-img.webp');">
					<div class="box-content text-center">
						<h3 class="text-uppercase">I want to Find a One Mission Fund to Support</h3>
						<a href="/support/" class="btn btn-lg btn-block btn-outline btn-outline-white absolute-btn">Find a Fund</a>
					</div>
				</div>

				<div class="box col-sm-4 bg-img bg-fade green" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/home/create-a-fund-bg.webp');">
					<div class="box-content text-center">
						<h3 class="text-uppercase">I want to create a One Mission Fund</h3>
						<a href="/fundraise/" class="btn btn-lg btn-block btn-outline btn-outline-white absolute-btn">Fundraise with Us</a>
					</div>
				</div>

				<div class="box col-sm-4 bg-img bg-fade blue" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/home/purchase-purpose-img.webp');">
					<div class="box-content text-center">
						<h3 class="text-uppercase">I want to Purchase with Purpose</h3>
						<a href="/products/" class="btn btn-lg btn-block btn-outline btn-outline-white absolute-btn">Shop Now</a>
					</div>
				</div>


			</div>
		</div>


		<div class="full-section clearfix">
			<div class="space-8 clearfix">

				<h2 class="text-uppercase text-center space-6">Product Fundraising + Crowdfunding</h2>

				<div class="mini-separator"></div>

				<div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 space-top-6 space-6 text-center">
					<p class="lead">A powerful combination of <a href="/about-us/product-standard/">product fundraising</a> (high-quality &amp; ethically made) and crowdfunding (truly free of <a href="/fees/">platform fees</a>) giving you more ways to change the world.</p>
				</div>

				<div class="boxes-flex space-top-6 clearfix">
					<div class="box col-sm-6 bg-fade darkBlue">
						<div class="box-content text-center">
							<h3 class="text-uppercase space-6"><span style="font-size: 60px; display: block; margin-bottom: 10px;">40%</span> is a big deal</h3>

							<div class="mini-separator light"></div>

							<p class="lead space-top-5">That's right, <br><strong>40% of each product purchase is donated</strong>. <br>Always.</p>
						</div>
					</div>
					<div class="box col-sm-6 bg-fade blue">
						<div class="box-content text-center">
							<h3 class="text-uppercase space-6">Game Changers</h3>

							<div class="mini-separator light"></div>

							<div class="space-top-6">
								<a href="/fundraise/custom-apparel/" class="btn btn-lg btn-block btn-outline btn-outline-white space-2" style="margin-bottom: 15px;">Custom Designed Tees</a>
								<a href="/fundraise/change-challenge/" class="btn btn-lg btn-block btn-outline btn-outline-white space-2" style="margin-bottom: 15px;">Change Challenges</a>
								<a href="/fundraise/customized-fundraiser" class="btn btn-lg btn-block btn-outline btn-outline-white">Custom Fundraisers</a>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>



		<div class="full-section clearfix">
			<div class="space-8 clearfix">

				<div class="container">
					<h2 class="text-uppercase text-center space-6">Raising Money with One Mission is Easy</h2>
				</div>

				<div class="mini-separator"></div>

				<div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 space-top-6 space-6 text-center">
					<p class="lead">So, come on! Be a world changer today!</p>
					<a href="#signup-form" class="btn btn-xl btn-success popup-with-form">Start a One Mission Fund</a>
				</div>

			</div>
		</div>



		<div class="full-section clearfix">
			<div class="space-8 clearfix">

				<div class="container">
					<h2 class="text-uppercase text-center space-6">One Mission Products</h2>
				</div>

				<div class="mini-separator"></div>

				<div class="container space-top-6">
					<?php include 'templates/part_product_categories.php'; ?>
				</div>

				<!-- <div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 space-top-6 space-6 text-center">
					<a href="#" class="btn btn-xl btn-primary">Browse All Products</a>
				</div> -->

			</div>
		</div>


		<div class="full-section clearfix">
			<div class="space-8 clearfix">

				<h2 class="text-uppercase text-center space-6">Find a Fund</h2>

				<div class="mini-separator"></div>

				<div class="container space-top-6">
					<div class="grid_content row">
						<div class="grid_item col-md-4 col-sm-4 col-xs-6">
							<a class="overlay-effect" href="/support/">
								<div class="overlay desaturate" style="background-image: url('https://onemission.fund/content/uploads/2016/01/youth-groups-250.jpg');">
									<div class="table">
										<div class="bottom">
											<h4>View All Fundraisers</h4>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="grid_item col-md-4 col-sm-4 col-xs-6">
							<a class="overlay-effect" href="/support/?by-type=churches-youth-groups">
								<div class="overlay desaturate" style="background-image: url('https://onemission.fund/content/uploads/2016/03/Community-Benefit.jpg');">
									<div class="table">
										<div class="bottom">
											<h4>Churches &amp; Youth Groups</h4>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="grid_item col-md-4 col-sm-4 col-xs-6">
							<a class="overlay-effect" href="/support/?by-type=mission-groups-missionaries">
								<div class="overlay desaturate" style="background-image: url('https://onemission.fund/content/uploads/2016/01/Mission-Fundraising-250.jpg');">
									<div class="table">
										<div class="bottom">
											<h4>Mission Trips &amp; Groups</h4>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="grid_item col-md-4 col-sm-4 col-xs-6">
							<a class="overlay-effect" href="/support/?by-type=non-profit-organizations">
								<div class="overlay desaturate" style="background-image: url('https://onemission.fund/content/uploads/2016/01/Non-Profits-250.jpg');">
									<div class="table">
										<div class="bottom">
											<h4>Non-Profit Organizations</h4>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="grid_item col-md-4 col-sm-4 col-xs-6">
							<a class="overlay-effect" href="/support/?by-type=schools-fine-arts-athletics">
								<div class="overlay desaturate" style="background-image: url('https://onemission.fund/content/uploads/2016/01/School-Fundraising-250.jpg');">
									<div class="table">
										<div class="bottom">
											<h4>Schools, Fine Arts, &amp; Athletics</h4>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="grid_item col-md-4 col-sm-4 col-xs-6">
							<a class="overlay-effect" href="/support/?by-type=community-benefits">
								<div class="overlay desaturate" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/hands-in.webp');">
									<div class="table">
										<div class="bottom">
											<h4>Community Benefits</h4>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="grid_item col-md-4 col-sm-4 col-xs-6">
							<a class="overlay-effect" href="/support/?by-type=medical-causes">
								<div class="overlay desaturate" style="background-image: url('https://onemission.fund/content/uploads/2016/01/medical-250.jpg');">
									<div class="table">
										<div class="bottom">
											<h4>Medical Causes</h4>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="grid_item col-md-4 col-sm-4 col-xs-6">
							<a class="overlay-effect" href="/support/?by-type=adoptive-parents">
								<div class="overlay desaturate" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/holding-hands.webp');">
									<div class="table">
										<div class="bottom">
											<h4>Adoptions</h4>
										</div>
									</div>
								</div>
							</a>
						</div>
						<div class="grid_item col-md-4 col-sm-4 col-xs-6">
							<a class="overlay-effect" href="/support/?by-type=other-groups-individuals">
								<div class="overlay desaturate" style="background-image: url('https://onemission.fund/content/uploads/2016/03/Memorial.jpg');">
									<div class="table">
										<div class="bottom">
											<h4>Other Groups &amp; Individuals</h4>
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>

				<!-- <div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 space-top-6 space-6 text-center">
					<a href="/fundraisers/" class="btn btn-lg btn-primary">Find a Fund</a>
					<a href="/fundraise/" class="btn btn-lg btn-success">Start a One Mission Fund</a>
					<a href="#" class="btn btn-lg btn-purple">Purchase with Purpose</a>
				</div> -->

			</div>
		</div>


		<div class="full-section clearfix">
			<div class="box bg-fade darkBlue" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/om-testimonial-bg.webp');">
				<div class="box-content no-padding container">
					<div class="space-10 space-top-10">

					<h2 class="text-uppercase text-center text-white space-5 space-top-6">What are People Saying?</h2>

					<div class="mini-separator light"></div>


					<?php
						$acf_fields = get_fields();
					?>

					<div class="space-top-5 clearfix">
						<div class="col-sm-4 space-5">
							<?php if( isset( $acf_fields['testimonial_1'] ) && ! empty( $acf_fields['testimonial_1'] ) ): ?>
								<div class="<?php echo get_testimonials_class( $acf_fields['testimonial_1'] ); ?> quote text-center">
									<p class="text-white"><?php echo $acf_fields['testimonial_1']; ?></p>
									<?php if( isset( $acf_fields['testimonial_1_byline'] ) && ! empty( $acf_fields['testimonial_1_byline'] ) ): ?>
										<span class="byline"><?php echo $acf_fields['testimonial_1_byline']; ?></span>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
						<div class="col-sm-4 space-5">
							<?php if( isset( $acf_fields['testimonial_2'] ) && ! empty( $acf_fields['testimonial_2'] ) ): ?>
								<div class="<?php echo get_testimonials_class( $acf_fields['testimonial_2'] ); ?> quote text-center">
									<p class="text-white"><?php echo $acf_fields['testimonial_2']; ?></p>
									<?php if( isset( $acf_fields['testimonial_2_byline'] ) && ! empty( $acf_fields['testimonial_2_byline'] ) ): ?>
										<span class="byline"><?php echo $acf_fields['testimonial_2_byline']; ?></span>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
						<div class="col-sm-4 space-5">
							<?php if( isset( $acf_fields['testimonial_3'] ) && ! empty( $acf_fields['testimonial_3'] ) ): ?>
								<div class="<?php echo get_testimonials_class( $acf_fields['testimonial_3'] ); ?> quote text-center">
									<p class="text-white"><?php echo $acf_fields['testimonial_3']; ?></p>
									<?php if( isset( $acf_fields['testimonial_3_byline'] ) && ! empty( $acf_fields['testimonial_3_byline'] ) ): ?>
										<span class="byline"><?php echo $acf_fields['testimonial_3_byline']; ?></span>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
					</div>


				</div>
				</div>
			</div>
		</div>


		<?php

	});

	function get_testimonials_class( $text ) {

		$text_length = strlen( $text );

		if( $text_length <= 50 ) {

			return 'small_quote';

		} elseif( $text_length <= 130 ) {

			return 'medium_quote';
		}

		return 'large_quote';
	}

	$holy_grail_class = 'full';
	include( get_template_directory() . '/templates/holy_grail_master.php' );

?>

<?php
	endif;
?>

