<?php
/**
 * Thankyou page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( $order ) : ?>

	<?php if ( $order->has_status( 'failed' ) ) : ?>

		<p><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction.', 'woocommerce' ); ?></p>

		<p><?php
			if ( is_user_logged_in() )
				_e( 'Please attempt your purchase again or go to your account page.', 'woocommerce' );
			else
				_e( 'Please attempt your purchase again.', 'woocommerce' );
		?></p>

		<p>
			<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'woocommerce' ) ?></a>
			<?php if ( is_user_logged_in() ) : ?>
			<a href="<?php echo esc_url( get_permalink( wc_get_page_id( 'myaccount' ) ) ); ?>" class="button pay"><?php _e( 'My Account', 'woocommerce' ); ?></a>
			<?php endif; ?>
		</p>

	<?php else : ?>

		<?php

			// Look-up and display custom thankyou message if applicable
			if( isset( $_COOKIE['shopping_for_id'] ) && ! empty( $_COOKIE['shopping_for_id'] ) ) {

				$custom_thanks = get_field( 'thank_you_message', $_COOKIE['shopping_for_id'] );
				$shop_for_link = $_COOKIE['fundraiser_guid'] . '?shopfor=' .$_COOKIE['shopping_for_id'];

				// Grab custom thanks if it exists
				if( $custom_thanks && !empty( $custom_thanks ) ) {

					$thanks_msg = nl2br( $custom_thanks );

				} else {

					$thanks_msg = 'Thank you for supporting ' . $_COOKIE['fundraiser_name'] . '!';
				}

				//
				//	Specific fundraising sharing / message
				//
				$twitter_share_btn  = '<a href="http://twitter.com/home?status=I+just+supported+' . $_COOKIE['fundraiser_name'] . '+through+One+Mission+Fundraising%21+' . $shop_for_link .'" class="btn btn-twitter" rel="nofollow" target="_blank" title="Share on Twitter"><span class="fa fa-twitter"></span>&nbsp; Share on Twitter</a>&nbsp;';
				$facebook_share_btn = '<a href="http://www.facebook.com/sharer.php?u=' . $shop_for_link . '&t=' . $_COOKIE['fundraiser_name'] . '" class="btn btn-facebook" rel="nofollow" target="_blank" title="Share on Facebook"><span class="fa fa-facebook"></span>&nbsp; Share on Facebook</a>&nbsp;';
				$google_share_btn   = '<a href="https://plus.google.com/share?url=' . $shop_for_link . '"  class="btn btn-google" rel="nofollow" target="_blank" title="Share on Google+"><span class="fa fa-google-plus"></span>&nbsp; Share on Google+</a>';
				$share_msg          = 'Help ' . $_COOKIE['fundraiser_name'] . ' by sharing their One Mission Fundraising profile!';

				//
				//	Facebook sharing block
				//
				$title      = $_COOKIE['fundraiser_name'];
				$fields     = get_fields( $_COOKIE['shopping_for_id'] );
				$short_desc = _om_limit_words( strip_tags( $fields['fundraiser_description'] ), 40 ) . ' [...]';

				// Ensure logo image is uploaded
				if( isset( $fields['logo_image']['sizes']['om-profile-280'] ) && ! empty( $fields['logo_image']['sizes']['om-profile-280'] ) ) {

					$logo      = $fields['logo_image']['sizes']['om-profile-280'];
					$logo_html = '<img id="order_thanks_logo_img" src="' . $logo . '" alt="' . $title . ' Logo Image" title="' . $title . ' Logo Image">';

				} else {

					$logo_html = '';
				}

			} else {

				//
				//	General sharing links for One Mission / message
				//
				$twitter_share_btn  = '<a href="http://twitter.com/home?status=One+Mission+Fundraising+http://onemissionfundraising.com" class="btn btn-twitter" rel="nofollow" target="_blank" title="Share on Twitter"><span class="fa fa-twitter"></span>&nbsp; Share on Twitter</a>&nbsp;';
				$facebook_share_btn = '<a href="http://www.facebook.com/sharer.php?u=http://onemissionfundraising.com&t=One+Mission+Fundraising" class="btn btn-facebook" rel="nofollow" target="_blank" title="Share on Facebook"><span class="fa fa-facebook"></span>&nbsp; Share on Facebook</a>&nbsp;';
				$google_share_btn   = '<a href="https://plus.google.com/share?url=http://onemissionfundraising.com" class="btn btn-google" rel="nofollow" target="_blank" title="Share on Google+"><span class="fa fa-google-plus"></span>&nbsp; Share on Google+</a>';
				$share_msg          = 'Help us by spreading the One Mission Fundraising cause!';
				$thanks_msg         = 'Thank you for supporting the <a href="/about-us/one-mission-love-fund/">One Mission Love Fund</a>.<br><br>The One Mission Love Fund is used to support the operational costs of the One Mission platform as well as causes and projects near and dear to One Mission’s heart. If you benefited the One Mission Love Fund when you intended to benefit a different Fund, just send us an email <a href="mailto:info@onemission.fund">(info@onemission.fund)</a> within 24 hours of the transaction and we’ll be happy to adjust the transaction to benefit your desired Fund.';

				//
				//	Facebook sharing block
				//	- Use "apple-touch-icon-ipad-precomposed.png" for logo image
				//
				$title      = 'One Mission Fundraising';
				$short_desc = 'Looking to start a successful fundraiser? Or perhaps you want to do a bit of shopping while supporting a great cause. One Mission Fundraising has you covered.';
				$logo       = get_stylesheet_directory_uri() . '/img/icons/apple-touch-icon-ipad-precomposed.png';
				$logo_html  = '<img id="order_thanks_logo_img" src="' . $logo . '" alt="' . $title . ' Logo Image" title="' . $title . ' Logo Image">';
			}

		?>

		<div class="alert alert-success text-center">

			<h2>Thank You!</h2>

			<p>Your order has been received!</p><br>

			<p><?php echo $thanks_msg; ?></p>

			<hr>

			<h2>More ways to help - Spread the Word!</h2>

			<p><?php echo $share_msg; ?></p>

			<hr>

			<p>
				<?php
					echo $twitter_share_btn;
					echo $facebook_share_btn;
					echo $google_share_btn;
				?>
			</p>

		</div>

		<!-- Modal sharing -->
		<div class="modal fade" id="overlay_modal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4 class="modal-title">Share <?php echo $title; ?> on Facebook!</h4>
					</div>
					<div class="modal-body">

						<div class="clearfix">
						<?php

							// Show logo and short description for sharing
							echo '<div id="order_thanks_img_center">' . $logo_html . '</div>';
							echo '<p>' . $short_desc . '</p>';

						?>
						</div>

						<hr>

						<p class="text-center"><?php echo $facebook_share_btn; ?></p>
					</div>
				</div>
			</div>
		</div>

		<script type="text/javascript">

			jQuery( window ).on( 'load', function() {

				jQuery( '#overlay_modal' ).modal( 'show' );
			});

		</script>

		<ul class="order_details">
			<li class="order">
				<?php _e( 'Order:', 'woocommerce' ); ?>
				<strong><?php echo $order->get_order_number(); ?></strong>
			</li>
			<li class="date">
				<?php _e( 'Date:', 'woocommerce' ); ?>
				<strong><?php echo date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ); ?></strong>
			</li>
			<li class="total">
				<?php _e( 'Total:', 'woocommerce' ); ?>
				<strong><?php echo $order->get_formatted_order_total(); ?></strong>
			</li>
			<?php if ( $order->payment_method_title ) : ?>
			<li class="method">
				<?php _e( 'Payment method:', 'woocommerce' ); ?>
				<strong><?php echo $order->payment_method_title; ?></strong>
			</li>
			<?php endif; ?>
		</ul>
		<div class="clear"></div>

	<?php endif; ?>

	<?php do_action( 'woocommerce_thankyou_' . $order->payment_method, $order->id ); ?>
	<?php do_action( 'woocommerce_thankyou', $order->id ); ?>

<?php else : ?>

	<p><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'woocommerce' ), null ); ?></p>

<?php endif; ?>
