
<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<?php if ( $checkout->get_checkout_fields() ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col2-set" id="customer_details">
			<div class="col-1">
        <!-- <h3><?php _e( 'Billing details', 'woocommerce' ); ?></h3> -->
        <ul class="billing_details" style="list-style: none">
<?php
      echo '<li class="billing_name">' . $checkout->get_value('billing_first_name') . ' ' . $checkout->get_value('billing_last_name') . '</li>';


      if (!empty(trim($checkout->get_value('billing_address_1')))) {
        echo '<li class="billing_address_1">' . $checkout->get_value('billing_address_1') . '</li>';
      }
      if (!empty(trim($checkout->get_value('billing_address_2')))) {
        echo '<li class="billing_address_2">' . $checkout->get_value('billing_address_2') . '</li>';
      }
      if (!empty(trim($checkout->get_value('billing_city')))) {
        echo '<li class="billing_city">' . $checkout->get_value('billing_city') . ', ' . $checkout->get_value('billing_state') . '  ' . $checkout->get_value('billing_postcode') . '</li>';
      }
      if (!empty(trim($checkout->get_value('billing_phone')))) {
        echo '<li class="billing_phone">' . $checkout->get_value('billing_phone') . '</li>';
      }
      if (!empty(trim($checkout->get_value('billing_email')))) {
        echo '<li class="billing_email">' . $checkout->get_value('billing_email') . '</li>';
      }
      // foreach ($checkout->checkout_fields['billing'] as $key => $field) {
      //   // $name = $woocommerce->customer->get_shipping_first_name();
      //   if (!empty(trim($checkout->get_value($key)))) {
      //     echo '<li>' . $checkout->get_value($key) . '</li>';
      //   }
      // }
?>
        </ul>
<?php
      do_action( 'woocommerce_checkout_billing' );
?>
			</div>

			<div class="col-2">
        <!-- <span class="change_shipping_wrapper" style="float: right"> -->
        <!--   <a class="popup&#45;with&#45;form" title="Change Address" href="#change&#45;shipping&#45;form"> -->
        <!--     <button id="om_change_shipping" class="btn btn&#45;info"> -->
        <!--       <i class="fa fa&#45;edit"></i> -->
        <!--       <?php // esc_attr_e( 'change', 'woocommerce' ); ?> -->
        <!--     </button> -->
        <!--   </a> -->
        </span>
        <h3><?php _e( 'Shipping details', 'woocommerce' ); ?></h3>
        <ul class="shipping_details" style="list-style: none">
				<?php do_action( 'woocommerce_checkout_shipping' ); ?>
<?php

      echo '<li class="shipping_name">' . $checkout->get_value('shipping_first_name') . ' ' . $checkout->get_value('shipping_last_name') . '</li>';

      if (!empty(trim($checkout->get_value('shipping_address_1')))) {
        echo '<li class="shipping_address_1">' . $checkout->get_value('shipping_address_1') . '</li>';
      }
      if (!empty(trim($checkout->get_value('shipping_address_2')))) {
        echo '<li class="shipping_address_2">' . $checkout->get_value('shipping_address_2') . '</li>';
      }
      if (!empty(trim($checkout->get_value('shipping_city')))) {
        echo '<li class="shipping_city">' .
          $checkout->get_value('shipping_city') .
          ', ' . $checkout->get_value('shipping_state') .
          '  ' . $checkout->get_value('shipping_postcode') .
          '</li>';
      }
      if (!empty(trim($checkout->get_value('shipping_phone')))) {
        echo '<li class="shipping_phone">' . $checkout->get_value('shipping_phone') . '</li>';
      }
?>
        </ul>
        <div class="shipping_changed_values">
        </div>
			</div>
		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

	<?php endif; ?>

	<h3 id="order_review_heading"><?php esc_html_e( 'Your order', 'woocommerce' ); ?></h3>

	<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

	<div id="order_review" class="woocommerce-checkout-review-order">
		<?php do_action( 'woocommerce_checkout_order_review' ); ?>
	</div>

	<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
