<?php
/**
 * Customer processing renewal order email
 *
 * @author	Brent Shepherd
 * @package WooCommerce_Subscriptions/Templates/Emails
 * @version 1.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
	$fundraiser = "";
	$order_meta = get_post_meta( $order->id );
    if( isset( $order_meta['Fundraiser Name'] ) && ! empty( $order_meta['Fundraiser Name'] ) && isset ($order_meta['_order_total']) && floatval( $order_meta['_order_total'][0]) && $order_meta['_order_total'][0] > 0.00 ) {
        $fundraiser = '<p><strong>Fundraiser Supported:</strong> <a href="' . $order_meta['Fundraiser GUID'][0] . '" target="_blank">' . $order_meta['Fundraiser Name'][0] . '</a></p>';
	 }

	$order_dt = strtotime($order->order_date);
	$order_date = date("Y-m-d", $order_dt);
?>

<?php do_action( 'woocommerce_email_header', $email_heading ); ?>

<p><strong><?php printf( __( 'Order Date: %s', 'woocommerce-subscriptions' ), $order_date ); ?></strong></p>
<h2><?php printf( __( 'Order #%s', 'woocommerce-subscriptions' ), $order->get_order_number() ); ?></h2>
<?php printf( __( $fundraiser  ) ); ?>

<p><?php esc_html_e( 'Your subscription order is being processed. You will receive an email notification once your order is complete, which will serve as your receipt. ', 'woocommerce-subscriptions' ); ?></p>

<?php do_action( 'woocommerce_email_before_order_table', $order, false, false ); ?>


<table cellspacing="0" cellpadding="6" style="width: 100%; border: 1px solid #eee;" border="1" bordercolor="#eee">
	<thead>
		<tr>
			<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php esc_html_e( 'Product', 'woocommerce-subscriptions' ); ?></th>
			<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php esc_html_e( 'Quantity', 'woocommerce-subscriptions' ); ?></th>
			<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php esc_html_e( 'Price', 'woocommerce-subscriptions' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php echo wp_kses_post( $order->email_order_items_table( $order->is_download_permitted(), true, ($order->status == 'processing') ? true : false ) ); ?>
	</tbody>
	<tfoot>
		<?php
		if ( $totals = $order->get_order_item_totals() ) {
			$i = 0;
			foreach ( $totals as $total ) {
				$i++; ?>
				<tr>
					<th scope="row" colspan="2" style="text-align:left; border: 1px solid #eee; <?php if ( 1 == $i ) { echo 'border-top-width: 4px;'; } ?>"><?php echo esc_html( $total['label'] ); ?></th>
					<td style="text-align:left; border: 1px solid #eee; <?php if ( 1 == $i ) { echo 'border-top-width: 4px;'; } ?>"><?php echo wp_kses_post( $total['value'] ); ?></td>
				</tr><?php
			}
		}
		?>
	</tfoot>
</table>

<?php do_action( 'woocommerce_email_after_order_table', $order, false, false, true ); ?>

<?php do_action( 'woocommerce_email_order_meta', $order, false, false ); ?>

<h2><?php esc_html_e( 'Customer details', 'woocommerce-subscriptions' ); ?></h2>

<?php if ( $order->billing_email ) : ?>
	<p><strong><?php esc_html_e( 'Email:', 'woocommerce-subscriptions' ); ?></strong> <?php echo esc_html( $order->billing_email ); ?></p>
<?php endif; ?>
<?php if ( $order->billing_phone ) : ?>
	<p><strong><?php esc_html_e( 'Tel:', 'woocommerce-subscriptions' ); ?></strong> <?php echo esc_html( $order->billing_phone ); ?></p>
<?php endif; ?>

<?php wc_get_template( 'emails/email-addresses.php', array( 'order' => $order ) ); ?>

<?php do_action( 'woocommerce_email_footer' ); ?>
