<?php
/**
 * Customer processing order email
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	$love_fund = !( isset( $order_meta['Fundraiser User ID'] ) &&
					! empty( $order_meta['Fundraiser User ID'] ) &&
					is_array( $order_meta['Fundraiser User ID'] ) &&
					isset( $order_meta['Fundraiser User ID'][0] ) &&
					$order_meta['Fundraiser User ID'][0]);

	$fundraiser = "";
	$order_meta = get_post_meta( $order->id );

	if ( isset( $order_meta['Fundraiser Name'] ) &&
		! empty( $order_meta['Fundraiser Name'] ) &&
		isset ($order_meta['_order_total'])
		&& floatval( $order_meta['_order_total'][0])
		&& $order_meta['_order_total'][0] > 0.00 ) {
			$fundraiser = '<p>You just made a difference for: <a href="' . $order_meta['Fundraiser GUID'][0] . '" target="_blank">' . $order_meta['Fundraiser Name'][0] . '</a></p>';
		} else {
			$fundraiser .= '<p>If you benefited the One Mission Love Fund when you really meant to benefit a specific fundraiser, just send us an email (<a href="mailto:info@onemissionfundraising.com">info@onemissionfundraising.com</a>) within 24 hours of the transaction and let us know and we’ll be happy to adjust the transaction to benefit your desired fundraiser.</p>';
		}

	$donation_amount = "";
  if(isset($order_meta['Total Donation Amount'])) {
    $donation_amount = $order_meta['Total Donation Amount'][0];
    $donation_amount = '<p><strong>Donated Total:</strong><br>$' . $donation_amount . '</p>';
  }

	$order_dt = strtotime($order->order_date);
	$order_date = date("Y-m-d", $order_dt);
?>

<?php do_action('woocommerce_email_header', $email_heading); ?>

<?php
	echo $fundraiser;
	echo $donation_amount;
?>

<p><?php _e( "Your order details are shown below. You will receive an email notification and receipt once your order is complete.", 'woocommerce' ); ?></p>

<?php do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text ); ?>

<h2><?php printf( __( 'Order #%s', 'woocommerce' ), $order->get_order_number() ); ?></h2>
<p><strong><?php printf( __( 'Order Date: %s', 'woocommerce' ), $order_date ); ?></strong></p>

<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
	<thead>
		<tr>
			<th class="td" scope="col" style="text-align:left;"><?php _e( 'Product', 'woocommerce' ); ?></th>
			<th class="td" scope="col" style="text-align:left;"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
			<th class="td" scope="col" style="text-align:left;"><?php _e( 'Price', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php echo $order->email_order_items_table( $order->is_download_permitted(), true, $order->has_status( 'processing' ) ); ?>
	</tbody>
	<tfoot>
		<?php
			if ( $totals = $order->get_order_item_totals() ) {
				$i = 0;
				foreach ( $totals as $total ) {
					$i++;
					?><tr>
						<th class="td" scope="row" colspan="2" style="text-align:left; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['label']; ?></th>
						<td class="td" style="text-align:left; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['value']; ?></td>
					</tr><?php
				}
			}
		?>
	</tfoot>
</table>

<?php // do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text ); ?>

<?php do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text ); ?>

<?php do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text ); ?>

<?php do_action( 'woocommerce_email_footer' ); ?>
