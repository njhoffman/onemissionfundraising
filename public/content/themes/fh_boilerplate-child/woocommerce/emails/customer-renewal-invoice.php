<?php
/**
 * Customer renewal invoice email
 *
 * @author	Brent Shepherd
 * @package WooCommerce_Subscriptions/Templates/Emails
 * @version 1.4
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
	$fundraiser = "";
	$order_meta = get_post_meta( $order->id );
    if( isset( $order_meta['Fundraiser Name'] ) && ! empty( $order_meta['Fundraiser Name'] ) && isset ($order_meta['_order_total']) && floatval( $order_meta['_order_total'][0]) && $order_meta['_order_total'][0] > 0.00 ) {
        $fundraiser = '<a href="' . $order_meta['Fundraiser GUID'][0] . '" target="_blank">' . $order_meta['Fundraiser Name'][0] . '</a>';
	 }

	$order_dt = strtotime($order->order_date);
	$order_date = date("Y-m-d", $order_dt);
?>


<?php do_action( 'woocommerce_email_header', $email_heading ); ?>

<?php if ( $order->status == 'pending' ) : ?>
	<p>
	<?php
		printf( esc_html(
			'An invoice has been created for you to renew your subscription with %s. To pay for this invoice please use the following link: %s', 'woocommerce-subscriptions' ), esc_html( get_bloginfo( 'name' ) ),
			'<br /><a href="' . esc_url( $order->get_checkout_payment_url() ) . '">' . esc_html__( 'Pay Now &raquo;', 'woocommerce-subscriptions' ) . '</a>' );
	?>
	</p>
<?php elseif ( 'failed' == $order->status ) : ?>
	<h2>Automatic Payment Failed</h2>
	<p style="color:#ff0000;">
		<?php
			printf( esc_html( 'The automatic payment to renew your subscription with %s for %s has %s. ' ), esc_html ( get_bloginfo( 'name' ) ), $fundraiser,
						'<strong><em>'. 	esc_html__('failed', 'woocommerce-subscriptions' ) . '</em></strong>');
		?>
	</p>
	<p>
		<?php
			printf( esc_html('To reactivate the subscription, please login and update your information from your account page.', 'woocommerce-subscriptions' ) );
		?>
	</p>
	<a href="<?= esc_url( $order->get_checkout_payment_url() );  ?>" style="background-color:#5cb85c;border:1px solid #4cae4c;border-radius:3px;color:#ffffff;display:inline-block;font-family:sans-serif;font-size:16px;line-height:44px;text-align:center;text-decoration:none;width:150px;-webkit-text-size-adjust:none;mso-hide:all;">Pay Now &raquo;</a>
	<br />
<?php endif; ?>

<?php do_action( 'woocommerce_email_before_order_table', $order, false, false ); ?>


<br />
<?php
	echo "<p><strong>Fundraiser Supported: </strong><br />" . $fundraiser . "</p>";
?>

<p><strong><?php printf( __( 'Order Date: %s', 'woocommerce' ), $order_date ); ?></strong></p>
<h2><?php printf( __( 'Order #%s', 'woocommerce' ), $order->get_order_number() ); ?></h2>

<table cellspacing="0" cellpadding="6" style="width: 100%; border: 1px solid #eee;" border="1" bordercolor="#eee">
	<thead>
		<tr>
			<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php esc_html_e( 'Product', 'woocommerce-subscriptions' ); ?></th>
			<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php esc_html_e( 'Quantity', 'woocommerce-subscriptions' ); ?></th>
			<th scope="col" style="text-align:left; border: 1px solid #eee;"><?php esc_html_e( 'Price', 'woocommerce-subscriptions' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php echo wp_kses_post( $order->email_order_items_table( false, true, false ) ); ?>
	</tbody>
	<tfoot>
		<?php
		if ( $totals = $order->get_order_item_totals() ) {
			$i = 0;
			foreach ( $totals as $total ) {
				$i++; ?>
				<tr>
					<th scope="row" colspan="2" style="text-align:left; border: 1px solid #eee; <?php if ( 1 == $i ) { echo 'border-top-width: 4px;'; } ?>"><?php echo esc_html( $total['label'] ); ?></th>
					<td style="text-align:left; border: 1px solid #eee; <?php if ( 1 == $i ) { echo 'border-top-width: 4px;'; } ?>"><?php echo wp_kses_post( $total['value'] ); ?></td>
				</tr><?php
			}
		}
		?>
	</tfoot>
</table>

<?php do_action( 'woocommerce_email_after_order_table', $order, false, false ); ?>

<?php do_action( 'woocommerce_email_footer' ); ?>
