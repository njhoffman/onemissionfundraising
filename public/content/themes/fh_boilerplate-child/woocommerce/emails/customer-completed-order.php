<?php
/**
 * Customer completed order email
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php do_action( 'woocommerce_email_header', $email_heading ); ?>


<?php
	$fundraiser = "";
	$order_meta = get_post_meta( $order->id );
    if( isset( $order_meta['Fundraiser Name'] ) && ! empty( $order_meta['Fundraiser Name'] ) && isset ($order_meta['_order_total']) && floatval( $order_meta['_order_total'][0]) && $order_meta['_order_total'][0] > 0.00 ) {
        $fundraiser = '<p><strong>Fundraiser Supported: </strong><br /> <a href="' . $order_meta['Fundraiser GUID'][0] . '" target="_blank">' . $order_meta['Fundraiser Name'][0] . '</a></p>';
	 }

	$order_over_0 = isset ($order_meta['_order_total']) && floatval( $order_meta['_order_total'][0]) && $order_meta['_order_total'][0] > 0.00 ? true : false;

	$donation_amount = "";
  if(isset($order_meta['Total Donation Amount'])) {
    $donation_amount = $order_meta['Total Donation Amount'][0];
    $donation_amount = '<p><strong>Donated Total:</strong><br>$' . $donation_amount . '</p>';
  }

	$thank_you = "";
	if(
		isset( $order_meta['Fundraiser User ID'] ) &&
		! empty( $order_meta['Fundraiser User ID'] ) &&
		is_array( $order_meta['Fundraiser User ID'] ) &&
		isset( $order_meta['Fundraiser User ID'][0] ) &&
		$order_meta['Fundraiser User ID'][0]
	) {

		// Grab the fundraiser ID and fundraiser profile ID
		$fundraiser_id = (int) $order_meta['Fundraiser User ID'][0];
		$fundraiser_profile_id = get_user_meta( $fundraiser_id, 'fundraiser_profile_id', true );
		// Get ACF field for fundraiser profile thanks
		$custom_thanks = get_field( 'thank_you_message', $fundraiser_profile_id );

		// If there's a 501c3 status on the fundraiser it's non-profit
		$tax_status_np = get_field( 'enable_501c', $fundraiser_profile_id );
		$np_message = "<hr />";
		if ($tax_status_np) {
			$np_message = "<hr /><p>The Donated Total above qualifies as a charitable donation for income tax purposes as no goods or service were exchanged for this portion of your payment. Consult with your professional tax advisor regarding your individual tax situation.</p>";
		}

		// Ensure there is a thank you message before appending to email
		if( $custom_thanks && ! empty( $custom_thanks ) ) {
			$thank_you = '<hr><p><strong>Fundraiser Thank You Message:</strong><br>' . nl2br( $custom_thanks ) . '</p>';
		}
	}

	$order_dt = strtotime($order->order_date);
	$order_date = date("Y-m-d", $order_dt);

?>

<?php do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text ); ?>

<br />

<?php

	echo $donation_amount;
	echo $fundraiser;
	if ($order_over_0 === true) {
		echo $thank_you;
		echo $np_message;
	}
?>

<p><?php printf( __( "Your order details are shown below.", 'woocommerce' ), get_option( 'blogname' ) ); ?></p>


<p><strong><?php printf( __( 'Order Date: %s', 'woocommerce' ), $order_date ); ?></strong></p>
<h2><?php printf( __( 'Order #%s', 'woocommerce' ), $order->get_order_number() ); ?></h2>

<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
	<thead>
		<tr>
			<th class="td" scope="col" style="text-align:left;"><?php _e( 'Product', 'woocommerce' ); ?></th>
			<th class="td" scope="col" style="text-align:left;"><?php _e( 'Quantity', 'woocommerce' ); ?></th>
			<th class="td" scope="col" style="text-align:left;"><?php _e( 'Price', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php echo $order->email_order_items_table( true, false, true ); ?>
	</tbody>
	<tfoot>
		<?php
			if ( $totals = $order->get_order_item_totals() ) {
				$i = 0;
				foreach ( $totals as $total ) {
					$i++;
					?><tr>
						<th class="td" scope="row" colspan="2" style="text-align:left; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['label']; ?></th>
						<td class="td" style="text-align:left; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['value']; ?></td>
					</tr><?php
				}
			}
		?>
	</tfoot>
</table>

<?php do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text ); ?>

<?php do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text ); ?>

<?php do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text ); ?>

<?php
	if ($order_over_0 === false) {
		echo '<p>Login to <a href="https://onemissionfundraising.com/my-account/" target="_blank">"My Account"</a> to manage your account and subscription.</p>';
	}

?>
<?php do_action( 'woocommerce_email_footer' ); ?>
