<?php

	// Load & enque assets for the custom canvas application
	include 'include/custom_canvas.php';

	wp_register_script( 'custom_canvas_upload', get_stylesheet_directory_uri() . '/js/custom_canvas_upload.js', array( 'jquery' ) );
	wp_enqueue_script( 'custom_canvas_upload' );

	$CustomCanvas = new CustomCanvas( false );

	//
	//	Loops through items in cart, check if product exists with OM_CUSTOM_CANVAS_POST_ID
	//	- if exists returns true.
	// 
	function _fh_canvas_exsits() {
	 
		global $woocommerce;
	 
		if( sizeof($woocommerce->cart->get_cart()) > 0 ){
	 
			foreach( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {
	 
				$_product = $values['data'];
	 
				if( $_product->id == OM_CUSTOM_CANVAS_POST_ID ) {

					// Product exists, return true
					return true;
				}
			}
		}

		return false;
	}

	function custom_content() {

		if( ! _fh_canvas_exsits() ) {

			global $CustomCanvas;

			?>

			<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/custom_canvas/canvas_example.jpg" title="Custom Canvas Example Image" alt="Custom Canvas Example Image" class="alignright">

			<p>IT'S TRUE: we all have some pretty great digital photos - of the kids &map; grandma, that last family vacation, or little Tommy in his baseball uniform - sitting on our phone or desktop when they should be hanging on our wall. Luckily enough, we can print those pictures for you quickly and easily (and on really nice canvas, nonetheless). Simply follow the steps to upload your picture right from your computer, choose your size and borders, and voilà - you'll have beautifully showcased some great memories, and will receive the added memory of helping out someone's important mission along the way.</p>

			<h2>The One Mission Promise:</h2>

			<ul class="list-unstyled">

				<li>
					<p><strong><i class="fa fa-check"></i> Handmade in the USA</strong><br>One Mission canvases are of the very highest quality, and are printed, UV-coated, and stretched by hand in Iowa.</p>
				</li>

				<li>
					<p><strong><i class="fa fa-check"></i> Free Digital Proofs</strong><br>We make sure each canvas is perfect by sending you a free digital proof by email before it is printed.</p>
				</li>

				<li>
					<p><strong><i class="fa fa-check"></i> 100% Satisfaction Guaranteed</strong><br>If you're not entirely happy with your canvas, we'll replace it. No questions asked.</p>
				</li>

				<li>
					<p><strong><i class="fa fa-check"></i> Questions?</strong><br>Please call <a href="tel:319-895-4072">319-895-4072</a> or email <a href="mailto:print@onemissionfundraising.com">print@onemissionfundraising.com</a></p>
				</li>

			</ul>

			<hr>

			<div id="custom_canvas_info_columns" class="row">

				<div class="col-md-4 text-center">

					<i class="fa fa-camera-retro" style="color: #63421A;"></i>

					<h2>1. Upload your photo</h2>

					<p>First, choose and upload the image you want on your canvas using the form below.</p>

				</div>

				<div class="col-md-4 text-center">

					<i class="fa fa-arrows-alt" style="color: #7A7A7A;"></i>

					<h2>2. Choose a canvas size</h2>

					<p>Next, choose your canvas size and printing options (canvas thickness and borders).</p>

				</div>

				<div class="col-md-4 text-center">

					<i class="fa fa-heart" style="color: #DF2626;"></i>

					<h2>3. Change the World!</h2>

					<p>40% of your purchase benefits the fundraiser of your choice.</p>

				</div>

			</div>

			<hr>

			<div class="row">

				<div class="col-sm-6">

					<h2 class="text-center">Prices and Donations</h2>

					<table id="custom_canvas_standard" class="table table-bordered table-striped">

						<tr>
							<td class="text-center" colspan="3"><strong>Standard Wrap</strong></td>
						</tr>

						<tr>
							<td class="text-center"><strong>Size</strong></td>
							<td class="text-center"><strong>Price</strong></td>
							<td class="text-center"><strong>40%<br>Donation</strong></td>
						</tr>

						<tr>
							<td>8x10</td>
							<td class="text-center">$40</td>
							<td class="text-center">$16</td>
						</tr>

						<tr>
							<td>11x14</td>
							<td class="text-center">$55</td>
							<td class="text-center">$22</td>
						</tr>

						<tr>
							<td>12x12</td>
							<td class="text-center">$55</td>
							<td class="text-center">$22</td>
						</tr>

						<tr>
							<td>12x18</td>
							<td class="text-center">$65</td>
							<td class="text-center">$26</td>
						</tr>

						<tr>
							<td>16x20</td>
							<td class="text-center">$80</td>
							<td class="text-center">$32</td>
						</tr>

						<tr>
							<td>18x24</td>
							<td class="text-center">$90</td>
							<td class="text-center">$36</td>
						</tr>

					</table>

					<table id="custom_canvas_gallery" class="table table-bordered table-striped">

						<tr>
							<td class="text-center" colspan="3"><strong>Gallery Wrap</strong></td>
						</tr>

						<tr>
							<td class="text-center"><strong>Size</strong></td>
							<td class="text-center"><strong>Price</strong></td>
							<td class="text-center"><strong>40%<br>Donation</strong></td>
						</tr>

						<tr>
							<td>8x10</td>
							<td class="text-center">$60</td>
							<td class="text-center">$24</td>
						</tr>

						<tr>
							<td>11x14</td>
							<td class="text-center">$75</td>
							<td class="text-center">$30</td>
						</tr>

						<tr>
							<td>12x12</td>
							<td class="text-center">$75</td>
							<td class="text-center">$30</td>
						</tr>

						<tr>
							<td>12x18</td>
							<td class="text-center">$85</td>
							<td class="text-center">$34</td>
						</tr>

						<tr>
							<td>16x20</td>
							<td class="text-center">$100</td>
							<td class="text-center">$40</td>
						</tr>

						<tr>
							<td>18x24</td>
							<td class="text-center">$110</td>
							<td class="text-center">$44</td>
						</tr>

					</table>

				</div>

				<div class="col-sm-6">

					<h2 class="text-center"><i class="fa fa-camera-retro"></i>&nbsp; Start Here!</h2>

					<div id="custom_canvas_upload_frm" class="well">

					<?php

						// Check for missing image data
						// - redirected if someone lands on custom canvas product w/o uploading img
						if( isset( $_GET['ccerror'] ) && $_GET['ccerror'] == 'missingimg' ) {

							$CustomCanvas->renderError( 'No image uploaded! Must upload an image first =)', true );
						}

						$CustomCanvas->getError( true );
						$CustomCanvas->getProgressBar( true );
						$CustomCanvas->getUploadForm( true );

					?>

					</div>
				</div>
			</div>

			<?php

		} else {

			echo '<div class="alert alert-info text-center">';
			echo '<p>You already have a Custom Canvas in your cart. Only one Custom Canvas is allowed per order at this time!</p>';
			echo '<p>You can also remove the existing custom canvas from your <a href="/cart/">shopping cart</a> if you would like to start over =)</p>';
			echo '<p>Want to purchase multiple Custom Canvasses at the same time? Email us at <a href="mailto:info@onemissionfundraising.com">info@onemissionfundraising.com</a>.</p>';
			echo '</div>';
		}

	}

	add_action( '_fh_the_content', 'custom_content' );

	$holy_grail_class = 'one_full';
	include( get_template_directory() . '/templates/holy_grail_master.php' );

?>