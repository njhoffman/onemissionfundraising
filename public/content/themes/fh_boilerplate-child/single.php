<?php

// Setup Holy Grail class
$holy_grail_class = 'full';

// Load post into global $loc_post context
$loc_post = Timber::get_post();

//	Disable Breadcrumbs
add_action( '_fh_the_crumbs', '__return_false' );

// Disable on-page social sharing
add_action( '_fh_on_page_social_sharing', '__return_false' );

// Remove default title, sidebars, and content
add_action( '_fh_the_title', '__return_false' );

// Remove main content action and add custom profile main content
remove_action( '_fh_the_content', '_fh_content' );
add_action( '_fh_the_content', '_om_main_content' );


	//
	//	MAIN CONTENT
	//
	function _om_main_content() {

		global $loc_post;

	// Get Category
	$postcat = get_the_category( $post->ID );

	// Get featured image
	$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );

	?> 

<?php if( get_field('banner_image') ):?>
	<div class="container">
		<div class="row">
			<img class="img-responsive" src="<?php the_field('banner_image');?>" />
		</div>
	</div>
<?php endif; ?>
	<div class="blog-wrap container">
		<div class="row">
			<div class="col-sm-8 col-lg-9 col-sm-push-4 col-lg-push-3">
				<div class="blog-post-content">

					<h1 class="h2 text-center"><?php the_title(); ?></h1>
					<p class="subtitle text-center"><a href="/blog">Back to Blog</a> &nbsp; &middot; &nbsp; <?php the_date(); ?> &nbsp; &middot; &nbsp; <?php if ( ! empty( $postcat ) ) { echo esc_html( $postcat[0]->name ); } ?></p>

					<!-- <div class="featured-image space-3">
						<img src="<?php echo $feat_image ?>" class="img-responsive" />
					</div> -->

					<div class="post-content">
						<?php the_content(); ?>

						<div class="navigation-links text-center">
							<a class="btn btn-muted btn-sm" href="/blog/"><span class="fa fa-arrow-left"></span> Back to Articles</a>
						</div>
					</div>
					
				</div>
			</div>

			<?php include( get_stylesheet_directory() . '/templates/part_blog_sidebar.php' ); ?>
			
		</div>
	</div>
		

<?php
// end MAIN CONTENT
} 


//	Grab the main template
include( get_template_directory() . '/templates/holy_grail_master.php' );

?>