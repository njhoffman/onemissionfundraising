<?php
if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

require_once('config/globals.php');
require_once('config/config.php');
require_once('config/logger.php');

$fundraiser_types = array(
    'churches-youth-groups'         => 'Churches &amp; Youth Groups',
    'schools-fine-arts-athletics'   => 'Schools, Fine Arts, &amp; Athletics',
    'non-profit-organizations'      => 'Non-profit Organizations',
    'mission-groups-missionaries'   => 'Mission Groups &amp; Missionaries',
    'medical-causes'                => 'Medical Causes',
    'adoptive-parents'              => 'Adoptive Parents',
    'community-benefits'            => 'Community Benefits',
    'other-groups-individuals'      => 'Other Groups &amp; Individuals'
);

//
//  State lookup array
//  - used for donation scroller to show full state name
//
$states_lookup = array(
    'AL'=>'Alabama',
    'AK'=>'Alaska',
    'AZ'=>'Arizona',
    'AR'=>'Arkansas',
    'CA'=>'California',
    'CO'=>'Colorado',
    'CT'=>'Connecticut',
    'DE'=>'Delaware',
    'DC'=>'District of Columbia',
    'FL'=>'Florida',
    'GA'=>'Georgia',
    'HI'=>'Hawaii',
    'ID'=>'Idaho',
    'IL'=>'Illinois',
    'IN'=>'Indiana',
    'IA'=>'Iowa',
    'KS'=>'Kansas',
    'KY'=>'Kentucky',
    'LA'=>'Louisiana',
    'ME'=>'Maine',
    'MD'=>'Maryland',
    'MA'=>'Massachusetts',
    'MI'=>'Michigan',
    'MN'=>'Minnesota',
    'MS'=>'Mississippi',
    'MO'=>'Missouri',
    'MT'=>'Montana',
    'NE'=>'Nebraska',
    'NV'=>'Nevada',
    'NH'=>'New Hampshire',
    'NJ'=>'New Jersey',
    'NM'=>'New Mexico',
    'NY'=>'New York',
    'NC'=>'North Carolina',
    'ND'=>'North Dakota',
    'OH'=>'Ohio',
    'OK'=>'Oklahoma',
    'OR'=>'Oregon',
    'PA'=>'Pennsylvania',
    'RI'=>'Rhode Island',
    'SC'=>'South Carolina',
    'SD'=>'South Dakota',
    'TN'=>'Tennessee',
    'TX'=>'Texas',
    'UT'=>'Utah',
    'VT'=>'Vermont',
    'VA'=>'Virginia',
    'WA'=>'Washington',
    'WV'=>'West Virginia',
    'WI'=>'Wisconsin',
    'WY'=>'Wyoming',
);

add_action( 'after_setup_theme', function () {

    // Profile logo images
    add_image_size( 'om-profile-280', 280 );

    // Profile banner images
	add_image_size( 'om-profile-900', 900 );

	// woocommerce 3.0 gallery features support
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
});

//
//  Grab includes
//
include 'include/ajax-fundraiser.php';
include 'include/ajax-login.php';
include 'include/ajax-checkout.php';
include 'include/hubspot_api.php';
include 'include/donation_logic.php';
include 'include/subscription_logic.php';
include 'include/fundraisers/admin.php';
include 'include/fundraisers/create.php';
include 'include/fundraisers/news.php';
include 'include/fundraisers/supporters.php';
include 'include/fundraisers/sharing.php';
include 'include/fundraisers/promotion.php';
include 'include/fundraisers/profile-editor.php';
include 'include/set_shop_for.php';
include 'include/social_icons.php';
include 'include/user_contact_methods.php';
include 'include/custom_canvas_product_view.php';
include 'include/love_fund_notification.php';
include 'include/admin/custom_reports.php';
include 'include/admin/devsite_sync.php';
include 'include/admin/customize_menu.php';
include 'include/admin/assign_checkout_fields.php';
include 'include/admin/plugin_versions.php';

function tofloat($num) {
  $dotPos = strrpos($num, '.');
  $commaPos = strrpos($num, ',');
  $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
    ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);

  if (!$sep) {
    return floatval(preg_replace("/[^0-9]/", "", $num));
  }

  return floatval(
    preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
    preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
  );
}


//
//  Add "Be Kind!" Bird to sidebars
//
$om_bird_sidebar_append_flag = false;

function _om_be_kind_bird_sidebar_append() {
    global $om_bird_sidebar_append_flag;
    if( ! $om_bird_sidebar_append_flag ) {
		?>
		<img id="sidebar_bird" src="<?php echo get_stylesheet_directory_uri(); ?>/img/characters/bird_be_kind_no_shadow.png" alt="Be Kind!" title="Be Kind!">
		<?php
	 }
}
add_action( '_fh_sidebar_right_append', '_om_be_kind_bird_sidebar_append' );

///////////////////////////////////////////////////
//
//  Custom group products
//
///////////////////////////////////////////////////

add_filter( 'get_terms', '_fh_filter_category_parent', 10, 3 );

//
//  Filter out fundraiser org categories
//
function _fh_filter_category_parent( $terms, $taxonomies, $args ) {
	$new_terms = array();
	// If a product category and on the shop page
	if ( in_array( 'product_cat', $taxonomies ) && ! is_admin() ) {
		// Iterrate over terms and filter out anything below ORG_PRODUCTS_PARENT_CAT_ID
		foreach ( $terms as $key => $term ) {
			if( is_object( $term ) ) {
				// Filter both the parent category and children category to the parent
				$filtered_ids_arr = array( $term->term_id, $term->parent );
				if ( ! in_array( ORG_PRODUCTS_PARENT_CAT_ID, $filtered_ids_arr ) ) {
					$new_terms[] = $term;
				}
			} elseif ( is_string( $term ) ) {
				$new_terms[] = $term;
			}
		}
		$terms = $new_terms;
	}
	return $terms;
}

//
//  Global buttons at end of content
//
add_action( '_fh_footer_inside_prepend', function() {
    ?>
    <div id="footer_cta" >
        <?php
            include 'templates/part_cta_buttons.php';
        ?>
    </div>
    <?php
});

/**
 * Add the field to the checkout
 */

function _om_checkout_additional_fields() {
	$fund_name = stripcslashes($_COOKIE['fundraiser_name']);
	if ($fund_name) {
		echo '<div class="checkout_fundraiser_update">';
		woocommerce_form_field( 'supported_email_updates', array(
			'type'          => 'checkbox',
			'class'         => array('my-field-class form-row-wide'),
			'label'         => __('Would you like to receive email updates about the "' . $fund_name . '" Fund?')
		), '1'); // $checkout->get_value( 'supported_email_updates' ));
		echo '</div>';
	}
}

/**
 * Remove '(optional)' from label of address 2 field
 */

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
  $new_label = 'Apartment, suite, unit etc.';
  $fields['billing']['billing_address_2']['placeholder'] = $new_label;
  $fields['billing']['billing_address_2']['label'] = $new_label;
  $fields['shipping']['shipping_address_2']['placeholder'] = $new_label;
  $fields['shipping']['shipping_address_2']['label'] = $new_label;
  return $fields;
}
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

function _om_checkout_enroll_options() {
	echo '<div class="purchase-with-purpose">';
	echo '<p>';
	echo '<label>';
	echo '<input id="purchase-with-purpose-checkbox" type="checkbox" name="purchase_with_purpose" value="1" checked />';
	echo '<span>Join One Mission\'s VIP Email List:</span>';
	echo '</p>';
	echo '</label>';
	echo '<i class="purchase-with-purpose-details">Stay in the know and be the first to hear about new One Mission products, get exclusive deals, and maybe even some freebies!</i>';
	echo '</div>';
}

function _om_after_checkout_billing_form() {
	_om_checkout_additional_fields();
	_om_checkout_enroll_options();
}
add_action('woocommerce_after_checkout_billing_form', '_om_after_checkout_billing_form');

// remove tip add on field if custom is purchasing any products (only show for custom donations)
function _om_remove_tip_for_product_purchases() {
  if ( function_exists( 'wc_checkout_add_ons' ) ) {
    $prod_check = false;
    // check each cart item for our category
    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
      $product = $cart_item['data'];
      if ($product->name !== 'Custom Donation') {
        $prod_check = true;
        break;
      }
    }

    // if a product in the cart is in our category, remove the add-ons
    if ($prod_check) {
      // get the add-ons current position so we know where to remove them from
      $position = apply_filters( 'wc_checkout_add_ons_position', get_option( 'wc_checkout_add_ons_position', 'woocommerce_checkout_after_customer_details' ) );
      remove_action( $position, array( wc_checkout_add_ons()->get_frontend_instance(), 'render_add_ons' ), 20 );
    }
  }
}
add_action( 'woocommerce_before_checkout_form', '_om_remove_tip_for_product_purchases' );

//
//  Public donation attribution
//  - based on WooCommerce docs (http://bit.ly/W1sN91)
//
function _om_donation_attribution( $checkout ) {
	// Only show donation attribution if you're shopping for a fundraiser group
	if( _om_check_shopping_for() !== 'onemission_love_fund' ) {
		echo '<div id="donation_attribution">';
		woocommerce_form_field( 'public_donation_attrib',
			array(
				'type'          => 'text',
				'class'         => array('form-row-wide'),
				'label'         => __( 'Public Supporter List' ),
				'placeholder'   => __( 'Name / Company' ),
			),
			$checkout->get_value( 'public_donation_attrib' )
		);
		echo '<p><em>(Leave blank for anonymous. Fund leader can view all supporters.)</em></p>';
		echo '</div>';
	}
}

function _om_after_order_notes($checkout) {
	_om_donation_attribution($checkout);
}
add_action( 'woocommerce_after_order_notes', '_om_after_order_notes' );


/* Checkout tip field */
function _om_formatted_tip_label($label) {
	if (strpos($label, ' tip amount')) {
		return "<span class='tip-title'>Tip for One Mission</span><br />" .
			"<span class='tip-description'>" .
			"We gratefully accept tips to support our mission of helping others do good in the world. " .
			"Since we do not charge platform fees, your tip will help us continue to operate and grow. " .
			"</span>";
	}
	return $label;
}
add_filter('wc_checkout_add_ons_formatted_add_on_label', '_om_formatted_tip_label');

function _om_modify_checkout_add_ons_attributes($checkout_fields) {
	$tip_field_id = 2;
	$tip_field = 'wc_checkout_add_ons_' . $tip_field_id;
	if ( isset( $checkout_fields['add_ons'][$tip_field] ) ) {
		array_walk($checkout_fields['add_ons'][$tip_field]['options'],
			function(&$item, $key) {
				$item = preg_replace('/\d+% \((.*)\)/', "$1 - ($key%)", $item);
			}
		);
	}
	return $checkout_fields;
}
add_filter('woocommerce_checkout_fields', '_om_modify_checkout_add_ons_attributes', 20);
//
//  Add One Mission T&C's "I agree" to checkout process
//
function _om_checkout_terms_content() {
    echo '<div class="alert alert-warning text-center">';
    echo '<p>By placing this order you explicitly agree to One Mission Fundraising\'s <a href="/about-us/terms-conditions/" target="_blank">Terms and Conditions</a>.</p>';
    echo '</div>';
}

add_action( 'woocommerce_after_checkout_form', '_om_checkout_terms_content' );

//
//  Add donation total to order email notifications
//

function _om_add_to_order_email( WC_Order $order, $is_admin_email, $plain_text, $hide_sub_info = false ) {

    $order_meta = get_post_meta( $order->id );
    $append = '';
    if ('failed' != $order->status && 'pending' != $order->status){
		// Add donation amount
		if(!$hide_sub_info){
			if(
				isset( $order_meta['Total Donation Amount'] ) &&
				! empty( $order_meta['Total Donation Amount'] ) &&
				floatval( $order_meta['Total Donation Amount'][0] )) {

				$donation_amount = floatval( $order_meta['Total Donation Amount'][0] );
				// $append .= '<br><p><strong>Donated Total:</strong><br>$' . number_format( $donation_amount, 2 ) . '</p>';
			} else {
				if(isset ($order_meta['_order_total']) && floatval( $order_meta['_order_total'][0]) && $order_meta['_order_total'][0] > 0.00){
					$append .= '<br><p><strong>Donated Total:</strong><br>$105</p>';
				} else {
					// $append .= '<br><p><strong>Donated Total:</strong><br> Check the <a href="https://onemissionfundraising.com/my-account/" target="_blank">"My Account"</a> area for the donation total!</p>';
					// $append .= '<p><em>Note: Sometimes the donation total doesn\'t show immediately as it may take some time to process! (ex: subscriptions)</em></p>';
					}
				}
		}
		// Add fundraiser supported
		if( isset( $order_meta['Fundraiser Name'] ) && ! empty( $order_meta['Fundraiser Name'] ) && isset ($order_meta['_order_total']) && floatval( $order_meta['_order_total'][0]) && $order_meta['_order_total'][0] > 0.00 ) {

			// $append .= '<p><strong>Fundraiser Supported:</strong><br><a href="' . $order_meta['Fundraiser GUID'][0] . '" target="_blank">' . $order_meta['Fundraiser Name'][0] . '</a></p>';

			// Is there a fundraiser user id associated to this order?
			// om no longer wants thank you message unless it is a om love fund - njh 8/3/16
			if(
					isset( $order_meta['Fundraiser User ID'] ) &&
					! empty( $order_meta['Fundraiser User ID'] ) &&
					is_array( $order_meta['Fundraiser User ID'] ) &&
					isset( $order_meta['Fundraiser User ID'][0] ) &&
					$order_meta['Fundraiser User ID'][0]
			) {

		//         // Grab the fundraiser ID and fundraiser profile ID
		//         $fundraiser_id = (int) $order_meta['Fundraiser User ID'][0];
		//         $fundraiser_profile_id = get_user_meta( $fundraiser_id, 'fundraiser_profile_id', true );
		// if(!$hide_sub_info){
		//         // Get ACF field for fundraiser profile thanks
		//         $custom_thanks = get_field( 'thank_you_message', $fundraiser_profile_id );
		//
		//         // Ensure there is a thank you message before appending to email
		//         if( $custom_thanks && ! empty( $custom_thanks ) ) {
		//
		//             $append .= '<hr>';
		//             $append .= '<p><strong>Fundraiser Thank You Message:</strong><br>' . nl2br( $custom_thanks ) . '</p>';
		//         }
		//
		//         // If there's a 501c3 status on the fundraiser then add to receipt email
		//         $tax_status = get_field( 'enable_501c', $fundraiser_profile_id );
		//
		//         if( $tax_status && $tax_status == true ) {
		//
		//             $tax_message = '<strong>' . nl2br( get_field( '501c3_text', 'option' ) ) . '</strong>';
		//
		//             $append .= $tax_message;
		//         }
		// }

			} else {

					$append .= '<p>If you benefited the One Mission Love Fund when you really meant to benefit a specific fundraiser, just send us an email (<a href="mailto:info@onemissionfundraising.com">info@onemissionfundraising.com</a>) within 24 hours of the transaction and let us know and we’ll be happy to adjust the transaction to benefit your desired fundraiser.</p>';
			}
		}
    }
    echo $append;
}

add_action( 'woocommerce_email_after_order_table', '_om_add_to_order_email', 15, 4 );


function _om_change_processing_email_subject($subject, $order) {
	// override processing order email subject line to add fundraiser name
	global $woocommerce;
	$order_meta = get_post_meta( $order->id );
	$fundraiser = "";
	if( isset( $order_meta['Fundraiser Name'] ) && ! empty( $order_meta['Fundraiser Name'] ) && isset ($order_meta['_order_total']) && floatval( $order_meta['_order_total'][0]) && $order_meta['_order_total'][0] > 0.00 ) {
			$fundraiser = ' Supporting: ' . $order_meta['Fundraiser Name'][0];
	}

	$subject = 'Your One Mission Order #' . $order->get_order_number() . $fundraiser;
	return $subject;
}
add_filter( 'woocommerce_email_subject_customer_processing_order', '_om_change_processing_email_subject', 1, 2);


function _om_change_completed_email_subject($subject, $order) {
	global $woocommerce;
	$subject = 'Your One Mission Order #' . $order->get_order_number() . " is Complete";
	return $subject;
}
add_filter( 'woocommerce_email_subject_customer_completed_order', '_om_change_completed_email_subject', 1, 2);

function _om_change_processing_renewal_email_subject($subject, $order) {
	global $woocommerce;
	$subject = 'Your One Mission Renewal Order #' . $order->get_order_number() . " is being processed";
	return $subject;
}
add_filter( 'woocommerce_email_subject_customer_processing_renewal_order', '_om_change_processing_renewal_email_subject', 1, 2);

function _om_change_completed_renewal_email_subject($subject, $order) {
	global $woocommerce;
	$subject = 'Receipt for your completed subscription renewal order';
	return $subject;
}
add_filter( 'woocommerce_subscriptions_email_subject_customer_completed_renewal_order', '_om_change_completed_renewal_email_subject', 1, 2);


function _om_change_renewal_invoice_email_subject($subject, $order) {
	global $woocommerce;
	if ($order->status == 'failed') {
		$subject = 'Action Needed: Subscription Renewal Order #' . $order->get_order_number(). ' Failed';
	} else {
		$subject = 'Invoice for Renewal Order #' . $order->get_order_number() . ' ';
	}
	return $subject;
}
add_filter( 'woocommerce_subscriptions_email_subject_new_renewal_order', '_om_change_renewal_invoice_email_subject', 1, 2);
//
//  Login/logout redirects to WooCommerce my-account page
//

// On logout go to my-account page (WooCommerce themed login page)
add_action( 'wp_logout', function() {
    wp_redirect( get_permalink( OM_LOGOUT_PAGE_ID ) );
    exit();
});

// On login go to either wp admin (for administrators and fundraisers) or default my account page for customers
function _om_login_redirect( $redirect_to, $user ) {

    if ( isset( $user->roles ) && is_array( $user->roles ) ) {
        if ( in_array( 'administrator', $user->roles ) ) {
            return admin_url();
		  } else if ( in_array( 'fundraiser', $user->roles ) ) {
            return admin_url();
		  }
    }

    return $redirect_to;
}

add_filter( 'woocommerce_login_redirect', '_om_login_redirect', 100, 2 );


//
//  Add 501c3 status message if applicable to thank-you and account history
//  - woocommerce_thankyou, woocommerce_view_order
function _om_501c3_status_on_orders( $order_id ) {
	$order_meta = get_post_meta( $order_id );
	if( isset( $order_meta['Fundraiser ID'] ) ) {
		$fundraiser_profile_id = intval( $order_meta['Fundraiser ID'][0] );

		// If there's a 501c3 status on the fundraiser then add to receipt email
		$tax_status = get_field( 'enable_501c', $fundraiser_profile_id );
		if( $tax_status && $tax_status == true ) {
			$tax_message = nl2br( get_field( '501c3_text', 'option' ) );
			echo $tax_message;
		}
	}
}
add_action( 'woocommerce_thankyou', '_om_501c3_status_on_orders' );
add_action( 'woocommerce_view_order', '_om_501c3_status_on_orders' );

//
//  Add in admin notification for Custom Canvas if applicable
//
function _om_custom_canvas_admin_email( $order, $is_admin_email ) {
	if ( $is_admin_email ) {
		if( isset( $_COOKIE['cc_print_image'] ) && ! empty( $_COOKIE['cc_print_image'] ) ) {
			echo '<p><strong>Custom Canvas Image Asset:</strong> <a href="' .
				$_COOKIE['cc_print_image'] . '" target="_blank">' .
				$_COOKIE['cc_print_image'] . '</a></p>';
		}
	}
}

add_action( 'woocommerce_email_after_order_table', '_om_custom_canvas_admin_email', 15, 2 );

//
//  Add custom headers for OM to WooCommerce CSV export plugin
//  - http://docs.woothemes.com/document/woocommerce-customer-order-csv-export-developer-documentation/
//
function _om_csv_export_modify_column_headers( $column_headers ) {
    $new_headers = array(
        'fundraiser_profile_id' => 'Fundraiser ID',
        'fundraiser_name' => 'Fundraiser Name',
        'net_revenue_from_stripe' => 'Net Revenue From Stripe',
        'stripe_fee' => 'Stripe Fee',
        'stripe_payment_id' => 'Stripe Payment ID',
        'total_donation_amount' => 'Total Donation Amount'
    );

    return array_merge( $column_headers, $new_headers );
}
add_filter( 'wc_customer_order_csv_export_order_headers', '_om_csv_export_modify_column_headers' );

//
//  Add custom fields for OM to WooCommerce CSV export plugin
//  - http://docs.woothemes.com/document/woocommerce-customer-order-csv-export-developer-documentation/
//
function _om_csv_export_modify_row_data( $order_data, $order ) {
	$fundraiser_user = get_post_meta( $order->id, 'Fundraiser User ID');
	$fundraiser_user_id = (int) $fundraiser_user[0];
	$fundraiser_profile_id = get_user_meta( $fundraiser_user_id, 'fundraiser_profile_id', true );

    $custom_data = array(
        'fundraiser_profile_id' => $fundraiser_profile_id,
        'fundraiser_name' => get_post_meta( $order->id, 'Fundraiser Name', true ),
        'net_revenue_from_stripe' => get_post_meta( $order->id, 'Net Revenue From Stripe', true ),
        'stripe_fee' => get_post_meta( $order->id, 'Stripe Fee', true ),
        'stripe_payment_id' => get_post_meta( $order->id, 'Stripe Payment ID', true ),
        'total_donation_amount' => get_post_meta( $order->id, 'Total Donation Amount', true )
    );

    return array_merge( $order_data, $custom_data );
}
add_filter( 'wc_customer_order_csv_export_order_row', '_om_csv_export_modify_row_data', 10, 2 );

//
//  Override _fh_get_social_sharing to add Pin-it Pinterest button
//
function _fh_get_social_sharing( $link, $title, $class = '', $echo = true ) {

    $queried_obj = get_queried_object();

    global $woocommerce_content_enable;

    if(
        ! has_action( '_fh_on_page_social_sharing' ) &&
        function_exists( 'get_field' ) &&
        ( function_exists( 'is_shop' ) && ! is_shop() ) && // WooCommerce shop page failes on $queried_obj get_field lookup
        get_field( 'enable_social_sharing', $queried_obj ) &&
        FH_SOCIAL_SHARING_ENABLED
    ) {

        $title = urlencode( $title );

        ob_start();
        ?>
        <div id="social_share_btns">
            <span>Share This:</span>
            <ul>
					<li>
						<a href="http://twitter.com/home?status=<?php echo $title ?>+%28<?php echo $link ?>%29" class="btn twitter" rel="nofollow" target="_blank" title="Share on Twitter">
							<i class="fa fa-twitter"></i>
						</a>
					</li>
					<li>
						<a href="http://www.facebook.com/sharer.php?u=<?php echo $link ?>&title=<?php echo urldecode($title); ?>" class="btn facebook" rel="nofollow" target="_blank" title="Share on Facebook">
							<i class="fa fa-facebook"></i>
						</a>
					</li>
					<li>
						<a href="https://plus.google.com/share?url=<?php echo $link ?>"  class="btn google" rel="nofollow" target="_blank" title="Share on Google+">
							<i class="fa fa-google-plus"></i>
						</a>
					</li>
            </ul>
            <div id="pinit_btn">
                <a href="//www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark" ><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" /></a>
            </div>
        </div>
        <?php

        // Echo or return html
        if( $echo ) {

            echo ob_get_clean();
            return;
        }

        return ob_get_clean();

    } else {

        do_action( '_fh_on_page_social_sharing' );
    }

    return false;
}

//
//  Adding JS Enqueues
//  - Pinterest Pin-it button JS
//  - BS3 modal
//
function _om_custom_enqueue_js() {

    $child_vendor_js   = get_stylesheet_directory_uri() . '/js/vendor';
    $parent_vendor_js  = get_template_directory_uri() . '/js/vendor';

    //
    //  Pinterest Pin-it
    //  - Addition to social sharing
    //
    wp_enqueue_script( 'pinterest_pin_it', 'https://assets.pinterest.com/js/pinit.js', array(), '', true );

    //
    //  Bootstrap modal
    //  - On checkout for sharing take-over
    //
    wp_register_script( 'bs_modal', $parent_vendor_js . '/bootstrap/bootstrap/modal.js', array('jquery') );
    wp_enqueue_script( 'bs_modal' );

    //
    //  Bootstrap tabs
    //  - On fundraiser profiles
    //
    wp_register_script( 'bs_tabs', $parent_vendor_js . '/bootstrap/bootstrap/tab.js', array('jquery') );
    wp_enqueue_script( 'bs_tabs' );

    //
    //  jQuery Cookie
    //  - Used for dismissal of love fund fund shopping notification
    //
    wp_register_script( 'jquery_cookie', $child_vendor_js . '/jquery.cookie.js', array('jquery') );
    wp_enqueue_script( 'jquery_cookie' );

    //
    //  unslider
    //  - Used for sliders
    //
    wp_register_script( 'unslider', $child_vendor_js . '/unslider/unslider-min.js', array('jquery') );
    wp_enqueue_script( 'unslider' );

}
add_action( 'wp_enqueue_scripts', '_om_custom_enqueue_js' );


//  Utilize WooCommerce breadcrumbs if applicable
//  - Overriden breadcrumbs global in woocommerce/global/breadcrumb.php
//  - Filter exists in breadcrumb.php that looks for "ORG_PRODUCTS_PARENT_CAT_ID"
//    to filter out any featured organization products.
//  - Fixes org favorite products showing up as breadcrumb categorization
//    bug presented on 2014-08-07 13:21 <jmparks>
//
function _fh_get_crumbs() {

    if( FH_BREADCRUMBS_ENABLED ) {

        // Look for crumbs-overriding custom action
        if( has_action( '_fh_the_crumbs' ) ) {

            ob_start();
            echo do_action( '_fh_the_crumbs' );
            return ob_get_clean();
        }

        global $woocommerce_content_enable, $post;

        if( $woocommerce_content_enable ) {

            ob_start();

            if( function_exists('woocommerce_breadcrumb') ) {

                woocommerce_breadcrumb( array( 'delimiter'  => ' > ' ) );
            }

            return ob_get_clean();
        }

        return ( function_exists('yoast_breadcrumb') && !is_front_page() ) ? yoast_breadcrumb( '', '', false ) : '';
    }

    return false;
}

//
//  Remove WooThemes Updater Notice
//
remove_action( 'admin_notices', 'woothemes_updater_notice' );


//
//  Utility function to check who you're currently shopping for
//  - Returns "onemission love fund" if onemission love fund fund
//  - Returns fundraiser page ID if shopping for fundraiser
//
function _om_check_shopping_for() {

    if(
        isset( $_COOKIE['shopping_for_id'] )
        && is_numeric( $_COOKIE['shopping_for_id'] )
        && $_COOKIE['shopping_for_id']
    ) {

        // Return shopping for ID
        return $_COOKIE['shopping_for_id'];

    } else {

        return 'onemission_love_fund';
    }
}


function _om_donation_attribution_update_order_meta( $order_id ) {

    // Update order meta with donation attribution
    if ( ! empty( $_POST['public_donation_attrib'] ) ) {

        update_post_meta( $order_id, 'Public Donation Attribution', sanitize_text_field( $_POST['public_donation_attrib'] ) );
    }
}
add_action( 'woocommerce_checkout_update_order_meta', '_om_donation_attribution_update_order_meta' );


// Gets timezone difference between OneMission location timezone and server timezone
function _om_get_timezone_offset($remote_tz, $origin_tz = null) {
	if($origin_tz === null) {
		if(!is_string($origin_tz = date_default_timezone_get())) {
			return false; // A UTC timestamp was returned -- bail out!
		}
	}
	$origin_dtz = new DateTimeZone($origin_tz);
	$remote_dtz = new DateTimeZone($remote_tz);
	$origin_dt = new DateTime("now", $origin_dtz);
	$remote_dt = new DateTime("now", $remote_dtz);
	$offset = $origin_dtz->getOffset($origin_dt) - $remote_dtz->getOffset($remote_dt);
	return $offset;
}

//  Function to determine if fundraiser is closed
//
function _om_is_fundraiser_closed( $type, $start, $end ) {

	// Only check times if it's a timed fundraiser
	if( $type == 'timed' ) {


		$unix_start_date = strtotime( $start );
		// end date should be included in the range, add 24 hours
		$unix_end_date   = strtotime( $end ) + (24 * 3600);
		$unix_now        = time();

		// calculate difference between One Misssion timezone (central), the server time zone and account for it
		$tz_offset = _om_get_timezone_offset(date_default_timezone_get(), "America/Chicago");
		$unix_now = $unix_now + $tz_offset;

		// Check dates to ensure fundraiser is open
		return !( $unix_start_date <= $unix_now && $unix_now <= $unix_end_date );
	}

	// Fundraiser is open
	return false;
}

//
//  Utility function to limit words
//  - Used on Facebook sharing modal through order thankyou WooCommerce template
//  - Based on: http://bit.ly/ZL7Xgq
//
function _om_limit_words( $string, $word_limit )
{
    $words = explode( ' ', $string );
    return implode( ' ', array_splice( $words, 0, $word_limit ) );
}

//
//  Utility function to check users roles
//  - based on http://docs.appthemes.com/tutorials/wordpress-check-user-role-function/
//
function _om_check_user_role( $role, $user_id = null ) {

    if ( is_numeric( $user_id ) ) {

        $user = get_userdata( $user_id );

    } else {

        $user = wp_get_current_user();
    }

    if ( ! empty( $user ) ) {

        return in_array( $role, (array) $user->roles );
    }

    return false;
}



function _om_needs_shipping($needs_shipping) {
	global $woocommerce;

	// don't show shipping options if cart only contains custom donation (cash donation)
	$has_other_than_custom = false;
	if (isset($woocommerce) && isset($woocommerce->cart)) {
		foreach( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {
			$product = $values['data'];
			if ($product->name !== 'Custom Donation') {
				$has_other_than_custom = true;
			}
		}
	}
	return ($needs_shipping && $has_other_than_custom);
}
add_filter( 'woocommerce_cart_needs_shipping', '_om_needs_shipping', 10, 1 );


//
//  Filter gift cards from shipped products
//  - Also filters custom donations from shippable products
//
function _om_filter_giftcards_from_shipping_packages( $packages ) {

    if( ! empty( $packages ) ) {

        // Loop through all packages and remove products with gift-card category
        foreach( $packages as $key => $package ) {

            // Loop through the contents of each package
            foreach( $packages[$key]['contents'] as $product_hash => $product ) {

                // Grab product categories for each product in package
                $product_categories = wp_get_post_terms( $product['product_id'], 'product_cat' );

                if( $product_categories && ! empty( $product_categories ) ) {

                    // Loop through product categories and look for "gift-cards" slug
                    foreach ( $product_categories as $category ) {

                        if( $category->slug == 'gift-cards' ) {

                            // Unset product in package contents by product hash
                            unset( $packages[$key]['contents'][$product_hash] );
                        }
                    }
                }

                // Also remove the custom donation from shippable products
                if( FH_CUSTOM_DONATION_ID == $product['product_id'] ) {

                    unset( $packages[$key]['contents'][$product_hash] );
                }

                // Also remove subscriptions from shippable products
                if( $product['data'] instanceof WC_Product_Subscription ) {

                    unset( $packages[$key]['contents'][$product_hash] );
                }
            }
        }

        // Remove any packages that are now empty (no contents)
        foreach( $packages as $key => $package ) {

            if( empty( $packages[$key]['contents'] ) ) {

                unset( $packages[$key] );
            }
        }
    }

    return $packages;
}

add_filter( 'woocommerce_cart_shipping_packages', '_om_filter_giftcards_from_shipping_packages' );

function _om_woocommerce_cart_actions() {
  // echo '<button>POOP</button>';
}
add_action('woocommerce_cart_collaterals', '_om_woocommerce_cart_actions', 10, 2);

// Gravity Forms button -- add classes and reformat to <button>
add_filter( 'gform_submit_button', 'form_submit_button', 10, 2 );
function form_submit_button( $button, $form ) {

    return "<button class='button btn btn-primary btn-md' id='gform_submit_button_{$form['id']}'><span>Submit</span></button>";
}

// Gravity Forms Description -- and text after button
add_filter( 'gform_submit_button', 'add_paragraph_below_submit', 10, 2 );
function add_paragraph_below_submit( $button, $form ) {

    return $button .= "<p class='spam-msg'>" . $form['description'] . "</p>";
}


// add_action('gform_after_submission_12', 'after_submission', 10, 2);
// function after_submission( $entry, $form ) {
// 	GFCommon::log_debug ( 'gform_after_submission: entry => ' . print_r( $entry, true ) );
// }
//

/**
 *Reduce the strength requirement on the woocommerce password.
 *
 * Strength Settings
 * 3 = Strong (default)
 * 2 = Medium
 * 1 = Weak
 * 0 = Very Weak / Anything
 */
// function reduce_woocommerce_min_strength_requirement( $strength ) {
//     return 1;
// }
// add_filter( 'woocommerce_min_password_strength', 'reduce_woocommerce_min_strength_requirement' );

add_filter( 'wc_password_strength_meter_params', '_om_strength_meter_custom_strings' );
function _om_strength_meter_custom_strings( $data ) {
    $data_new = array(
		'min_password_strength' => apply_filters( 'woocommerce_min_password_strength', 1 ),
        'i18n_password_error'   => esc_attr__( '<span class="mr-red">Please enter a stronger password.</span>', 'woocommerce' ),
        'i18n_password_hint'    => esc_attr__( 'Your password must be <strong>at least 7 characters</strong> and contain a mix of <strong>UPPER</strong> and <strong>lowercase</strong> letters, <strong>numbers</strong>, and <strong>symbols</strong> (e.g., <strong> ! " ? $ % ^ & </strong>). Keep adding additional characters and/or variation until this prompt disappears—you cannot save changes until it is gone.', 'woocommerce' )
    );

    return array_merge( $data, $data_new );
}


// sort woocommerce shipping methods by cost
add_filter( 'woocommerce_package_rates' , 'patricks_sort_woocommerce_available_shipping_methods', 10, 2 );
function patricks_sort_woocommerce_available_shipping_methods( $rates, $package ) {
	//  if there are no rates don't do anything
	if ( ! $rates ) {
		return;
	}

	// get an array of prices
	$prices = array();
	foreach( $rates as $rate ) {
		$prices[] = $rate->cost;
	}

	// use the prices to sort the rates
	array_multisort( $prices, $rates );

	// return the rates
	return $rates;
}

// add custom fundraisers name field to woocommerce orders table

add_filter( 'manage_edit-shop_order_columns', '_om_add_fundraiser_column_to_order_table' );
function _om_add_fundraiser_column_to_order_table($columns){
    $new_columns = (is_array($columns)) ? $columns : array();
    unset( $new_columns['order_actions'] );

    // columns will be added before the actions column
    $new_columns['fundraiser_name'] = 'Fundraiser Name';

    $new_columns['order_actions'] = $columns['order_actions'];
    return $new_columns;
}

add_action( 'manage_shop_order_posts_custom_column', '_om_add_fundraiser_column_to_order_table_values', 2 );
function _om_add_fundraiser_column_to_order_table_values($column){
    global $post;
    $data = get_post_meta( $post->ID );

    if ( $column == 'fundraiser_name' ) {
        echo (isset($data['Fundraiser Name']) ? $data['Fundraiser Name'][0] : 'None');
    }
}

// make custom fundraisers name field sortable, doesn't work, have to setup custom queries
add_filter( "manage_edit-shop_order_sortable_columns", '_om_add_fundraiser_column_to_order_table_sort' );
function _om_add_fundraiser_column_to_order_table_sort( $columns ) {
	// $columns['fundraiser_name'] = 'fundraiser_name';
	return $columns;
}

// allow custom characters inside of wordpress usernames
add_filter ('sanitize_user', '_om_create_attendee_sanitize_user', 10, 3);
function _om_create_attendee_sanitize_user($username, $raw_username, $strict) {
	$allowed_symbols = "a-z0-9+ _.\-@"; //yes we allow whitespace which will be trimmed further down script
	// strip HTML Tags
	$username = wp_strip_all_tags ($raw_username);
	// remove accents
	$username = remove_accents ($username);
	// kill octets
	$username = preg_replace ('|%([a-fA-F0-9][a-fA-F0-9])|', '', $username);
	// kill entities
	$username = preg_replace ('/&.+?;/', '', $username);
	// allow + symbol
	$username = preg_replace ('|[^'.$allowed_symbols.']|iu', '', $username);
	$username = trim ($username);
	$username = preg_replace ('|\s+|', ' ', $username);
	return $username;
}

// custom character counting functionality for tiny mce's, looks for class character-count with data atribute
add_filter( 'tiny_mce_before_init', 'wpse24113_tiny_mce_before_init' );
function wpse24113_tiny_mce_before_init( $initArray ) {
    $initArray['setup'] = <<<JS
[function(ed) {
	 ed.onKeyUp.add(function(ed, e) {
		var ccEl = document.getElementsByClassName('character-count');
		if (ccEl && ccEl.length > 0) {
			var min = ccEl[0].getAttribute('data-count') ? ccEl[0].getAttribute('data-count') : 0;
			var total = e.currentTarget.innerText.trim().length;
			ccEl[0].setAttribute('data-currcount', total);
			ccEl[0].innerHTML =  total + ' characters (' + min + ' minimum)';
		}
	 });
}][0]
JS;
    return $initArray;
}

// if user can't edit other posts (i.e. fundraiser) only show their posts in the admin screen
function posts_for_current_author($query) {
	global $pagenow;

	if( 'edit.php' != $pagenow || !$query->is_admin )
		return $query;

	if( !current_user_can( 'edit_others_posts' ) ) {
		global $user_ID;
		$query->set('author', $user_ID );
	}
	return $query;
}
add_filter('pre_get_posts', 'posts_for_current_author');

// notify admin on page edit view if fundraiser is pending
add_action( 'admin_init', 'check_page_template' );
function check_page_template() {
	add_action( 'load-post.php', '_om_post_listing_page' );
}

function _om_post_listing_page() {
	global $HubSpotApi;
	$post_id = isset($_GET["post"]) ? $_GET["post"] : false;;
	$approve_fundraiser = isset($_GET["approve-fundraiser"]) ? true : false;;

	if (! $post_id) {
		return;
	}
	$post = get_post($post_id);
	$post_meta = get_post_meta($post_id);
	$author_id = $post->post_author;
	$author = get_userdata($author_id);
	$pending_approval = isset($post_meta['pending_approval']) ? $post_meta['pending_approval'][0] : false;
	if ($post->post_parent != OM_FUNDRAISER_PAGE_PARENT_ID) {
		return;
	}

	if ($approve_fundraiser) {
		 update_field('pending_approval', "", $post_id);
		 update_user_meta( $author_id, 'fund_creation_status', 'Approved');

		 // update hubspot contact fields
		 $hubspot_fields = array( array('property' => 'fund_creation_status', 'value' => 'Approved'));
		 $hs_update = $HubSpotApi->update_contact_by_email($author->user_email, $hubspot_fields);
		 wp_update_post( array(
			 'ID' => $post_id,
			 'post_status' => 'private'
		 ));
	} else if (!empty($pending_approval)) {
		add_action( 'admin_notices', '_om_fundraiser_pending_warning' );
	}
}

// return most recent child category of 'Featured Items' parent category
function _om_get_featured_items_category($parent_cat_ID) {
	$args = array(
		'hierarchical' => 1,
		'show_option_none' => '',
		'hide_empty' => 0,
		'parent' => $parent_cat_ID,
		'taxonomy' => 'product_cat'
	);
	$subcats = get_categories($args);
	// first result should be newest category
	return !empty($subcats) ? $subcats[0] : false;
}

// return most recent child category of 'Featured Items' parent category
function _om_get_featured_categories() {
	$cat = get_term(OM_FUNDRAISER_FEATURED_CATEGORIES_ID);
	$featured_cats = [];
	if (!empty($cat) && !empty($cat->description)) {
		$featured_ids = explode(',', $cat->description);
		foreach($featured_ids as $id) {
			$featured_cat = get_term($id);
			array_push($featured_cats, $featured_cat);
		}
	}
	return $featured_cats;
}

function _om_get_gsa_total($gsa_str) {
	global $post;
	$gsa_total = 0;
	if (empty($gsa_str) && isset($post->ID)) {
		$gsa_str = get_field('goal_status_adjustment', $post->ID);
	}

	$gsas = explode('|', $gsa_str);
	$gsas = array_filter($gsas);
	foreach ($gsas as $gsa) {
		$gsa_fields = explode('::', $gsa);
		if (count($gsa_fields) ==  3) {
			$gsa_total += floatval($gsa_fields[1]);
		} else if (!empty($gsa)) {
			$gsa_total += floatval($gsa);
		}
	}
	return $gsa_total;
}


/**
 * Only allow one payment plan purchase (Subscription) at a time
 */

function _om_block_multiple_subscriptions( $valid, $product_id, $quantity ) {
	global $woocommerce;

	// Get the current product
	$current_product = wc_get_product( $product_id );

	if(($current_product instanceof WC_Product_Subscription ||
		$current_product instanceof WC_Product_Variable_Subscription) && $quantity > 1) {
		wc_add_notice( "We are sorry but only one subscription account can be purchased at a time." );
		return false;
	}

	// See if the current product user's are viewing is a subscription product
	if ( $current_product instanceof WC_Product_Subscription ||
		$current_product instanceof WC_Product_Variable_Subscription ) {
		foreach( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {
			// Loop through all products in the cart
			$_product = $values['data'];
			// Check if current item is a subscription type
			if( $_product instanceof WC_Product_Subscription || $_product instanceof WC_Product_Subscription_Variation ) {
				// Display a notice and cancel the addition of item to cart
				wc_add_notice( "Only one payment plan can be purchased." );
				return false;
			}
		}
	}

	// check if user already subscribed
	$user_info = wp_get_current_user();
	$has_sub = wcs_user_has_subscription( '', '', 'active' );
	if ($user_info instanceof WP_User && $has_sub) {
		$sub_name = 'unknown';
		$sub = array_pop(wcs_get_users_subscriptions());
		foreach ($sub->get_items() as $item_id => $item) {
			$sub_name = $item['name'];
		}
		wc_add_notice( "You already have an active subscription for \"$sub_name\"");
		// return false;
	}
	return $valid;
}
add_filter( 'woocommerce_add_to_cart_validation', '_om_block_multiple_subscriptions', 10, 3 );

function _om_block_multiple_subscriptions_update($valid, $product_id, $values, $quantity) {
	return _om_block_multiple_subscriptions($valid, $product_id, $quantity);
}
add_filter( 'woocommerce_update_cart_validation', '_om_block_multiple_subscriptions_update', 10, 4 );


function _om_get_total_donation_amount($profile_id) {
	$user_id = get_current_user_id();
	$profile_id = isset($profile_id) ? $profile_id : get_user_meta( $user_id, 'fundraiser_profile_id' )[0];
	$fundraiser_post = get_page($profile_id);
	// Look up orders via $loc_post->post_author (Fundraiser User ID)
	$post_status = array( 'wc-processing', 'wc-completed' );

	// Setup args for custom orders query
	$order_args = array(
		'posts_per_page' => -1,
		'post_type' =>      'shop_order',
		'post_status' =>    $post_status,
		'orderby' =>        'post_date',
		'order' =>          'DESC',
		'meta_query' => array(
			array(
				'key' =>   'Fundraiser User ID',
				'value' => $fundraiser_post->post_author
			)
		)
	);

	$fundraiser_goal_total_from_orders = 0;

	// Run orders query
	$orders = new WP_Query( $order_args );
	if( !empty( $orders->posts ) ) {
		$i = 0;
		// Itterate over orders and tally up total donation amount
		foreach( $orders->posts as $order ) {
			$order_id         = $order->ID;
			$fields           = get_post_custom( $order->ID );
      $fundraiser_goal_total_from_orders += isset($fields['Total Donation Amount'])
        ? (float) str_replace(",", "", $fields['Total Donation Amount'][0]) : 0;
			$i++;
		}
	}
	return $fundraiser_goal_total_from_orders;
}

// output featured image for blog posts
function _om_blog_content_prepend() {
	global $post;

	if ($post->post_type === 'post') {
		$featured_image = get_the_post_thumbnail($post->id, 'large', 'style=max-width:100%;height:auto');
		$profile_id = get_user_meta( $post->post_author, 'fundraiser_profile_id' )[0];
		$fundraiser_post = get_page($profile_id);
		$fundraiser_url = get_permalink($profile_id);
		echo "<a href='" . $fundraiser_url . "'>" . $featured_image . "</a>";
	}
}
// add_action('_fh_content_prepend', '_om_blog_content_prepend');

// output social icons for blog posts, overrides template_functions
function _fh_display_post_meta() {
	global $post;
   $html         = '';
   $time         = get_the_time( 'Y-m-d' );
   $humnan_time  = get_the_date() . ' ' . get_the_time();
   $display_name = get_the_author_meta( 'display_name', $post->post_author );

	$share_link = get_permalink( $post->ID ) . '?shopfor=' . $post->ID;

	// Conditionally display author information
   $author = ( FH_DISPLAY_AUTHOR_INFO ) ?
      '<span class="author_right"><span class="fa fa-user"></span>&nbsp; <a href="' . get_author_posts_url( $post->post_author ) . '">' . $display_name . '</a></span>' :
      '';

	$social =
		'<div class="icon_buttons">' .
			'<a class="btn-facebook" ' .
				'href="http://www.facebook.com/sharer.php?u=' . urlencode($share_link) .  '&title=' . urlencode( $post->post_title ) . '" ' .
				'class="btn facebook" rel="nofollow" target="_blank" title="Share on Facebook">' .
				'<span class="fa fa-facebook"></span>' .
			'</a>' .
			'<a class="btn-twitter" ' .
				'href="http://twitter.com/home?status=' . $share_link  . '+%28' . urlencode( $post->post_title ) . '%29" ' .
				'class="btn twitter" rel="nofollow" target="_blank" title="Share on Twitter"> ' .
				'<span class="fa fa-twitter"></span>' .
			'</a>' .
			'<a class="btn-google" class="btn google" rel="nofollow" title="Email a Friend" ' .
				'href=\'mailto:?subject=Check out this fundraiser for ' . $post->post_title .
				'&body=Check out the fundraiser here: \' + ' .  $share_link .   '\'">' .
				'<span class="fa fa-envelope"></span>' .
			'</a>' .
		'</div>';


   // Article meta/comments sub-header
   $html .= '<div class="article_meta clearfix">' . "\n";

   // Date/time, byline for article
   $html .= "\t" . '<time datetime="' . $time . '"><span class="fa fa-calendar-o"></span>&nbsp; ' . $humnan_time . '</time> ' . $author . $social . "\n";

   // End article sub-header
   $html .= '</div>' . "\n";

   $return = apply_filters( '_fh_display_post_meta', $html, $post );

   // echo rendered html
   return $return;

}

// append fundraiser name to facebook title if viewing a product
function _om_head_title_filter($title) {
   global $post;

   $shopfor = !empty($_GET['shopfor']) ? $_GET['shopfor']
      : (!empty($_COOKIE['shopping_for_id']) ? $_COOKIE['shopping_for_id'] : false);
   if ($shopfor && isset($post->post_type) && $post->post_type === 'product') {
      $fundraiser = get_post($shopfor);
      if (!empty($fundraiser) && !empty($fundraiser->post_title)) {
         $title = 'Supporting ' . $fundraiser->post_title . ' - ' . $title;
      }
   }
   return $title;
}
add_filter('wpseo_title', '_om_head_title_filter', 20);

// remove --Wordpress from admin page titles
function _om_admin_page_title($admin_title, $title) {
	return str_replace(' &#8212; WordPress', '', $admin_title);
}
add_filter('admin_title', '_om_admin_page_title', 10, 2);


// extra profile fields to track fundraiser progress
function extra_user_profile_fields( $user ) { ?>
<style type="text/css">
	.completed, .incomplete {
		padding-left: 15px;
	}
	.incomplete:before {
		content: '\00d7';
		margin-left: -2px;
		width: 15px;
		display: inline-block;
		color: red;
	}
	.completed:before {
    content: '\2713';
    margin-right: 2px;
    margin-left: -2px;
    width: 15px;
    display: inline-block;
    color: #29ad67;
	}
</style>
    <h3><?php _e("Fundraiser Status", "blank"); ?></h3>

    <table class="form-table">
    <tr>
        <th><label for="fund_creation_status"><?php _e("Fund Creation Status"); ?></label></th>
        <td>
            <input type="text" disabled name="fund_creation_status" id="fund_creation_status" value="<?php echo esc_attr( get_the_author_meta( 'fund_creation_status', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Either Pre-Fund, In Progress, Completed, Pending, or Approved."); ?></span>
        </td>
    </tr>
    <tr>
        <th><label for="fund_creation_progress"><?php _e("Fund Creation Progress"); ?></label></th>
        <td>
			  <ul>
<?php
	// TODO: define as a constant
	$steps = array(
		1 => 'Basic Information',
		2 => 'Your Goal & Timeline',
		3 => 'Tell Your Story',
		4 => 'Add Images/Video',
		5 => 'Say "Thank You"',
		6 => 'Contact Information',
		7 => 'Your Social Links',
		8 => 'Game Changers',
		9 => 'Ready to Launch'
	);
	foreach(get_the_author_meta('fund_creation_progress', $user->ID) as $fund_step => $fund_status) {
		echo "<li class ='" . ($fund_status === false ? 'incomplete' : 'completed') . "'>" .  $steps[$fund_step] . "</li>";
	}
?>
				</ul>
        </td>
    </tr>
    </table>
<?php }

add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );



function save_extra_user_profile_fields( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) ) {
        return false;
    }
    update_user_meta( $user_id, 'fund_creation_status', $_POST['fund_creation_status'] );
    update_user_meta( $user_id, 'fund_creation_progress', $_POST['fund_creation_progress'] );
}
add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );

function _om_fundraiser_pending_warning() {
	global $post;
	$post_meta = get_post_meta($post->ID);
	$pending_approval = $post_meta['pending_approval'][0];
	$pending_date = date_create($pending_approval);
	$current_time = date_create();
	$pending_formatted = '';
	if ($pending_date) {
		$pending_interval = date_diff($current_time, $pending_date);
		$pending_formatted = $pending_interval->format("%a days, %h hours and %i minutes");
	}
?>
	<style type="text/css">
		.fundraiser-pending span.dashicons {
			color: orange;
		   font-size: 2.0em;
			width: 25px;
			height: 25px;
			vertical-align: middle;
		}
		.fundraiser-pending span.text {
	 	  margin-left: 5px;
		  font-size: 1.2em;
			vertical-align: middle;
		}
		.fundraiser-pending button.approve-fundraiser {
			margin-left: 15px;
			vertical-align: middle;
		}
	</style>
	<div class="fundraiser-pending notice notice-warning">
	<h4>
		<span class="dashicons dashicons-warning"></span>
		<span class="text">This fundraiser has been pending for <?php echo $pending_formatted; ?>.</span>
		<button onclick="window.location.href += '&approve-fundraiser';"
			class="button button-secondary approve-fundraiser">Approve</button>
	</h4>
	</div>
<?php
}


// join posts and postmeta tables
function cf_search_join( $join ) {
    global $wpdb;
    if ( is_search() ) {
        $join .=' LEFT JOIN '. $wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }
    return $join;
}
add_filter('posts_join', 'cf_search_join' );

// modify search query posts where
function cf_search_where( $where ) {
    global $pagenow, $wpdb;
    if ( is_search() ) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
    }
    return $where;
}
add_filter( 'posts_where', 'cf_search_where' );

// prevent duplicates
function cf_search_distinct( $where ) {
    global $wpdb;
    if ( is_search() ) {
        return "DISTINCT";
    }
    return $where;
}
add_filter( 'posts_distinct', 'cf_search_distinct' );

// hide related products
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

// fix "failed to send buffer of zlib output compression" error
remove_action( 'shutdown', 'wp_ob_end_flush_all', 1 );

DEFINE( 'CHILD_THEME_LOADED', TRUE );

