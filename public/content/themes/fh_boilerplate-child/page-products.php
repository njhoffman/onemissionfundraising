<?php

	function custom_content() {

		include 'templates/part_product_categories.php';
	}

	add_action( '_fh_the_content', 'custom_content' );

	$holy_grail_class = 'one_full';
	include( get_template_directory() . '/templates/holy_grail_master.php' );

?>