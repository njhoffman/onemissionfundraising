<?php

/*
    Template Name: 3 column, sidebars right (A left, B right)
*/

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

$holy_grail_class = 'three_right_1';
include( 'holy_grail_master.php');

//
//	╔═══════════════════════════════════╗
//	║ © Copyright 2014 Folkhack Studios ║
//	╚═══════════════════════════════════╝
//

?>