<?php

/*
    Template Name: 2 column, B left
*/

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

$holy_grail_class = 'two_left_2';
include( 'holy_grail_master.php');

//
//	╔═══════════════════════════════════╗
//	║ © Copyright 2014 Folkhack Studios ║
//	╚═══════════════════════════════════╝
//

?>