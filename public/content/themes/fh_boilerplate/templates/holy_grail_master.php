<?php

/**
 *
 * Holy Grail master template hnadling
 * - Final step before Timber render
 *
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

Utils::include_part( array( 'templates/part_html_head', 'templates/part_header' ) );

do_action( '_fh_before_content' );

// Load post into global $post context
Timber::get_post();

// Short circuit sharing ACF check if non-ACF-able
$override_acf_sharing = false;

if(
    FH_SOCIAL_SHARING_ENABLED &&
    ( FH_SOCIAL_SHARE_ON_DATE && is_date() ) ||
    ( FH_SOCIAL_SHARE_ON_SEARCH && is_search() ) ||
    ( FH_SOCIAL_SHARE_ON_BLOG_INDEX && is_home() ) ||
    ( FH_SOCIAL_SHARE_ON_AUTHORS && is_author() ) ||
    ( function_exists( 'is_shop' ) && FH_SOCIAL_SHARE_ON_MAIN_SHOP && is_shop() )
) {

    $override_acf_sharing = true;
}

// Load appropriate $data elements for Timber rendering
$page_arr                    = _fh_get_page_data();
$data                        = Timber::get_context();
$data['template_code_class'] = $holy_grail_class;
$data['crumbs']              = $page_arr['crumbs'];
$data['page_title']          = $page_arr['page_title'];
$data['page_content']        = $page_arr['page_content'];
$data['warn_overlay']        = isset($_GET['warn']) ? 'warn_overlay' : '';


// Posts have their own sharing handling built in
if(
    FH_SOCIAL_SHARING_ENABLED &&
    ( ! is_single() || ( function_exists( 'is_product' ) && is_product() ) ) &&
    (
        // Ensure WooCommerce is not loaded OR that we're not on cart/checkout/account page
        ! function_exists( 'is_woocommerce' ) ||
        (
            ( function_exists( 'is_cart' ) && ! is_cart() ) &&
            ( function_exists( 'is_checkout' ) && ! is_checkout() ) &&
            ( function_exists( 'is_account_page' ) && ! is_account_page() )
        )
    )
) {

    if(
        ( FH_SOCIAL_SHARE_ON_TAXONOMY && ( is_category() || is_tag() ) ) ||
        ( FH_SOCIAL_SHARE_ON_DATE && is_date() ) ||
        ( FH_SOCIAL_SHARE_ON_PAGES && is_page() ) ||
        ( FH_SOCIAL_SHARE_ON_SEARCH && is_search() ) ||
        ( FH_SOCIAL_SHARE_ON_BLOG_INDEX && is_home() ) ||
        ( FH_SOCIAL_SHARE_ON_AUTHORS && is_author() ) ||
        ( function_exists( 'is_shop' ) && FH_SOCIAL_SHARE_ON_MAIN_SHOP && is_shop() ) ||
        ( function_exists( 'is_product_category' ) && FH_SOCIAL_SHARE_ON_SHOP_TAXONOMY && is_product_category() ) ||
        ( function_exists( 'is_product_tag' ) && FH_SOCIAL_SHARE_ON_SHOP_TAXONOMY && is_product_tag() ) ||
        ( function_exists( 'is_product' ) && FH_SOCIAL_SHARE_ON_PRODUCTS && is_product() )
    ) {

        $data['share_page'] = $page_arr['share_page'];
    }
}

// Sidebar conditionals
switch( $holy_grail_class ) {

    // Only sidebar right enabled
    case 'two_right_1':
    case 'two_right_2':
        $data['sidebar_right_segments'] = $page_arr['sidebars_arr']['sidebar_right_segments'];
        break;

    // Only sidebar left enabled
    case 'two_left_1':
    case 'two_left_2':
        $data['sidebar_left_segments'] = $page_arr['sidebars_arr']['sidebar_left_segments'];
        break;

    // Both sidebars enabled
    case 'three_left_1':
    case 'three_left_2':
    case 'three_right_1':
    case 'three_right_2':
    case 'three_wrapped_1':
    case 'three_wrapped_2':
        $data['sidebar_right_segments'] = $page_arr['sidebars_arr']['sidebar_right_segments'];
        $data['sidebar_left_segments'] = $page_arr['sidebars_arr']['sidebar_left_segments'];
        break;
}

// Render the Timber view
Timber::render('fh-holy-grail.twig', $data);

do_action( '_fh_after_content' );

Utils::include_part( array( 'templates/part_footer','templates/part_html_foot' ));

?>
