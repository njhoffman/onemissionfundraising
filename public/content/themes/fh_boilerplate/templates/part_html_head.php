<?php

/**
 *
 * Default Boilerplate HTML Header Include
 *
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

<?php if( FH_EASTER_EGG ): ?>
<!-- HEY! Stop looking up my skirt ;) -->
<?php endif; ?>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<?php

	//
	// Logic to test for empty title and strip delimiter from front of string
	//
	ob_start();
	wp_title( FH_TITLE_DELIMITER );
	$wp_title = ob_get_clean();

	if( $wp_title[0] == FH_TITLE_DELIMITER ) {

		$wp_title = trim( substr( $wp_title, 1 ) );
	}
?>

<title><?php echo $wp_title; ?></title>

<!-- Responsive Viewport -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Author -->
<meta name="author" content="<?php echo FH_AUTHOR_META; ?>">

<!-- Pingback -->
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<!-- Anchor for "Back to Top" buttons -->
<a id="_fh_top"></a>

<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->