<?php

/**
 *
 * MixItUp/Masonry/BS3 Carousel Gallery Support Content Include
 *
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

do_action( 'pre_blog_article' );

// Get gallery type
$gallery_type = ( get_field('gallery_type') ) ? get_field('gallery_type') : 'Masonry One Column Width';

if( $gallery_type !== 'No Gallery' && $images = get_field('gallery') ) {

	switch( $gallery_type ) {

		case 'Masonry One Column Width':

			// Generate Masonry Gallery (1 col)
			$gallery_html = _fh_masonry_gallery( $images );

			break;

		case 'Masonry Two Column Widths':

			// Generate Masonry Gallery (2 col)
			$gallery_html = _fh_masonry_gallery( $images, true );

			break;

		case 'MixItUp':

			// Generate MixItUp Gallery
			$gallery_html = _fh_mixitup_gallery( $images );

			break;

		case 'Carousel':

			// Generate Carousel Gallery w/Options
			$carousel_enable_controls    = get_field('display_carousel_controls');
			$carousel_enable_dots        = get_field('display_carousel_indicators');
			$carousel_enable_caption     = get_field('display_carousel_image_captions');
			$carousel_enable_image_links = get_field('enable_carousel_image_links');
			$carousel_slide_duration     = get_field('carousel_slide_duration');

			$gallery_html = _fh_bs3carousel_gallery(
				$images,
				$carousel_enable_controls,
				$carousel_enable_caption,
				$carousel_enable_dots,
				$carousel_enable_image_links,
				$carousel_slide_duration
			);

			break;

		default:
			$gallery_html = 'There was an issue in loading this gallery.';
	}

	$order = get_field( 'gallery_order' );
	$gallery_title = get_field( 'gallery_title' );

	// Get the content before to test for empty content
	ob_start();
	the_content();
	$gallery_content = ob_get_clean();

	if( $order == 'Gallery Before Content' ) {

		// Gallery before content

		// Display gallery title
		echo ( ! empty( $gallery_title ) ) ? '<h2 class="text-center">' . $gallery_title . '</h2>' : '';

		// Display gallery
		echo ( ! empty( $gallery_html ) ) ? '<div class="gallery_wrap">' . $gallery_html . '</div>' : '';

		if( ! empty( $gallery_content ) ) {

			// Display post content
			echo '<hr>';
			_fh_content();
		}

	} else {

		// Gallery after content

		if( ! empty( $gallery_content ) ) {

			// Display post content
			_fh_content();
			echo '<hr>';
		}

		// Display gallery title
		echo ( ! empty( $gallery_title ) ) ? '<h2 class="text-center">' . $gallery_title . '</h2>' : '';

		// Display gallery
		echo ( ! empty( $gallery_html ) ) ? '<div class="gallery_wrap">' . $gallery_html . '</div>' : '';
	}

} else {

	// Just display the_content as normal (ie: "No Gallery")
	_fh_content();
}

do_action( 'post_blog_article' );

?>