<?php

/*
	Template Name: 1 column full-width content
*/

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

$holy_grail_class = 'one_full';
include( 'holy_grail_master.php');

//
//	╔═══════════════════════════════════╗
//	║ © Copyright 2014 Folkhack Studios ║
//	╚═══════════════════════════════════╝
//

?>