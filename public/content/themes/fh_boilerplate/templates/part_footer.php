<?php

/**
 *
 * Default Boilerplate Footer Include
 *
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

do_action( '_fh_footer_prepend' );

?>

	<noscript class="alert alert-danger">
        <strong>Uh oh!</strong> Looks like you have JavaScript disabled. To use all of this site's features you must enable JavaScript! This is just a friendly warning =)
    </noscript>

	<footer>

		<?php do_action( '_fh_footer_inside_prepend' ); ?>

		<?php

			if( FH_FOOTER_WIDGETS_NUM ) {

				echo '<div id="foot_widgets" class="row">' . "\n";

				for( $i = 1; $i <= FH_FOOTER_WIDGETS_NUM; $i++ ) {

					echo '<div class="col-sm-' . FH_FOOTER_WIDGETS_BS_COL . '">' . "\n";
					echo '<div class="' . FH_FOOTER_WIDGETS_CLASSES . '">' . "\n";

					echo _fh_get_sidebar_html( FH_FOOTER_WIDGETS_ID . '_' . $i );

					echo '</div>' . "\n";
					echo '</div>' . "\n";
				}

				echo '</div>' . "\n";
			}

		?>

		<div class="bottom text-center">
			<?php do_action( '_fh_footer_cw_prepend' ); ?>

			<?php
				if( has_action( '_fh_footer_cw_content' ) ):

					do_action( '_fh_footer_cw_content' );

				else:

					$company_name = get_bloginfo( 'name' );

					// If last character of company name is . then do not show period in footer
					$company_name_period = ( mb_substr( $company_name, -1 ) == '.' ) ? '' : '.';
			?>

				<p>&copy; <?php echo date('Y'); ?> <a href="<?php echo home_url(); ?>"><?php echo $company_name; ?></a><?php echo $company_name_period; ?> All rights reserved.</p>

			<?php endif; ?>

			<?php do_action( '_fh_footer_cw_append' ); ?>
		</div>

		<?php do_action( '_fh_footer_inside_append' ); ?>

	</footer>
</div>

<?php do_action( '_fh_footer_append' ); ?>