<?php

/**
 *
 * Default Boilerplate HTML Footer Include
 *
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

?>

<a id="fh_backtotop_btn" class="smooth_scroll" href="#fh_top"><img title="Back to top" src="<?php echo get_template_directory_uri(); ?>/img/actions/top.png" /><span class="sr-only">Back to top</span></a>

<?php

	// Call WordPress footer function
	// Very important that this gets called as this is an important hook for many plugins (like Disqus, Yoast, etc.)
	wp_footer();
?>

</body>
</html>