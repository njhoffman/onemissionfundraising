<?php

/**
 *
 * Default Boilerplate Header Include
 *
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

do_action( '_fh_header_prepend' );

?>
<div class="body_wrap">

    <?php do_action( '_fh_header_prepend_in_wrap' ); ?>

	<header class="page_header">

		<div class="vertical_align_wrap">

			<div class="left">
				<h1><a href="<?php echo home_url(); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" title="<?php bloginfo( 'name' ); ?>" alt="<?php bloginfo( 'name' ); ?>" /></a></h1>

                <!-- Information shown when printing document -->
                <div id="print_information" class="hidden">

                    <hr class="small">

                    <div class="clearfix">

                        <table id="print_info_table">
                            <tr>
                                <td>
                                    <h4>Printed Web Document Information:</h4>
                                    <p>
                                        <strong>Base Website URL</strong> - <?php echo home_url(); ?><br>
                                        <strong>Page URL</strong> - <span class="doc_url"></span><br>
                                        <strong>Rendered Date</strong> - <?php echo date( 'F j, Y, g:i a' ); ?>
                                    </p>
                                </td>
                                <td class="text-right">
                                    <canvas id="qrcode"></canvas>
                                </td>
                            </tr>
                        </table>

                    </div>

                    <hr class="small">

                </div>

			</div>

			<div class="right">
                <div id="head_top_nav" class="clearfix">
                    <nav>
                        <?php
                            wp_nav_menu(array(
                                'menu'            => 'top-nav',
                                'theme_location'  => 'top-nav',
                                'depth'           => 2,
                                'container'       => 'div',
                                'container_class' => 'menu-top',
                                'container_id'    => 'bs-top-nav-1',
                                'menu_class'      => 'menu nav nav-pills',
                                'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                                'walker'          => new wp_bootstrap_navwalker()
                            ));

                        ?>
                    </nav>
                </div>
                <?php if( FH_SITE_OPTION_DRIVEN_SOCIALS ): ?>
    				<div id="head_socials">
    		          	<?php _fh_social_icons(); ?>
    		        </div>
                <?php endif; ?>
                <div id="head_search">
                    <?php echo get_search_form(); ?>
                </div>
			</div>

		</div>

        <!-- When navigation is affixed keep the same height element on the page in it's place -->
        <div id="nav_affix_spacer">

            <div id="nav_affix">

                <div class="nav_affix_side_gutters">

            		<nav id="nav_header" class="navbar navbar-default" role="navigation">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-main-nav-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="<?php echo home_url(); ?>">Home</a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <?php

                            //
                            //  Append a WooCommerce simple shopping cart button if applicable
                            //
                            $woo_cart = ( function_exists( '_fh_woo_simple_cart_link_html' ) ) ?
                                '<div id="navbar_cart" class="navbar-right">' . _fh_woo_simple_cart_link_html() . '</div>' :
                                '';

                            wp_nav_menu(array(
                                'menu'            => 'main-nav',
                                'theme_location'  => 'main-nav',
                                'depth'           => 2,
                                'container'       => 'div',
                                'container_class' => 'collapse navbar-collapse menu-header',
                                'container_id'    => 'bs-main-nav-collapse-1',
                                'menu_class'      => 'menu nav navbar-nav',
                                'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                                'walker'          => new wp_bootstrap_navwalker(),
                                'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>' . $woo_cart
                            ));
                        ?>

                    </nav>

                </div>

            </div>

        </div>

    </header>

    <?php do_action( '_fh_header_append_in_wrap' ); ?>