<?php

/*
    Template Name: Contact Us
*/

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

function _fh_load_contact_gmaps() {

    $location       = get_field( 'location' );
    $zoom_level     = get_field( 'zoom_level' );
    $marker_title   = get_field( 'marker_title' );
    $marker_content = trim(preg_replace( '/\n/', '', get_field( 'marker_content' ) ));

    if( $location && $zoom_level && $marker_title && $marker_content ) {

        wp_enqueue_script( 'google-maps', '//maps.googleapis.com/maps/api/js?&sensor=false', array(), '3', true );
        wp_register_script( 'contact_us_js', get_template_directory_uri() . '/js/' . 'contact.js', array( 'jquery', 'google-maps' ) );

        $contact_vars_expose = array(
            'lat'            => $location['lat'],
            'lng'            => $location['lng'],
            'zoom_level'     => $zoom_level,
            'marker_title'   => $marker_title,
            'marker_content' => $marker_content
        );

        wp_localize_script( 'contact_us_js', 'contact_vars', $contact_vars_expose );
        wp_enqueue_script( 'contact_us_js' );
    }
}

//
//  Load JS for Google Maps
//
if( FH_CONTACT_GMAPS_ENABLED ) {

    add_action( 'wp_enqueue_scripts', '_fh_load_contact_gmaps' );
}


function _fh_contact_sidebar() {

    // Get appropriate fields from ACF
    $email_address          = get_field( 'email_address' );
    $phone_number           = get_field( 'phone_number' );
    $fax_number             = get_field( 'fax_number' );
    $text_address           = strip_tags( get_field( 'text_address' ), '<a><br>' );
    $location               = get_field( 'location' );
    $driving_directions_url = get_field( 'driving_directions_url' );


    //
    //  Email/phone/address information
    //
    if(
        ( FH_CONTACT_EMAIL_ADDR_ENABLED && $email_address ) ||
        ( FH_CONTACT_PHONE_NUM_ENABLED && $phone_number ) ||
        ( FH_CONTACT_FAX_NUM_ENABLED && $fax_number ) ||
        ( FH_CONTACT_TEXT_ADDR_ENABLED && $text_address )
    ):
    ?>

        <div class="segment <?php echo FH_SIDEBAR_CLASSES; ?>">

            <?php if( FH_CONTACT_EMAIL_ADDR_ENABLED && $email_address ): ?>
            <p><strong>Email Address:</strong><br>
                <i class="fa fa-envelope fa-fw"></i>&nbsp; <a href="mailto:<?php echo $email_address; ?>"><?php echo $email_address; ?></a>
            </p>
            <?php endif; ?>

            <?php if( FH_CONTACT_PHONE_NUM_ENABLED && $phone_number ): ?>
            <p><strong>Phone Number:</strong><br>
                <i class="fa fa-phone fa-fw"></i>&nbsp; <a href="tel:<?php echo $phone_number; ?>"><?php echo $phone_number; ?></a>
            </p>
            <?php endif; ?>

            <?php if( FH_CONTACT_FAX_NUM_ENABLED && $fax_number ): ?>
            <p><strong>Fax Number:</strong><br>
                <i class="fa fa-fax fa-fw"></i>&nbsp; <?php echo $fax_number; ?>
            </p>
            <?php endif; ?>

            <?php if( FH_CONTACT_TEXT_ADDR_ENABLED && $text_address ): ?>
            <p><strong>Address:</strong><br>
                <?php echo $text_address; ?>
            </p>
            <?php endif; ?>

        </div>

    <?php
    endif;


    //
    //  Gmaps + Driving directions in sidebar
    //
    if(
        ( FH_CONTACT_GMAPS_ENABLED && $location ) ||
        ( FH_CONTACT_DRIVING_DIRECTIONS_ENABLED && $driving_directions_url )
    ):

        ?>

        <div class="segment <?php echo FH_SIDEBAR_CLASSES; ?>">

            <?php if( FH_CONTACT_GMAPS_ENABLED ): ?>
                <div style="height: 250px;" id="map_canvas"></div>
                <br>
            <?php endif; ?>


            <?php if( FH_CONTACT_DRIVING_DIRECTIONS_ENABLED && ! empty( $driving_directions_url ) ): ?>
                <div class="text-center"><a href="<?php echo $driving_directions_url; ?>" target="_blank" class="btn btn-primary"><i class="fa fa-road"></i> Driving Directions</a></div>
            <?php endif; ?>

        </div>

        <?php

    endif;

}
add_action( FH_CONTACT_SIDEBAR_ACTION, '_fh_contact_sidebar' );

// Call default holy_grail_master.php
$holy_grail_class = FH_CONTACT_HOLY_GRAIL_CLASS;
include( 'holy_grail_master.php');

//
//  ╔═══════════════════════════════════╗
//  ║ © Copyright 2014 Folkhack Studios ║
//  ╚═══════════════════════════════════╝
//

?>