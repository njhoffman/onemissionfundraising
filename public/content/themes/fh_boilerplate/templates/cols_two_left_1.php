<?php

/*
    Template Name: 2 column, A left
*/

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

$holy_grail_class = 'two_left_1';
include( 'holy_grail_master.php');

//
//	╔═══════════════════════════════════╗
//	║ © Copyright 2014 Folkhack Studios ║
//	╚═══════════════════════════════════╝
//

?>