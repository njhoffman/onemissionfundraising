
//
//  JS asset for Contact Us page template
//
//  - Heavily based on simple Google Maps API example (on purpose for extending)
//  - https://developers.google.com/maps/documentation/javascript/examples/infowindow-simple
//
( function() {

    function _fh_gmaps_initialize( map_id ) {

        var myLatlng = new google.maps.LatLng( contact_vars.lat, contact_vars.lng );
        var mapOptions = {
            zoom: parseInt( contact_vars.zoom_level ),
            center: myLatlng,
            disableDefaultUI: true
        };

        // Holds infowindow HTML content
        var contentString = contact_vars.marker_content;

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        // Looped for potential adding of multiple maps

            map = new google.maps.Map( jQuery( map_id )[0], mapOptions );

            marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: contact_vars.marker_title
            });

            // Add listener for infowindow
            google.maps.event.addListener( marker, 'click', function() {

                infowindow.open( map, marker );
            });
    }

    jQuery( function() {

        // Run on DOM load
        _fh_gmaps_initialize( '#map_canvas' );

        // Sidr Map loading support (flagged to pervent multiple loads)
        var sidr_map_loaded = false;

        jQuery( 'body' ).on( 'fh_sidr_opened', function() {

            if( ! sidr_map_loaded ) {

                _fh_gmaps_initialize( '#fh_sidr_map_canvas' );
            }
        });
    });
})();