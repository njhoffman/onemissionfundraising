//
//	Boilerplate-driven WooCommerce JavaScript
//
//	- Setup Boilerplate proceed to checkout fix
//	- Adds Bootstrap 3 Responsive Table Wrapper <div>
//	- Open external products (affiliate links) in new tab
//

jQuery( function() {

	_fh_woo_responsive_bs3_tables();
	_fh_woo_affiliate_new_tab();

	//
	//	Setup Boilerplate proceed to checkout fix
	//
	if( jQuery( '#fh_woo_cart_checkout_proceed' ) ) {

		jQuery( '#fh_alt_checkout_proceed' ).on( 'click', function(e) {

			// Have to append the value of the original proceed to checkout button to form
			// or WooCommerce will not proceed to checkout page... wtf?
			var input = jQuery("<input>")
				.attr("type", "hidden")
				.attr("name", "proceed")
				.val("Proceed to Checkout");
			jQuery( '#fh_woo_cart_checkout_proceed' ).append( jQuery( input ) );

			// Submit the checkout form
			document.forms["fh_woo_cart_checkout_proceed"].submit();
		});
	}
});

//
//	Adds Bootstrap 3 Responsive Table Wrapper <div>
//
function _fh_woo_responsive_bs3_tables() {

	if( jQuery( '.woocommerce-page table' ).length ) {

		jQuery( '.woocommerce-page table' ).wrap( '<div class="table-responsive"></div>' );
	}
}

//
//	Open external products (affiliate links) in new tab
//
function _fh_woo_affiliate_new_tab() {

	if( jQuery( 'a.product_type_external, .product-type-external .single_add_to_cart_button' ).length ) {

		jQuery( 'a.product_type_external, .product-type-external .single_add_to_cart_button' ).attr( 'target', '_blank' );
	}
}