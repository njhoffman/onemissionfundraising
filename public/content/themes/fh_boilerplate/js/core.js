//
//	Core Boilerplate JavaScript
//
//	- Check for Gravity forms date fields and convert to date input type
//	- QRJS (for print-page links)
//	- Set .doc_url to document.URL for use within theme/etc.
//	- WordPress adminbar fly-out hack
//	- Fly-out responsive sidebars using Sidr
//	- Setup Global Smooth-scroll (ex: back-to-top link)
//	- Back-to-top hide
//	- Look for a gallery on the page and add appropriate .magnific classes if appropriate
//	- Magnific Popup
//	- Handler for when Sidr is opened
//	- Refreshes WooCommerce cart in responsive sidr menu
//	- Gets content from sidebar and prepends ID attributes with fh_sidr_
//	- Sets up an affix with a top/bottom sampled target to auto-calculate offsets
//	- Initializes QRJS for handling print QR codes
//	- Initializes Magnific for handling click-zooming of images
//

//	Track if affix has been setup yet
var fh_affix_setup = fh_affix_setup || false;

jQuery( function() {

	// DOM/Window ready

	//	Check for Gravity forms date fields and convert to date input type
	//	- TODO clean this up once Gravity Forms gets their stuff together on this
	if( jQuery( '.ginput_container .datepicker' ).length ) {

		jQuery( '.ginput_container .datepicker' ).prop( 'type', 'date' );
		jQuery( '.ginput_container .datepicker' ).on( 'click', function(e) {

			jQuery(this).prop('type', 'date');
		});
	}

	//
	//	QRJS (for print-page links)
	//
	_fh_util_qr_init( jQuery( '#qrcode' ) );

	//
	//	Set .doc_url to document.URL for use within theme/etc.
	//	- used by QRJS print-page links
	//
	if( jQuery( '.doc_url' ).length ) {

		jQuery( '.doc_url' ).html( document.URL );
	}

	jQuery( window ).on( 'resize load', function() {

		// Clear global window affix
		if( fhbp_config.fh_sticky_sidebars !== '' || fhbp_config.fh_sticky_header !== '' ) {

			// Unload affix
			// - https://github.com/twbs/bootstrap/issues/5870
			jQuery( window ).off( '.affix' );
		}

		//
		//	Setup Bootstrap 3 affix for .gutter_asides (sidebars)
		//	- Depreciated
		//
		// if( fhbp_config.fh_sticky_sidebars !== '' ) {

		// 	var asides = jQuery( '.gutter_aside' );
		// 	var compare_height = 0;
		// 	var content_height = jQuery( '#content > .gutter_content' ).height();

		// 	// Check that the content is longer than the sidebars
		// 	if( asides.length > 1 ) {

		// 		jQuery.each( asides, function( index, gutter ) {

		// 			var each_height = jQuery( gutter ).height();
		// 			compare_height = ( each_height > compare_height ) ? each_height : compare_height;
		// 		});

		// 	} else {

		// 		compare_height = asides.height();
		// 	}

		// 	// Only load sticky sidebars if content is longer than sidebars
		// 	if( content_height > compare_height ) {

		// 		_fh_setup_affix( '.gutter_aside', 'header .vertical_align_wrap', 'footer' );
		// 	}
		// }

		//
		//	Setup Bootstrap 3 affix for #nav_header navbar
		//
		if( fhbp_config.fh_sticky_header !== '' ) {

			_fh_setup_affix( '#nav_affix', 'header .vertical_align_wrap', '' );
		}
	});

	//
	//	WordPress adminbar fly-out hack
	//
	var wp_adminbar_logo = jQuery('#wp-admin-bar-wp-logo a');
	wp_adminbar_logo.attr( 'aria-haspopup', false );
	wp_adminbar_logo.attr( 'title', 'Fly-out Administration Menu' );
	wp_adminbar_logo.unbind( 'click' );
	wp_adminbar_logo.on( 'click', function(e) {

		e.preventDefault();
		jQuery( '#wpadminbar' ).toggleClass( 'flyout' );
	});

	//
	//	Fly-out responsive sidebars using Sidr
	//
	if( fhbp_config.fh_sidr !== '' ) {

		var right_content = '#sidebar_right .gutter_aside';
		var left_content = '#sidebar_left .gutter_aside';

		// Right sidebar
		if( jQuery( right_content ).length ) {

			jQuery( '#fh_flyout_right_btn' ).sidr({
				name: 'sidr-right',
				side: 'right',
				source: function () { return _fh_sidr_get_content( right_content ) },
				displace: false,
				onOpen: _fh_sidr_open
			});

		} else {

			jQuery( '#fh_flyout_right_btn' ).remove();
		}

		// Left sidebar
		if( jQuery( left_content ).length ) {

			jQuery( '#fh_flyout_left_btn' ).sidr({
				name: 'sidr-left',
				side: 'left',
				source: function () { return _fh_sidr_get_content( left_content ) },
				displace: false,
				onOpen: _fh_sidr_open
			});

		} else {

			jQuery( '#fh_flyout_left_btn' ).remove();
		}

		// WooCommerce refresh cart if applicable
		jQuery( 'body' ).on( 'added_to_cart', _fh_sidr_refresh_woo_cart );

	}

	//
	//	Setup Global Smooth-scroll (ex: back-to-top link)
	//
	jQuery( '.smooth_scroll' ).smoothScroll();

	//
	//	Back-to-top hide
	//
	jQuery( window ).on( 'scroll', function() {

		if( fhbp_config.fh_backtotop_show_at <= jQuery(document).scrollTop() ) {

			jQuery( '#fh_backtotop_btn' ).show();

		} else {

			jQuery( '#fh_backtotop_btn' ).hide();
		}
	});

	//
	//	Look for a gallery on the page and add appropriate .magnific classes if appropriate
	//
	var add_magnify_links = jQuery( '.gallery a, p.attachment a' );

	jQuery.each( add_magnify_links, function( index, element ) {

		if( jQuery( this ).attr( 'href' ).match( /\.(jpeg|jpg|gif|png)$/ ) != null ) {

			jQuery( this ).addClass( 'magnific' );
		}
	});

	//
	//	Magnific Popup
	//
	_fh_util_magnific_init();

});

//
//	Handler for when Sidr is opened
//	- Fires custom "fh_sidr_opened" event
//
function _fh_sidr_open() {

	// Refreshes WooCommerce shopping cart (if exists)
	_fh_sidr_refresh_woo_cart();

	jQuery( 'body' ).trigger( 'fh_sidr_opened' );
}

//
//	Refreshes WooCommerce cart in responsive sidr menu
//
function _fh_sidr_refresh_woo_cart() {

	var woo_target = '.widget_shopping_cart_content';

	if( jQuery( woo_target ).length ) {

		var woo_content = jQuery( woo_target ).html();
		var sidr_target = jQuery( '.sidr-class-widget_shopping_cart_content' );

		sidr_target.html( woo_content );
	}
}

//
//	Gets content from sidebar and prepends ID attributes with fh_sidr_
//
function _fh_sidr_get_content( content_target ) {

	// Create a new version of the passed content
	var content = jQuery( '<div />' ).html( jQuery( content_target ).html() );

	// Itterate over each element and check for an ID, replace as needed
	content.find('*').each( function( index, element ) {

		var local_element = jQuery( element );
		var id = local_element.attr( 'id' );

		if( typeof id === 'string' && '' !== id ) {

			local_element.attr(
				'id', id.replace( /([A-Za-z0-9_.\-]+)/g , 'fh_sidr_$1' )
			);
		}
	});

	return content;
}

//
//	Sets up an affix with a top/bottom sampled target to auto-calculate offsets
//
function _fh_setup_affix( target, top_sample_target, bottom_sample_target ) {

	// Remove any previously setup affix instances
	if( fh_affix_setup ) {

		// Unload affix
		// - https://github.com/twbs/bootstrap/issues/5870
		jQuery( target )
			.removeData( 'bs.affix' )
			.removeClass( 'affix affix-top affix-bottom' );
	}

	if( jQuery( target ).length ) {

		var top_adjust = ( top_sample_target.length ) ?
			jQuery( top_sample_target ).outerHeight( true ) : 0;
		// 20px on the bottom adjust to pad against bottom sampled item
		var bottom_adjust = ( bottom_sample_target.length ) ?
			jQuery( bottom_sample_target ).outerHeight( true ) + 20 : 0;

		jQuery( target ).affix({

			offset: {
				top: function() {
					return ( this.top = top_adjust )
				},
				bottom: function () {
					return ( this.bottom = bottom_adjust )
				}
			}
		});

		fh_affix_setup = true;
	}

	// When navigation is affixed keep the same height element on the page in it's place
	jQuery( '#nav_affix_spacer' ).css({ height: jQuery( target ).outerHeight( true ) });

	return;
}

//
//	Initializes QRJS for handling print QR codes
//
function _fh_util_qr_init( target_img ) {

	if( target_img.length ) {

		qr.canvas({
			canvas: target_img[0],
			value: document.URL
		});
	}
}

//
//	Initializes Magnific for handling click-zooming of images
//
function _fh_util_magnific_init() {

	// magnificPopup
	if( jQuery( '.magnific' ).length ) {

		var target = jQuery( '.magnific' );
		var args = {
			type: 'image',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [1, 1]
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
				titleSrc: function(item) {

					if( item.el.attr('title') && item.el.attr('title').length ) {

						return item.el.attr('title');
					}
				}
			}
		};

		// If .magnific is not an a element assume delegation:
		if( ! target.is('a') ) {

			args['delegate'] = 'a';
		}

		jQuery('.magnific').magnificPopup( args );

		// Show/hide any BS3 affixed divs + navbars (overlays)
		var hideme = '.affix, .affix-bottom, .affix-top, .navbar';

		jQuery('.magnific').on( 'mfpOpen', function(e) {

			jQuery( hideme ).hide();
		});

		jQuery('.magnific').on( 'mfpClose', function(e) {

			jQuery( hideme ).show();
		});
	}
};