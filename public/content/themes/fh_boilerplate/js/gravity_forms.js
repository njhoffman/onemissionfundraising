
jQuery( function() {

	// Image radio interaction extension
	if( jQuery( '.image-radio' ).length ) {

		jQuery( '.image-radio .gfield_radio li' ).on( 'click', function () {

			// Active class set
			jQuery( this ).siblings().removeClass( 'active' );
			jQuery( this ).addClass( 'active' );

			// Set checked for radio input
			jQuery( this ).find( 'input' ).prop( 'checked', true );

			// jQuery( this ).find( 'input' ).trigger( 'click' );

			return false;
		});

		// Check for pre-selection and add active class
		jQuery( '.image-radio input:checked' ).closest( 'li' ).addClass( 'active' );

		// Propegate any click events up to .image_radio li
		// - accounts for Gravity Forms adding "onclick" events to input elements
		jQuery( '.image-radio input' ).each( function( index ) {

			var orig_click_event = jQuery( this ).attr( 'onclick' );

			jQuery( this ).closest( 'li' ).attr( 'onclick', orig_click_event );
		});
	}

	// Image check interaction extension
	if( jQuery( '.image-check' ).length ) {

		jQuery( '.image-check .gfield_checkbox li' ).on( 'click', function () {

			// Un-Check / Check
			if( jQuery( this ).hasClass( 'active' ) ) {

				jQuery( this ).removeClass( 'active' );
				jQuery( this ).find( 'input' ).prop( 'checked', false );

			} else {

				jQuery( this ).addClass( 'active' );
				jQuery( this ).find( 'input' ).prop( 'checked', true );
			}

			return false;
		});

		// Check for pre-selection and add active class
		jQuery( '.image-check input:checked' ).closest( 'li' ).addClass( 'active' );

		// Propegate any click events up to .image_radio li
		// - accounts for Gravity Forms adding "onclick" events to input elements
		jQuery( '.image-check input' ).each( function( index ) {

			var orig_click_event = jQuery( this ).attr( 'onclick' );

			jQuery( this ).closest( 'li' ).attr( 'onclick', orig_click_event );
		});
	}
});