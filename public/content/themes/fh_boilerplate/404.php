<?php

/**
 *
 * Default Boilerplate 404 Page
 *
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

function custom_title() {
	?> <h1>404</h1> <?php
}
add_action( '_fh_the_title', 'custom_title' );

function custom_content() {
	?> <p>The page you have requested was not found.</p> <?php
}
remove_action( '_fh_the_content', '_fh_content' );
add_action( '_fh_the_content', 'custom_content' );

$holy_grail_class = 'one_full';

include( 'templates/holy_grail_master.php');

?>