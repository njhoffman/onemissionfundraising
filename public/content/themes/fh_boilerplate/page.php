<?php

/**
 *
 * Default Boilerplate WordPress Page
 *
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

//
//  Check for WooCommerce page
//
if(
    ( function_exists( 'is_cart' ) && is_cart() ) ||
    ( function_exists( 'is_checkout' ) && is_checkout() ) ||
    ( function_exists( 'is_account_page' ) && is_account_page() )
) {

    $woocommerce_sidebars_enable = true;
}

//
//	Custom page content
//
if( ! function_exists( '_fh_page_content' ) ):
function _fh_page_content() {

    if( FH_GALLERIES_ENABLED && FH_PAGE_GALLERIES_ENABLED ) {

        require 'templates/gallery_content.php';

    } else {

        // Display post content
        _fh_content();
    }
}
remove_action( '_fh_the_content', '_fh_content' );
add_action( '_fh_the_content', '_fh_page_content' );
endif;

// Conditional checks for child theme template, if doesn't exist defaults to boilerplate
if( file_exists( get_stylesheet_directory() . '/templates/' . FH_DEFAULT_PAGE_TEMPLATE ) ) {

	require get_stylesheet_directory() . '/templates/' . FH_DEFAULT_PAGE_TEMPLATE;

} else {

	require get_template_directory() . '/templates/' . FH_DEFAULT_PAGE_TEMPLATE;
}

?>