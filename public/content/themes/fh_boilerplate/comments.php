<?php

/**
 *
 * Default Boilerplate WordPress Comments Section
 * - based on Bones WordPress theme (https://github.com/eddiemachado/bones/)
 *
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

// Don't load it if can't comment
if ( post_password_required() ) {
  return;
}
?>

<?php if ( have_comments() ) : ?>

  <h3 id="comments-title" class="h2"><?php comments_number( __( '<span>No</span> Comments', 'fh_theme' ), __( '<span>One</span> Comment', 'fh_theme' ), _n( '<span>%</span> Comments', '<span>%</span> Comments', get_comments_number(), 'fh_theme' ) );?></h3>

  <section class="commentlist">
    <?php
      wp_list_comments( array(
        'style'             => 'div',
        'short_ping'        => true,
        'avatar_size'       => 40,
        'type'              => 'all',
        'reply_text'        => 'Reply',
        'page'              => '',
        'per_page'          => '',
        'reverse_top_level' => null,
        'reverse_children'  => ''
      ) );
    ?>
  </section>

  <?php /* TODO: Comments pagination */ ?>
  <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
  	<nav class="navigation comment-navigation" role="navigation">
    	<div class="comment-nav-prev"><?php previous_comments_link( __( '&larr; Previous Comments', 'fh_theme' ) ); ?></div>
    	<div class="comment-nav-next"><?php next_comments_link( __( 'More Comments &rarr;', 'fh_theme' ) ); ?></div>
  	</nav>
  <?php endif; ?>

  <?php if ( ! comments_open() ) : ?>
  	<p class="no-comments"><?php __( 'Comments are closed.' , 'fh_theme' ); ?></p>
  <?php endif; ?>

<?php endif; ?>

<?php comment_form(); ?>
