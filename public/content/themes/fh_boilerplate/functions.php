<?php

/**
 *
 * Boilerplate "functions.php" master WordPress theme bootstrap
 *
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

//
// Base loaded PHP asset. If child theme exists (fh_boilerplate-child) then this file is prepended by
// the child theme's respect functions.php. Functions are wrapped in if_function_exists() conditionals
// so that if a function is declared before the base boilerplate version it is overridden.
//

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

//
//	Global PHP Application configuration options
//	- Checks for if FH_CONFIG_SET which means there is a configuration that's set in our child theme
//
if( ! defined( 'FH_CONFIG_SET' ) ) {

	require get_template_directory() . '/config/config.php';
}

//
//	Child priority require
//	- If the asset exists within the child-theme then load
//
if( ! function_exists( '_fh_child_priority_require' ) ):
function _fh_child_priority_require( $asset ) {

	// Look to see if file exists within child theme
	if( FH_CHILD_THEME_LOADED && file_exists( get_stylesheet_directory() . $asset ) ) {

		require get_stylesheet_directory() . $asset;

	} else {

		// Grab asset from parent theme
		require get_template_directory() . $asset;
	}
}
endif;

//
//	Namespaced Base PHP Includes
//
if( ! function_exists( '_fh_base_php_includes' ) ):
function _fh_base_php_includes() {

	//
	//	Site includes array
	//
	$fh_site_includes = array(

		// Small theme utility functions
		'util_funcs.php',
		// Basic WP features setup - body_class, post-thumbnails, etc.
		'wp_setup.php',
		// JavaScript / CSS Site Asset Loading
		'site_includes.php',
		// Custom branded WP login
		'login.php',
		// Bootstrap Pagination Support
		'wp_bootstrap_paginate.php',
		// Social icons and "Share This:" HTML block
		'socials.php',
		// Specific rendering/utility functions to Boilerplate
		'template_functions.php',
		// Masonry, MixItUp, and BS3 Carousel Galleries
		'galleries.php',
		// Extra contact info for user profiles
		'user_contactmethods.php',
		// Custom Post Type Archives in Nav Menus
		'nav_functions.php',
		// Custom Nav Menu Locations
		'menus.php',
		// Custom Bootstrap 3.x Navigation Walker
		'wp_bootstrap_navwalker.php',
		// Sidebar configuration
		'sidebars.php',
		// Shortcode implementations (galleries)
		'shortcodes.php',
		// Page-links pagination
		'wp_bootstrap_page_links.php',
		// Password protected posts
		'wp_bootstrap_password_form.php',
		// Commenting BS3 Support
		'comments.php',
		// ACF theme customizations
		'acf.php',
		// Add login/logout link support to menus - DEPRECIATED!
		// 'login_logout_menu_items.php',
		// Configure installed plugins
		'plugin_configuration.php',
		// Configure WooCommerce if installed
		'woocommerce_setup.php',
		// Configure Gravity Forms if installed
		'gravity_forms_setup.php'
	);

	//
	//	If debug flag is set then include utility debugging functions
	//
	if( FH_DEVELOPMENT_MODE ) {

		$fh_site_includes[] = 'debug.php';
	}

	//
	//	Get all of the stuff. (there's a bit.)
	//
	foreach( $fh_site_includes as $asset ) {

		_fh_child_priority_require( '/include/' . $asset );
	}
}
endif;
_fh_base_php_includes();

//
//	Add WooCommerce Theme Support
//
add_theme_support( 'woocommerce' );

define( 'WOOCOMMERCE_USE_CSS', false );

?>