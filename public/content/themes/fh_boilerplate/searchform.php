<?php

/**
 *
 * Default Boilerplate WordPress Search Form
 *
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

?>

<!-- Custom search form -->
<form role="search" method="get" class="search_form" action="<?php echo esc_url( home_url( '/' ) ); ?>" >

	<!-- Label wrapping due to screen-readers/ID's/etc -->
	<div class="input-group">
		<label for="head_search_input" class="sr-only">Search Website:</label>
		<input id="head_search_input" type="text" class="form-control" value="<?php echo get_search_query(); ?>" name="s" placeholder="search website" />

		<span class="input-group-btn">
			<button type="submit" class="btn btn-primary">
				<span class="fa fa-search"></span>
			</button>
		</span>
	</div>
</form>