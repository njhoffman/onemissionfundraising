# Favicon Sizing #
* apple-touch-icon-precomposed.png - 57x57
* apple-touch-icon-ipad-precomposed.png - 72x72
* apple-touch-icon-iphone-retina-precomposed.png - 114x114
* apple-touch-icon-ipad-retina-precomposed.png - 144x144
* favicon.ico - 64x64
* favicon.png - 256x256
* opengraph.png - 1024x1024

# HTML Header Include #
```html
<link href='/img/icons/apple-touch-icon-precomposed.png' rel='apple-touch-icon-precomposed'>
<link href='/img/icons/apple-touch-icon-ipad-precomposed.png' rel='apple-touch-icon-precomposed' sizes='72x72'>
<link href='/img/icons/apple-touch-icon-iphone-retina-precomposed.png' rel='apple-touch-icon-precomposed' sizes='114x114'>
<link href='/img/icons/apple-touch-icon-ipad-retina-precomposed.png' rel='apple-touch-icon-precomposed' sizes='144x144'>
<link href='/img/icons/favicon.ico' rel='icon' type='image/vnd.microsoft.icon'>
<link href='/img/icons/favicon.ico' rel='shortcut icon'>
<meta content='#f5f5f5' name='msapplication-TileColor'>
<meta content='/img/icons/favicon.png' name='msapplication-TileImage'>
```