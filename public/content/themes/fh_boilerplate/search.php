<?php

/**
 *
 * Default Boilerplate WordPress Search Page
 *
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

//
//  Custom heading
//
if( ! function_exists( '_fh_blog_search_title' ) ):
function _fh_blog_search_title() {

    echo '<h1>Searching for: &quot;' . str_replace("\\", "", get_search_query()) . '&quot;</h1>';
}
add_action( '_fh_the_title', '_fh_blog_search_title' );
endif;

//
//  Remove post meta from displayed results
//
if( ! function_exists( '_fh_search_post_meta' ) ):
function _fh_search_post_meta( $html, $post ) {

    return ( $post->post_type == 'post' ) ? $html : '';
}
add_filter( '_fh_display_post_meta', '_fh_search_post_meta', 10, 2 );
endif;

//
//  Prepend blog content with search form
//
if( ! function_exists( '_fh_blog_prepend_searchform' ) ):
function _fh_blog_prepend_searchform() {

    ?>
    <!-- Custom search form -->
    <form id="content_search_form" role="search" method="get" class="search_form" action="<?php echo esc_url( home_url( '/' ) ); ?>" >

        <!-- Label wrapping due to screen-readers/ID's/etc -->
        <div class="input-group">
            <label for="page_search_input" class="sr-only">Search Website:</label>
            <input id="page_search_input" type="text" class="form-control" value="<?php echo str_replace("\\", "", get_search_query()); ?>" name="s" placeholder="search website" />

            <span class="input-group-btn">
                <button type="submit" class="btn btn-primary">
                    <span class="fa fa-search"></span>
                </button>
            </span>
        </div>
    </form>
    <?php
}
add_action( '_fh_content_prepend', '_fh_blog_prepend_searchform' );
endif;

// Unload default content action and load blog loop
remove_action( '_fh_the_content', '_fh_content' );
add_action( '_fh_the_content', '_fh_blog_content' );

$holy_grail_class = FH_DEFAULT_SEARCH_FORMAT;
include( 'templates/holy_grail_master.php');

?>
