<?php

/**
 *
 * Default Boilerplate WordPress Tag Page
 *
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

//
//  Custom tag heading
//
if( ! function_exists( '_fh_blog_tag_title' ) ):
function _fh_blog_tag_title() {

    echo '<h1>Tag Archive: ' . single_tag_title( '', false ) . '</h1>';
}
add_action( '_fh_the_title', '_fh_blog_tag_title' );
endif;

// Unload default content action and load blog loop
remove_action( '_fh_the_content', '_fh_content' );
add_action( '_fh_the_content', '_fh_blog_content' );

$holy_grail_class = FH_DEFAULT_BLOG_FORMAT;
include( 'templates/holy_grail_master.php');

?>