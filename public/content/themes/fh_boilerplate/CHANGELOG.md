## 1.6.3 (2015-10-10) WooCommerce Update
* Updated WooCommerce theme assets to reflect recent version
* Updated version to 1.6.3 in bower.json, config.php, package.json, style.css, and VERSION
* Updated CHANGELOG.md documentation

## 1.6.2 (2015-08-09 22:36) Fix WooCommerce Cart Totals
* BUGFIX: Fix WooCommerce duplicate cart totals
* Updated version to 1.6.2 in bower.json, config.php, package.json, style.css, and VERSION
* Updated CHANGELOG.md documentation

## 1.6.1 (2015-08-09 22:36) Advanced Video Embed Removal
* Removed advanced responsive video embedding plugin (glitching out on GF management page)
* Updated version to 1.6.1 in bower.json, config.php, package.json, style.css, and VERSION
* Updated CHANGELOG.md documentation

## 1.6.0 (2015-07-26 15:36) Investonomics Push
* Updated WooCommerce theme assets for 2.3.0 compatibility
* Added actions for pre_blog_article and post_blog_article
* Removed .woocommerce-page namespace from and simplified WooCommerce buttons
* Lessen the drop-shadow for Sidr fly-out sidebars
* Limiting bottom margin of article.loop for responsive view
* Implemented a sane z-index scheme (thank fucking god)
* Added Gravity Forms support for payment form
* Disabled all WooCommerce styling globally
* Added in twig-based actions _fh_twig_pre_template, _fh_twig_post_template
* Locked the lodash version in bower.json
* Select2 BS3 styling for WooCommerce
* Updated CHANGELOG.md documentation
* Updated version to 1.6.0 in bower.json, config.php, package.json, style.css, and VERSION

## 1.5.0-RC20 (2015-03-01 15:55) Fixed Open-sans SASS assets
* BUGFIX: Fixed open sans sass assets due to update dependency
* Updated CHANGELOG.md documentation
* Updated version to 1.5.0-RC20 in bower.json, config.php, package.json, style.css, and VERSION

## 1.5.0-RC19 (2015-01-19 23:13) Fixed WooCommerce rendering issue
* BUGFIX: Fix WooCommerce number of products returned box on product browsing
* BUGFIX: Fixing validation input background color issue with gravity forms
* DOCS: Converted CHANGELOG to CHANGELOG.md for formatting utilizing GitLab flavored markdown
* Updated SASS assets to point to BS3 $screen-x variables when applicable
* Updated CHANGELOG.md documentation
* Updated version to 1.5.0-RC19 in bower.json, config.php, package.json, style.css, and VERSION

## 1.5.0-RC18 (2015-01-19 12:05) Added WooCommerce Utility
* Added _fh_woo_get_cart_total_float() WooCommerce utility function to grab cart total as a float value
* Updated CHANGELOG documentation
* Updated version to 1.5.0-RC18 in bower.json, config.php, package.json, style.css, and VERSION

## 1.5.0-RC17 (2015-01-17 16:22) OpenSans Bugfix
* BUGFIX: Updated opensans font SASS framework to point to correct assets
* Updated CHANGELOG documentation
* Updated version to 1.5.0-RC17 in bower.json, config.php, package.json, style.css, and VERSION

## 1.5.0-RC16 (2014-11-27 00:37) WooCommerce Update
* Updated package.json build assets for grunt to newest versions
* Updated WooCommerce form-login.php for 2.2.6 update
* Updated CHANGELOG documentation
* Updated version to 1.5.0-RC16 in bower.json, config.php, package.json, style.css, and VERSION

## 1.5.0-RC15 (2014-11-26 18:40) Front-end Asset Update
* Updated field validation error highlighting for Gravity Forms in _gravity_forms.scss to be more visually prominent
* Updated Bootstrap from 3.2.* to 3.3.* in bower.json
* Updated MixItUp from 2.1.5 to 2.1.7 in bower.json
* Updated imagesloaded from 3.1.4 to 3.1.8 in bower.json
* Updated qr-js from 1.1.1 to 1.1.3 in bower.json
* Updated jquery.smooth-scroll from 1.4.13 to 1.5.4 in bower.json
* Updated Masonry from 3.1.5 to 3.2.1 in bower.json
	- Removed compilation from Gruntfile.js (now packaged assets)
	- De-registered WordPress default Masonry in galleries.php
	- Pointed galleries.php to new packaged assets
* Froze Sidr at 1.2.1 in bower.json
* Froze Respond at 1.4.2 in bower.json
* Updated CHANGELOG documentation
* Updated version to 1.5.0-RC15 in bower.json, config.php, package.json, style.css, and VERSION

## 1.5.0-RC14 (2014-11-18 11:16) Add Fax Number
* Added fax # into contact us page template + enabled configuration value "FH_CONTACT_FAX_NUM_ENABLED"
* Updated CHANGELOG documentation
* Updated version to 1.5.0-RC14 in bower.json, config.php, package.json, style.css, and VERSION

## 1.5.0-RC13 (2014-11-02 20:16) OMF Changes Update
* BUGFIX: Removed 404 _fh_the_content action (404 showing recent blogpost)
* Movecd prev/next post into template_funcitons.php function _fh_prev_next_posts()
* Updated CHANGELOG documentation
* Updated version to 1.5.0-RC13 in bower.json, config.php, package.json, style.css, and VERSION

## 1.5.0-RC12 (2014-10-19 22:26) OMF WooCommerce Update
* BUGFIX: Move custom adminbar inline CSS to admin_head (was causing uploader issues)
* BUGFIX: Increase width of selects within Gravity Forms date selection
* UPGRADE: Updated all WooCommerce custom templates for WooCommerce 2.2.0 compatibility
* Updated CHANGELOG documentation
* Updated version to 1.5.0-RC12 in bower.json, config.php, package.json, style.css, and VERSION

## 1.5.0-RC11 (2014-10-13 18:34) Blah
* Fixed FH_ASIDE_POST_FORMAT_ENABLED bug when set to false hiding blog posts
* Updated CHANGELOG documentation
* Updated version to 1.5.0-RC11 in bower.json, config.php, package.json, style.css, and VERSION

## 1.5.0-RC10 (2014-10-13 18:10) Holy Grail layout fixes
* Fixed footer layout removing top 20px margin
* Updated header responsive layout for 100% width breaks
* Added $s_resize_safety and $s_first_resize for first 100% resize to handle veritcal scrollbar issue
* Updated sticky navs for all responsive break situations
* Added a nav_affix_side_gutters to header for side gutter enabled layout sticky header
* Updated CHANGELOG documentation
* Updated version to 1.5.0-RC10 in bower.json, config.php, package.json, style.css, and VERSION

## 1.5.0-RC9 (2014-10-11 17:47) Misc. Lynch Legal Updates
* Changed "Site Map" to "Sitemap" for menu location
* Fixed author block website and social icon spacing
* Added Really Simple Captcha support for Gravity Forms BS3 styling
* Including Font Awesome fixed width SASS asset by default
* Fixed contact us template to look for "FH_SIDEBAR_CLASSES" configuration for it's sidebar classes
* BUGFIX: Fixed author social icons displaying site social icons when no social profiles enabled
* BUGFIX: Fixed author display name not showing up issue on single posts
* Updated CHANGELOG documentation
* Updated version to 1.5.0-RC9 in bower.json, config.php, package.json, style.css, and VERSION

## 1.5.0-RC8 (2014-10-09 16:52) Added hooks
* Added in the following action hooks for theme development:
	- '_fh_header_prepend'
	- '_fh_header_prepend_in_wrap'
	- '_fh_header_append_in_wrap'
	- '_fh_before_content'
	- '_fh_after_content'
	- NOTE: Added to holy_grail_master.php and part_header.php templates
* Documented new hooks in README.md and template_functions.php
* Updated CHANGELOG documentation
* Updated version to 1.5.0-RC8 in bower.json, config.php, package.json, style.css, and VERSION

## 1.5.0-RC7 (2014-09-27 19:08) Emergency Fatal Error Fix
* Removed ini settings from parent functions.php - should not be set within a theme
* Fixed if conditional with constant within wp_setup.php "_fh_fix_email_headers" function - fatal error on older PHP's
* Updated CHANGELOG documentation
* Updated version to 1.5.0-RC7 in bower.json, config.php, package.json, style.css, and VERSION

## 1.5.0-RC6 (2014-09-15 12:53) WooCommerce SASS Compilation if
* Fixed if conditional variable assinment in product browsing SASS for WooCommerce
* Renamed "fh_product_padding" to "fh_woo_product_padding" for better WooCommerce namespacing
* Updated CHANGELOG documentation
* Updated version to 1.5.0-RC6 in bower.json, config.php, package.json, style.css, and VERSION

## 1.5.0-RC5 (2014-09-14 21:39) RC5 Updates
* Removed old TODO list sitting in theme root
* Load post via "the_post()" for WP 4.0 fix in template_functions.php
* gravity_forms.js event propegation fix for image-radio, and image-check image selection
* True/false case fixing within config.php (set all to lowercase) + un-indent formatting
* Added FH_DISPLAY_AUTHOR_INFO configuration value for showing/hiding all author info + 301 redirect on author page
* Added "Email headers fix" section to wp_setup.php along with configuration for FH_MAIL_FROM_SENDER
* Added in Lil' B credit for typographic styling
* Added Laravel "dd()" Dump & Die function to debug.php
* Added in proper documentation header to gravity_forms_setup.php
* Updated core SASS compilation to compact CSS & diable line numbers in Gruntfile.js
* Added in sourcemap for QRJS "qr.min.js" in Gruntfile.js (caused 404 on use of minified assets)
* Removed .fade display: none; within admin SASS admin.scss
* Converted Bootstrap 3 glyphicons to Font Awesome icons to avoid loading two icon fonts
* Major clean-up of all sass. Added in headers, CW, and reformatted many files
* Added in CW header or footer (whatever was appropriate) into all theme assets and un-indented when needed
* Updated README.md documentation
* Updated CHANGELOG documentation
* Updated version to 1.5.0-RC5 in bower.json, config.php, package.json, style.css, and VERSION

## 1.5.0-RC4 (2014-09-03 14:31) Misc Seiko Updates
* DEPRECIATED: Gruntfile.js SASS compilation "sourcemap: true" not needed anymore - sourcemaps generated by default now
* FEATURE: Added in Gravity Forms include "gravity_forms_setup.php" to enqueue custom "gravity_forms.js" JS for image radio/check options
* FEATURE: Added in base styling for Gravity Forms image radio/check "image-radio" and "image-check" classes + configuration variables ("_vars.scss, "bs3_extensions/_gravity_forms.scss")
* BUGFIX: Added js/main.js back in due to 404 on lookup when only using parent theme
* BUGFIX: Changed htmlentities to strip_tags on post excerpt rendering in "template_functions.php" (not rendering & entities correctly)
* FORMATTING: Converted "bs3_extensions/_gravity_forms.scss" to spaces
* FORMATTING: Fixed add_action included in function_exists in wp_setup.php
* FEATURE: Added in config driven value for site-wide temporary CSS ACF field 'FH_SITE_TEMP_CSS_ENABLED' in "config.php", "template_functions.php" and "acf.php" - loads on admin options page
* Added VERSION file
* Added dates into 1.5.0 RC1-3 CHANGELOG entries
* Updated CHANGELOG documentation
* Updated version to 1.5.0-RC4 in bower.json, config.php, package.json, style.css, and VERSION

## 1.5.0-RC3 (2014-08-19) Ignored server ports
* BUGFIX: Ignored server ports (for generating URL's in util_funcs.php - get_current_url())
	- Updated config.php for config driven
* BUGFIX: Fixed empty( get_the_title() ) "Can't use function return value in write context" issue within single.php
* Updated CHANGELOG documentation
* Updated version to 1.5.0-RC3 in bower.json, config.php, package.json, and style.css

## 1.5.0-RC2 (2014-08-18) BCB Bugfixes
* BUGFIX: Ensure WooCommerce is not loaded OR that we're not on cart/checkout/account page on holy_grail_master.php (issue with no sharing showing up if WooCommerce not loaded)
* Added config value for "FH_EASTER_EGG" for the "up skirt" head comment
* Added config value for "FH_AUTHOR_META" for config driven author meta information in head
* Added config value for "FH_DISPLAY_COMMENTS_ENABLED" for enabling/disabling commenting site-wide
* Depreciated _fh_posted_on() in template_functions.php (not used...?)
* Fixed sticky post add class conditional ! is_archive() -> is_home() in template_functions.php
* Updated "No posts to display" on no thingys returned to be "No items to display!" in alert-info box
* Added an ID (content_search_form) to search.php template and styled for better search box support
* Added in a filter for '_fh_display_post_meta' and implemented on search.php to hide meta information on pages
* Added in better title and excerpt handling into "_fh_display_post_excerpt()"
* Updated CHANGELOG documentation
* Updated version to 1.5.0-RC2 in bower.json, config.php, package.json, and style.css

## 1.5.0-RC1 (2014-08-15) Bootstrap 3.2 Push
* Updated Bootstrap from 3.1.x to 3.2.x
* Converted all btn-info buttons to btn-primary
* BUG: fixed hiding of wordpress thanks and version for super admins
* Removed WordPress core custom background feature
* Added more strict error handling within theme for dev (E_ERROR -> E_WARNING) in functions.php
* config/config.php
	- BUG: added default DEFINE( 'CHILD_THEME_LOADED', false ) for when not using a child theme
	- Name-spaced "$super_admins" array to "$fh_super_admins"
	- Removed trailing whitespace
	- Added FH_* namespace to config.php THUMBNAIL_QUALITY, EXCERPT_WORD_LENGTH, and EXCERPT_ENDING
	- Added in base config options for loading in of Open Sans and Font Awesome ('FH_USE_OPEN_SANS', 'FH_USE_FONT_AWESOME')
	- Namespaced 'SOCIAL_SHARING_ENABLED' to 'FH_SOCIAL_SHARING_ENABLED'
	- Added in config values for Contact Us Custom Fields/Template Options
	- Added in config values for Masonry, MixItUp, Bootstrap 3 Gallery Support
	- Added in config values for Post formats
	- Namespaced "CHILD_THEME_LOADED" -> "FH_CHILD_THEME_LOADED"
	- Fixed FH_ namespacing within config.php
	- Added in documentation header
	- Added Boilerplate version in 'FH_BOILERPLATE_VERSION' constant
	- Changed 'FH_DEFAULT_PAGE_TEMPLATE' to cols_two_right_1.php
	- Added in title delimiter config value 'FH_TITLE_DELIMITER'
	- Added site-wide Option-driven Social Account Links under 'FH_SITE_OPTION_DRIVEN_SOCIALS'
	- Added in super-granular control over social sharing through FH_SOCIAL_SHARE_ON_* configuration values
	- Added in feature to enable/disable article meta and comments on attachments
	- Changed default single format to "two_right_1"
	- Re-did entire social icons configuration to include author flag to show/hide on user_contactmethods.php, added different social values for new Font Awesome version, added beter sorting
* functions.php
	- Removed login/logout link support
	- Updated documentation and general formatting
* include/util_funcs.php
	- Added custom admin check for dipshit mode/is_super_admin() - Utils::check_is_admin()
	- General formatting updates + documentation
* include/wp_setup.php
	- Fixed bones init cleaning up code and re-namespacing to _fh_ rather than bones
	- Removed remove_meta_box for "Recent Drafts Widget"
	- Removed hiding of dashboard_activity, dashboard_right_now, dashboard_quick_press, and dashboard_recent_comments if not super admin
	- Renamed "DIPSHIT_MODE_ENABLED" -> "FH_DIPSHIT_MODE_ENABLED", added hiding of "General Settings" admin page, enabled dipshit mode by default
	- Setup text domain for translation as "fh_boilerplate"
	- Removed "Add User" from top admin bar for non-admins and dipshit users
	- Removed Better Search dashboard metaboxes for non-admins and dipshit users
	- Consolidated dipshit user and non-admin logic into Utils::check_is_admin() for simplification of admin dashboards
	- Updated translation and text domain on the login disabled notice
	- Remove capital_P_dangit filter for content, comments, and titles
	- Fixed formatting, fixed indentions, moved add_action/add_filter within function_exists conditionals, and added file documentation
	- Removed trailing whitespace
	- Added FH_* namespace to config.php THUMBNAIL_QUALITY, EXCERPT_WORD_LENGTH, and EXCERPT_ENDING
	- Moved gallery add_image_size logic into include/galleries.php (originall in wp_setup.php)
	- Added in config values for Post formats (added chat)
* include/site_includes.php
	* Removed trailing whitespace
	* Moved '_fh_site_includes' add_action within function_exists
	* Added in base config options for loading in of Open Sans and Font Awesome ('FH_USE_OPEN_SANS', 'FH_USE_FONT_AWESOME')
	* Converted tabs to spaces and added documentation file header
* include/template_functions.php
	- BUG: Fixed title generation for social sharing
	- BUG: Fixed social sharing w/o WooCommerce logic issue in _fh_get_social_sharing
	- BUG: Fixed multiple call to _fh_get_social_sharing on single.php
	- Cleaned-up redundant code in "_fh_author_block" for globals and unused $soc_icon_width
	- Moved '_fh_page_prepend' and '_fh_page_append' add_actions within function_exists
	- General formatting updates
	- BUG: Fixed byline/date being put into `<time>` tag for _fh_display_post_meta
	- Added "the_content" and "clearfix" class wrap to _fh_content()
	- Major documentation update
	- Fixed text domain for some translation
	- Fixed spacing for author block visit website button
	- Added "Authored Posts Archive" link to bottom of author block
	- Added "sticky" class to article for sticy posts
	- Changed "Read More" button to be larger and have a different icon
	- Removed un-necessary logic from sharing (extra is_single check)
	- Added "$woocommerce_sidebars_enable" to ensure WooCommerce sidebars load on WooCommerce specific pages (like cart)
* include/acf.php
	- Organized + added in documentation
	- Added in config gate logic for Contact Us template
	- Added in config gate logic for ACF driven galleries
	- Added in config gate logic for post formats
	- Added in config gate logic for social sharing
	- Added in config gate logic for site options page
	- Added in BS3 Carousel ACF controls for gallery post format to:
		- Display Carousel Controls
		- Display Carousel Indicators
		- Display Carousel Image Captions
		- Enable Images to View Full-size on Carousel Click
		- Modify Carousel Slide Duration
* include/galleries.php
	- Moved gallery add_image_size logic into include/galleries.php (originall in wp_setup.php)
	- Made default galleries.php gallery generation functions args config driven
	- Removed ID from MixItUp gallery generation in lieu of utilzing a class
	- Made BS3 Carousel ID passed by argument
	- Reformatted and added in documenation header
* include/plugin_configuration.php
	- Depreciating If Menu in lieu of Nav Menu Roles Plugin (keeping around to ensure don't need it again) - commenting out plugin config.
	- Adding in a prioritization for moving Yoast admin to the bottom of posts/pages (push behind ACF fields)
* include/shortcodes.php
	- Updated for new gallery args
	- Added documentation header
* include/user_contactmethods.php
	- Unset new default user contact methods (facebook, twitter, googleplus)
	- Added in check for "author" set from $fh_social_icons for config-driven user_contactmethods
	- Added documentation and general formatting update
* page.php
	- Added gallery support to pages through 'templates/gallery_content.php' include
* single.php
	- Link post format styling better
	- Removing pre from status post format and styling better
	- Added in fh_post_format_ and fh_post_type_ for general single.php article class
	- Added in FH_ASIDE_POST_FORMAT_ENABLED configuration value to title generation conditional
	- Moved single.php gallery generation to templates/gallery_content.php for use by pages
	- Added _fh_content() calls rather than the_content() for wrapping of content in "the_content" class
	- Added in post format configuration gates for aside, image, etc.
	- Removed title `<h1>` generation if empty title
	- Added 'fh_empty_title' class to article on empty title
	- Added "FH_ARTICLE_META_ON_ATTACHMENTS_ENABLED" gate to displaying post meta
	- Added empty title support to "next" and "previous" buttons
	- Reformatted back to articles link to be larger and have proper spacing
	- Added attachment social sharing and commenting support
* templates/template_contact.php
	- Adding in new config gates for FH_CONTACT_SIDEBAR_ACTION and FH_CONTACT_HOLY_GRAIL_CLASS
	- Added in config gates for everything ACF driven within the Contact Us template
	- Changed contact us template name from "Contact Us (with Google Map)" to just "Contact Us"
	- Renamed load Gmaps function
* templates/part_header.php
	- General formatting updates
	- Removed "Home" icon from navbar-brand element in navbar
	- Added in FH_SITE_OPTION_DRIVEN_SOCIALS gate to top social icons
* sass/_vars.scss
	- Added in config values for gallery sizing/padding
* sass/boilerplate/_galleries.scss
	- Added in config values for gallery sizing/padding
	- Re-factored Masonry SASS
	- Fixed static max-widths for responsive to be based on $screen-blah BS3 configuration vals
* sass/boilerplate/_audio_video.scss
	- Centering and adding margin to single audio player
	- Fixing audio/video playlist padding
	- Centering video embed
* sass/boilerplate/_general.scss
	- Fixed #fh_crumbs breadcrumb padding
	- Updated author block styling
* sass/boilerplate/_holygrail.scss
	- Made content padding less on $screen-xs to fit more content on small devices
* sass/boilerplate/_posts.scss
	- Removed p:past-of-type on article, moved into typography under .the_content
	- Fixing up article meta (author, date) padding/floating
	- Fix article_meta padding on aside post format
	- Fixing article loop margins and padding
	- Link post format styling better
	- Removing pre from status post format and styling better
	- Fixed article_meta padding on empty title posts and aside formatting
	- Centered content on attachment pages
	- Added in sticky post styling
	- Fixing taxonomy max-width for hella categories/tags
* sass/boilerplate/_typography.scss
	- Removed p:past-of-type on article, moved into typography under .the_content
	- Removed margin bottom from first H1 in content
	- Add "word-wrap: break-word;" to all typographic elements to break large non-spaced words on small screens
* include/login.php
	- Moved default WP login styling/configuration to include/login.php
	- Added in function_exists gates to include/login.php
	- Documented include/login.php - converted tabs to spaces
	- General reformatting
* js/core.js
	- Updated documentation and general formatting
	- Depreciated "Bootstrap 3 affix for .gutter_asides" - not ready yet/buggy
* Added proper "Text Domain:" within style.css to be "fh_boilerplate"
* Moved _fh_paginate into own include file wp_bootstrap_paginate.php. Added documentation/better formatting.
* Moved "_fh_kill_adminbar" utility function into Utils class, renamed it to "kill_adminbar" and documented change.
* Re-documented util_funcs.php and fixed formatting
* Namespaced "$social_icons" to "$fh_social_icons" global in:
	- config/config.php
	- include/template_functions.php (_fh_author_block)
	- include/user_contactmethods.php
	- include/socials.php
* Moved social icons and "Share this:" HTML block into own include file from template_functions.php -> "socials.php"
* Re-ordered _fh_get_social_sharing conditional to have FH_SOCIAL_SHARING_ENABLED first in conditional in "socials.php"
* Documented socials.php with header description and converted tabs to spaces
* Moved Masonry, MixItUp, and BS3 Carousel gallery support into galleries.php
* Added window load event to Masonry and MixItUp galleries
* Updated gallery assets Masonry (3.1.4 -> 3.1.5) and MixItUp (2.1.4 -> 2.1.5) to newest versions
* BUGFIX: Added an extra gap to MixItUp galleries
* Removed 1% margin-bottom from MixItUp galleries for consistent spacing in sass/boilerplate/_galleries.scss
* Added in conditional `<hr>`'s if the_content returns anything on gallery post formats
* General formatting/inline documentation updates in single.php
* Fixed positioning of BS3 Carousel indicators in sass/boilerplate/_galleries.scss
* Added 'No Gallery' option for ACF and re-org'd gallery ACF fields. Added conditionals for ACF boilerplate gallery fields due to new 'No Gallery' option
* Added experimental "width: fit-content" to default WP 3.9 galleries and fixed 1-9 columnn formatting with xn+1 "clear: left;" property
* Removing bower copy for Mono Social Icons font (not implemented anymore) in Gruntfile.js
* Removing Mono Social Icon Fonts from bower.json (not implemented anymore)
* Reformatted comments.php and added in documentation header
* Reformatted include/debug.php and added in documentation header
* General formatting updates and added in text domain for translation in include/menus.php
* Added in documenation header in include/nav_functions.php
* Added in documentation header into include/plugin_configuration.php
* Added in documentation header, fixed text domain for translation, general formatting updates to include/sidebars.php
* Added in $override_acf_sharing override for short circuiting get_field check for non-adminable pages in include/socials.php
* Removed js/main.js (not needed)
* Updated all package.json npm devDependencies
* Fixed image alignment issues in sass/boilerplate/_images.scss
* Fixed navbar brand larger font size in sass/boilerplate/_navs.scss
* Center social sharing buttons on attachments in sass/boilerplate/_social.sass
* Added in WAY more granular social sharing support to tempaltes/holy_grail_master.php
* Added logic to test for empty title and strip delimiter from front of string to templates/part_html_head.php
* General formatting and documentation updates to
	- include/wp_bootstrap_navwalker.php
	- include/wp_bootstrap_page_links.php
	- include/wp_bootstrap_paginate.php
	- include/wp_bootstrap_password_form.php
	- index.php
	- js/contact.js
	- search.php
	- searchform.php
	- tempaltes/cols_one_full.php
	- tempaltes/cols_*
	- tempaltes/gallery_content.php
	- templates/part_footer.php
	- templates/part_html_foot.php
	- templates/part_footer.php
* WooCommerce
	- Added in better breadcrumb and H1 padding/margins
	- Changed WooCommerce cart button class from info to primary for better readability
	- Added "$woocommerce_sidebars_enable" to ensure WooCommerce sidebars load on WooCommerce specific pages (like cart)
	- include/woocommerce_setup.php
		- Added documentation header
		- Added visible-sm to responsive checkout button
		- Updated text domain for translation
		- Documentation and general formatting update
	- js/woocommerce.js
		- Added documentation header + general documentation
		- Added open external products (affiliate links) in new tab functionality
	- Added $woocommerce_sidebars_enable check in page.php to ensure WooCommerce sidebars on my account, cart, and checkout pages
	- Updated woo_product_height_browse default SASS var 320 -> 330px
	- Fixed "added to cart" button unstyled issue
	- Added left/right padding to cart actions
	- Removed display: none from checkout button below cart (who care if the responsive one and the small one show at the same time?)
	- Fixed right margin for navbar right cart button
	- Fixed alert notification margins
	- Fixed customer details dl/dt margins
	- Removed "Name Your Price" support
	- Fixed result count and order by wrap height
	- Added "Avg. Rating: " content before star rating and centered content on product browse
	- Fixed margin for "on sale" alert
	- Removed top-padding from images and description on product view
	- Image overlay affix fix (navbar was over image view overlays)
	- Fixed price slider to have gray background for slider bar
	- Added 'Avg. Rating: ' to reviewer for product list/reviews widgets and fixed padding
	- Removed underline from ins in product list widgets
	- Made reviewer name smaller in widgets
	- General formatting updates to woocommerce.php
	- Added in rating numbers to rating dropdown and changed "Very Poor" to just "Poor"
* Gravity Forms (sass/bs3_extensions/_gravity_forms.scss)
	- Added in documentation for some TODO items
	- Added margin to the bottom of Gravity Forms
	- Added in "other" option styling for Gravity Forms radio
	- Added better section title formatting
	- Added in better list formatting for multi-column lists
	- Added in multi-file upload formatting
	- Fixed margins on field descriptions
	- Added ReCaptcha support
* Updated CHANGELOG documentation
* Updated version to 1.5.0-RC1 in bower.json, config.php, package.json, and style.css

## 1.4.5 (2014-08-03 16:56) Updating composer.json Dependencies
* Removed both "search-everything" and "wordpress-importer" from Boilerplate composer.json
	- Don't need importer every install
	- Updating standard search plugin to "better-search"
* Updated CHANGELOG documentation and version #
* Updated bower.json, package.json, style.css version #

## 1.4.4 (2014-07-28 13:33) Custom pagination query support
* Adding in pagination support for custom queries (if passed as an arg then page based on custom query, if not page on global $wp_query)
* Updated CHANGELOG documentation and version #
* Updated bower.json, package.json, style.css version #

## 1.4.3 (2014-06-30 17:11) Major One Mission Launch Catch-up
* Added WooCommerce config.php support for the following configuration options:
	- FH_WOOCOMMERCE_SINGLE_PRODUCT_UPSELLS_LIMIT
	- FH_WOOCOMMERCE_SINGLE_PRODUCT_UPSELLS_NUM_COLS
	- FH_WOOCOMMERCE_BROWSE_PRODUCT_NUM_COLS
	- NOTE: Corresponding hooked changes made in include/woocommerce_setup.php
* Added ACF social sharing support to product post types (WooCommerce), and taxonomies in include/acf.php
* Added footer action hooks:
	- _fh_footer_prepend
	- _fh_footer_inside_prepend
	- _fh_footer_cw_prepend
	- _fh_footer_cw_content
	- _fh_footer_cw_append
	- _fh_footer_inside_append
	- _fh_footer_append
* Fixed pagination URL encoding (didn't exist before - no spaces in search result query bug)
* Added utility method Utils::_fh_get_current_url() to get current page URL for use with social sharing, etc. in include/util_funcs.php
* Removed commented out debugging statement from include/template_functions.php
* Add in fixed URL support (via utility function) and title support to social sharing widgets for both Yoast and defaut WP in include/template_functions.php
* Add in better social sharing gating and support to "_fh_get_social_sharing()" in include/template_functions.php
* Add in clearfixed div wrapper for number of results and shopping order for WooCommerce in include/woocommerce_setup.php
* Add in WooCommerce SASS configuration variable for $fh_woo_product_category_height_browse in _vars.scss
* Remove HTML5 required from include/comments.php for website URL
* Fixed default footer styling - fixed side margins in _footer.scss
* Added in multi-page form support into Gravity Forms support _gravity_forms.scss
* Added honeypot validation container hiding to Gravity Forms support _gravity_forms.scss
* Added description styling to Gravity Forms extension _gravity_forms.scss
* Added better placement of Gravity Forms AJAX spinner _gravity_forms.scss
* Major rewrite of WooCommerce products browse styling - better mobile support etc. _product_browse.scss
* Fixed "Add to Cart" button being off in FF in single product view _product_view.scss
* Add in single product support for social sharing into main holy_grail_master.php template pre-loader
* Add in Holy Grail configuration support for search page via "FH_DEFAULT_SEARCH_FORMAT" config variable
* Updated CHANGELOG documentation and version #
* Updated bower.json, package.json, style.css version #

## 1.4.2 (2014-06-06) Fixing WooCommerce Sidebars
* Fixed WooCommerce sidebar issue loading default sidebars
* Updated CHANGELOG documentation and version #
* Updated bower.json, package.json, style.css version #

## 1.4.1 (2014-06-05 02:56) Blog Index Bugfix
* Fixed non-needed if conditional for FH_BLOG_H1 on blog index. Was causing issues on live servers.
* Updated CHANGELOG documentation and version #
* Updated bower.json, package.json, style.css version #

## 1.4.0 (2014-06-05 01:25) Contact Template Fixes
* Removed inline JS from contact template - moved to js/contact.js core asset w/localization passing of vars
* Convert "Text Address" (text_address) field to WYSIWYG editor in include/acf.php
* Removed requirements for contact template ACF fields in include/acf.php
* Updated CHANGELOG documentation and version #
* Updated bower.json, package.json, style.css version #

## 1.4.0-RC7 (2014-06-04 01:52) BWP Launch Fixes
* Un-nested functions from if statement - was breaking on Firefox + bad practices (whoops!)
* Temporarily disabling mapping feature - buggy as hell =/
* Updated CHANGELOG documentation and version #
* Updated bower.json, package.json, style.css version #

## 1.4.0-RC6 (2014-06-03 15:18) Hella Sidebars Update
* Updated general config.php
	- Updated all default holy grail layout configs to cols_two_right_1
	- Redid all sidebar configuration vars to be A/B rather than 1/2 or left/right
	- Added in support for disabling both blog and WooCommerce related sidebars in lieu of defaults
	- Added multi-segment support for blog sidebars
	- Disabled 'FH_STICKY_SIDEBARS' by default (buggy - needs moar work)
	- Disabled 'DIPSHIT_MODE_ENABLED' by default
* Major sidebar registration revision in include/sidebars.php
	- Added in "FH_SIDEBAR_A/B_ENABLED" for default, blog and WooCommerce sidebars
	- Added in better descriptions (Shows on pages/blog pages/shopping pages)
	- Added in default FH_BLOG_DEFAULT_SIDEBARS and FH_WOO_DEFAULT_SIDEBARS overrides
	- Added in looped segment support to blog sidebars
* Major sidebar rendering revision in include/template_functions.php
	- Renamed function "_fh_get_template_sidebars" -> "_fh_get_template_sidebars_arr"
	- Renamed function "_fh_get_dynamic_sidebar" -> "_fh_get_dynamic_sidebar_html_no_echo"
	- Renamed function "_fh_get_sidebars" -> "_fh_get_sidebar_segments_html"
	- Renamed function "_fh_get_sidebar" -> "_fh_get_sidebar_html"
	- Added function for rendering of Sidebar A "_fh_sidebar_a_render"
	- Added function for rendering of Sidebar B "_fh_sidebar_b_render"
	- Added logic within "_fh_get_template_sidebars_arr" for proper ordering of sidebars A/B
	    - Looks at Holy Grail layout class and orders properly based on page template name
	    - Calls "_fh_sidebar_a/b_render" functions based on order
	- Added in actions for:
	    - _fh_sidebar_a/b_prepend/append
* Added in test function for determining if on a blog related page: _fh_is_blog_related
* Updated SCSS variable naming convention to reflect sidebar A/B setup:
	- $s_sidebar_large_width -> $s_sidebar_a_width
	- $s_sidebar_small_width -> $s_sidebar_b_width
	- Kept old var names due to depreciation
* Degraded sidebar affix related properties within _sidebars.scss (commented) due to bugs. Needs more testing.
* Updated footer template to reflect the "_fh_get_sidebar" -> "_fh_get_sidebar_html" function name update
* Updated all cols* templates to reflect the A/B sidebar changes
	EX: "Template Name: 3 column, sidebars right (ordered sidebar #1, sidebar #2)" is now:
	    "Template Name: 3 column, sidebars right (A left, B right)"
* Updated CHANGELOG documentation and version #
* Updated bower.json, package.json, style.css version #

## 1.4.0-RC5 (2014-06-02 12:26) Misc. Somatek fixes
* Added in new phone #, email address, and text address field to the include/acf.php for use on the contact page template
* Fixing Yoast's Breadcrumb disabling to disable via is_front_page() rather than is_home()
* Added in action hooks for modifying footer copywrite content:
	- _fh_footer_cw_prepend
	- _fh_footer_cw_content
	- _fh_footer_cw_append
* Adding in period check to fix the "Somatek Inc.." -> "Somatek Inc." issue in the footer copywrite.
* Fixed sticky nav bug: When navigation is affixed keep the same height element on the page in it's place
* Added handler for when Sidr is opened and replaced "_fh_sidr_refresh_woo_cart" in calls:
	- jQuery( '#fh_flyout_right_btn' ).sidr()
	- jQuery( '#fh_flyout_left_btn' ).sidr()
* Added in "Borders Color" $bp_bottom_border_color configuration value within boilerplate core SASS. Changes header underlines and general border colors throughout site. Affected vals:
	- .comment-meta, .comment-author border-bottom
	- #respond border-bottom
	- #fh_crumbs border-bottom
	- hr border-top
	- .article_meta border-bottom
	- sidebar main widget li border-bottom
	- content h1:first-of-type border-bottom
* Added in BS3 vendor import for BS3 variable use within Boilerplate (in core boilerplate.scss)
* Removed "responsive.scss" and moved responsive logic into respective .scss module (ex: header.scss's responsive breaks are within it's own file.. easier to find this way). Changed static responsive sizes to use BS3 configuration values (ex: $screen-xs or $screen-lg)
* Fixed footer width to match header/content width within _footer.scss
* Un-nested all Holy Grail layout classes (ex: one_full, two_right_1) from .body_wrap and did general re-org of content. Added in responsive .body_wrap segment from depreciated _responsive.scss
* Fixed sidebar widget li top/bottom spacing issue within _sidebars.scss
* Major re-org and fixing of contact-us template within templates/template_contact.php:
	- Added in proper wp_enqueue_script for Google Maps
	- Added in support for base email address, text address, and phone # via ACF fields, loads into sidebar segment above map
	- Added in support for multiple maps placement via for loop within "_fh_gmaps_initialize"
	- Fixed bug for loading Google Maps within Sidr via jQuery().on() event
	- Named variables functions to better describe purpose of action
* Updated CHANGELOG documentation and version #
* Updated bower.json, package.json, style.css version #

## 1.4.0-RC4 (2014-05-30 07:03) Misc. BWP fixes
* Added in function for wrapping pages in ID "page_article" to template_functions.php
* Added in !default SCSS configuration for the first Bootstrap break-point to _vars.scss
* Adding non-default loading of Gravity Forms and WooCommerece extensions (should be loading in through childt heme) by modifying bootstrap.scss
* Updated CHANGELOG documentation and version #
* Updated bower.json, package.json, style.css version #

## 1.4.0-RC3 (2014-05-14 23:02) Grunt fixes
* Removing "sass" tasks in lieu of "compile-sass" task
* Added "precision: 10" option to SASS compilation due to Chrome 1px off bug
* Stopped './css/bootstrap.css' from being linted
* Updated CHANGELOG documentation and version #
* Updated bower.json, package.json, style.css version #

## 1.4.0-RC2 (2014-05-14) Agnostic composer.json Versions
* Made compser.json version agnostic (kept at seat level would be best =P)

## 1.4.0-RC1 (2014-05-14 20:41) SASS/CSS cleanup
* Added support for CSS Linting
* Terminated opening "<?php" with "?>" at EOF
* Added in CSS blessing and with re-org'd SASS assets
	- http://blesscss.com/
* HUGE SASS/CSS re-org due to many reasons. Many small rendering issues fixed with Boilerplate, Holy Grail, Gravity Forms, WooCommerce, etc. (sorry for a shitty CHANGELOG entry - was not worth micro updates on what's happening)
* Updated author block alignment on author.php page
* Added in config.php option for shutting off Yoast for non-admins
* Added in config.php option for using blessed CSS
* Added in simple "FH_DEVELOPMENT_MODE" check within "functions.php" to for simple asset to hold debug utility functions
* Added function for listing all hooked WordPress functions for debug purposes only within new "include/debug.php" theme asset
* Added logic to add a CSS stylesheet for the administrative area in "include/side_includes.php"
* Added theme hooks "_fh_sidebar_right_content" and "_fh_sidebar_left_content" within "include/template_functions.php"
* Added in $_COOKIE injection utility function (really useful!!!) to "include/util_funcs.php"
* Added a wrapper for the WooCommerce looped product titles for vertical aligns
* Updated "count" text on simple cart button
* Updated lots with wp_setup to be less opinionated for what is shown/hidden within WP admin area in "include/wp_setup.php"
	- Updated widgets on dashboard that are shown/hidden to admins
	- Removed the "options-general.php" override
	- Updated ACF menu page override
	- Added "if ( ! is_super_admin() )" guards to help hide things if not an admin
	- Added logic to disable default administrative top menu items
		- wp-logo
		- comments
		- wpseo-menu
		- new-media
		- user-info
	- Removed default administrative footer text/version
	- Disabled admin bar for users
* Added magnific popup fix to hide affixed items within core
* Updated single.php author block author gravatar from 'alignright' to 'author_img_right'
* Updated the TODO doc a bit
* Froze mixitup version in bower.json
* Updated CHANGELOG documentation and version #
* Updated bower.json, package.json, style.css version #


## 1.3.0-RC1 (2014-04-20 13:21) Back-to-top, Sidr, WP 3.9
* Added bower installation assets for sidr, jqueryscroller, respond.js into Gruntfile.js, and bower.json
* Added in WP asset registration and enqueing into include/site_includes.php for:
	- Smooth Scroll
	- Sidr
	- Respond.js
* Added in configuration pass-through via wp_localize_script for FH_SIDR_ENABLED, and FH_BACKTOTOP_SHOW_AT
* Added BP configuratioon options for FH_SIDR_ENABLED and FH_BACKTOTOP_SHOW_AT
* Added in custom .png icon assets for sidebar flyouts (left + right), and back-to-top
* Added in action hooked to _fh_content_append to check if Sidr is enabled and append show/hide sidebar fly-out buttons
* Added core.js support for fly-out sidebars using Sidr - http://www.berriart.com/sidr/
* Added global back-to-top scroll button support with conditional show/hide at certain scroll positions
* Added in magnific popup fix to hide affixed stuff on image popup
* Updated sidebar styling to be compatible with new responsive Sidr sidebar support in _boilerplate.scss
* Updated footer padding so back-to-top icon doesn't overlay copyright on bottom of page scroll
* Added sidr buttons show/hide support to base _holygrail.scss asset
* Fixed small issue with image-alignment on responsive layout (added text-align: center to image asset .align* classes)
* Added in print-override hide support for Sidr menus, back-to-top buttons, and the comment form in _print_overrides.scss
* Fixed "reqd more" -> "read more" spelling mistake in _print_overrides.scss
* Added in header fixes for fixed-height headers in _print_overrides.scss
* Added an obnoxious z-index to the WP admin bar on smaller responsive breaks to overlay the Sidr fly-out sidebars in _responsive.scss
* Added custom SASS asset for handling Sidr styling within BP - _sidr.scss
* Added in _vars.scss global support for back-to-top and Sidr actions:
	- $fh_sidr_bottom_actions_opacity
	- $fh_sidr_widths
* Added base WooCommerce and new _images.scss imports into print.scss
* Added in back-to-top button into part_html_foot.php partial
* Added theme support for HTML galleries and captions via "add_theme_support( 'html5', array( 'gallery', 'caption' ) );"
* Added core.js functionality to look for a gallery on the page and add appropriate .magnific classes if appropriate
* Added audio/video SASS asset via _audio_video.scss
* Added centering of content on attachment page in _boilerplate.scss
* Removed copy-right left/right padding on footer in _boilerplate.scss
* Removed base .mejs-container styling in _boilerplate.scss
* Removed old responsive video embed styilng from _boilerplate.scss
* Updated image captions to be HTML5 in _images.scss
* Added in WP 3.9 multi-column gallery support based on TwentyFourteen theme in _images.scss
* Updating footer styling in _print_overrides.scss - hide the border above the copyright.
* Added in "is_attachment()" conditional to single.php to hide/show article meta, sharing tools, author block, and back to articles
* Removed old "Make WP default video embeds responsive" functionality from include/wp_setup.php in lieu of new video support
* Updated CHANGELOG documentation and version #
* Updated bower.json, package.json, style.css version #

## 1.2.0-RC2 (2014-04-14 10:05) 1.2.0-RC2 Fix
* Moved cart button generation into woocommerce_setup.php
	- Updated header partial to reflect change
* Updated versions

## 1.2.0-RC1 (2014-04-14 07:48) OM Push
* Updated 404 content and template (404.php)
* Grunt.js
	- Added in line-number references to expanded CSS, use sourcemaps
	- Implemented live-reload
	- Pairing down BS3 JS assets (comment out those we don't use by default)
* Updated "Holy Grail" templating to remove redundant code and move template generation logic into "holy_grail_master.php":
	- (added) templates/holy_grail_master.php
	- (updated) archive.php
	- (updated) author.php
	- (updated) category.php
	- (updated) index.php
	- (updated) page-sitemap.php
	- (updated) page.php
	- (updated) search.php
	- (updated) tag.php
	- (updated) templates/cols_one_full.php
	- (updated) templates/cols_three_left_*.php
	- (updated) templates/cols_three_right_*.php
	- (updated) templates/cols_three_wrapped_*.php
	- (updated) templates/two_left_*.php
	- (updated) templates/two_right_*.php
	- (updated) templates/template_contact.php
* Comments support through custom ./comments.php based on Bones
	- Added in SASS styling
	- Added custom filters/actions for BS3 through ./include/comments.php
* Re-org of config/config.php. Added:
	- WooCommerce configuration options
	- Sticky header/footer options
* Updated includes in functions.php:
	- Added custom BS3 menu walker (wp_bootstrap_navwalker.php)
	- Added page links BS3 pagination (wp_page_links.php)
	- Added BS3 comment support (comments.php)
	- Added login/logout menu items support (login_logout_menu_items.php)
	- Added WooCommerce setup (woocommerce_setup.php)
* Added/updated WooCommerce theme support in functions.php through:
	- add_theme_support( 'woocommerce' );
	- define( 'WOOCOMMERCE_USE_CSS', false );
* Added Login/logout link feature to administrative menu editor through include/login_logout_menu_items.php
* Removed old navigation walker in lieu of new BS3 compatible one - include/wp_bootstrap_navwalker.php. (has support for glyphs, dividers, etc.)
* Started basic plugins configuration in include/plugin_configuration.php - currently only If Menu support for WooCommerce & "User is not logged in"
* Setup array of configuration values to expose to the JS side of things (core.js) via localization in include/site_includes.php
* Added/updated template content/sidebar handling functions. Added actions for:
	- _fh_the_crumbs
	- _fh_the_title
	- _fh_content_prepend
	- _fh_the_content
	- _fh_content_append
* Re-vamping social sharing rendering, added config gate (through social functions in template_functions.php)
* Added a simple BS3 cart button generator in template_functions.php
* Added general WooCommerce setup options through include/woocommerce_setup.php
	- Limit # products per page
	- Queue WooCommerce FH specific JS
	- Added in proceed to checkout button on cart page after cart
* Added in handlers for post page links and password protected posts:
	- wp_boostrap_page_links.php
	- wo_boostrap_password_form.php
* General WP setup option updates in wp_setup.php
	- Re-enabled comments administration
	- Added in twitter post embed check into responsive media embed
	- Disabled admin bar "push-down" inline CSS
* Core JS updates core.js:
	- Added BS3 navigation bar and sidebars affixing
	- Added WP adminbar fly-out hack in core.js
* Added WooCommerce woocommerce.js core JS for custom theme functionality:
	- Adds BS3 responsive table wrapper `<div>`
	- Setup custom checkout proceed button
* General Boilerplate SASS updates in _boilerplate.scss
	- Add heights to heading due to affix
	- Styled new walker navigation in top navigation
	- General minor padding footer updates
	- Added in simple media break for author meta
	- Redo table extension "article table" -> "#content table" due to WooCommerce
	- Sized social icons down for author blocks
	- Totally re-vamped social sharing icons
	- Center tweet embeds
* General Boilerplate SASS updates in _elements.scss
	- Removed image styling and moved to own SASS asset (_images.scss)
	- Small typography updates
* Gravity Forms SASS extension in _gravity_forms.scss
	- Fix time input field bug
	- Added in password field styling
	- Updated list field styling
* Updated SASS var $s_side_gutters -> $s_content_side_gutters (_holygrail.scss)
* Updated responsive SASS in _responsive.scss
	- Removed image styling in lieu of new images library (_images.scss)
	- Updated header breaks for affix
	- Fixed up taxonomy and sharing
	- Top navigation font size to 80% on small screens
* Updated global Boilerplate _vars.scss asset
	- Added Bootflat Color Pallet
	- Updated "side_gutters" -> $s_content_side_gutters
	- Added in affix configuration
	- Added in header height configuration
	- Added in aligned image padding configuration
	- Added in WooCommerce configuration values
	- Removed BS3 overrides
* Added in full SASS library for WooCommerce:
	- sass/woocommerce/_buttons.scss
	- sass/woocommerce/_cart.scss
	- sass/woocommerce/_checkout.scss
	- sass/woocommerce/_forms.scss
	- sass/woocommerce/_general.scss
	- sass/woocommerce/_my-account.scss
	- sass/woocommerce/_order_details.scss
	- sass/woocommerce/_payment.scss
	- sass/woocommerce/_plugins_conf.scss
	- sass/woocommerce/_products_browse.scss
	- sass/woocommerce/_product_view.scss
	- sass/woocommerce/_reviews.scss
	- sass/woocommerce/_widgets.scss
* single.php updates - to new "holy grail" style. Fixed back to articles button bug
* Updated header partial to take advantage of new BS3 nav walker
* Removed blog URL exposure via JS
* Updating base twig asset
	- Remove `<h1>`'s from heading generation
	- Add article_actions styling to share-page
* Added in WooCommerce review/respond form overrides into base ./woocommerce.php asset (only load on WooCommerce specific pages)
* Added in WooCommerce template overrides:
	- woocommerce/cart/cart-shipping.php
	- woocommerce/cart/cart.php
	- woocommerce/myaccount/form-login.php
	- woocommerce/myaccount/form-lost-password.php
	- woocommerce/single-product/add-to-cart/simple.php
	- woocommerce/single-product/meta.php
	- woocommerce/single-product/sale-flash
	- NOTE! all revisions are noted within file-header. These assets will need to be updated parallel with WooCommerce!
* Updated documentation/versions

## 1.1.4 (2014-04-07 20:19) Updated composer.json plugin depenencies
* Updated base dependency versions for:
	- timber-library 0.17.1 -> 0.17.2
	- advanced-responsive-video-embedder 4.0.0 -> 4.0.2

## 1.1.3a (2014-03-30 18:39) PHP 5.3 shortcodes.php HOTFIX
* Added in an array fix to ensure PHP 5.3 compatbility after PU launch issues

## 1.1.3 (2014-03-30 12:33) PU Boilerplate Updates
* Remove lame 404 content from 404.php
* Added prepend/append hooks for blog sidebars
* Added "FH_DISPLAY_AUTHOR_BLOCK" gate for disabling author block support
* Added jQuery deps to image gallery scripts
* Going back to jQuery 1.x branch due to IE support

## 1.1.2 (2014-03-24 12:00) Final child theme prep & run-through
* Fixing project meta in bower/package.json and style.css
* Added in priority file_exists load for base required assets within parent theme
	- Located in base /functions.php
	- Done through "_fh_base_php_includes" function and '$fh_site_includes' var
	- If a child theme loaded it looks in
		- get_stylesheet_directory() // child theme directory THEN
		- get_template_directory() // parent theme directory
	- Works via simple file_exists( get_stylesheet_directory() . $asset ) conditional
* Fixes to make parent more compatible and overidable by child theme
	- Update pathing/variable names for front-end asset registration and enqueuing in "site_includes.php"
* Add in print overrides as it's own base included SASS asset "print_overrides.scss"
* Remove imagemin from package.json (bad Win32 issues... don't ask)
* Updating version information in all files
* Updating TODO documentation
* Adding in screenshot (screenshot.png)
* Fixing CHANGELOG

## 1.1.1 (2014-03-22) Updating composer.json with correct repo name
* Removing direct script access with check for ABSPATH
* W3C Validation run-through, general fixes
* Fixing composer package names
* Updating TODO documentation

## 1.1.0 (2014-03-22) Grunt.js, Bower, less -> sass, Bootstrap 3.0 -> 3.1, Composer, Deve...
* Added Bower.io managed front-end depenecies for
	- modernizr
	- twbs-bootstrap-sass
	- jquery
	- font-awesome
	- lodash
	- monosocialiconsfont
	- open-sans-fontface
	- masonry
	- magnific-popup
	- mixitup
	- imagesloaded
	- qr-js
* Removed Koala in favor of Grunt.js
* Grunt.js tasks
	- install - Get and install site assets from Bower into correct locations
	- compile-site - Compile full public website (SASS, CSS, and JS)
	- compile-sass - Compiles site SASS and runs CSS minification
	- watch - Watch SASS asset dir and compile assets
	- help - Display this help doc. (also default)... also sup baby ;)
* Moved header/aside/footer structural styling to new _boilerplate.scss asset
* Moved all "Boilerplate" styling to new _boilerplate.scss asset
* Removed compiled assets from repo - Grunt.js improvement
* Removed all vendor assets from repo - Bower/Grunt.js improvement
* Added in Composer support (through addition of composer.json)
* Setup base requirements for package (should have custom hosted plugin locations defined in Folkhack's WordPress Seat)
	- composer/installers
	- acf-options-page
	- acf-gallery
	- acf-repeater
	- wpackagist-plugin/advanced-custom-fields
	- wpackagist-plugin/timber-library
	- wpackagist-plugin/simple-image-sizes
	- wpackagist-plugin/advanced-responsive-video-embedder
	- wpackagist-plugin/advanced-custom-fields-location-field-add-on
	- wpackagist-plugin/avatar-manager
* Ensure meta information for project in proper locations, ensure consistent
	- style.scss - for WordPress
	- package.json
	- bower.json
	- composer.json
* Updated .gitignore to ignore appropriate assets (as preiviously covered)
* Conversion of #body_wrap -> .body_wrap as per new Holy Grail guidelines
* Adding of a basic todolist
* Now concats Masonry's JS assets into one file
* Update Mix-it-up implementation from 1.x -> 2.x
* Added QR code + extra page information on printing (through print stylesheet), utilizes qr.js
* Updated readme, moved some roadmap items/etc into todolist
* Added in an override config option for overriding base jQuery version supplied by WordPress
* Updated logo to not suck
* Updated acf-location Google Maps support to be acf-google-maps (easier for admin it seems)
* Updated all social sharing to be based on basic Font Awesome provided social icons
* Removed custom fontello script in lieu of newest Font Awesome
* Removed a couple of PHP notices being thrown
* Added in look-up method within for "includes/site_includes.php" for minified assets
	- Like doing this by hand for development/control purposes
* Cleaned up a lot of general formatting and added inline documentation
* Updated author block styling and rendering
* Updating article thumb, excerpt, and actions styling and rendering
* Updating custom post taxonomy rendering
* Removed JS method from social sharing buttons, now just simple sharing links
* Added in imagesloaded to fix height bug with Masonry
* Disabled root crumb generation for Yoast's breadcrumbs
* General styling updates (tweaks/general organization/print stylesheet)
	- Header (side padding on responsive)
	- Tables
	- Article excerpts, actions, responsive, and meta
	- Gravity forms
	- .body_wrap (remove bottom margin as per new Holy Grail guidelines)
	- Added a second custom font declaration to "_vars.scss" (verdana by default)
	- Moved SASS overrides for includes, etc to "_vars.scss"
	- Wrap content in clearfix (in case of floated content in posts/pages)
* Add home link back into navbar-brand for main responsive navigation
* General W3C compliance fixes

## 1.0.0 (2013-12-27) Initial release
* WordPress 3.8 compatible theme utilizing Bootstrap 3.0
	- Support for Bootstrap 3's dropdown menu system through custom walker
	- Custom Bootstrap 3 Pagination
	- Gravity forms integration, styled by extended Bootstrap 3
	- Twig/Timber driven templating
* Custom "Utils" class in "include/util_funcs.php" for including files and adding slug to body
* Styled via compiled SASS/LESS
* Koala compilation of SASS/LESS assets (config file koala.json)
* Folkhack's Holy Grail responsive templates built in and setup as page templates
* ACF pre-configuration for post-types, global site options page, etc.
* Gallery support through post-types and shortcodes for
	- Bootstrap 3's carousel (shortcode: masonry_one_col_gallery)
	- Masonry (both one and two column) (shortcode: masonry_two_col_gallery)
	- Mix-it-up (shortcode: mixitup_gallery)
	- Magnific image zoom (shortcode: bs3carousel_gallery)
* Basic social sharing support
* Author block support (small bio/custom profile image/social links available)
* Configuration driven (/config/)
	- Can disable login
	- Multiple sidebar/footer/blog widget configurations
	- Set default page template
	- Enable/disable blog
	- Put site into development mode
	- Allow only restricted IPs to login
	- Site maintenence mode
	- Define a protected mode for less privileged admins
	- Set configuration loaded flag
	- Disable admin bar
* Custom font-stack for icons (fontello)
* Custom set of favicons for development
* Enqueues site assets in proper WordPress fashion
* Custom post excerpt HTML generation through "_fh_display_post_excerpt"
* Custom post taxonomy HTML generation through "_fh_display_post_taxonomy"
* Favicon block HTML generation through "_fh_favicon"
* Social icons HTML generation through "_fh_social_icons"
* Social sharing HTML generation through "_fh_display_social_sharing"
* Custom WordPress init setup with lots of clean-up/customizations
	- Custom thumbnail sizes set
	- Allow custom background colors
	- Disable version rendering in multiple WordPress locations
	- Clean-up rendering generally for front-end
	- Disable non-needed things on the admin side of things
	- Override login with custom message
* Page support for
	- Archives
	- Authors
	- Categories
	- Index
	- Sitemap
	- Page
	- Search
	- Single
	- Tag
	- Contact Us
* Establish basic README.md
* Child-theme support through Folkhack's Boilerplate Child Theme


## NOTE

CHANGELOG loosly follows Richard Stallman's approach: http://bit.ly/1djd2CB
