<?php

/**
 *
 * shortcodes.php
 *
 * Boilerplate specific shortcodes for access to internal rendering functions.
 *  - masonry_one_col_gallery - Masonry one column gallery
 *  - masonry_two_col_gallery - Masonry two column gallery
 *  - mixitup_gallery - MixItUp gallery
 *  - bs3carousel_gallery - BS3 Carousel slider gallery
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

//
//	Preps image array passed to gallery render functions to match ACF
//	- Takes comma delimited strin as input and passes back ACF-esque gallery array
//
if ( ! function_exists( '_fh_prep_acf_gallery' ) ) :
function _fh_prep_acf_gallery( $string ) {

	// Images is a comma delimited string of id's
	$images_arr = explode( ',', $string );
	$return_arr = array();

	foreach( $images_arr as $image_id ) {

		$image_data = _fh_get_img_attachment( $image_id );
		$add_arr = wp_get_attachment_metadata( $image_id );
		$add_arr['url'] = wp_get_attachment_url( $image_id );
		$add_arr['alt'] = $image_data['alt'];
		$add_arr['title'] = $image_data['title'];
		$add_arr['caption'] = $image_data['caption'];
		$add_arr['description'] = $image_data['description'];

		foreach( $add_arr['sizes'] as $size_key => $size ) {
			$tmp_arr = wp_get_attachment_image_src( $image_id, $size_key );
			$add_arr['sizes'][ $size_key ]['src'] = $tmp_arr[0];
		}

		$return_arr[$image_id] = $add_arr;
	}

	return $return_arr;
}
endif;

//
//	Shortcode for Masonry Support
//	- Passthrough for _fh_masonry_gallery (in template_functions.php)
//
if ( ! function_exists( '_fh_sc_masonry_one_col_gallery' ) ) :
function _fh_sc_masonry_one_col_gallery( $atts ){

	extract( shortcode_atts( array(
		'images'                => '',
		'two_col_width_enabled' => false,
		'one_col_width'         => FH_GAL_MASONRY_SINGLE_IMG_WIDTH,
		'two_col_width'         => FH_GAL_MASONRY_DOUBLE_IMG_WIDTH,
		'padding'               => FH_GAL_MASONRY_GUTTER,
		'one_col_resize'        => FH_GAL_MASONRY_IMG_SIZE_NAME_SINGLE,
		'two_col_resize'        => FH_GAL_MASONRY_IMG_SIZE_NAME_DOUBLE
	), $atts ) );

	return _fh_masonry_gallery(
		_fh_prep_acf_gallery( $images ),
		$two_col_width_enabled,
		$one_col_width,
		$two_col_width,
		$padding,
		$one_col_resize,
		$two_col_resize
	);
}
add_shortcode( 'masonry_one_col_gallery', '_fh_sc_masonry_one_col_gallery' );
endif;

//
//	Shortcode for Masonry Support
//	- Passthrough for _fh_masonry_gallery (in template_functions.php)
//
if ( ! function_exists( '_fh_sc_masonry_two_col_gallery' ) ) :
function _fh_sc_masonry_two_col_gallery( $atts ){

	extract( shortcode_atts( array(
		'images'                => '',
		'two_col_width_enabled' => true,
		'one_col_width'         => FH_GAL_MASONRY_SINGLE_IMG_WIDTH,
		'two_col_width'         => FH_GAL_MASONRY_DOUBLE_IMG_WIDTH,
		'padding'               => FH_GAL_MASONRY_GUTTER,
		'one_col_resize'        => FH_GAL_MASONRY_IMG_SIZE_NAME_SINGLE,
		'two_col_resize'        => FH_GAL_MASONRY_IMG_SIZE_NAME_DOUBLE
	), $atts ) );

	return _fh_masonry_gallery(
		_fh_prep_acf_gallery( $images ),
		$two_col_width_enabled,
		$one_col_width,
		$two_col_width,
		$padding,
		$one_col_resize,
		$two_col_resize
	);
}
add_shortcode( 'masonry_two_col_gallery', '_fh_sc_masonry_two_col_gallery' );
endif;

//
//	Shortcode for MixItUp Support
//	- Passthrough for _fh_mixitup_gallery (in template_functions.php)
//
if ( ! function_exists( '_fh_sc_mixitup_gallery' ) ) :
function _fh_sc_mixitup_gallery( $atts ){

	extract( shortcode_atts( array(
		'images' => '',
		'width'  => FH_GAL_MIXITUP_IMG_MAX_WIDTH,
		'height' => FH_GAL_MIXITUP_IMG_MAX_HEIGHT,
		'resize' => FH_GAL_MIXITUP_IMG_SIZE_NAME
	), $atts ) );

	return _fh_mixitup_gallery(
		_fh_prep_acf_gallery( $images ),
		$width,
		$height,
		$resize
	);
}
add_shortcode( 'mixitup_gallery', '_fh_sc_mixitup_gallery' );
endif;

//
//	Shortcode for Bootstrap Carousel Support
//	- Passthrough for _fh_bs3carousel_gallery (in template_functions.php)
//
if ( ! function_exists( '_fh_sc_bs3carousel_gallery' ) ) :
function _fh_sc_bs3carousel_gallery( $atts ){

	extract( shortcode_atts( array(
		'images'         => '',
		'controls'       => true,
		'caption'        => true,
		'dots'           => true,
		'image_links'    => true,
		'slide_interval' => 5000,
		'width'          => FH_GAL_BS3_CAROUSEL_MAX_WIDTH,
		'height'         => FH_GAL_BS3_CAROUSEL_MAX_HEIGHT,
		'resize'         => FH_GAL_BS3_CAROUSEL_IMG_SIZE_NAME,
		'carousel_id'    => FH_GAL_BS3_CAROUSEL_DEFAULT_ID
	), $atts ) );

	return _fh_bs3carousel_gallery(
		_fh_prep_acf_gallery( $images ),
		$controls,
		$caption,
		$dots,
		$image_links,
		$slide_interval,
		$width,
		$height,
		$resize,
		$carousel_id
	);
}
add_shortcode( 'bs3carousel_gallery', '_fh_sc_bs3carousel_gallery' );
endif;

?>