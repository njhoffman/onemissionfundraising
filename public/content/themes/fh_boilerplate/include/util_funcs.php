<?php

/**
 *
 * util_funcs.php
 *
 * Utils class containing helpful theme utilities.
 * - Based on https://github.com/viewportindustries/starkers
 *
 * Contents:
 * - include_part - Simple wrapper for native get_template_part()
 * - add_slug_to_body_class - Append namespaced page slugs to the body class
 * - inject_cookie - $_COOKIE injection utility function
 * - get_current_url - current url lookup function
 * - check_is_admin - Custom admin check for dipshit mode and is_super_admin
 * - kill_adminbar - Function to hide the admin bar and fix 28px html margin-top
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

class Utils {

    //
    // Simple wrapper for native get_template_part()
    // - @author Keir Whitaker
    //
    public static function include_part( $parts = array() ) {

        foreach( $parts as $part ) {

            get_template_part( $part );
        };
    }

    //
    // Append page slugs to the body class
    // - Added "-fhpage" for namespacing body class
    // - @author Keir Whitaker
    //
    public static function add_slug_to_body_class( $classes ) {

        global $post;

        if( is_page() ) {

            $classes[] = sanitize_html_class( $post->post_name . '-fhpage' );

        } elseif(is_singular()) {

            $classes[] = sanitize_html_class( $post->post_name . '-fhpage' );
        };

        return $classes;
    }

    //
    //  Injects an array into the active cookie
    //
    public static function inject_cookie( $inject_into_cookie, $expiry = 3600, $root = '/' ) {

        foreach( $inject_into_cookie as $key => $value ) {

            // Must unset cookie values first
            setcookie( $key, '', time() - $expiry, $root );
            unset( $_COOKIE[$key] );

            // Set that bitch (and cookies are a bitch - trust me =/)
            setcookie( $key, $value, time() + $expiry, $root );
        }

        // Primes $_COOKIE so it is correct on first load
        $_COOKIE = array_merge( $inject_into_cookie, $_COOKIE );
    }

    //
    //  Get the current URL taking into account https and port
    //  @link http://css-tricks.com/snippets/php/get-current-page-url/
    //  @version Refactored by @AlexParraSilva
    //
    public static function get_current_url() {

        $ignore_ports = explode( ',', FH_IGNORED_SERVER_PORTS );

        $url  = isset( $_SERVER['HTTPS'] ) && 'on' === $_SERVER['HTTPS'] ? 'https' : 'http';
        $url .= '://' . $_SERVER['SERVER_NAME'];
        $url .= in_array( $_SERVER['SERVER_PORT'], $ignore_ports ) ? '' : ':' . $_SERVER['SERVER_PORT'];
        $url .= $_SERVER['REQUEST_URI'];

        return $url;
    }

    //
    //  Custom admin check for dipshit mode and is_super_admin
    //
    public static function check_is_admin() {

        global $fh_super_admins, $current_user;

        return ( is_super_admin() && ! ( FH_DIPSHIT_MODE_ENABLED && ! in_array( $current_user->user_login, $fh_super_admins ) ) );
    }

    //
    //  Function to hide the admin bar and fix 28px html margin-top.
    //  - Use directly after html-header include
    //
    public static function kill_adminbar() {

        ob_start();

        ?>

<style type="text/css" media="screen">
html { margin-top: 0 !important; }
* html body { margin-top: 0 !important; }
#wpadminbar { display: none; }
</style>

        <?php

        echo ob_get_clean();
    }
}


?>