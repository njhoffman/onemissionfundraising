<?php

/**
 *
 * user_contactmethods.php
 *
 * Add extra contact info to user profile page via user_contactmethods
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

//
//	Add extra contact info to user profile page (user_contactmethods)
//
if ( ! function_exists( '_fh_custom_user_fields' ) ) :
function _fh_custom_user_fields( $contactmethods ) {

	// Unset default user contact methods
    unset( $contactmethods['facebook'] );
    unset( $contactmethods['twitter'] );
    unset( $contactmethods['googleplus'] );
    unset( $contactmethods['aim'] );
    unset( $contactmethods['yim'] );
    unset( $contactmethods['jabber'] );

 	global $fh_social_icons;

 	// Iterate over $fh_social_icons configuration array and set contact methods
 	foreach( $fh_social_icons as $icon_class => $icon_data ) {

 		// Only display for authors if enabled in config.php
 		if( $icon_data['author'] ) {

	 		if( $icon_class != 'rss' ) {

				// Dash throws off WordPress contact method - underscore seems fine.
				$icon_class = preg_replace( '/-/', '_', $icon_class );

				$contactmethods[ $icon_class ] = $icon_data['network'] . ' URL';
			}
 		}
 	}

    return $contactmethods;
}
add_filter('user_contactmethods', '_fh_custom_user_fields');
endif;

?>