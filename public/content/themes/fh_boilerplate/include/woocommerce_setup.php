<?php

/**
 *
 * woocommerce_setup.php
 *
 * Custom Boilerplate configuration for WooCommerce
 *
 * - Limit # products per page
 * - Queue WooCommerce FH specific JS
 * - Adds in responsive proceed to checkout button on cart page after the cart
 * - Adds a simple BS3 cart button
 * - Registers add to cart element for use in AJAX add to cart
 * - Add a wrapper for the looped product titles (vertical aligns)
 * - Add in a wrap for orderby and # of results for store pages
 * - Override up-sells to limit products and columns
 * - Modify # of colums used for shopping pages
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

if( FH_WOOCOMMERCE_SUPPORT ) {

	//
	//	Disable WooCommerce Styling
	//
	add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

	//
	//	Limit # products per page
	//
	if( ! function_exists( '_fh_woo_loop_num_products_per_page' ) ):
	function _fh_woo_loop_num_products_per_page( $cols ) {

		return FH_WOOCOMMERCE_NUM_PRODUCTS_PER_PAGE;
	}
	add_filter( 'loop_shop_per_page', '_fh_woo_loop_num_products_per_page', 20 );
	endif;

	//
	//	Queue WooCommerce FH specific assets
	//
	if( ! function_exists( '_fh_woo_queue_assets' ) ):
	function _fh_woo_queue_assets() {

		// Enqueue Custom WooCommerce JS
		wp_enqueue_script( 'fh_woocommerce', get_template_directory_uri() . '/js/woocommerce.js', false );

	}
	add_filter( 'wp_enqueue_scripts', '_fh_woo_queue_assets', 20 );
	endif;

	//
	//	Adds in responsive proceed to checkout button on cart page after the cart
	//
	if( ! function_exists( '_fh_add_responsive_checkout_btn') ):
	function _fh_add_responsive_checkout_btn() {

		?>
		<div class="visible-sm visible-xs">
			<hr class="small">
			<button id="fh_alt_checkout_proceed" class="btn btn-block btn-lg btn-success">Proceed to Checkout</button>
			<br>
		</div>
		<?php

	}
	add_filter( 'woocommerce_after_cart', '_fh_add_responsive_checkout_btn', 20 );
	endif;

	//
	//	Adds a simple BS3 cart button
	//
	if ( ! function_exists( '_fh_woo_simple_cart_link_html' ) ) :
	function _fh_woo_simple_cart_link_html(
		$classes = FH_WOOCOMMERCE_SIMPLE_LINK_CLASSES,
		$glyph = FH_WOOCOMMERCE_CART_GLYPH,
		$num_enable = FH_WOOCOMMERCE_SIMPLE_LINK_NUM_ENABLE,
		$total_enable = FH_WOOCOMMERCE_SIMPLE_LINK_TOTAL_ENABLE
	) {

		global $woocommerce;
		$count = $woocommerce->cart->cart_contents_count;
		$tmp_arr = array();

		if( $count ) {

			if( $glyph )        array_push( $tmp_arr, '<span class="' . $glyph . '"></span>' );
			if( $num_enable )   array_push( $tmp_arr, sprintf( __('%d item(s)', '%d item', $count, 'fh_boilerplate' ), $count ) );
			if( $total_enable ) array_push( $tmp_arr, '(' . $woocommerce->cart->get_cart_total() . ')' );

			$ret_html =  '<a class="cart-contents ' . $classes . '" href="' . $woocommerce->cart->get_cart_url() . '" title="' . __('View shopping cart', 'fh_boilerplate') . '">';
			$ret_html .= implode( ' ', $tmp_arr );
			$ret_html .= '</a>';

			return $ret_html;

		} else {

			return '<a class="cart-contents"></a>';
		}
	}
	endif;

	//
	//	Registers add to cart element for use in AJAX add to cart
	//
	if ( ! function_exists( '_fh_woo_header_add_to_cart_fragment' ) ) :
	function _fh_woo_header_add_to_cart_fragment( $fragments ) {

		$fragments['a.cart-contents'] = _fh_woo_simple_cart_link_html();
		return $fragments;
	}
	add_filter( 'woocommerce_add_to_cart_fragments', '_fh_woo_header_add_to_cart_fragment' );
	endif;

	//
	//  Add a wrapper for the looped product titles (vertical aligns)
	//
	if ( ! function_exists( '_fh_before_product_loop_title_wrap' ) ) :
	function _fh_before_product_loop_title_wrap() {

		echo '<span class="fh_product_loop_title">';
	}
	add_action( 'woocommerce_before_shop_loop_item_title', '_fh_before_product_loop_title_wrap', 9999 );
	add_action( 'woocommerce_before_subcategory_title', '_fh_before_product_loop_title_wrap', 9999 );
	endif;

	if ( ! function_exists( '_fh_after_product_loop_title_wrap' ) ) :
	function _fh_after_product_loop_title_wrap() {

		echo '</span>';
	}
	add_action( 'woocommerce_after_shop_loop_item_title', '_fh_after_product_loop_title_wrap', 0 );
	add_action( 'woocommerce_after_subcategory_title', '_fh_after_product_loop_title_wrap', 0 );
	endif;

	//
	//	Add in a wrap for orderby and # of results for store pages
	//
	if ( ! function_exists( '_fh_wrap_results_num_ordering_before' ) ) :
	function _fh_wrap_results_num_ordering_before() {

		echo '<div class="fh_result_count_order_wrap clearfix">';
	}
	add_action( 'woocommerce_before_shop_loop', '_fh_wrap_results_num_ordering_before', 0 );
	endif;

	if ( ! function_exists( '_fh_wrap_results_num_ordering_after' ) ) :
	function _fh_wrap_results_num_ordering_after() {

		echo '</div>';
	}
	add_action( 'woocommerce_before_shop_loop', '_fh_wrap_results_num_ordering_after', 50 );
	endif;

	//
	//	Override up-sells to limit products and columns
	//    - modeled off of the woocommerce_upsell_display() function in woocommerce-template.php
	//    - change $columns to desired number of columns
	//
	if ( ! function_exists( '_fh_woocommerce_upsell_display' ) ) :
	function _fh_woocommerce_upsell_display() {

		woocommerce_get_template( 'single-product/up-sells.php', array(
			'posts_per_page' => (string) FH_WOOCOMMERCE_SINGLE_PRODUCT_UPSELLS_LIMIT,
			'orderby'        => 'rand',
			'columns'        => FH_WOOCOMMERCE_SINGLE_PRODUCT_UPSELLS_NUM_COLS
		));
	}
	// Remove the action defined in woocommerce-hooks.php and re-add the action for display
	// remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
	// add_action( 'woocommerce_after_single_product_summary', '_fh_woocommerce_upsell_display', 15 );
	endif;

	//
	//	Modify # of colums used for shopping pages
	//
	if ( ! function_exists( '_fh_num_woo_loop_columns' ) ) :
	function _fh_num_woo_loop_columns() {

		return FH_WOOCOMMERCE_BROWSE_PRODUCT_NUM_COLS;
	}
	add_filter( 'loop_shop_columns', '_fh_num_woo_loop_columns', 999 );
	endif;

	//
	//	Utility function to get a non-formatted cart total price
	//	- Based on "get_cart_total()" from WooCommerce core (http://goo.gl/cpT565)
	//
	function _fh_woo_get_cart_total_float( $with_taxes = false, $with_discounts_applied = false, $with_shipping = false ) {

		global $woocommerce;

		$cart_contents_total = 0;

		// Force WooCommerce to calculate totals earlier
    	$woocommerce->cart->calculate_totals();

    	// If shipping included
		if( $with_shipping ) {

			$cart_contents_total += $woocommerce->cart->shipping_total;
		}

		// If prices include tax OR not taxes included
		if ( $woocommerce->cart->prices_include_tax || ! $with_taxes ) {

			$cart_contents_total += $woocommerce->cart->cart_contents_total;

		} else {

			$cart_contents_total += $woocommerce->cart->cart_contents_total + $woocommerce->cart->tax_total;
		}

		// Apply discount if applicable
		if( $with_discounts_applied ) {

			$cart_contents_total = $cart_contents_total - $woocommerce->cart->discount_total;
		}

		return floatval( $cart_contents_total );
	}
}

?>
