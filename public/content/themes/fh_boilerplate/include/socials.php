<?php

/**
 *
 * socials.php
 *
 * Logic for ACF social/RSS icons (by default in header) and "Share This:"
 * social sharing through Facebook, Twitter, and Google+
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

//
//  Logic to go through and display social icons with links
//  - Utilizes ACF global options page: http://www.advancedcustomfields.com/resources/how-to/how-to-get-values-from-an-options-page/
//  - Utilizes Font Awesome: http://fontawesome.io/
//
if ( ! function_exists( '_fh_social_icons' ) ) :
function _fh_social_icons( $links = array(), $echo = true, $object = null, $rss_icon_enabled = true ) {

    // $links = array( array( 'fa-facebook' => 'http://facebook.com/mr-orgasmo', ... ), ... );

    // If object is empty set to blog name
    $object = ( empty( $object ) ) ? get_bloginfo( 'name' ) : $object;

    global $fh_social_icons;

    // If there's nothing usable passed in then look for ACF-set options
    if( ! sizeof( $links ) ) {

        $socials_arr = array();

        // Grab ACF Repeater field
        $options = get_field( 'social_accounts', 'option' );

        if( $options ) {
            foreach( $options as $key => $option ) {

                $socials_arr[ $option['icon_key'] ] = $option['social_account_url'];
            }
        }

    } else {

        // Sets social links array to passed in value, probably for an author block
        $socials_arr = $links;
    }

    // Check for enabled RSS link
    if( $rss_icon_enabled ) {

        // Enable checkbox in ACF site options page
        $rss_enabled = get_field( 'rss_icon_enabled', 'option' );

        if( $rss_enabled == 'true' ) {

            $socials_arr[ 'rss' ] = get_bloginfo_rss( 'rss2_url' );
        }
    }

    // Holds output HTML
    $html = '';

    // Iterate over socail links array and output social links
    foreach( $socials_arr as $icon_class => $link ) {

        if( !empty( $link ) ) {

            $html .= '<a href="' . $link . '" target="_blank" class="social_icon" title="' . $object . $fh_social_icons[ $icon_class ]['join'] . $fh_social_icons[ $icon_class ]['network'] . '"><i class="fa fa-' . $icon_class . '"></i></a> ' . "\n";
        }
    }

    // Echo or return html
    if( $echo ) {

        echo $html;
        return;
    }

    return $html;
}
endif;


//
//  "Share this:" sharing HTML block for:
//  - Facebook
//  - Twitter
//  - Google+
//
if ( ! function_exists( '_fh_get_social_sharing' ) ) :
function _fh_get_social_sharing( $link, $title, $class = '', $echo = true ) {

    global $override_acf_sharing;
    global $woocommerce_content_enable;

    $queried_obj = get_queried_object();

    if(
        FH_SOCIAL_SHARING_ENABLED &&
        ! has_action( '_fh_on_page_social_sharing' ) &&
        // Provide a global method to short circuit ACF sharing (for date archives, searches, etc.)
        $override_acf_sharing ||
        (
            function_exists( 'get_field' ) &&
            (
                // WooCommerce shop page failes on $queried_obj get_field lookup:
                ( function_exists( 'is_shop' ) && ! is_shop() ) || ! function_exists( 'is_shop' )
            ) &&
            get_field( 'enable_social_sharing', $queried_obj )
        )
    ) {

        $title = urlencode( $title );

        ob_start();
        ?>
        <div id="social_share_btns">
            <span>Share This:</span>
            <ul>
                <li><a href="http://twitter.com/home?status=<?php echo $title ?>+%28<?php echo $link ?>%29" class="btn twitter" rel="nofollow" target="_blank" title="Share on Twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a href="http://www.facebook.com/sharer.php?u=<?php echo $link ?>&t=<?php echo $title ?>" class="btn facebook" rel="nofollow" target="_blank" title="Share on Facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://plus.google.com/share?url=<?php echo $link ?>"  class="btn google" rel="nofollow" target="_blank" title="Share on Google+"><i class="fa fa-google-plus"></i></a></li>
            </ul>
        </div>
        <?php

        // Echo or return html
        if( $echo ) {

            echo ob_get_clean();
            return;
        }

        return ob_get_clean();

    } else {

        do_action( '_fh_on_page_social_sharing' );
    }

    return false;
}
endif;

?>