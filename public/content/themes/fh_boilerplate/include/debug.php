<?php

/**
 *
 * debug.php
 *
 * Simple debug function(s) to be implemented during development
 * - _fh_list_hooked_functions to list all hooked WordPress functions
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

//
//  List all hooked WordPress functions
//  - DEBUG PURPOSES ONLY!
//  - http://www.wprecipes.com/list-all-hooked-wordpress-functions
//
if( ! function_exists( '_fh_list_hooked_functions' ) ):
function _fh_list_hooked_functions( $tag = false ) {

    global $wp_filter;

    if ($tag) {

        $hook[ $tag ] = $wp_filter[ $tag ];

        if( ! is_array( $hook[ $tag ] ) ) {

            trigger_error( "Nothing found for '$tag' hook", E_USER_WARNING );
            return;
        }

    } else {

        $hook = $wp_filter;
        ksort($hook);

    }

    echo '<pre>';

    foreach( $hook as $tag => $priority ) {

        echo "<br />&gt;&gt;&gt;&gt;&gt;\t<strong>$tag</strong><br />";
        ksort($priority);

        foreach($priority as $priority => $function){

            echo $priority;

            foreach($function as $name => $properties) {
                echo "\t$name<br />";
            }
        }
    }

    echo '</pre>';

    return;
}
endif;

//
//  Laravel "dd" Dump & Die debugging function
//
if ( ! function_exists('dd') ):
function dd() {

    array_map( function( $x ) { var_dump( $x ); }, func_get_args() );
    die;
}
endif;

?>