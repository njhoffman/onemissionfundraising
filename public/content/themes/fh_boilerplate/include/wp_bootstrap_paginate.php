<?php

/**
 *
 * wp_bootstrap_paginate.php
 *
 * Adds Twitter Bootstrap 3 pagination support at end of blog pages.
 * - Accepts "$custom_query" for custom WP_Query support. If $custom_query
 *   is passed it uses that for pagination, else it uses the $wp_query global
 *   object.
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

if ( ! function_exists( '_fh_paginate' ) ) :
function _fh_paginate( $custom_query = false ) {

    global $wp_query, $wp_rewrite;

    $use_query = ( $custom_query ) ? $custom_query : $wp_query;

    $current = ( $use_query->query_vars['paged'] > 1 ) ? $use_query->query_vars['paged'] : 1;

    $pagination = array(
        'base' => @add_query_arg('page','%#%'),
        'format' => '',
        'total' => $use_query->max_num_pages,
        'current' => $current,
        'show_all' => true,
        'type' => 'list',
        'next_text' => '&raquo;',
        'prev_text' => '&laquo;',
        'type' => 'array'
    );

	 // jesus fucking christ parks - njh 6/22/2016
    if( $wp_rewrite->using_permalinks() ) {
        $pagination['base'] = user_trailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );
    }

	 // need to place query at end of new pagination - njh 6/22/2016
	 $pPattern = '/([^\?]+)\/(\?[^\/]+)(\/page\/%#%\/)/i';
	 $pReplacement = '$1$3$2';
	 if (preg_match($pPattern, $pagination['base'])) {
		 $pagination['base'] = preg_replace($pPattern, $pReplacement, $pagination['base']);
	 }

    if( !empty($use_query->query_vars['s']) ) {

        $pagination['add_args'] = array( 's' => urlencode( get_query_var( 's' ) ) );
    }

    $pagination_arr = paginate_links( $pagination );

    if( sizeof( $pagination_arr ) ) {

        echo '<div class="text-center"><ul class="pagination">' . "\n";

        foreach ( $pagination_arr as $link ) {


            // Search for "page-numbers current" in each returned paginate_links array element and set $li_cass to active for Bootstrap 3
            // jmparks: Messy I know, but WordPress should really add some custom class support into their paginate_links() func
            $li_class = ( strpos( $link, 'page-numbers current' ) ) ? 'active' : '';

            echo '<li class="' . $li_class . '">' . $link . '</li>' . "\n";
        }

        echo '</ul></div>' . "\n";
    }
}
endif;

?>
