<?php

/**
 *
 * wp_bootstrap_password_form.php
 *
 * Overrides default WordPress password form with Bootstrap 3 for password protected posts
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

if( ! function_exists( '_fh_custom_password_form' ) ):
function _fh_custom_password_form() {

	global $post;

	$post  = get_post( $post );
	$label = 'pwbox-' . ( empty( $post->ID ) ? rand() : $post->ID );
	$frm_action = esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) );
	$warn =  __( 'This content is password protected. To view it please enter the content\'s password below:' );

	$output =
	'<form action="' . $frm_action . '" class="post-password-form" method="post">' .
		'<div class="alert alert-danger col-sm-4 col-sm-offset-4">' . $warn . '</div>' .
		'<div class="well col-sm-4 col-sm-offset-4">' .
			'<label for="' . $label . '">' . __( 'Password:' ) . '</label>' .
			'<div class="input-group">' .
				'<input name="post_password" id="' . $label . '" type="password" class="form-control" />' .
				'<span class="input-group-btn">' .
				'<input type="submit" name="Submit" class="btn btn-primary" value="' . esc_attr__( 'Submit' ) . '" />' .
				'</span>' .
			'</div>' .
		'</div>' .
	'</form>';

	return $output;
}
add_filter( 'the_password_form', '_fh_custom_password_form' );
endif;

?>