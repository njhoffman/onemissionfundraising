<?php

/**
 *
 * wp_bootstrap_page_links.php
 *
 * Style page links for multi-paged posts
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

/**
 * Folkhack's Link Pages
 * @author folkhack, toscha
 * @link http://wordpress.stackexchange.com/questions/14406/how-to-style-current-page-number-wp-link-pages
 * @param  array $args
 * @return void
 * Modification of wp_link_pages() with an extra element to highlight the current page.
 */

function _fh_bootstrap_link_pages( $args = array () ) {

    global $page, $numpages, $multipage, $more, $pagenow;

    // Return if not multi-page post
    if ( ! $multipage ) { return false; }

    $defaults = array(
        'before'      => '<p>' . __('Pages:'),
        'after'       => '</p>',
        'before_link' => '',
        'after_link'  => '',
        'current_before' => '',
        'current_after' => '',
        'link_before' => '',
        'link_after'  => '',
        'pagelink'    => '%',
        'echo'        => 1
    );

    $r = wp_parse_args( $args, $defaults );
    $r = apply_filters( 'wp_link_pages_args', $r );
    extract( $r, EXTR_SKIP );

    $output = $before;

    for ( $i = 1; $i < ( $numpages + 1 ); $i++ )
    {
        $j = str_replace( '%', $i, $pagelink );
        $output .= ' ';

        if ( $i != $page || ( ! $more && 1 == $page ) ) {

            $output .=
                "{$before_link}" .
                _wp_link_page( $i ) .
                "{$link_before}{$j}{$link_after}</a>{$after_link}";
        }
        else {

            $output .=
                "{$current_before}{$link_before}<a>{$j}</a>{$link_after}{$current_after}";
        }
    }

    $output .= $after;

    if( $echo ) {

        echo $output;
    }

    return $output;
}
?>