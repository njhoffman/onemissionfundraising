<?php

/**
 *
 * login_logout_menu_items.php
 *
 * Adding Login/logout link feature to administrative menu editor
 * - Refactored from:
 *  * http://www.johnmorrisonline.com/how-to-add-a-fully-functional-custom-meta-box-to-wordpress-navigation-menus/
 *  * http://www.johnmorrisonline.com/how-to-add-a-loginlogout-link-to-wordpress-menus/
 *
 * DEPRECIATED - not working as of WordPress 3.9.2 and Boilerplate 1.5
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

// Adds "Logout" text to nav item whne logged in
if ( ! function_exists( '_fh_login_menu_logout_link' ) ):
function _fh_login_menu_logout_link( $objects ) {

	// Display when logout link when logged in
	if ( is_user_logged_in() ) {

		// If they are logged in, we search through the objects for items with the 
		// class fh-login-pop and we change the text and url into a logout link
		foreach ( $objects as $k => $object ) {

			if ( in_array( 'fh-login-pop', $object->classes ) ) {

				$objects[$k]->title = 'Logout';

				// Look for fa-sign-in within the attr_title and replace with log-out
				// - Would be set as a feature to the BS3.x navwalker in "wp_bootstrap_navwalker.php"
				if( strpos( 'fa-sign-in', $objects[$k]->attr_title ) !== false ) {

					$objects[$k]->attr_title = 'fa-sign-out';
				}

				$objects[$k]->url = wp_logout_url();
				$remove_key = array_search( 'fh-login-pop', $object->classes );

				unset( $objects[$k]->classes[$remove_key] );
			}
		}
	}

	return $objects;
}
add_filter('wp_nav_menu_objects', '_fh_login_menu_logout_link', 10, 2);
endif;

// Adds Administration Meta Boxes
if ( ! function_exists( '_fh_login_menu_admin_meta_boxes' ) ):
function _fh_login_menu_admin_meta_boxes( $objects ) {

	// Add the custom Administration box
	add_meta_box(
		'fh_login_nav_link',
		__('Login/Logout'),
		'_fh_login_menu_admin_meta_box_html',
		'nav-menus',
		'side',
		'low'
	);
}
add_action('admin_init', '_fh_login_menu_admin_meta_boxes' );
endif;

// Administration Meta Boxes HTML
if ( ! function_exists( '_fh_login_menu_admin_meta_box_html' ) ):
function _fh_login_menu_admin_meta_box_html( $objects ) {
	{ ?>
	<div id="posttype-fh-login" class="posttypediv">
		<div id="tabs-panel-folkhack-login" class="tabs-panel tabs-panel-active">
			<ul id ="folkhack-login-checklist" class="categorychecklist form-no-clear">
				<li>
					<label class="menu-item-title">
						<input type="checkbox" class="menu-item-checkbox" name="menu-item[-1][menu-item-object-id]" value="-1"> Login/Logout Link
					</label>
					<input type="hidden" class="menu-item-type" name="menu-item[-1][menu-item-type]" value="custom">
					<input type="hidden" class="menu-item-title" name="menu-item[-1][menu-item-title]" value="Login">
					<input type="hidden" class="menu-item-url" name="menu-item[-1][menu-item-url]" value="<?php bloginfo('wpurl'); ?>/wp-login.php">
					<input type="hidden" class="menu-item-classes" name="menu-item[-1][menu-item-classes]" value="fh-login-pop">
				</li>
			</ul>
		</div>
		<p class="button-controls">
			<span class="list-controls">
				<a href="/wordpress/wp-admin/nav-menus.php?page-tab=all&amp;selectall=1#posttype-page" class="select-all">Select All</a>
			</span>
			<span class="add-to-menu">
				<input type="submit" class="button-secondary submit-add-to-menu right" value="Add to Menu" name="add-post-type-menu-item" id="submit-posttype-fh-login">
				<span class="spinner"></span>
			</span>
		</p>
	</div>
	<?php }
}
endif;

?>