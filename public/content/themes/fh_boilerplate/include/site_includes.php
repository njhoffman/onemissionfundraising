<?php

/**
 *
 * site_includes.php
 *
 * Registers JS then CSS assets to be used for boilerplate. Handles config
 * for minified and blessed CSS assets. Passes configuration to main core
 * JS via localization.
 *
 * - JavaScript / CSS Site Asset Loading
 * - Minified asset lookup (handled manually)
 * - Blessed CSS asset lookup
 * - Loads in base site 3rd party libraries:
 *   - jQuery
 *   - Modernizr & Respond - legacy browser support
 *   - Bootstrap 3
 *   - Magnific Popup - responsive jQuery lightbox & dialog plugin
 *   - qr.js - Generates QR codes
 *   - Smooth Scroll - Smooth scrolling - ex: back-to-top
 *   - Sidr - fly-out responsive sidebars
 *   - Boilerplate Core and Site JS
 *     - core.js (in parent)
 *     - main.js (in child)
 *   - Font Awesome - extended icons
 *   - Opensans - custom site fonts
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

//
//  JavaScript / CSS Site Asset Loading (wp_enqueue_scripts)
//  - Uses get_template_directory_uri() for non-child-theme-override-able assets and get_stylesheet_directory_uri() otherwise
//
if ( ! function_exists( '_fh_site_includes' ) ) :
function _fh_site_includes() {

    //
    //  Looks up theme minified assets, managed manually
    //
    function mini_lookup( $asset = '' ) {

        //
        //  Registered minified assets
        //
        $minified_assets_lookup = array(

            'jquery.js'               => 'jquery.min.js',
            'jquery.magnific.js'      => 'jquery.magnific.min.js',
            'jquery.smooth-scroll.js' => 'jquery.smooth-scroll.min.js',
            'respond.js'              => 'respond.min.js',
            'qr.js'                   => 'qr.min.js',

            // CSS
            'bootstrap.css'           => 'bootstrap.min.css',
            'bootstrap.blessed.css'   => 'bootstrap.blessed.min.css',
            'boilerplate.css'         => 'boilerplate.min.css',
            'font_awesome.css'        => 'font_awesome.min.css',
            'open_sans.css'           => 'open_sans.min.css'
        );

        if(
            !empty( $asset ) &&
            isset( $minified_assets_lookup[ $asset ] ) &&
            FH_MINIFIED_ASSETS_ENABLED
        ) {

            // Look for registered minified asset and return minified version
            return $minified_assets_lookup[ $asset ];
        }

        return $asset;
    }

    // JavaScript directories
    $vendor_js  = get_template_directory_uri() . '/js/vendor';
    $parent_js  = get_template_directory_uri() . '/js';
    $child_js   = get_stylesheet_directory_uri() . '/js';

    // CSS directories
    $vendor_css = get_stylesheet_directory_uri() . '/css/vendor';
    $parent_css = get_template_directory_uri() . '/css';
    $child_css  = get_stylesheet_directory_uri() . '/css';

    //
    //  Modernizr & Respond - legacy browser support
    //  - Should be enqueued first
    //
    wp_register_script( 'modernizr', $vendor_js . '/' . mini_lookup( 'modernizr.js' ) );
    wp_enqueue_script( 'modernizr' );
    wp_register_script( 'respond', $vendor_js . '/respond/' . mini_lookup( 'respond.js' ) );
    wp_enqueue_script( 'respond' );

    //
    //  jQuery
    //  - if config option set deregister WP's default jQuery version
    //  - Should be enqueued second
    //
    if( FH_OVERRIDE_JQUERY_WP_DEFAULT ) {

        wp_deregister_script( 'jquery' );
        wp_register_script( 'jquery', $vendor_js . '/jquery/' . mini_lookup( 'jquery.js' ) );
        wp_enqueue_script( 'jquery' );
    }

    //
    //  Bootstrap 3 JS
    //  - Should be enqueued third
    //
    wp_register_script( 'bootstrap', $vendor_js . '/bootstrap/' . mini_lookup( 'bootstrap.js' ), array( 'jquery' ) );
    wp_enqueue_script( 'bootstrap' );

    //
    //  Misc. Vendor / Site JS
    //
    // - Magnific Popup - responsive jQuery lightbox & dialog plugin
    wp_register_script( 'magnific_popup', $vendor_js . '/magnific/' . mini_lookup( 'jquery.magnific.js'), array( 'jquery' ) );
    wp_enqueue_script( 'magnific_popup' );

    // - qr.js - Generates QR codes
    wp_register_script( 'qrjs', $vendor_js . '/qrjs/' . mini_lookup( 'qr.js') );
    wp_enqueue_script( 'qrjs' );

    // - Smooth Scroll - Smooth scrolling - ex: back-to-top
    wp_register_script( 'smooth-scroll', $vendor_js . '/smooth-scroll/' . mini_lookup( 'jquery.smooth-scroll.js') );
    wp_enqueue_script( 'smooth-scroll' );

    //  - Sidr for fly-out responsive sidebars
    if( FH_SIDR_ENABLED ) {

        wp_register_script( 'sidr', $vendor_js . '/sidr/' . mini_lookup( 'jquery.sidr.min.js' ), array( 'jquery' ) );
        wp_enqueue_script( 'sidr' );
    }

    // Define base required assets for site to load
    $base_required_assets = array( 'jquery', 'bootstrap', 'magnific_popup' );

    //
    //  Boilerplate Core and Site JS
    //  - Setup array of configuration values to expose to the JS side of things
    //  via localization: http://codex.wordpress.org/Function_Reference/wp_localize_script
    //
    wp_register_script( 'boilerplate_core', $parent_js . '/' . mini_lookup( 'core.js' ), $base_required_assets );

    $js_config_expose = array(
        'fh_sticky_header' => FH_STICKY_HEADER,
        'fh_sticky_sidebars' => FH_STICKY_SIDEBARS,
        'fh_sidr' => FH_SIDR_ENABLED,
        'fh_backtotop_show_at' => FH_BACKTOTOP_SHOW_AT
    );
    wp_localize_script( 'boilerplate_core', 'fhbp_config', $js_config_expose );
    wp_register_script( 'site', $child_js . '/' . mini_lookup( 'main.js' ), $base_required_assets );

    wp_enqueue_script( 'boilerplate_core' ); // Folkhack's Boilerplate core JS asset
    wp_enqueue_script( 'site' );             // Site asset (either parent or child)

    //
    //  CSS registration
    //

    // Check if blessed CSS assets is turned on (IE fix for max 4096 CSS selectors)
    // - http://blesscss.com/
    $bootstrap_css_asset = ( FH_USE_BLESSED_CSS ) ? 'bootstrap.blessed.css' : 'bootstrap.css';

    // Bootstrap + Boilerplate
    wp_register_style( 'bootstrap', $child_css . '/' . mini_lookup( $bootstrap_css_asset ), '', '', 'all' );
    wp_register_style( 'boilerplate', $child_css . '/' . mini_lookup( 'boilerplate.css' ), '', '', 'all' );
    wp_enqueue_style( 'bootstrap' );
    wp_enqueue_style( 'boilerplate' );

    // Open Sans
    if( FH_USE_OPEN_SANS ) {

        wp_register_style( 'open_sans', $child_css . '/' . mini_lookup( 'open_sans.css' ), '', '', 'all' );
        wp_enqueue_style( 'open_sans' );
    }

    // Font Awesome
    if( FH_USE_FONT_AWESOME ) {

        wp_register_style( 'font_awesome', $child_css . '/' . mini_lookup( 'font_awesome.css' ), '', '', 'all' );
        wp_enqueue_style( 'font_awesome' );
    }

    //
    //  Misc. Vendor / Site CSS
    //
    //  - Magnific popup for image zooming
    wp_register_style( 'magnific_popup', $vendor_js . '/magnific/' . mini_lookup( 'magnific.css' ), '', '', 'screen' );
    wp_enqueue_style( 'magnific_popup' );
}
add_action( 'wp_enqueue_scripts', '_fh_site_includes' );
endif;


//
//  Adds a CSS stylesheet for the administrative area
//
if ( ! function_exists( '_fh_admin_css' ) ) :
function _fh_admin_css() {

    wp_enqueue_style( 'custom_admin', get_stylesheet_directory_uri() . '/css/admin.css', false );
}
add_action( 'admin_enqueue_scripts', '_fh_admin_css' );
endif;

?>