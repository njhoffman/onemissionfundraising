<?php

/**
 *
 * acf.php
 *
 * Controls all ACF functionality for base Boilerplate. Config driven via config.php
 *
 * - Contact Us w/Google Maps
 * - Page/post Galleries
 * - Post Format: Image
 * - Post Format: Link
 * - Post Format: Quote
 * - Post Format: Status
 * - Social Sharing Checkbox (enable social sharing)
 * - Site-wide options page
 *     - Social accounts
 *     - Enable RSS
 * 	   - Site-wide temporary CSS
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

if(function_exists("register_field_group"))
{

	//
	//	Contact Us w/Google Maps
	//
	$contact_fields = array();

	if( FH_CONTACT_GMAPS_ENABLED ) {

		array_push( $contact_fields,
			array (
				'key' => 'field_52ae3f59f369d',
				'label' => 'Location',
				'name' => 'location',
				'type' => 'google_map',
				'required' => 0,
				'val' => 'address',
				'center_lat' => '32.7150',
				'center_lng' => '-117.1625',
				'zoom' => 10,
				'scrollwheel' => 1,
				'mapTypeControl' => 1,
				'streetViewControl' => 1,
				'PointOfInterest' => 1,
			),
			array (
				'key' => 'field_52ae3f70f369e',
				'label' => 'Zoom Level',
				'name' => 'zoom_level',
				'type' => 'number',
				'required' => 0,
				'default_value' => 8,
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => 0,
				'max' => 21,
				'step' => '',
			),
			array (
				'key' => 'field_52ae3f8af369f',
				'label' => 'Marker Title',
				'name' => 'marker_title',
				'type' => 'text',
				'required' => 0,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_52ae3f98f36a0',
				'label' => 'Marker Content',
				'name' => 'marker_content',
				'type' => 'wysiwyg',
				'required' => 0,
				'default_value' => '',
				'toolbar' => 'basic',
				'media_upload' => 'no',
			)
		);
	}

	if( FH_CONTACT_DRIVING_DIRECTIONS_ENABLED ) {

		array_push( $contact_fields,
			array (
				'key' => 'field_52ae3fa7f36a1',
				'label' => 'Driving Directions URL',
				'name' => 'driving_directions_url',
				'type' => 'text',
				'instructions' => 'https://maps.google.com?daddr=1234+Test+ST+San+Diego+CA+92103',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			)
		);
	}

	if( FH_CONTACT_TEXT_ADDR_ENABLED ) {

		array_push( $contact_fields,
			array (
				'key' => 'field_5388afb90ec46',
				'label' => 'Text Address',
				'name' => 'text_address',
				'type' => 'wysiwyg',
				'required' => 0,
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'no',
			)
		);
	}

	if( FH_CONTACT_PHONE_NUM_ENABLED ) {

		array_push( $contact_fields,
			array (
				'key' => 'field_5388aff10ec47',
				'label' => 'Phone Number',
				'name' => 'phone_number',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '555-555-5555',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			)
		);
	}

	if( FH_CONTACT_FAX_NUM_ENABLED ) {

		array_push( $contact_fields,
			array (
				'key' => 'field_5388aff10dd00',
				'label' => 'Fax Number',
				'name' => 'fax_number',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '555-555-5555',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			)
		);
	}

	if( FH_CONTACT_EMAIL_ADDR_ENABLED ) {

		array_push( $contact_fields,
			array (
				'key' => 'field_5388b1efed327',
				'label' => 'Email Address',
				'name' => 'email_address',
				'type' => 'email',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
			)
		);
	}

	if( ! empty( $contact_fields ) ) {

		register_field_group(array (
			'id' => 'acf_contact-us-custom-fields',
			'title' => 'Contact Us Custom Fields',
			'fields' => $contact_fields,
			'location' => array (
				array (
					array (
						'param' => 'page_template',
						'operator' => '==',
						'value' => 'templates/template_contact.php',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'no_box',
				'hide_on_screen' => array (
					0 => 'permalink',
					1 => 'excerpt',
					2 => 'custom_fields',
					3 => 'discussion',
					4 => 'comments',
					5 => 'slug',
					6 => 'author',
					7 => 'featured_image',
					8 => 'categories',
					9 => 'tags',
					10 => 'send-trackbacks',
				),
			),
			'menu_order' => 0,
		));
	}


	//
	//	Pages Galleries
	//	Post Format: Gallery
	//
	if( FH_GALLERIES_ENABLED ) {

		// Gallery Types/Choices
		$gal_choices_arr = array(
			'No Gallery' => 'No Boilerplate-driven Gallery'
		);

		if( FH_GAL_MASONRY_ENABLED ) {

			$gal_choices_arr['Masonry One Column Width'] = 'Masonry One Column Width';
			$gal_choices_arr['Masonry Two Column Widths'] = 'Masonry Two Column Widths';
		}

		if( FH_GAL_MIXITUP_ENABLED ) {

			$gal_choices_arr['MixItUp'] = 'MixItUp';
		}

		if( FH_GAL_BS3_CAROUSEL_ENABLED ) {

			$gal_choices_arr['Carousel'] = 'Carousel';
		}

		//
		//	Start Global Gallery Fields
		//	- Gallery Type
		//	- Gallery Title
		//
		$gal_fields = array(

			// Gallery Type
			array (
				'key' => 'field_52af865d67e58',
				'label' => 'Gallery Type',
				'name' => 'gallery_type',
				'type' => 'radio',
				'required' => 1,
				'choices' => $gal_choices_arr,
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => 'No Gallery',
				'layout' => 'vertical',
			),

			// Gallery Title
			array (
				'key' => 'field_52afad2b1e8f4',
				'label' => 'Gallery Title',
				'name' => 'gallery_title',
				'type' => 'text',
				'instructions' => 'Title that displays before gallery',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_52af865d67e58',
							'operator' => '==',
							'value' => 'Carousel',
						),
						array (
							'field' => 'field_52af865d67e58',
							'operator' => '==',
							'value' => 'Masonry One Column Width',
						),
						array (
							'field' => 'field_52af865d67e58',
							'operator' => '==',
							'value' => 'Masonry Two Column Widths',
						),
						array (
							'field' => 'field_52af865d67e58',
							'operator' => '==',
							'value' => 'MixItUp',
						),
					),
					'allorany' => 'any',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			)
		);

		//
		//	Add Carousel Specific Fields
		//	- Display Carousel Controls
		//  - Display Carousel Indicators
		//  - Display Carousel Image Captions
		//  - Enable Images to View Full-size on Carousel Click
		//  - Carousel Slide Duration
		//
		if( FH_GAL_BS3_CAROUSEL_ENABLED ) {

			array_push( $gal_fields,
				array (
					'key' => 'field_53e7d4c9c35c8',
					'label' => 'Display Carousel Controls',
					'name' => 'display_carousel_controls',
					'type' => 'true_false',
					'instructions' => '',
					'conditional_logic' => array (
						'status' => 1,
						'rules' => array (
							array (
								'field' => 'field_52af865d67e58',
								'operator' => '==',
								'value' => 'Carousel',
							),
						),
						'allorany' => 'all',
					),
					'message' => 'Display left/right chevron carousel controls',
					'default_value' => 1,
				),
				array (
					'key' => 'field_53e7d534c35c9',
					'label' => 'Display Carousel Indicators',
					'name' => 'display_carousel_indicators',
					'type' => 'true_false',
					'instructions' => '',
					'conditional_logic' => array (
						'status' => 1,
						'rules' => array (
							array (
								'field' => 'field_52af865d67e58',
								'operator' => '==',
								'value' => 'Carousel',
							),
						),
						'allorany' => 'all',
					),
					'message' => 'Display carousel slideshow position dot indicators',
					'default_value' => 1,
				),
				array (
					'key' => 'field_53e7d56bc35ca',
					'label' => 'Display Carousel Image Captions',
					'name' => 'display_carousel_image_captions',
					'type' => 'true_false',
					'instructions' => '',
					'conditional_logic' => array (
						'status' => 1,
						'rules' => array (
							array (
								'field' => 'field_52af865d67e58',
								'operator' => '==',
								'value' => 'Carousel',
							),
						),
						'allorany' => 'all',
					),
					'message' => 'Display captions generated from image "Title"',
					'default_value' => 1,
				),
				array (
					'key' => 'field_53e7d5b4c35cb',
					'label' => 'Enable Images to View Full-size on Carousel Click',
					'name' => 'enable_carousel_image_links',
					'type' => 'true_false',
					'instructions' => '',
					'conditional_logic' => array (
						'status' => 1,
						'rules' => array (
							array (
								'field' => 'field_52af865d67e58',
								'operator' => '==',
								'value' => 'Carousel',
							),
						),
						'allorany' => 'all',
					),
					'message' => 'On click of carousel image overlay full-sized version of image',
					'default_value' => 1,
				),
				array (
					'key' => 'field_53e7d9648c69f',
					'label' => 'Carousel Slide Duration',
					'name' => 'carousel_slide_duration',
					'type' => 'number',
					'instructions' => 'Duration in milliseconds for each slide',
					'required' => 1,
					'conditional_logic' => array (
						'status' => 1,
						'rules' => array (
							array (
								'field' => 'field_52af865d67e58',
								'operator' => '==',
								'value' => 'Carousel',
							),
						),
						'allorany' => 'all',
					),
					'default_value' => 5000,
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'min' => 0,
					'max' => '',
					'step' => '',
				)
			);
		}

		//
		//	Push rest of global gallery fields
		//  - Before/after content
		//	- Gallery images
		//
		array_push( $gal_fields,
			array (
				'key' => 'field_52afae531e8f5',
				'label' => 'Gallery Order',
				'name' => 'gallery_order',
				'type' => 'radio',
				'required' => 1,
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_52af865d67e58',
							'operator' => '==',
							'value' => 'Carousel',
						),
						array (
							'field' => 'field_52af865d67e58',
							'operator' => '==',
							'value' => 'Masonry One Column Width',
						),
						array (
							'field' => 'field_52af865d67e58',
							'operator' => '==',
							'value' => 'Masonry Two Column Widths',
						),
						array (
							'field' => 'field_52af865d67e58',
							'operator' => '==',
							'value' => 'MixItUp',
						),
					),
					'allorany' => 'any',
				),
				'choices' => array (
					'Gallery Before Content' => 'Gallery Before Content',
					'Gallery After Content' => 'Gallery After Content',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => 'Gallery After Content',
				'layout' => 'vertical',
			),
			array (
				'key' => 'field_52ae8f268b36d',
				'label' => 'Gallery Images',
				'name' => 'gallery',
				'type' => 'gallery',
				'preview_size' => 'thumbnail',
				'library' => 'all',
				'conditional_logic' => array (
					'status' => 1,
					'rules' => array (
						array (
							'field' => 'field_52af865d67e58',
							'operator' => '==',
							'value' => 'Carousel',
						),
						array (
							'field' => 'field_52af865d67e58',
							'operator' => '==',
							'value' => 'Masonry One Column Width',
						),
						array (
							'field' => 'field_52af865d67e58',
							'operator' => '==',
							'value' => 'Masonry Two Column Widths',
						),
						array (
							'field' => 'field_52af865d67e58',
							'operator' => '==',
							'value' => 'MixItUp',
						),
					),
					'allorany' => 'any',
				),
			)
		);


		//
		//	Gallery Locations
		//
		$gal_locations = array();

		if( FH_POST_GALLERIES_ENABLED ) {

			array_push( $gal_locations,
				array (
					array (
						'param' => 'post_format',
						'operator' => '==',
						'value' => 'gallery',
						'order_no' => 0,
						'group_no' => 0,
					),
				)
			);
		}

		if( FH_PAGE_GALLERIES_ENABLED ) {

			array_push( $gal_locations,
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'page',
						'order_no' => 0,
						'group_no' => 1,
					),
				)
			);
		}

		//
		//	Register Gallery ACF Fields
		//
		register_field_group(array (
			'id' => 'acf_gallery-post-format',
			'title' => 'Post Format: Gallery and Gallery on Page Support',
			'fields' => $gal_fields,
			'location' => $gal_locations,
			'options' => array (
				'position' => 'normal',
				'layout' => 'no_box',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
	}


	//
	//	Post Format: Image
	//
	if( FH_IMAGE_POST_FORMAT_ENABLED ) {

		register_field_group(array (
			'id' => 'acf_image-post-format',
			'title' => 'Image Post Format',
			'fields' => array (
				array (
					'key' => 'field_52afcf5d67a93',
					'label' => 'Image',
					'name' => 'image',
					'type' => 'image',
					'required' => 1,
					'save_format' => 'object',
					'preview_size' => 'thumbnail',
					'library' => 'all',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_format',
						'operator' => '==',
						'value' => 'image',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'acf_after_title',
				'layout' => 'no_box',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
	}


	//
	//	Post Format: Link
	//
	if( FH_LINK_POST_FORMAT_ENABLED ) {

		register_field_group(array (
			'id' => 'acf_link-post-format',
			'title' => 'Link Post Format',
			'fields' => array (
				array (
					'key' => 'field_52afb769e69f2',
					'label' => 'Link URL',
					'name' => 'link_url',
					'type' => 'text',
					'required' => 1,
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'formatting' => 'none',
					'maxlength' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_format',
						'operator' => '==',
						'value' => 'link',
						'order_no' => 0,
						'group_no' => 0,
					),
				)
			),
			'options' => array (
				'position' => 'acf_after_title',
				'layout' => 'no_box',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
	}


	//
	//	Post Format: Quote
	//
	if( FH_QUOTE_POST_FORMAT_ENABLED ) {

		register_field_group(array (
			'id' => 'acf_quote-post-format',
			'title' => 'Quote Post Format',
			'fields' => array (
				array (
					'key' => 'field_52b4fbd6fbc7b',
					'label' => 'Quote',
					'name' => 'quote',
					'type' => 'wysiwyg',
					'required' => 1,
					'default_value' => '',
					'toolbar' => 'full',
					'media_upload' => 'yes',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_format',
						'operator' => '==',
						'value' => 'quote',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'acf_after_title',
				'layout' => 'no_box',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
	}


	//
	//	Post Format: Status
	//
	if( FH_STATUS_POST_FORMAT_ENABLED ) {

		register_field_group(array (
			'id' => 'acf_status-post-format',
			'title' => 'Status Post Format',
			'fields' => array (
				array (
					'key' => 'field_52b4fc0be7839',
					'label' => 'Status',
					'name' => 'status',
					'type' => 'wysiwyg',
					'required' => 1,
					'default_value' => '',
					'toolbar' => 'basic',
					'media_upload' => 'no',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_format',
						'operator' => '==',
						'value' => 'status',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'acf_after_title',
				'layout' => 'no_box',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
	}


	//
	//	Social Sharing Checkbox (enable social sharing)
	//
	if( FH_SOCIAL_SHARING_ENABLED ) {

		// Setup social sharing locations
		$share_on_locations_arr = array();

		if( FH_SOCIAL_SHARE_ON_POSTS ) {

			array_push( $share_on_locations_arr,
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'post',
						'order_no' => 1,
						'group_no' => 1,
					)
				)
			);
		}
		if( FH_SOCIAL_SHARE_ON_PRODUCTS ) {

			array_push( $share_on_locations_arr,
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'product',
						'order_no' => 0,
						'group_no' => 2,
					),
				)
			);
		}
		if( FH_SOCIAL_SHARE_ON_TAXONOMY || FH_SOCIAL_SHARE_ON_SHOP_TAXONOMY ) {

			array_push( $share_on_locations_arr,
				array (
					array (
						'param' => 'ef_taxonomy',
						'operator' => '==',
						'value' => 'all',
						'order_no' => 0,
						'group_no' => 3,
					),
				)
			);
		}
		if( FH_SOCIAL_SHARE_ON_PAGES ) {

			array_push( $share_on_locations_arr,
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'page',
						'order_no' => 0,
						'group_no' => 4,
					)
				)
			);
		}
		if( FH_SOCIAL_SHARE_ON_ATTACHMENTS ) {

			array_push( $share_on_locations_arr,
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'attachment',
						'order_no' => 0,
						'group_no' => 5,
					),
				)
			);
		}

		if( ! empty( $share_on_locations_arr ) ) {

			register_field_group(array (
				'id' => 'acf_share-page-option',
				'title' => 'Social Share Option',
				'fields' => array (
					array (
						'key' => 'field_52adddeacdb6e',
						'label' => 'Enable Social Sharing',
						'name' => 'enable_social_sharing',
						'type' => 'true_false',
						'message' => 'Enable Twitter, Facebook, and Google+ sharing buttons below content',
						'default_value' => 0,
					),
				),
				'location' => $share_on_locations_arr,
				'options' => array (
					'position' => 'normal',
					'layout' => 'no_box',
					'hide_on_screen' => array (),
				),
				'menu_order' => 0,
			));
		}
	}


	//
	//	Site-wide options page
	//	- Social accounts
	//	- Enable RSS
	//
	if( FH_SITE_OPTION_DRIVEN_SOCIALS ) {

		$social_list_style = 'list-style: disc; padding-left: 30px; width: 12%; float: left; margin-top: -5px; margin-bottom: 5px;';

		register_field_group(array (
			'id' => 'acf_site-wide-configuration-options',
			'title' => 'Site Wide Configuration Options',
			'fields' => array (
				array (
					'key' => 'field_52ae2b404c48b',
					'label' => 'Social Accounts',
					'name' => 'social_accounts',
					'type' => 'repeater',
					'instructions' => '
					Choose from one of the following keys (copy-paste into "Icon Key"):
					<ul style="' . $social_list_style . '">
						<li>facebook</li>
						<li>twitter</li>
						<li>google-plus</li>
						<li>linkedin</li>
						<li>tumblr</li>
						<li>flickr</li>
						<li>instagram</li>
						<li>pinterest</li>
					</ul>
					<ul style="' . $social_list_style . '">
						<li>youtube</li>
						<li>vimeo-square</li>
						<li>vine</li>
						<li>github</li>
						<li>dribbble</li>
						<li>adn</li>
						<li>android</li>
						<li>apple</li>
					</ul>
					<ul style="' . $social_list_style . '">
						<li>behance</li>
						<li>bitbucket</li>
						<li>codepen</li>
						<li>delicious</li>
						<li>deviantart</li>
						<li>digg</li>
						<li>dropbox</li>
						<li>foursquare</li>
					</ul>
					<ul style="' . $social_list_style . '">
						<li>git</li>
						<li>gittip</li>
						<li>hacker-news</li>
						<li>linux</li>
						<li>openid</li>
						<li>pagelines</li>
						<li>reddit</li>
						<li>renren</li>
					</ul>
					<ul style="' . $social_list_style . '">
						<li>skype</li>
						<li>soundcloud</li>
						<li>spotify</li>
						<li>stack-exchange</li>
						<li>stack-overflow</li>
						<li>steam</li>
						<li>stumbleupon</li>
						<li>trello</li>
					</ul>
					<ul style="' . $social_list_style . '">
						<li>vk</li>
						<li>weibo</li>
						<li>windows</li>
						<li>wordpress</li>
						<li>xing</li>
						<li>yahoo</li>
					</ul>
					',
					'sub_fields' => array (
						array (
							'key' => 'field_52ae2bc44c48c',
							'label' => 'Icon Key',
							'name' => 'icon_key',
							'type' => 'text',
							'instructions' => 'ex: "facebook"',
							'column_width' => '',
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'formatting' => 'none',
							'maxlength' => '',
						),
						array (
							'key' => 'field_52ae2bf04c48d',
							'label' => 'Social Account URL',
							'name' => 'social_account_url',
							'type' => 'text',
							'instructions' => 'ex: "https://facebook.com/fh_boilerplate"',
							'column_width' => '',
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'formatting' => 'none',
							'maxlength' => '',
						),
					),
					'row_min' => 0,
					'row_limit' => '',
					'layout' => 'table',
					'button_label' => 'Add Account',
				),
				array (
					'key' => 'field_53e7b9df9e650',
					'label' => 'Display RSS Feed Icon After Social Icons',
					'name' => 'rss_icon_enabled',
					'type' => 'true_false',
					'instructions' => 'Display a RSS icon at the end of the social icons that links to the default WordPress blog RSS feed',
					'message' => '',
					'default_value' => 0,
				)
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'acf-options',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'no_box',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
	}

	//
	//	Site-wide options page
	//	- Site-wide temporary CSS
	//
	if( FH_SITE_TEMP_CSS_ENABLED ) {

		register_field_group(array (
			'id' => 'acf_site-wide-temporary-css',
			'title' => 'Site-wide Temporary CSS',
			'fields' => array (
				array (
					'key' => 'field_54078302d24ff',
					'label' => 'Site-wide Temporary CSS',
					'name' => 'sitewide_temp_css',
					'type' => 'textarea',
					'instructions' => 'Add temporary site-wide CSS into site header - FOR TEMPORARY FIX/USE ONLY! No HTML! (do not include style tags)',
					'default_value' => '',
					'placeholder' => '',
					'maxlength' => '',
					'rows' => 3,
					'formatting' => 'none',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'acf-options',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'no_box',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
	}
}

?>