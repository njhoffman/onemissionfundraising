<?php

/**
 *
 * sidebars.php
 *
 * Sidebar and footer widget locations configurations/registration and Sidr fly-out sidebar
 * support.
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

//
//	Sidebar configurations/registration
//
if ( ! function_exists( '_fh_sidebar_widgets' ) ) :
function _fh_sidebar_widgets() {

	if( FH_SIDEBAR_A_ENABLED ) {

		for( $i = 1; $i <= FH_SIDEBAR_A_SEGMENTS; $i++ ) {

			$sidebar_id = FH_SIDEBAR_A_ID . '_' . $i;

			register_sidebar( array(
				'name'          => __( FH_SIDEBAR_A_NAME . ' - Segment #' . $i, 'fh_boilerplate' ),
				'description'   => __( 'Shows on pages (' . $sidebar_id . ')' ),
				'id'            => $sidebar_id,
				'before_widget' => '<li>',
				'after_widget'  => '</li>',
				'before_title'  => '<h4>',
				'after_title'   => '</h4>'
			));
		}
	}

	if( FH_SIDEBAR_B_ENABLED ) {

		for( $i = 1; $i <= FH_SIDEBAR_B_SEGMENTS; $i++ ) {

			$sidebar_id = FH_SIDEBAR_B_ID . '_' . $i;

			register_sidebar( array(
				'name'          => __( FH_SIDEBAR_B_NAME . ' - Segment #' . $i, 'fh_boilerplate' ),
				'description'   => __( 'Shows on pages (' . $sidebar_id . ')' ),
				'id'            => $sidebar_id,
				'before_widget' => '<li>',
				'after_widget'  => '</li>',
				'before_title'  => '<h4>',
				'after_title'   => '</h4>'
			));
		}
	}

	//
	//	WooCommerce Sidebars
	//
	if( FH_WOOCOMMERCE_SUPPORT && ! FH_WOO_DEFAULT_SIDEBARS ) {


		if( FH_WOO_SIDEBAR_A_ENABLED ) {

			for( $i = 1; $i <= FH_WOO_SIDEBAR_A_SEGMENTS; $i++ ) {

				$sidebar_id = FH_WOO_SIDEBAR_A_ID . '_' . $i;

				register_sidebar( array(
					'name'          => __( FH_WOO_SIDEBAR_A_NAME . ' - Segment #' . $i, 'fh_boilerplate' ),
					'description'   => __( 'Shows on shopping pages (' . $sidebar_id . ')' ),
					'id'            => $sidebar_id,
					'before_widget' => '<li>',
					'after_widget'  => '</li>',
					'before_title'  => '<h4>',
					'after_title'   => '</h4>'
				));
			}
		}

		if( FH_WOO_SIDEBAR_B_ENABLED ) {

			for( $i = 1; $i <= FH_WOO_SIDEBAR_B_SEGMENTS; $i++ ) {

				$sidebar_id = FH_WOO_SIDEBAR_B_ID . '_' . $i;

				register_sidebar( array(
					'name'          => __( FH_WOO_SIDEBAR_B_NAME . ' - Segment #' . $i, 'fh_boilerplate' ),
					'description'   => __( 'Shows on shopping pages (' . $sidebar_id . ')' ),
					'id'            => $sidebar_id,
					'before_widget' => '<li>',
					'after_widget'  => '</li>',
					'before_title'  => '<h4>',
					'after_title'   => '</h4>'
				));
			}
		}
	}

	if( FH_BLOG_ENABLED && ! FH_BLOG_DEFAULT_SIDEBARS ) {

		if( FH_BLOG_SIDEBAR_A_ENABLED ) {

			for( $i = 1; $i <= FH_BLOG_SIDEBAR_A_SEGMENTS; $i++ ) {

				$sidebar_id = FH_BLOG_SIDEBAR_A_ID . '_' . $i;

				register_sidebar( array(
					'name'          => __( FH_BLOG_SIDEBAR_A_NAME . ' - Segment #' . $i, 'fh_boilerplate' ),
					'description'   => __( 'Shows on blog pages (' . $sidebar_id . ')' ),
					'id'            => $sidebar_id,
					'before_widget' => '<li>',
					'after_widget'  => '</li>',
					'before_title'  => '<h4>',
					'after_title'   => '</h4>'
				));
			}
		}

		if( FH_BLOG_SIDEBAR_B_ENABLED ) {

			for( $i = 1; $i <= FH_BLOG_SIDEBAR_B_SEGMENTS; $i++ ) {

				$sidebar_id = FH_BLOG_SIDEBAR_B_ID . '_' . $i;

				register_sidebar( array(
					'name'          => __( FH_BLOG_SIDEBAR_B_NAME . ' - Segment #' . $i, 'fh_boilerplate' ),
					'description'   => __( 'Shows on blog pages (' . $sidebar_id . ')' ),
					'id'            => $sidebar_id,
					'before_widget' => '<li>',
					'after_widget'  => '</li>',
					'before_title'  => '<h4>',
					'after_title'   => '</h4>'
				));
			}
		}
	}

	// Footer Widgets
	for( $i = 1; $i <= FH_FOOTER_WIDGETS_NUM; $i++ ) {

		$sidebar_id = FH_FOOTER_WIDGETS_ID . '_' . $i;

		register_sidebar( array(
			'name'          => __( 'Footer Widget, Column #' . $i, 'fh_boilerplate' ),
			'description'   => __( 'Shows in footer (' . $sidebar_id . ')' ),
			'id'            => $sidebar_id,
			'before_widget' => '<li>',
			'after_widget'  => '</li>',
			'before_title'  => '<h4>',
			'after_title'   => '</h4>'
		));
	}
}
add_action( 'widgets_init', '_fh_sidebar_widgets' );
endif;

//
//	Check if Sidr is enabled and append show/hide sidebar buttons to content
//
if ( ! function_exists( '_fh_sidr_append_btns' ) && FH_SIDR_ENABLED ) :
function _fh_sidr_append_btns() {

	?>
	<!-- Sidr Navigation / Back-to-top -->
    <div class="sidr_btns">
        <a id="fh_flyout_left_btn" href="#" title="Left Fly-out"><img src="<?php echo get_template_directory_uri(); ?>/img/actions/sidr_left.png"><span class="sr-only">Left Sidebar Fly-out Menu</span></a>
        <a id="fh_flyout_right_btn" href="#" title="Right Fly-out"><img src="<?php echo get_template_directory_uri(); ?>/img/actions/sidr_right.png"><span class="sr-only">Right Sidebar Fly-out Menu</span></a>
    </div>
	<?php
}
add_action( '_fh_content_append', '_fh_sidr_append_btns' );
endif;

?>