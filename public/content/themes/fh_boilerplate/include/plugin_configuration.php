<?php

/**
 *
 * plugin_configuration.php
 *
 * Individual plugin configurations that are used in most Boilerplate installations
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

//
//	If Menu (http://wordpress.org/plugins/if-menu)
//	- DEPRECIATED in lieu of Nav Menu Roles Plugin (keeping around to ensure don't need it again)
//
// if ( in_array( 'if-menu/if-menu.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ):

// 	// Add new conditional statement as per plugin FAQ (http://wordpress.org/plugins/if-menu/faq/)
// 	// - Adds support for WooCommerce User Types
// 	if( ! function_exists( '_fh_if_menu_conditions' ) ):
// 	function _fh_if_menu_conditions( $conditions ) {

// 		// $conditions[] = array(
// 		// 	// Name of the condition
// 		// 	'name' => 'If single custom-post-type',
// 		// 	// Callback - must return TRUE or FALSE
// 		// 	'condition' => function() {
// 		// 		return is_singular( 'my-custom-post-type' );
// 		// 	}
// 		// );

// 		// Add conditional for "User is not logged in"
// 		$conditions[] = array(
// 			'name' => 'User is not logged in',
// 			'condition' => function() {
// 				return ( ! is_user_logged_in() );
// 			}
// 		);

// 		// Adds support for WooCommerce User Types
// 		// Ensure WooCommerce is loaded
// 		if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

// 			// WooCommerce User is Customer
// 			$conditions[] = array(
// 				'name' => 'WooCommerce User is Customer',
// 				'condition' => function() {
// 					global $current_user;
// 					if( is_user_logged_in() ) { return in_array( 'customer', $current_user->roles ); }
// 					return false;
// 				}
// 			);

// 			// WooCommerce User is Shop Manager
// 			$conditions[] = array(
// 				'name' => 'WooCommerce User is Shop Manager',
// 				'condition' => function() {
// 					global $current_user;
// 					if( is_user_logged_in() ) { return in_array( 'shop_manager', $current_user->roles ); }
// 					return false;
// 				}
// 			);
// 		}

// 		return $conditions;
// 	}
// 	add_filter( 'if_menu_conditions', '_fh_if_menu_conditions' );
// 	endif;

// // END If Menu
// endif;


//
//	Yoast SEO
//
if (
	in_array( 'wordpress-seo/wp-seo.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) )
):

	// Remove Yoast SEO for non-admins
	if( ! function_exists( '_fh_remove_yoast_admin' ) ):
	function _fh_remove_yoast_admin() {

		global $wpseo_metabox;

		// Remove metaboxes from pages and posts
		remove_meta_box('wpseo_meta', 'page', 'normal');
		remove_meta_box('wpseo_meta', 'post', 'normal');

		// Remove "Check SEO" thingy from publish sidebar
		remove_action( 'post_submitbox_misc_actions', array( $wpseo_metabox, 'publish_box' ) );
	}

	if( ! is_super_admin() && FH_YOAST_DISABLED_FOR_NON_ADMINS ) {

		add_action( 'add_meta_boxes', '_fh_remove_yoast_admin', 999999 );
	}
	endif;

	// Move Yoast to bottom
	if( ! function_exists( '_fh_yoast_to_bottom' ) ):
	function _fh_yoast_to_bottom() {

		return 'low';
	}
	add_filter( 'wpseo_metabox_prio', '_fh_yoast_to_bottom' );
	endif;

endif;

?>