<?php

/**
 *
 * comments.php
 *
 * Customizes the default WP commenting engine to be BS3 and HTML5 driven
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

//
//	Customize the coment button to be BS3 driven
//
if( ! function_exists( '_fh_bs3_comment_button' ) ):
function _fh_bs3_comment_button() {

    echo '<button class="btn btn-primary" type="submit">' . __( 'Submit' ) . '</button>';
}
add_action('comment_form', '_fh_bs3_comment_button' );
endif;

//
//	Customize the comment notes above the form fields
//
if( ! function_exists( '_fh_comment_form_defaults' ) ):
function _fh_comment_form_defaults($defaults) {

	$defaults['comment_notes_before'] = '<p class="comment-notes">' . __( 'Your email address will not be published. Required fields are marked', 'folkhack' ) . '</p>';
	return $defaults;
}
add_filter( 'comment_form_defaults', '_fh_comment_form_defaults' );
endif;

//
//	Customize the comments title
//
if( ! function_exists( '_fh_comment_form_title' ) ):
function _fh_comment_form_title ($defaults) {

	$defaults['title_reply'] = __( 'Post a comment', 'folkhack' );
	return $defaults;
}
add_filter('comment_form_defaults','_fh_comment_form_title');
endif;

//
//	Customized the comment form fields for Author, email, URL ( not the comment text area )
//
if( ! function_exists( '_fh_comment_form_args' ) ):
function _fh_comment_form_args($args) {

	$args['author'] = '<div class="form-group comment-form-author"><input class="form-control" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="40" tabindex="1" aria-required="true" title="'. __( 'Your Name (required)','folkhack' ) .'" placeholder="'. __( 'Your Name (required)','folkhack' ) .'" required /><!-- .comment-form-author .form-section --></div>';
	$args['email'] = '<div class="form-group comment-form-email"><input class="form-control" id="email" name="email" type="email" value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="40" tabindex="2" aria-required="true" title="'. __( 'Email Address (required)','folkhack' ) .'" placeholder="'. __( 'Email Address (required)','folkhack' ) .'" required /><!-- .comment-form-email .form-section --></div>';
	$args['url'] = '<div class="form-group comment-form-url"><input class="form-control" id="url" name="url" type="url" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="40" tabindex="3" aria-required="false" title="'. __( 'Website (url)','folkhack' ) .'" placeholder="'. __( 'Website (url)','folkhack' ) .'" /><!-- .comment-form-url .form-section --></div>';

	return $args;
}
add_filter('comment_form_default_fields', '_fh_comment_form_args');
endif;

//
//	Customizes the text area - you have to do this here, rather than in comment_form_default_fields
//
if( ! function_exists( '_fh_comment_form_field_comment' ) ):
function _fh_comment_form_field_comment($field) {

	return '<div class="form-group comment-form-comment"><textarea id="comment" class="form-control" name="comment" cols="45" rows="8" tabindex="4" title="' . __( 'Comment','folkhack' ) . '" placeholder="' . __( 'Comment','folkhack' ) . '" aria-required="true"></textarea><!-- #form-section-comment .form-section --></div>';
}
add_filter('comment_form_field_comment', '_fh_comment_form_field_comment');
endif;

?>