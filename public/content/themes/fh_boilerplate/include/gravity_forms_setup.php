<?php

/**
 * gravity_forms_setup.php
 *
 * Registers custom JS asset for Gravity Forms
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if( ! function_exists( '_fh_gravity_forms_custom_js' ) ):
function _fh_gravity_forms_custom_js( $form ) {

    wp_enqueue_script( 'fh_gforms_custom', get_template_directory_uri() . '/js/gravity_forms.js', array( 'jquery' ) );
}
add_action( 'gform_enqueue_scripts', '_fh_gravity_forms_custom_js', 10, 2 );
endif;

?>
