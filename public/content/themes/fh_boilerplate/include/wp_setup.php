<?php

/**
 *
 * wp_setup.php
 *
 * This file modifies base functionality that is default within WordPress
 * applications.
 *
 * - Adds a class to the body based off of page name
 * - Make theme available for translation
 * - Add default posts and comments RSS feed links to head
 * - Enable support for Post Thumbnails on posts and pages
 * - Enable support for all post formats (minus chat)
 * - Bones-based init and head cleanup items:
 *     - Remove WP default injected CSS for CSS from gallery
 *     - Edit URI link
 *     - Windows live writer
 *     - Index link
 *     - Previous link
 *     - Start link
 *     - Links for adjacent posts
 *     - Remove "Generator" WP version
 *     - Remove WP version from css
 *     - Remove WP version from scripts
 *     - Remove WP version from RSS
 *     - Remove pesky injected css for recent comments widget
 *     - Clean WP comment styles in the head
 *     - Clean WP gallery output
 * - Clean-up default administrative dashboard widgets/nav items
 * - Remove parts from admin depending on if Utils::check_is_admin false
 * - Disable default administrative top menu items if Utils::check_is_admin
 *   false
 * - Fixes left padding on user information WP admin bar if
 *   Utils::check_is_admin false
 * - Remove default administrative footer text/version if
 *   Utils::check_is_admin false
 * - Change the default excerpt length
 * - Change the default excerpt ending
 * - Set custom thumbnail quality
 * - Disable admin bar for users
 * - Disable admin bar "push-down" inline CSS
 * - Disable login and display friendly message
 * - Remove capital_P_dangit filter for content, comments, and titles
 * - Add theme support for HTML galleries and captions
 * - Email headers fix
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

//
//  Basic WP features setup - body_class, post-thumbnails, post-formats, etc.
//
if ( ! function_exists( '_fh_setup' ) ) :
function _fh_setup() {

    //  Adds a class to the body based off of page name
    add_filter( 'body_class', array( 'Utils', 'add_slug_to_body_class' ) );

    // Make theme available for translation.
    // - Translations can be filed in the /languages/ directory.
    load_theme_textdomain( 'fh_boilerplate', get_stylesheet_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    // Enable support for Post Thumbnails on posts and pages.
    // - @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
    add_theme_support( 'post-thumbnails' );

    //
    //  Setup post formats:
    //
    if( FH_BLOG_ENABLED ) {

        $post_formats = array();

        // Masonry, Bootstrap 3 Slider, MixItUp Galleries with Magnific Popup Overlay
        if( FH_POST_GALLERIES_ENABLED ) {

            array_push( $post_formats, 'gallery' );
        }

        // Vimeo or Youtube embed through "Advanced Responsive Video Embedder"
        if( FH_VIDEO_POST_FORMAT_ENABLED ) {

            array_push( $post_formats, 'video' );
        }

        // Podcasting - http://en.support.wordpress.com/audio/
        if( FH_AUDIO_POST_FORMAT_ENABLED ) {

            array_push( $post_formats, 'audio' );
        }

        // Link is displayed prominently before content
        if( FH_LINK_POST_FORMAT_ENABLED ) {

            array_push( $post_formats, 'link' );
        }

        // Single image w/Magnific Popup
        if( FH_IMAGE_POST_FORMAT_ENABLED ) {

            array_push( $post_formats, 'image' );
        }

        // Blockquote preceeds content
        if( FH_QUOTE_POST_FORMAT_ENABLED ) {

            array_push( $post_formats, 'quote' );
        }

        // Content with no header
        if( FH_ASIDE_POST_FORMAT_ENABLED ) {

            array_push( $post_formats, 'aside' );
        }

        // Basic HTML text update wrapped in pre
        if( FH_STATUS_POST_FORMAT_ENABLED ) {

            array_push( $post_formats, 'status' );
        }

        // Default content (no specifics)
        if( FH_CHAT_POST_FORMAT_ENABLED ) {

            array_push( $post_formats, 'chat' );
        }

        // Support for Post Formats:
        if( ! empty( $post_formats ) ) {

            add_theme_support( 'post-formats', $post_formats );
        }
    }
}
add_action( 'after_setup_theme', '_fh_setup' );
endif;


//
// Bones init and cleanup items
// - https://github.com/eddiemachado/bones/blob/master/library/bones.php
//
if ( ! function_exists( '_fh_cleanup' ) ) :
function _fh_cleanup() {

    // remove WP version from RSS
    function _fh_rss_version() { return ''; }

    // remove WP version from scripts
    function _fh_remove_wp_ver_css_js( $src ) {

        if ( strpos( $src, 'ver=' ) ) {

            $src = remove_query_arg( 'ver', $src );
        }

        return $src;
    }

    // remove injected CSS for recent comments widget
    function _fh_remove_wp_widget_recent_comments_style() {

        if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {

            remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
        }
    }

    // remove injected CSS from recent comments widget
    function _fh_remove_recent_comments_style() {

        global $wp_widget_factory;

        if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {

            remove_action( 'wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style') );
        }
    }

    // remove injected CSS from gallery
    function _fh_gallery_style($css) {
        return preg_replace( "!<style type='text/css'>(.*?)</style>!s", '', $css );
    }

    // Edit URI link
    remove_action( 'wp_head', 'rsd_link' );

    // Windows live writer
    remove_action( 'wp_head', 'wlwmanifest_link' );

    // Index link
    remove_action( 'wp_head', 'index_rel_link' );

    // Previous link
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );

    // Start link
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );

    // Links for adjacent posts
    remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );

    // WP version
    remove_action( 'wp_head', 'wp_generator' );

    // Remove WP version from css
    add_filter( 'style_loader_src', '_fh_remove_wp_ver_css_js', 9999 );

    // Remove WP version from scripts
    add_filter( 'script_loader_src', '_fh_remove_wp_ver_css_js', 9999 );

    // Remove WP version from RSS
    add_filter( 'the_generator', '_fh_rss_version' );

    // Remove pesky injected css for recent comments widget
    add_filter( 'wp_head', '_fh_remove_wp_widget_recent_comments_style', 1 );

    // Clean WP comment styles in the head
    add_action( 'wp_head', '_fh_remove_recent_comments_style', 1 );

    // Clean WP gallery output in wp
    add_filter( 'gallery_style', '_fh_gallery_style' );
}
add_action( 'init', '_fh_cleanup' );
endif;


//
//  Disable default administrative dashboard widgets/nav items
//
if ( ! function_exists( '_fh_disable_admin_dash_crap' ) ) :
function _fh_disable_admin_dash_crap() {

    remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'core' );  // Incoming Links Widget
    remove_meta_box( 'dashboard_plugins', 'dashboard', 'core' );         // Plugins Widget
    remove_meta_box( 'dashboard_primary', 'dashboard', 'core' );         // Remove WordPress news
    remove_meta_box( 'dashboard_secondary', 'dashboard', 'core' );       // Remove other WordPress news

    // Remove parts from admin depending on if custom check_is_admin false
    if( ! Utils::check_is_admin() ) {

        // Metaboxes
        remove_meta_box( 'dashboard_right_now', 'dashboard', 'core' );           // "At a Glance"
        remove_meta_box( 'yoast_db_widget', 'dashboard', 'normal' );             // Yoast's SEO Plugin Widget
        remove_meta_box( 'rg_forms_dashboard', 'dashboard', 'normal' );          // Gravity Forms
        remove_meta_box( 'bsearch_pop_dashboard', 'dashboard', 'normal' );       // Better Search
        remove_meta_box( 'bsearch_pop_daily_dashboard', 'dashboard', 'normal' ); // Better Search

        // Menu items
        remove_menu_page( 'themes.php' );             // Appearance
        remove_menu_page( 'plugins.php' );            // Plugins
        remove_menu_page( 'users.php' );              // Users
        remove_menu_page( 'tools.php' );              // Tools
        remove_menu_page( 'options-general.php' );    // General Settings
        remove_menu_page( 'edit.php?post_type=acf' ); // ACF "Custom Fields"
        remove_menu_page( 'acf-options' );            // ACF "Options"
        remove_menu_page( 'wpseo_dashboard' );        // Yoast SEO
    }
}
add_action( 'admin_menu', '_fh_disable_admin_dash_crap', 999 );
endif;


//
//  Disable default administrative top menu items
//
if ( ! function_exists( '_fh_remove_adminbar_items' ) ) :
function _fh_remove_adminbar_items( $wp_admin_bar ) {

    if( ! Utils::check_is_admin() ) {

        $wp_admin_bar->remove_node( 'wp-logo' );
        $wp_admin_bar->remove_node( 'comments' );
        $wp_admin_bar->remove_node( 'wpseo-menu' );
        $wp_admin_bar->remove_node( 'new-media' );
        $wp_admin_bar->remove_node( 'user-info' );
        $wp_admin_bar->remove_node( 'new-user' );
    }
}
add_action( 'admin_bar_menu', '_fh_remove_adminbar_items', 999 );
endif;


//
//  Fixes left padding on user information WP admin bar
//
if ( ! function_exists( '_fh_add_custom_adminbar_inline_css' ) ) :
function _fh_add_custom_adminbar_inline_css() {

    if( ! Utils::check_is_admin() ) {

        ?>
<style>
#wp-admin-bar-user-actions > li { margin-left: 16px !important; }
</style>
        <?php
    }
}
add_action( 'admin_head', '_fh_add_custom_adminbar_inline_css', 9999 );
endif;


//
//  Remove default administrative footer text/version
//
if ( ! function_exists( '_fh_remove_footer_admin' ) ) :
function _fh_remove_footer_admin( $html ) {

    // Only remove thanks/version when not super-admin
    if( ! Utils::check_is_admin() ) {

        return '';
    }

    return $html;
}
add_filter( 'admin_footer_text', '_fh_remove_footer_admin', 9999 );
add_filter( 'update_footer', '_fh_remove_footer_admin', 9999 );
endif;


//
//  Change the default excerpt length
//
if ( ! function_exists( '_fh_excerpt_length' ) ) :
function _fh_excerpt_length( $length ) {

    return FH_EXCERPT_WORD_LENGTH;
}
add_filter( 'excerpt_length', '_fh_excerpt_length', 999 );
endif;


//
//  Change the default excerpt ending
//
if ( ! function_exists( '_fh_excerpt_ending' ) ) :
function _fh_excerpt_ending( $more ) {

    return FH_EXCERPT_ENDING;
}
add_filter( 'excerpt_more', '_fh_excerpt_ending' );
endif;


//
//  Set custom thumbnail quality
//
if ( ! function_exists( '_fh_thumbnail_quality' ) ) :
function _fh_thumbnail_quality( $quality ) {

    return FH_THUMBNAIL_QUALITY;
}
add_filter( 'jpeg_quality', '_fh_thumbnail_quality' );
add_filter( 'wp_editor_set_quality', '_fh_thumbnail_quality' );
endif;


//
//  Disable admin bar for users
//
if ( ! function_exists( '_fh_disable_user_admin_bar' && HIDE_ADMIN_BAR_FROM_USERS ) ) :
function _fh_disable_user_admin_bar()
{
    if( ! is_super_admin() && ! is_blog_admin() ) {

        return false;
    }

    return true;
}
add_filter( 'show_admin_bar', '_fh_disable_user_admin_bar' );
endif;


//
//  Disable admin bar "push-down" inline CSS
//  - http://wordpress.org/support/topic/31-update-causing-28px-top-spacing-in-html-body
//  - http://discourse.roots.io/t/override-admin-bar-css/755/6
//
if ( ! function_exists( '_fh_remove_adminbar_inline_css' ) ) :
function _fh_remove_adminbar_inline_css() {
    remove_action( 'wp_head', '_admin_bar_bump_cb' );
}
add_action( 'admin_bar_init', '_fh_remove_adminbar_inline_css', 9999 );
endif;


//
//  Disable login and display friendly message
//
if ( ! function_exists( '_fh_login_init' ) ) :
function _fh_login_init() {

    ob_start();

    ?>
    <!doctype html>
    <html lang=en>
        <head>
            <meta charset=utf-8>
            <title><?php _e( 'WordPress Administrative Dashboard Disabled', 'fh_boilerplate' ); ?></title>
        </head>
        <style>
        body {
            background: #f5f5f5;
            font-family: verdana;
            font-size: .8em;
        }
        a { color: red; text-decoration: none; }
        a:hover { text-decoration: underline; }
        section {
            background: #fff;
            box-shadow: #ccc 0 5px 10px;
            line-height: 1.5em;
            margin: 50px auto;
            padding: 20px 30px;
            width: 600px;
        }
        .footer_ip {
            color: #ccc;
            text-align: center;
        }
        </style>
        <body>
            <section>
                <p style="font-size: 1.5em;"><strong><?php _e( 'Hi there!', 'fh_boilerplate' ); ?></strong></p>
                <p><?php _e( 'You\'re probably wondering where your administrative dashboard is', 'fh_boilerplate' ); ?> =P</p>
                <p><?php _e( 'Your loving developers have temporarily disabled administrative access to your site. This is (probably) due to active development and/or administration currently happening! Sometimes when we\'re working underneath the hood things can get weird with the administrative area of your website.', 'fh_boilerplate' ); ?></p>
                <p><?php _e( 'We do this to avoid nasty things like data-loss, concurrency issues, and things breaking while you\'re updating your website!', 'fh_boilerplate' ); ?></p>
                <p><?php _e( 'This loss of access is likely a communicated and planned event for feature requests, bug fixes, etc. However, it is possible you caught us in the middle of security patching or system-update - oops!', 'fh_boilerplate' ); ?></p>
                <p><?php _e( 'Sit back and relax - we\'ve got you (and your website) covered! Things will be back up in no-time', 'fh_boilerplate'); ?> =)</p>
                <br />
                <p style="font-size: 1.5em; text-align:center;"><span style="color: red;">&hearts;</span> <?php _e( 'Your Developers', 'fh_boilerplate' ); ?></p>
            </section>
            <p class="footer_ip"><?php echo $_SERVER['REMOTE_ADDR']; ?></p>
        </body>
    </html>

    <?php

    die( ob_get_clean() );
}

if( FH_DISABLE_LOGIN && !( FH_DISABLED_LOGIN_ALLOW_IP == $_SERVER['REMOTE_ADDR'] ) ) {

    add_filter( 'login_init', '_fh_login_init' );
}
endif;


//
//  Remove capital_P_dangit filter for content, comments, and titles
//  - http://codex.wordpress.org/Function_Reference/capital_P_dangit
//
remove_filter( 'the_title', 'capital_P_dangit', 11 );
remove_filter( 'the_content', 'capital_P_dangit', 11 );
remove_filter( 'comment_text', 'capital_P_dangit', 31 );


//
//  Add theme support for HTML galleries and captions
//
add_theme_support( 'html5', array( 'gallery', 'caption' ) );

//
//  Email headers fix
//  - based on (http://abdussamad.com/archives/567-Fixing-the-WordPress-Email-Return-Path-Header.html)
//
if ( ! function_exists( '_fh_fix_email_headers' ) ) :
function _fh_fix_email_headers( $phpmailer ) {

    $tmp_check = FH_MAIL_FROM_SENDER;

    if( ! empty( $tmp_check ) ) {

        $phpmailer->From = FH_MAIL_FROM_SENDER;
    }

    $phpmailer->Sender   = $phpmailer->From;
    $phpmailer->FromName = get_bloginfo( 'name' ) . ' Website';
}
add_action( 'phpmailer_init', '_fh_fix_email_headers' );
endif;

?>