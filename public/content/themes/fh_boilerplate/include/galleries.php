<?php

/**
 *
 * galleries.php
 *
 * Register additional image sizes + Logic for Masonry, MixItUp, and Bootstrap 3
 * Carousel Galleries
 *
 * - Add additional gallery image sizes via add_image_size
 * - Masonry Gallery rendering function
 * - MixItUp Gallery rendering function
 * - Bootstrap 3 Carousel Gallery rendering function
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

//
//	Add additional gallery image sizes
//
if ( ! function_exists( '_fh_galleries_setup' ) ) :
function _fh_galleries_setup() {

	// For Masonry Galleries:
    add_image_size( 'fh-masonry-onecol', FH_GAL_MASONRY_SINGLE_IMG_WIDTH, 9999 ); // One column image width
    add_image_size( 'fh-masonry-twocol', FH_GAL_MASONRY_DOUBLE_IMG_WIDTH, 9999 ); // One column image width * 2 + gutter

    // For MixItUp Galleries:
   	add_image_size( 'fh-mixitup', FH_GAL_MIXITUP_IMG_MAX_WIDTH, FH_GAL_MIXITUP_IMG_MAX_HEIGHT, true );

    // Bootstrap 3 Carousel Sliders:
   	add_image_size( 'fh-bs3carousel-cropped', FH_GAL_BS3_CAROUSEL_MAX_WIDTH, FH_GAL_BS3_CAROUSEL_MAX_HEIGHT, true );
}
add_action( 'after_setup_theme', '_fh_galleries_setup' );
endif;


//
//	Masonry Gallery
//	- http://masonry.desandro.com/
//
if ( ! function_exists( '_fh_masonry_gallery' ) ) :
function _fh_masonry_gallery(
		$images,
		$two_col_width_enabled = false,
		$one_col_width         = FH_GAL_MASONRY_SINGLE_IMG_WIDTH,
		$two_col_width         = FH_GAL_MASONRY_DOUBLE_IMG_WIDTH,
		$padding               = FH_GAL_MASONRY_GUTTER,
		$one_col_resize        = FH_GAL_MASONRY_IMG_SIZE_NAME_SINGLE,
		$two_col_resize        = FH_GAL_MASONRY_IMG_SIZE_NAME_DOUBLE,
		$js_override           = false
	) {

	// Register masonry assets

	// Un-register WP included Masonry
	wp_deregister_script( 'masonry' );

	// Figure out asset to use
	$masonry_js_src = ( FH_MINIFIED_ASSETS_ENABLED ) ?
		'jquery.masonry.pkgd.min.js' :
		'jquery.masonry.pkgd.js';

	wp_register_script( 'masonry', get_template_directory_uri() . '/js/vendor/masonry/' . $masonry_js_src, array( 'jquery' ) );
	wp_register_script( 'imagesloaded', get_template_directory_uri() . '/js/vendor/imagesloaded.js', array( 'jquery' ) );

	wp_enqueue_script( 'masonry' );
	wp_enqueue_script( 'imagesloaded' );

	$gallery_html = '<div class="gallery_wrap">' . "\n";
	$gallery_html .= '<div class="masonry_wrap clearfix magnific">' . "\n";

	foreach( $images as $image ) {

		$add_class = '';
		$width = $one_col_width;
		$src = ( is_string( $image['sizes'][ $one_col_resize ] ) ?
			$image['sizes'][ $one_col_resize ] :
			$image['sizes'][ $one_col_resize ][ 'src' ] );

		// If width greater than height show as 2 col span
		if( $two_col_width_enabled && $image['width'] > $image['height'] ) {

			$add_class = 'wide';
			$width = $two_col_width;
			$src = ( is_string( $image['sizes'][ $two_col_resize ] ) ?
				$image['sizes'][ $two_col_resize ] :
				$image['sizes'][ $two_col_resize ][ 'src' ] );
		}

		// Figure out prportional image height
		$height = ( $width / $image['width'] ) * $image['height'];

		$gallery_html .= '<div class="item ' . $add_class . '">';
		$gallery_html .= '<a href="' . $image['url'] . '" target="blank" title="' . $image['title'] .'">';
		$gallery_html .= '<img src="' . $src . '" alt="'. $image['alt'] .'" title="' . $image['title'] .'" height="' . $height . '" width="' . $width . '" />';
		$gallery_html .= '</a></div>' . "\n";
	}

	$gallery_html .= '</div>' . "\n";
	$gallery_html .= '</div>' . "\n";

	// If there's override javascript then display
	if( $js_override ) {

		$gallery_html .= $js_override;

	} else {

		ob_start();
		?>
		<script>
		var masonry_loaded = masonry_loaded || false;
		jQuery( window ).on( 'load', function() {

			if( ! masonry_loaded ) {

				// Masonry
				// - checks for images loaded first
				jQuery('.masonry_wrap').imagesLoaded( function () {

					jQuery('.masonry_wrap').masonry({
						columnWidth: <?php echo ( $one_col_width + $padding ); ?>,
						itemSelector: '.item',
						isFitWidth: true
					});
				});

				masonry_loaded = true;
			}
		});
		</script>
		<?php

		$gallery_html .= ob_get_clean();
	}

	return $gallery_html;
}
endif;


//
//	MixItUp Gallery
//	- https://mixitup.kunkalabs.com/
//
if ( ! function_exists( '_fh_mixitup_gallery' ) ) :
function _fh_mixitup_gallery(
	$images,
	$width       = FH_GAL_MIXITUP_IMG_MAX_WIDTH,
	$height      = FH_GAL_MIXITUP_IMG_MAX_HEIGHT,
	$resize      = FH_GAL_MIXITUP_IMG_SIZE_NAME,
	$js_override = false
) {

	// Register MixItUp assets
	wp_register_script( 'mixitup', get_template_directory_uri() . '/js/vendor/mixitup/jquery.mixitup.min.js', array( 'jquery' ) );
	wp_enqueue_script( 'mixitup' );

	$gallery_html = '<div class="gallery_wrap">' . "\n";
	$gallery_html .= '<div class="mixitup_grid list-unstyled">' . "\n";

	$gallery_img_types = array();


	foreach( $images as $image ) {

		$src = ( is_string( $image['sizes'][ $resize ] ) ?
				$image['sizes'][ $resize ] :
				$image['sizes'][ $resize ][ 'src' ] );
		$add_class = crc32( $image['alt'] ); // Types by alt attribute

		if( !empty( $add_class ) ) {

			$gallery_img_types[ $add_class ] = $image['alt']; // Types by alt attribute
		}

		$gallery_html .= '<div class="mix magnific ' . $add_class . '">';
		$gallery_html .= '<a href="' . $image['url'] . '" target="blank" title="' . $image['title'] .'">';
		$gallery_html .= '<img src="' . $src . '" alt="'. $image['alt'] .'" title="' . $image['title'] .'" height="' . $height . '" width="' . $width . '" />';
		$gallery_html .= '</a></div>' . "\n";
	}

	$gallery_html .= '<div class="gap"></div>' . "\n";
	$gallery_html .= '<div class="gap"></div>' . "\n";
	$gallery_html .= '<div class="gap"></div>' . "\n";
	$gallery_html .= '<div class="gap"></div>' . "\n";

	$gallery_html .= '</div>' . "\n";

	// Type based sorting/filtering of items
	$types_html = '';

	if( sizeof( $gallery_img_types ) ) {

		$types_html .= '<div class="mixitup_buttons text-center">' . "\n";
		$types_html .= '<div class="btn-group">' . "\n";
		$types_html .= '<button type="button" class="btn btn-default btn-xs filter" data-filter="all">Show All</button>' . "\n";

		foreach ( $gallery_img_types as $key => $gallery_img_type ) {

			$types_html .= '<button type="button" class="btn btn-default btn-xs filter" data-filter=".' . $key . '">' . $gallery_img_type . '</button>' . "\n";
		}

		$types_html .= '</div>' . "\n";
		$types_html .= '</div><br />' . "\n";
	}

	$gallery_html = $types_html . $gallery_html;

	$gallery_html .= '</div>' . "\n";

	// If there's override javascript then display
	if( $js_override ) {

		$gallery_html .= $js_override;

	} else {

		ob_start();
		?>
		<script>
			var mixitup_loaded = mixitup_loaded || false;
			jQuery( window ).on( 'load', function() {

				if( ! mixitup_loaded ) {

					// MixItUp
					jQuery('.mixitup_grid').mixItUp();

					mixitup_loaded = true;
				}
			});
		</script>
		<?php

		$gallery_html .= ob_get_clean();
	}

	return $gallery_html;
}
endif;


//
//	Bootstrap 3 Carousel Gallery
//	- http://getbootstrap.com/javascript/#carousel
//
if ( ! function_exists( '_fh_bs3carousel_gallery' ) ) :
function _fh_bs3carousel_gallery(
		$images,
		$controls       = true,
		$caption        = true,
		$dots           = true,
		$image_links    = true,
		$slide_interval = 5000,
		$width          = FH_GAL_BS3_CAROUSEL_MAX_WIDTH,
		$height         = FH_GAL_BS3_CAROUSEL_MAX_HEIGHT,
		$resize         = FH_GAL_BS3_CAROUSEL_IMG_SIZE_NAME,
		$carousel_id    = FH_GAL_BS3_CAROUSEL_DEFAULT_ID
	) {

	$gallery_html .= '<div class="gallery_wrap">' . "\n";
	$gallery_html .= '<div id="' . $carousel_id . '" class="carousel slide" data-ride="carousel" data-interval="' . $slide_interval . '">' . "\n";

	// Add in carousel-indicators
	if( $dots ) {

		$gallery_html .= '<ol class="carousel-indicators list-unstyled">' . "\n";

		for( $i = 0; $i < sizeof( $images ); $i++ ) {

			// Add active class to first slide
			$class = ( ! $i ) ? 'active' : '';

			$gallery_html .= '<li data-target="#' . $carousel_id . '" data-slide-to="' . $i . '" class="' . $class . '"></li>' . "\n";
		}
		$gallery_html .= '</ol>' . "\n";

	}

	$gallery_html .= '<div class="carousel-inner">' . "\n";

	$i = 0;

	foreach( $images as $image ) {

		// Add active class to first slide
		$class = ( ! $i ) ? 'active' : '';
		$src = ( is_string( $image['sizes'][ $resize ] ) ?
			$image['sizes'][ $resize ] :
			$image['sizes'][ $resize ][ 'src' ] );

		$gallery_html .= '<div class="item magnific ' . $class . '">' . "\n";
		$gallery_html .= ( $image_links ) ? '<a href="' . $image['url'] . '" target="blank" title="' . $image['title'] .'">' : '';
		$gallery_html .= '<img src="' . $src . '" alt="'. $image['alt'] .'" title="' . $image['title'] .'" height="' . $height . '" width="' . $width . '" />';
		$gallery_html .= ( $image_links ) ? '</a>' . "\n" : '';

		if( !empty( $image['title'] ) && $caption ) {

			$gallery_html .= '<div class="carousel-caption">' . "\n";
			$gallery_html .= $image['title'];
			$gallery_html .= '</div>' . "\n";
		}

		$gallery_html .= '</div>' . "\n";

		$i++;
	}

	$gallery_html .= '</div>' . "\n";

	if( $controls ) {

		// Add in left/right controls
		$gallery_html .= '<a class="left carousel-control" href="#' . $carousel_id . '" data-slide="prev">' . "\n";
		$gallery_html .= '<span class="fa fa-chevron-left"></span>' . "\n";
		$gallery_html .= '</a>' . "\n";

		$gallery_html .= '<a class="right carousel-control" href="#' . $carousel_id . '" data-slide="next">' . "\n";
		$gallery_html .= '<span class="fa fa-chevron-right"></span>' . "\n";
		$gallery_html .= '</a>' . "\n";
	}

	$gallery_html .= '</div>' . "\n";
	$gallery_html .= '</div>' . "\n";

	return $gallery_html;
}
endif;

?>