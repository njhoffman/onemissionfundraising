<?php

/**
 *
 * template_functions.php
 *
 * Base functions for rendering Boilerplate theme such as sidebar and content rendering functions.
 *
 *  Notes:
 *  - based on _s_ WordPress Theme https://raw.github.com/Automattic/_s/master/inc/template-tags.php
 *  - based on Starkers WordPress Theme https://github.com/viewportindustries/starkers
 *  - based on a collection of Folkhack specific functions for WordPress theme maniuplation and handling
 *
 *  Included functionality (table of contents):
 *  - Prints HTML with meta information for the current post-date/time and author. (_s_ based)
 * 		- DEPRECIATED!
 *  - Folkhack's custom author block support
 *  - Folkhack's custom post excerpt rendering
 *  - Folkhack's custom post taxonomy rendering
 *  - Folkhack's custom post meta rendering
 *  - Retrieves all custom data for page/post rendering
 *  - Function for getting crumbs and running actions
 *  - Function for wrapping pages in ID "page_article"
 *  - Function for getting the title and running actions
 *  - Function for getting the content and running actions
 *  - Base function for only displaying content
 *  - Blog post content generation
 *  - Function for getting a dynamic sidebar without immediate echo
 *  - Function for loading the default folkhack sidebar segments
 *  - Function for loading individual sidebar segments
 *  - Looks for certain configuration values and returns correct sidebars
 *  - Sidebar A rendering function
 *  - Sidebar B rendering function
 *  - Returns true if a blog has more than 1 category.
 *  - Flush out the transients used in _fh_categorized_blog
 *  - favicon support for both front-end & back-end
 *  - Temporary CSS in <head> support for both front-end & back-end
 *  - Function for previous/next post navigation
 *  - Utility function to grab image data from Wordpress attachments
 *  - WordPress' missing _fh_is_blog_related() function.
 *
 * Custom actions:
 *  - _fh_the_crumbs
 *  - _fh_the_title
 *  - _fh_header_prepend
 *  - _fh_header_prepend_in_wrap
 *  - _fh_header_append_in_wrap
 *  - _fh_before_content
 *  - _fh_content_prepend
 *  - _fh_the_content
 *  - _fh_content_append
 *  - _fh_after_content
 *  - _fh_sidebar_right_prepend
 *  - _fh_sidebar_right_append
 *  - _fh_sidebar_left_prepend
 *  - _fh_sidebar_left_append
 *  - _fh_sidebar_right_content
 *  - _fh_sidebar_left_content
 *  - _fh_sidebar_a_prepend
 *  - _fh_sidebar_a_append
 *  - _fh_sidebar_b_prepend
 *  - _fh_sidebar_b_append
 *  - _fh_footer_prepend
 *  - _fh_footer_inside_prepend
 *  - _fh_footer_cw_prepend
 *  - _fh_footer_cw_content
 *  - _fh_footer_cw_append
 *  - _fh_footer_inside_append
 *  - _fh_footer_append
 *
 * Custom Filters:
 * 	- _fh_display_post_meta
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

//
//	Prints HTML with meta information for the current post-date/time and author. (_s_ based)
//	- DEPRECIATED
//
// if ( ! function_exists( '_fh_posted_on' ) ) :
// function _fh_posted_on() {
// 	$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';
// 	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
// 		$time_string .= '<time class="updated" datetime="%3$s">%4$s</time>';
// 	}

// 	$time_string = sprintf( $time_string,
// 		esc_attr( get_the_date( 'c' ) ),
// 		esc_html( get_the_date() ),
// 		esc_attr( get_the_modified_date( 'c' ) ),
// 		esc_html( get_the_modified_date() )
// 	);

// 	printf( __( '<span class="posted-on">Posted on %1$s</span><span class="byline"> by %2$s</span>', 'fh_boilerplate' ),
// 		sprintf( '<a href="%1$s" rel="bookmark">%2$s</a>',
// 			esc_url( get_permalink() ),
// 			$time_string
// 		),
// 		sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s">%2$s</a></span>',
// 			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
// 			esc_html( get_the_author() )
// 		)
// 	);
// }
// endif;

//
//	Folkhack's custom author block support
//
if ( ! function_exists( '_fh_author_block' ) ) :
function _fh_author_block( $user_id, $display_header = false, $image_container_class = 'alignleft', $echo = true ) {

	if( FH_DISPLAY_AUTHOR_BLOCK && FH_DISPLAY_AUTHOR_INFO ) {

		// Grab icons to iterate through and grab author social links
		global $fh_social_icons;

		// If user has a description filled out then display their profile
		if ( get_the_author_meta( 'description', $user_id ) ) {

			// Avatar size
			$avatar_size = 150;

			// Grab basic information on user - guaranteed avatar, author, description
			$avatar      = get_avatar( $user_id, $avatar_size );
			$author      = get_the_author_meta( 'display_name', $user_id );
			$description = get_the_author_meta( 'description', $user_id );
			$website     = get_the_author_meta( 'user_url', $user_id );
			$posts_url   = get_author_posts_url( $user_id );
			$ret_html    = '';
			$soc_arr     = array();

			// Iterate over soc_arr looking to see if user has any of their social accounts set
			foreach ( $fh_social_icons as $icon_class => $icon_data ) {

				// Dash throws off WordPress contact method - convert to underscores
				$author_meta_class = preg_replace( '/-/', '_', $icon_class );
				$link = get_the_author_meta( $author_meta_class );

				if( $link && !empty( $link ) ) {

					$soc_arr[$icon_class] = $link;
				}
			}


			if( ! empty( $soc_arr ) ) {

				$soc_html = _fh_social_icons( $soc_arr, false, $author, false );
			}

			// Display the author block
			$ret_html .= '<div id="author_block" class="clearfix">' . "\n";
			$ret_html .= '<div class="' . $image_container_class . '">' . "\n";
			$ret_html .= '<div class="image">' . $avatar . '</div>' . "\n";
			$ret_html .= ( ! empty( $soc_html ) ) ? '<div class="socials">' . "\n" . $soc_html . '</div>' . "\n" : '';
			$ret_html .= ( ! empty( $website ) ) ? '<div class="website"><a class="btn btn-primary btn-sm" href="' . $website . '" target="_blank"><span class="fa fa-globe"></span>&nbsp; Visit Website</a></div>' . "\n" : '';
			$ret_html .= '</div>' . "\n";
			$ret_html .= '<div>' . "\n";
			$ret_html .= '<div class="description">' . "\n";
			$ret_html .= ( $display_header ) ? '<h3><a href="' . $posts_url . '">About ' . $author . '</a></h3>' . "\n" : '';
			$ret_html .= '<p>' . nl2br( $description ) . '</p>' . "\n";
			$ret_html .= ( ! is_author() ) ? '<hr class="small"><p class="text-center"><a href="' . $posts_url . '">Authored Posts Archive &raquo;</a></p>' . "\n" : '';
			$ret_html .= '</div>' . "\n";
			$ret_html .= '</div>' . "\n";
			$ret_html .= '</div>' . "\n";

			// echo rendered html
			if( $echo ) {

				echo $ret_html;
			}

			// return rendered html
			return $ret_html;
		}

	}
}
endif;

//
//	Folkhack's custom post excerpt rendering
//	- TODO: implement https://codex.wordpress.org/Function_Reference/the_title_attribute
//
if ( ! function_exists( '_fh_display_post_excerpt' ) ) :
function _fh_display_post_excerpt( $post, $echo = true ) {

	$loop_classes = array( 'loop', 'clearfix' );

	if( is_sticky() && is_home() ) {

		array_push( $loop_classes, 'sticky' );
	}

	// Holds rendered html for post
	$html = '<article class="' . implode( ' ', $loop_classes ) . '">' . "\n";

	// Get post information
	// $title     = htmlentities( get_the_title( $post->ID ) );
	$title     = strip_tags( get_the_title( $post->ID ) );
	$permalink = esc_url( get_permalink( $post->ID ) );
	$thumb     = get_the_post_thumbnail( $post->ID, array( 200, 200 ) );

	// Get page/post excerpt
	$excerpt   = ( ! empty( $post->post_excerpt ) ) ?
		$post->post_excerpt :
		get_the_excerpt();

	// load fundraiser description if excerpt can't be found
	if (empty($excerpt) && !empty($post->custom['fundraiser_description'])) {
		$excerpt = strip_shortcodes($post->custom['fundraiser_description']);
		$excerpt_length = apply_filters( 'excerpt_length', 55 );
		$excerpt_more = apply_filters( 'excerpt_more', ' ' . '[&hellip;]' );
		$excerpt = wp_trim_words( $excerpt, $excerpt_length, $excerpt_more );
	}

	// show full article content (hidden with less is more) unless site search then show excerpt
	$is_search = isset($_GET["s"]);
	$full_article = ( ! empty( $post->post_content) && ! $is_search ) ? apply_filters( 'the_content', $post->post_content ) : $excerpt;

	// Article header
	$html .= '<h2><a href="' . $permalink . '" title="Permalink to ' . $title . '" rel="bookmark">' . $title . '</a></h2>' . "\n";

	// Get post meta information
	$html .= _fh_display_post_meta( $post, false );

	// Show post excerpt
	if( ! empty( $full_article ) ) {

		$html .= '<div class="clearfix">' . "\n";

		// Display a thumbnail for the article
		if( !empty( $thumb ) ) {

			$html .= '<div class="article_thumb alignright"><a href="' . $permalink . '" title="Permalink to ' . $title . '">' . $thumb . '</a></div>' . "\n";
		}

		// Article excerpt -- old
		// $html .= '<div class="article_excerpt">' . "\n";
		// $html .= '<p>' . nl2br( rtrim( $excerpt ) ) . '</p>' . "\n";
		// $html .= '</div>' . "\n";

		$html .= '<div class="article_excerpt less-is-more">' . "\n";
		$html .= '<p>' . nl2br( rtrim( $full_article) ) . '</p>' . "\n";
		$html .= '</div>' . "\n";

		$html .= '</div>' . "\n";

		$html .= '<hr class="small">';

	} elseif( ! empty( $thumb ) ) {

		$bigger = get_the_post_thumbnail( $post->ID );

		$html .= '<div class="article_thumb_full text-center"><a href="' . $permalink . '" title="Permalink to ' . $title . '">' . $bigger . '</a></div>' . "\n";
		$html .= '<hr class="small">';
	}

	// Article actions
	$html .= '<div class="article_actions clearfix">' . "\n";

	// Read more
	// old read more that links to it's own article, we want the whole article displayed on this page - njh 11/10/16
	// $html .= '<span class="read_more"><a href="' . $permalink . '" title="Read more on ' . $title . '" class="btn btn-primary btn-md"><span class="hidden-print">Read more &nbsp;</span><span class="fa fa-chevron-right"></span></a></span>' . "\n";

	// Get post taxonomy information
	// $html .= _fh_display_post_taxonomy( $post, 'post_taxonomy', false );

	$html .= '</div>' . "\n";

	$html .= '</article>' . "\n";

	// echo rendered post html
	if( $echo ) {

		echo $html;
	}

	// return rendered post html
	return $html;
}
endif;

//
//	Folkhack's custom post taxonomy rendering
//
if ( ! function_exists( '_fh_display_post_taxonomy' ) ) :
function _fh_display_post_taxonomy( $post, $custom_class = '', $echo = true ) {

	$html   = '';
	$out_arr = array();

	// Get the categories
	$post_categories = get_the_category();
	$categories_html = '';

	if( $post_categories ) {
		foreach( $post_categories as $category ) {
			$category_url = get_category_link( $category->term_id );
			$category_title = sprintf( __( "View all posts in %s" ), $category->name );
			$categories_html .= '<a href="' . $category_url . '" title="' . $category_title . '" class="cat-slug-' . $category->slug . ' btn btn-xs btn-default">' . $category->name . '</a> ';
		}
	}

	// Get the tags
	$post_tags = get_the_tags();
	$tags_html = '';

	if( $post_tags ) {
		foreach( $post_tags as $tag ) {
			$tag_url = get_tag_link( $tag->term_id );
			$tags_html .= '<a href="' . $tag_url . '" class="tag-slug-' . $tag->slug . ' btn btn-xs btn-default">' . $tag->name . '</a> ';
		}
	}

	if( ! empty( $categories_html ) ) {

		array_push( $out_arr, '<span class="taxonomy_label">Posted in: </span>' . $categories_html );
	}

	if( ! empty( $tags_html ) ) {

		array_push( $out_arr, '<span class="taxonomy_label">Tagged as: </span>' . $tags_html );
	}

	if( sizeof( $out_arr ) ) {

		$html .= '<div class="' . $custom_class . '"><p>' . implode( '</p><p>', $out_arr ) . '</p></div>' . "\n";
	}

	// echo rendered html
	if( $echo ) {

		echo $html;
	}

	// return rendered html
	return $html;
}
endif;

//
//	Folkhack's custom post meta rendering
//
if ( ! function_exists( '_fh_display_post_meta' ) ) :
function _fh_display_post_meta( $post, $echo = true ) {

	$html         = '';
	$time         = get_the_time( 'Y-m-d' );
	$humnan_time  = get_the_date() . ' ' . get_the_time();
	$display_name = get_the_author_meta( 'display_name', $post->post_author );

	// Conditionally display author information
	$author = ( FH_DISPLAY_AUTHOR_INFO ) ?
		'<span class="author_right"><span class="fa fa-user"></span>&nbsp; <a href="' . get_author_posts_url( $post->post_author ) . '">' . $display_name . '</a></span>' :
		'';

	// Article meta/comments sub-header
	$html .= '<div class="article_meta clearfix">' . "\n";

	// Date/time, byline for article
	$html .= "\t" . '<time datetime="' . $time . '"><span class="fa fa-calendar-o"></span>&nbsp; ' . $humnan_time . '</time> ' . $author . "\n";

	// End article sub-header
	$html .= '</div>' . "\n";

	$return = apply_filters( '_fh_display_post_meta', $html, $post );

	// echo rendered html
	if( $echo ) {

		echo $return;
	}

	// return rendered html
	return $return;
}
endif;

//
//	Retrieves all custom data for page/post rendering
//
if( ! function_exists( '_fh_get_page_data' ) ):
function _fh_get_page_data() {

    global $woocommerce_content_enable, $post, $wpseo_front;

    // Get current page url via utlity function
    $current_url = Utils::get_current_url();

   	// WordPress title generation
   	$current_title = wp_title( '-', false );

    $share_page = _fh_get_social_sharing( $current_url, $current_title, '', false, false );

    return array (
        'crumbs' => _fh_get_crumbs(),
        'page_title' => _fh_get_title(),
        'page_content' => _fh_get_content(),
        'sidebars_arr' => _fh_get_template_sidebars_arr(),
        'share_page' => $share_page
    );
}
endif;

//
//	Function for getting crumbs and running actions
//
if ( ! function_exists( '_fh_get_crumbs' ) ) :
function _fh_get_crumbs() {

	if( FH_BREADCRUMBS_ENABLED ) {

		// Look for crumbs-overriding custom action
		if( has_action( '_fh_the_crumbs' ) ) {

			ob_start();
			echo do_action( '_fh_the_crumbs' );
			return ob_get_clean();
		}

		global $woocommerce_content_enable, $post;
		return ( function_exists('yoast_breadcrumb') && !is_front_page() ) ? yoast_breadcrumb( '', '', false ) : '';
	}

	return false;
}
endif;

//
//	Function for wrapping pages in ID "page_article"
//
if ( ! function_exists( '_fh_page_prepend' ) ) :
function _fh_page_wrap_test() {

	return ( is_page() || is_search() || is_404() );
}
endif;

if ( ! function_exists( '_fh_page_prepend' ) ) :
function _fh_page_prepend() {

	if( _fh_page_wrap_test() ) {

		echo '<div id="page_article">';
	}
}
add_action( '_fh_content_prepend', '_fh_page_prepend' );
endif;

if ( ! function_exists( '_fh_page_append' ) ) :
function _fh_page_append() {

	if( _fh_page_wrap_test() ) {

		echo '</div>';
	}
}
add_action( '_fh_content_append', '_fh_page_append' );
endif;


//
//  Function for getting the title and running actions
//
if ( ! function_exists( '_fh_get_title' ) ) :
function _fh_get_title() {

	global $woocommerce_content_enable, $post;

	// Look for title-overriding custom action
	if( has_action( '_fh_the_title' ) ) {

		ob_start();
		echo do_action( '_fh_the_title' );
		return ob_get_clean();
	}

	return ( $woocommerce_content_enable ) ? '' : '<h1>' . $post->post_title . '</h1>';
}
endif;

//
//  Function for getting the content and running actions
//
if ( ! function_exists( '_fh_get_content' ) ) :
function _fh_get_content( $echo = false ) {

    ob_start();
    do_action('_fh_content_prepend');
    do_action('_fh_the_content');
    do_action('_fh_content_append');

    if( $echo ) {

    	$content = ob_get_clean();
    	echo $content;
    	return $content;
    }

    return ob_get_clean();
}
endif;

//
//	Base function for only displaying content
//
if ( ! function_exists( '_fh_content' ) ) :
function _fh_content() {

	global $woocommerce_content_enable;

	echo '<div class="the_content clearfix">';

    if( empty( $woocommerce_content_enable ) ) {

    	// Load post
    	the_post();

    	// Display content
    	the_content();

    } elseif( function_exists( woocommerce_content() ) ) {

    	woocommerce_content();
    }

	echo '</div>';

    return;
}
add_action( '_fh_the_content', '_fh_content' );
endif;

//
//  Blog post content generation
//
if ( ! function_exists( '_fh_blog_content' ) ) :
function _fh_blog_content() {

    global $post;

    // Blog Articles
    if ( have_posts() ) {

        // Loop over posts
        while ( have_posts() ) {

            // Load post
            the_post();

            // Call custom post excerpt display function
            _fh_display_post_excerpt( $post );
        }

        // Clear post to ensure nothing is lingering in the global namespace
        // - NOTE: not doing this will cause issues with the social sharing options
        $post = null;

        // Display pagination
        echo '<div id="wrap_paginate">' . "\n";
        echo _fh_paginate();
        echo '</div>' . "\n";

    } else {

    	echo '<div class="alert alert-info">';
        echo '<p class="text-center">No items to display!</p>';
        echo '</div>';
    }
}
endif;

//
//  Function for getting a dynamic sidebar without immediate echo
//
if ( ! function_exists( '_fh_get_dynamic_sidebar_html_no_echo' ) ) :
function _fh_get_dynamic_sidebar_html_no_echo( $sidebar_id ) {

    ob_start();
    dynamic_sidebar( $sidebar_id );
    return ob_get_clean();

}
endif;

//
//  Function for loading the default folkhack sidebar segments
//  - Normally called like - _fh_get_sidebar_segments_html( FH_SIDEBAR_A_ID, FH_SIDEBAR_A_SEGMENTS, FH_SIDEBAR_CLASSES )
//
if ( ! function_exists( '_fh_get_sidebar_segments_html' ) ) :
function _fh_get_sidebar_segments_html( $sidebar_id, $sidebar_segments, $sidebar_classses ) {

    $html = '';

    for( $i = 1; $i <= $sidebar_segments; $i++ ) {

        $local_id = $sidebar_id . '_' . $i;

        if( is_active_sidebar( $local_id ) && is_dynamic_sidebar( $local_id ) ) {

            // Output sidebar logic
            $html .= '<div id="_fh_sidebar_' . $local_id . '" class="segment ' . $sidebar_classses . '">' . "\n";
            $html .= '<ul>' . "\n";

            $html .= _fh_get_dynamic_sidebar_html_no_echo( $local_id );

            $html .= '</ul>' . "\n";
            $html .= '</div>' . "\n";
        }
    }

    return $html;
}
endif;

//
//  Function for loading individual sidebar segments
//
if ( ! function_exists( '_fh_get_sidebar_html' ) ) :
function _fh_get_sidebar_html( $sidebar_id, $sidebar_classses = '' ) {

    $html = '';

    if( is_active_sidebar( $sidebar_id ) && is_dynamic_sidebar( $sidebar_id ) ) {

        // Output sidebar logic
        $html .= '<div id="_fh_sidebar_' . $sidebar_id . '" class="segment ' . $sidebar_classses . '">' . "\n";
        $html .= '<ul>' . "\n";

        $html .= _fh_get_dynamic_sidebar_html_no_echo( $sidebar_id );

        $html .= '</ul>' . "\n";
        $html .= '</div>' . "\n";
    }

    return $html;
}
endif;

//
//	Looks for certain configuration values and returns correct sidebars
//  - Returns array with 'sidebar_left_segments' and 'sidebar_right_segments' containing
//	  rendered HTML for sidebars
//
if ( ! function_exists( '_fh_get_template_sidebars_arr' ) ) :
function _fh_get_template_sidebars_arr() {

	// Grab the global vars to test on
    global $woocommerce_content_enable;
    global $woocommerce_sidebars_enable;
    global $holy_grail_class;

    // Check to see if page is blog related
    $blog_content_enable = _fh_is_blog_related( FH_SEARCH_IS_BLOG_RELATED );

    // Return array
    $ret_arr = array();

    //////////////////// SIDEBAR LEFT /////////////////////
    ob_start();
    do_action( '_fh_sidebar_left_prepend' );

    // Look for sidebar left custom action override
	if( has_action( '_fh_sidebar_left_content' ) ) {

		do_action( '_fh_sidebar_left_content' );

	} else {

		// Look at current Holy Grail layout class to see if we're rendering sidebar A or B
		switch( $holy_grail_class ) {

			case 'three_left_1':
			case 'three_right_1':
			case 'three_wrapped_1':
			case 'two_left_1':
			case 'two_right_2':

				_fh_sidebar_a_render( $blog_content_enable, $woocommerce_content_enable, $woocommerce_sidebars_enable );
				break;

			default:

				_fh_sidebar_b_render( $blog_content_enable, $woocommerce_content_enable, $woocommerce_sidebars_enable );
				break;
		}
	}

    do_action( '_fh_sidebar_left_append' );
    $ret_arr['sidebar_left_segments'] = ob_get_clean();

    ////////////////// END SIDEBAR LEFT ///////////////////

    //////////////////// SIDEBAR RIGHT /////////////////////
    ob_start();
    do_action( '_fh_sidebar_right_prepend' );

    // Look for sidebar right custom action override
	if( has_action( '_fh_sidebar_right_content' ) ) {

		do_action( '_fh_sidebar_right_content' );

	} else {

		// Look at current Holy Grail layout class to see if we're rendering sidebar A or B
		switch( $holy_grail_class ) {

			case 'three_left_2':
			case 'three_right_2':
			case 'three_wrapped_2':
			case 'two_left_2':
			case 'two_right_1':

				_fh_sidebar_a_render( $blog_content_enable, $woocommerce_content_enable, $woocommerce_sidebars_enable );
				break;

			default:
				_fh_sidebar_b_render( $blog_content_enable, $woocommerce_content_enable, $woocommerce_sidebars_enable );
				break;
		}
	}

    do_action( '_fh_sidebar_right_append' );
    $ret_arr['sidebar_right_segments'] = ob_get_clean();

    ////////////////// END SIDEBAR RIGHT ///////////////////

    // Return array with 'sidebar_left_segments' and 'sidebar_right_segments' containing rendered HTML for sidebars
    return $ret_arr;
}
endif;

//
//	Sidebar A rendering function
//
if ( ! function_exists( '_fh_sidebar_a_render' ) ) :
function _fh_sidebar_a_render( $blog_content_enable, $woocommerce_content_enable, $woocommerce_sidebars_enable ) {

	do_action( '_fh_sidebar_a_prepend' );

	// Display sidebars
    if( $blog_content_enable && ! FH_BLOG_DEFAULT_SIDEBARS && ! $woocommerce_content_enable ) {

        // Blog sidebars
        echo _fh_get_sidebar_segments_html( FH_BLOG_SIDEBAR_A_ID, FH_BLOG_SIDEBAR_A_SEGMENTS, FH_SIDEBAR_CLASSES . ' blog_sidebars' );

    } elseif(
    	( isset( $woocommerce_sidebars_enable ) && $woocommerce_sidebars_enable ) ||
    	( isset( $woocommerce_content_enable ) && $woocommerce_content_enable ) &&
    	! FH_WOO_DEFAULT_SIDEBARS
    ) {

        // WooCommerce sidebars
        echo _fh_get_sidebar_segments_html( FH_WOO_SIDEBAR_A_ID, FH_WOO_SIDEBAR_A_SEGMENTS, FH_SIDEBAR_CLASSES . ' woo_sidebars' );

    } else {

        // Default sidebars
        echo _fh_get_sidebar_segments_html( FH_SIDEBAR_A_ID, FH_SIDEBAR_A_SEGMENTS, FH_SIDEBAR_CLASSES . ' default_sidebars' );
    }

    do_action( '_fh_sidebar_a_append' );
}
endif;

//
//	Sidebar B rendering function
//
if ( ! function_exists( '_fh_sidebar_b_render' ) ) :
function _fh_sidebar_b_render( $blog_content_enable, $woocommerce_content_enable, $woocommerce_sidebars_enable ) {

	do_action( '_fh_sidebar_b_prepend' );

	// Display sidebars
    if( $blog_content_enable && ! FH_BLOG_DEFAULT_SIDEBARS && ! $woocommerce_content_enable ) {

        // Blog sidebars
        echo _fh_get_sidebar_segments_html( FH_BLOG_SIDEBAR_B_ID, FH_BLOG_SIDEBAR_B_SEGMENTS, FH_SIDEBAR_CLASSES . ' blog_sidebars' );

    } elseif(
    	( isset( $woocommerce_sidebars_enable ) && $woocommerce_sidebars_enable ) ||
    	( isset( $woocommerce_content_enable ) && $woocommerce_content_enable ) &&
    	! FH_WOO_DEFAULT_SIDEBARS
    ) {

        // WooCommerce sidebars
        echo _fh_get_sidebar_segments_html( FH_WOO_SIDEBAR_B_ID, FH_WOO_SIDEBAR_B_SEGMENTS, FH_SIDEBAR_CLASSES . ' woo_sidebars' );

    } else {

        // Default sidebars
        echo _fh_get_sidebar_segments_html( FH_SIDEBAR_B_ID, FH_SIDEBAR_B_SEGMENTS, FH_SIDEBAR_CLASSES . ' default_sidebars' );
    }

    do_action( '_fh_sidebar_b_append' );
}
endif;

//
//	Returns true if a blog has more than 1 category.
//  - http://themeshaper.com/2012/11/01/the-wordpress-theme-index-template/
//
if ( ! function_exists( '_fh_categorized_blog' ) ) :
function _fh_categorized_blog() {

	if ( false === ( $all_the_cool_cats = get_transient( 'all_the_cool_cats' ) ) ) {

		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'hide_empty' => 1,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'all_the_cool_cats', $all_the_cool_cats );
	}

	if ( '1' != $all_the_cool_cats ) {
		// This blog has more than 1 category so _fh_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so _fh_categorized_blog should return false.
		return false;
	}
}
endif;

//
//	Flush out the transients used in _fh_categorized_blog
//
if ( ! function_exists( '_fh_category_transient_flusher' ) ) :
function _fh_category_transient_flusher() {

	// Like, beat it. Dig?
	delete_transient( 'all_the_cool_cats' );
}
add_action( 'edit_category', '_fh_category_transient_flusher' );
add_action( 'save_post',     '_fh_category_transient_flusher' );
endif;

//
//	favicon support for both front-end & back-end
//
if ( ! function_exists( '_fh_favicon' ) ) :
function _fh_favicon() {

	ob_start();
	?>

<!-- Favicon -->
<link href='<?php echo get_stylesheet_directory_uri(); ?>/img/icons/apple-touch-icon-precomposed.png' rel='apple-touch-icon-precomposed'>
<link href='<?php echo get_stylesheet_directory_uri(); ?>/img/icons/apple-touch-icon-ipad-precomposed.png' rel='apple-touch-icon-precomposed' sizes='72x72'>
<link href='<?php echo get_stylesheet_directory_uri(); ?>/img/icons/apple-touch-icon-iphone-retina-precomposed.png' rel='apple-touch-icon-precomposed' sizes='114x114'>
<link href='<?php echo get_stylesheet_directory_uri(); ?>/img/icons/apple-touch-icon-ipad-retina-precomposed.png' rel='apple-touch-icon-precomposed' sizes='144x144'>
<link href='<?php echo get_stylesheet_directory_uri(); ?>/img/icons/favicon.ico' rel='icon' type='image/vnd.microsoft.icon'>
<link href='<?php echo get_stylesheet_directory_uri(); ?>/img/icons/favicon.ico' rel='shortcut icon'>
<meta content='#f5f5f5' name='msapplication-TileColor'>
<meta content='<?php echo get_stylesheet_directory_uri(); ?>/img/icons/favicon.png' name='msapplication-TileImage'>
<!-- /Favicon -->

	<?php

	echo ob_get_clean();
	return;
}
add_action( 'wp_head', '_fh_favicon' );
add_action( 'admin_head', '_fh_favicon' );
endif;

//
//	Temporary CSS in <head> support for both front-end & back-end
//
if ( ! function_exists( '_fh_sitewide_temp_css' ) && FH_SITE_TEMP_CSS_ENABLED ) :
function _fh_sitewide_temp_css() {

	$temp_css = strip_tags( trim( get_field( 'sitewide_temp_css', 'options' ) ) );

	if( ! empty( $temp_css ) ) {

		echo '<style>'  . "\n";
		echo $temp_css  . "\n";
		echo '</style>' . "\n";
	}

	return;
}
add_action( 'wp_head', '_fh_sitewide_temp_css', 9999 );
add_action( 'admin_head', '_fh_sitewide_temp_css', 9999 );
endif;


//
//	Function for previous/next post navigation
//
if ( ! function_exists( '_fh_prev_next_posts' ) ) :
function _fh_prev_next_posts() {
?>
<br>

<!-- Previous/next post: -->
<div class="text-center">
	<div id="blog_article_nav">
		<?php

			// Prev post:
			$prev_post = get_previous_post();
			if ( ! empty( $prev_post ) ):

				$prev_post_title = ( ! empty( $prev_post->post_title ) ) ?
					$prev_post->post_title :
					'Previous Post';
		?>

			<a href="<?php echo get_permalink( $prev_post->ID ); ?>" class="btn btn-default btn-xs pull-left"><span class="fa fa-arrow-left"></span> &nbsp;<?php echo $prev_post_title; ?></a>

		<?php endif; ?>

		<?php

			// Next post:
			$next_post = get_next_post();
			if ( ! empty( $next_post ) ):

				$next_post_title = ( ! empty( $next_post->post_title ) ) ?
					$next_post->post_title :
					'Next Post';
		?>

			<a href="<?php echo get_permalink( $next_post->ID ); ?>" class="btn btn-default btn-xs pull-right"><?php echo $next_post_title; ?> &nbsp;<span class="fa fa-arrow-right"></span></a>

		<?php endif; ?>

		<br>
	</div>
</div>
<?php
}
endif;


//
//	Utility function to grab image data from Wordpress attachments
//	- Based on http://wordpress.org/ideas/topic/functions-to-get-an-attachments-caption-title-alt-description
//
if ( ! function_exists( '_fh_get_img_attachment' ) ) :
function _fh_get_img_attachment( $attachment_id ) {

	$attachment = get_post( $attachment_id );

	return array(
		'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
		'caption' => $attachment->post_excerpt,
		'description' => $attachment->post_content,
		'href' => get_permalink( $attachment->ID ),
		'src' => $attachment->guid,
		'title' => $attachment->post_title
	);
}
endif;


//
//	WordPress' missing _fh_is_blog_related() function. Determines if the currently viewed page is
//	one of the blog pages, including the blog home page, archive, category/tag, author, or single
//	post pages.
//	- Based on https://gist.github.com/wesbos/1189639
//
if ( ! function_exists( '_fh_is_blog_related' ) ) :
function _fh_is_blog_related( $include_search = true ) {

    global $post;

    //Post type must be 'post'.
    $post_type = get_post_type($post);

    // Check all blog-related conditional tags, as well as the current post type,
    // to determine if we're viewing a blog page.
    return (
        ( is_home() || is_archive() || is_single() )
        || ( $include_search && is_search() )
        || ( $post_type == 'post' )
    );
}
endif;


//
//	Twig Actions
//
function __fh_twig_pre_template() { do_action( '_fh_twig_pre_template' ); }
function __fh_twig_post_template() { do_action( '_fh_twig_post_template' ); }

?>
