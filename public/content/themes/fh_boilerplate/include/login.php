<?php

/**
 *
 * login.php
 *
 * Customizes default WordPress login page to be client-branded.
 *
 * - Enqueue a custom login page css asset and fix WordPress link
 * - Change logo link from wordpress.org current site
 * - Change logo alt text to site name
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

//
//  Enqueue a custom login page css asset and fix WordPress link
//
if( ! function_exists( '_fh_login_css' ) ):
function _fh_login_css() {

    // Login stylesheet small enough, no minification required
    wp_enqueue_style( 'custom_login', get_stylesheet_directory_uri() . '/css/login.css', false );
}
add_action( 'login_enqueue_scripts', '_fh_login_css', 10 );
endif;


//
//  Change logo link from wordpress.org current site
//
if( ! function_exists( '_fh_login_url' ) ):
function _fh_login_url() { return home_url(); }
add_filter( 'login_headerurl', '_fh_login_url' );
endif;


//
//  Change logo alt text to site name
//
if( ! function_exists( '_fh_login_title' ) ):
function _fh_login_title() { return get_option( 'blogname' ); }
add_filter( 'login_headertitle', '_fh_login_title' );
endif;

?>