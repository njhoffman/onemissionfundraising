<?php

/**
 *
 * menus.php
 *
 * Registers base menu locations for Boilerplate
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

//
//	Initialize custom menu locations
//
if ( ! function_exists( '_fh_custom_menus' ) ) :
function _fh_custom_menus() {

	register_nav_menus(
		array(
			'main-nav'   => __( 'Main Navigation', 'fh_boilerplate' ),
			'top-nav'    => __( 'Top Navigation', 'fh_boilerplate' ),
			'footer-nav' => __( 'Footer Navigation', 'fh_boilerplate' ),
			'site-map'   => __( 'Sitemap', 'fh_boilerplate' )
		)
	);
}
add_action( 'init', '_fh_custom_menus' );
endif;

?>