<?php

/**
 *
 * Default Boilerplate WordPress Archive Page
 *
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

//
//  Generates correct title tag based off of archive
//
if( ! function_exists( '_fh_blog_archive_title' ) ):
function _fh_blog_archive_title() {

    if ( have_posts() ) {

        // Calculate date of archive
        if ( is_day() ) {
            $archive_date = get_the_date( 'F D, Y' );
        } elseif ( is_month() ) {
            $archive_date = get_the_date( 'F Y' );
        } elseif ( is_year() ) {
            $archive_date = get_the_date( 'Y' );
        } else {
            $archive_date = '';
        }
    }

    // Formating for "Archive" vs "Archive: "
    echo ( ! empty( $archive_date) ) ? '<h1>' . $archive_date . '</h1>' : '';
}
add_action( '_fh_the_title', '_fh_blog_archive_title' );
endif;

// Unload default content action and load blog loop
remove_action( '_fh_the_content', '_fh_content' );
add_action( '_fh_the_content', '_fh_blog_content' );

$holy_grail_class = FH_DEFAULT_BLOG_FORMAT;
include( 'templates/holy_grail_master.php');

?>