<?php

/**
 * config.php
 *
 * Site-wide configuration options for Boilerplate - duplicate config can be found in child theme
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! DEFINED( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

DEFINE( 'FH_BOILERPLATE_VERSION', '1.6.3' );

//
//  Development / Utility
//  - Development mode, disabled login, disable wpautop
//
DEFINE( 'FH_DEVELOPMENT_MODE',        true ); // WARNING: Disable on production sites!!!
DEFINE( 'FH_DISABLE_LOGIN',           false );
DEFINE( 'FH_DISABLED_LOGIN_ALLOW_IP', 'x.x.x.x' );
DEFINE( 'FH_DISABLE_WPAUTOP',         false );

//
//  Ignored server ports (for generating URL's in util_funcs.php - get_current_url())
//  - Typically 80 and 443
//  - If running through reverse proxy then you may need to add something like 8080
//    ie. Varnish setup
//  - Comma delimited, gets converted into array via explode - no spaces
//
DEFINE( 'FH_IGNORED_SERVER_PORTS', '80,443,8080' );

//
//  Setup default page template for page.php
//
DEFINE( 'FH_DEFAULT_PAGE_TEMPLATE', 'cols_two_right_1.php' );

//
//  Setup breadcrumbs
//
DEFINE( 'FH_BREADCRUMBS_ENABLED', true );

//
//  Title delimiter
//
DEFINE( 'FH_TITLE_DELIMITER', '|' );

//
//  Author Meta
//
DEFINE( 'FH_AUTHOR_META', 'Folkhack Studios - http://folkhack.com' );

//
//  Up-skirt Easter Egg
//
DEFINE( 'FH_EASTER_EGG', true );

//
//  Enable Site-wide ACF-driven CSS in header on "Options" admin page
//
DEFINE( 'FH_SITE_TEMP_CSS_ENABLED', true );

//
//  JS/CSS Asset Configuration
//
DEFINE( 'FH_OVERRIDE_JQUERY_WP_DEFAULT', false );
DEFINE( 'FH_MINIFIED_ASSETS_ENABLED',    false );
DEFINE( 'FH_USE_BLESSED_CSS',            false );
DEFINE( 'FH_USE_OPEN_SANS',              true );
DEFINE( 'FH_USE_FONT_AWESOME',           true );


//
//  Sidebars widgets configuration
//

//  Global sidebars classes
DEFINE( 'FH_SIDEBAR_CLASSES',   'well' );

//  Default sidebars:
DEFINE( 'FH_SIDEBAR_A_NAME',    'Sidebar A' ); // Sidebar right name
DEFINE( 'FH_SIDEBAR_B_NAME',    'Sidebar B' ); // Sidebar left name
DEFINE( 'FH_SIDEBAR_A_ENABLED',  true );       // Enable right sidebar
DEFINE( 'FH_SIDEBAR_B_ENABLED',  true );       // Enable left sidebar
DEFINE( 'FH_SIDEBAR_A_ID',      'sidebar_a' ); // A sidebar ID
DEFINE( 'FH_SIDEBAR_B_ID',      'sidebar_b' ); // B sidebar ID
DEFINE( 'FH_SIDEBAR_A_SEGMENTS', 2 );          // A sidebar num of segments
DEFINE( 'FH_SIDEBAR_B_SEGMENTS', 2 );          // B sidebar num of segments

//
//  Site-wide Option-driven Social Account Links
//
DEFINE( 'FH_SITE_OPTION_DRIVEN_SOCIALS', true );

//
//  Footer widgets configuration
//
DEFINE( 'FH_FOOTER_WIDGETS_ID',      'foot_widget' );
DEFINE( 'FH_FOOTER_WIDGETS_NUM',     3 );
DEFINE( 'FH_FOOTER_WIDGETS_BS_COL',  ( 12 / FH_FOOTER_WIDGETS_NUM ) ); // Bootstrap 12 col grid layout
DEFINE( 'FH_FOOTER_WIDGETS_CLASSES', 'foot_widget' );

//
//  Simple social buttons on pages/blog posts
//
DEFINE( 'FH_SOCIAL_SHARING_ENABLED',         true ); // Enables social sharing for site
DEFINE( 'FH_SOCIAL_SHARE_ON_POSTS',          true ); // Enable sharing for single posts
DEFINE( 'FH_SOCIAL_SHARE_ON_TAXONOMY',       true ); // Enable sharing for categories and tags
DEFINE( 'FH_SOCIAL_SHARE_ON_DATE',           true ); // Enable sharing for date archives
DEFINE( 'FH_SOCIAL_SHARE_ON_PAGES',          true ); // Enable sharing for pages
DEFINE( 'FH_SOCIAL_SHARE_ON_SEARCH',         true ); // Enable sharing for searches
DEFINE( 'FH_SOCIAL_SHARE_ON_BLOG_INDEX',     true ); // Enable sharing for blog index (is_home)
DEFINE( 'FH_SOCIAL_SHARE_ON_AUTHORS',        true ); // Enable sharing for authors
DEFINE( 'FH_SOCIAL_SHARE_ON_ATTACHMENTS',    true ); // Enable sharing for attachments
DEFINE( 'FH_SOCIAL_SHARE_ON_PRODUCTS',       true ); // Enable sharing for WooCommerce products
DEFINE( 'FH_SOCIAL_SHARE_ON_SHOP_TAXONOMY',  true ); // Enable sharing for WooCommerce categories and tags
DEFINE( 'FH_SOCIAL_SHARE_ON_MAIN_SHOP',      true ); // Enable sharing for WooCommerce main shop page

//
//  Attachments comments and meta
//
DEFINE( 'FH_ARTICLE_META_ON_ATTACHMENTS_ENABLED', true );
DEFINE( 'FH_COMMENTS_ON_ATTACHMENTS_ENABLED',     true );

//
//  Thumbnail/resizing quality percentage
//
DEFINE( 'FH_THUMBNAIL_QUALITY', 100 );

//
//  Disallow file edits/mods
//
DEFINE( 'DISALLOW_FILE_EDIT', true ); // Cannot change .php files
DEFINE( 'DISALLOW_FILE_MODS', true ); // Can't install plugins, themes, etc.

//
//  Hides WP admin bar from users
//
DEFINE( 'HIDE_ADMIN_BAR_FROM_USERS', true );

//
//  Search
//
DEFINE( 'FH_SEARCH_IS_BLOG_RELATED', false ); // Is search considered part of the blog?
DEFINE( 'FH_DEFAULT_SEARCH_FORMAT', 'three_wrapped_1' );

//
//  Blog configuration
//  - Enable override, Holy Grail config, sidebars, header, excerpt handling
//
DEFINE( 'FH_BLOG_ENABLED',            true );
DEFINE( 'FH_DEFAULT_BLOG_FORMAT',     'three_wrapped_1' );
DEFINE( 'FH_BLOG_H1',                 'Blog' );
DEFINE( 'FH_EXCERPT_WORD_LENGTH',     30 ); // Excerpt word length
DEFINE( 'FH_EXCERPT_ENDING',          ' [&#46;&#46;&#46;]' ); // String that appends excerpts

//  Blog Sidebars
DEFINE( 'FH_BLOG_DEFAULT_SIDEBARS',   true ); // Disable blog sidebars and use defaults

//  See "Default Sidebars" for configuration descriptions:
DEFINE( 'FH_BLOG_SIDEBAR_A_NAME',     'Blog A Sidebar' );
DEFINE( 'FH_BLOG_SIDEBAR_B_NAME',     'Blog B Sidebar' );
DEFINE( 'FH_BLOG_SIDEBAR_A_ENABLED',  true );
DEFINE( 'FH_BLOG_SIDEBAR_B_ENABLED',  true );
DEFINE( 'FH_BLOG_SIDEBAR_A_ID',       'blog_sidebar_a' );
DEFINE( 'FH_BLOG_SIDEBAR_B_ID',       'blog_sidebar_b' );
DEFINE( 'FH_BLOG_SIDEBAR_A_SEGMENTS', 2 );
DEFINE( 'FH_BLOG_SIDEBAR_B_SEGMENTS', 2 );

//
//  Single post Holy Grail configuration
//
DEFINE( 'FH_DEFAULT_SINGLE_FORMAT', 'two_right_1' );

//
//  Enable/disable commenting
//
DEFINE( 'FH_DISPLAY_COMMENTS_ENABLED', true );

//
//  Author configuraiton
//  - Author block support (author.php and single.php)
//  - Hide all author "Posted by: " and disable author.php
//      - NOTE: Also disables author blocks
//
DEFINE( 'FH_DISPLAY_AUTHOR_BLOCK', true );
DEFINE( 'FH_DISPLAY_AUTHOR_INFO', true );

//
//  WordPress PHPMailer Configuration
//  - leave empty for default "wordpress@site-domain.com"
//
DEFINE( 'FH_MAIL_FROM_SENDER', '' );

//
//  Sticky headers & sidebars
//
DEFINE( 'FH_STICKY_HEADER',   true );
DEFINE( 'FH_STICKY_SIDEBARS', false ); // TODO - get working - keep false =(

//
//  Sidr responsive asides support
//
DEFINE( 'FH_SIDR_ENABLED', true );

//
//  Back to Top Show at (in px)
//
DEFINE( 'FH_BACKTOTOP_SHOW_AT', 150 );

//
//  Contact Us Custom Fields/Template Options
//
DEFINE( 'FH_CONTACT_GMAPS_ENABLED',              true );
DEFINE( 'FH_CONTACT_DRIVING_DIRECTIONS_ENABLED', true );
DEFINE( 'FH_CONTACT_TEXT_ADDR_ENABLED',          true );
DEFINE( 'FH_CONTACT_PHONE_NUM_ENABLED',          true );
DEFINE( 'FH_CONTACT_FAX_NUM_ENABLED',            true );
DEFINE( 'FH_CONTACT_EMAIL_ADDR_ENABLED',         true );
DEFINE( 'FH_CONTACT_SIDEBAR_ACTION',             '_fh_sidebar_right_content' );
DEFINE( 'FH_CONTACT_HOLY_GRAIL_CLASS',           'two_right_1' );

//
//  Masonry, MixItUp, Bootstrap 3 Gallery Support
//  - For both pages and gallery post format
//  - Corresponding SASS configuration in _vars.scss
//
DEFINE( 'FH_GALLERIES_ENABLED',                true ); // Controls ACF fields
DEFINE( 'FH_PAGE_GALLERIES_ENABLED',           true ); // Controls ACF fields
DEFINE( 'FH_POST_GALLERIES_ENABLED',           true ); // Controls ACF fields
DEFINE( 'FH_GAL_MASONRY_ENABLED',              true ); // Controls ACF fields
DEFINE( 'FH_GAL_MIXITUP_ENABLED',              true ); // Controls ACF fields
DEFINE( 'FH_GAL_BS3_CAROUSEL_ENABLED',         true ); // Controls ACF fields
DEFINE( 'FH_GAL_MASONRY_GUTTER',               5 ); // Default value for _fh_masonry_gallery args
DEFINE( 'FH_GAL_MASONRY_SINGLE_IMG_WIDTH',     270 ); // Default values for 'fh-masonry-onecol', and _fh_masonry_gallery args
DEFINE( 'FH_GAL_MASONRY_DOUBLE_IMG_WIDTH',     ( FH_GAL_MASONRY_SINGLE_IMG_WIDTH * 2 + FH_GAL_MASONRY_GUTTER ) ); // Default values for 'fh-masonry-twocol', and _fh_masonry_gallery args
DEFINE( 'FH_GAL_MASONRY_IMG_SIZE_NAME_SINGLE', 'fh-masonry-onecol' ); // Override to use different image size
DEFINE( 'FH_GAL_MASONRY_IMG_SIZE_NAME_DOUBLE', 'fh-masonry-twocol' ); // Override to use different image size
DEFINE( 'FH_GAL_MIXITUP_IMG_MAX_WIDTH',        230 ); // Default values for 'fh-mixitup', and _fh_mixitup_gallery args
DEFINE( 'FH_GAL_MIXITUP_IMG_MAX_HEIGHT',       230 ); // Default values for 'fh-mixitup', and _fh_mixitup_gallery args
DEFINE( 'FH_GAL_MIXITUP_IMG_SIZE_NAME',        'fh-mixitup' ); // Override to use different image size
DEFINE( 'FH_GAL_BS3_CAROUSEL_MAX_WIDTH',       640 ); // Default values for 'fh-bs3carousel-cropped', and _fh_bs3carousel_gallery args
DEFINE( 'FH_GAL_BS3_CAROUSEL_MAX_HEIGHT',      480 ); // Default values for 'fh-bs3carousel-cropped', and _fh_bs3carousel_gallery args
DEFINE( 'FH_GAL_BS3_CAROUSEL_IMG_SIZE_NAME',   'fh-bs3carousel-cropped' ); // Override to use different image size
DEFINE( 'FH_GAL_BS3_CAROUSEL_DEFAULT_ID',      'bs3_carousel'); // Default value for _fh_bs3carousel_gallery args

//
//  Post formats (minus gallery)
//  - Gallery post format controled via "FH_POST_GALLERIES_ENABLED"
//
DEFINE( 'FH_ASIDE_POST_FORMAT_ENABLED',  true );
DEFINE( 'FH_AUDIO_POST_FORMAT_ENABLED',  true );
DEFINE( 'FH_LINK_POST_FORMAT_ENABLED',   true );
DEFINE( 'FH_IMAGE_POST_FORMAT_ENABLED',  true );
DEFINE( 'FH_VIDEO_POST_FORMAT_ENABLED',  true );
DEFINE( 'FH_QUOTE_POST_FORMAT_ENABLED',  true );
DEFINE( 'FH_STATUS_POST_FORMAT_ENABLED', true );
DEFINE( 'FH_CHAT_POST_FORMAT_ENABLED',   true );

DEFINE( 'FH_LINK_POST_URL_CHAR_LIMIT',   9999 );

//
//  WooCommerce Support
//
DEFINE( 'FH_WOOCOMMERCE_SUPPORT',                         true );
DEFINE( 'FH_DEFAULT_WOOCOMMERCE_TEMPLATE',                'cols_two_right_1.php' );
DEFINE( 'FH_WOOCOMMERCE_CART_GLYPH',                      'fa fa-shopping-cart' );
DEFINE( 'FH_WOOCOMMERCE_SIMPLE_LINK_CLASSES',             'btn btn-primary navbar-btn' );
DEFINE( 'FH_WOOCOMMERCE_SIMPLE_LINK_NUM_ENABLE',          true );
DEFINE( 'FH_WOOCOMMERCE_SIMPLE_LINK_TOTAL_ENABLE',        true );
DEFINE( 'FH_WOOCOMMERCE_NUM_PRODUCTS_PER_PAGE',           24 );
DEFINE( 'FH_WOOCOMMERCE_SINGLE_PRODUCT_UPSELLS_NUM_COLS', 4 );
DEFINE( 'FH_WOOCOMMERCE_SINGLE_PRODUCT_UPSELLS_LIMIT',    -1 ); // -1 for unlimited
DEFINE( 'FH_WOOCOMMERCE_BROWSE_PRODUCT_NUM_COLS',         4 );

//  WooCommerce Sidebars
DEFINE( 'FH_WOO_DEFAULT_SIDEBARS', false ); // Disable WooCommerce sidebars and use defaults

//  See "Default Sidebars" for configuration descriptions:
DEFINE( 'FH_WOO_SIDEBAR_A_NAME',     'WooCommerce A Sidebar' );
DEFINE( 'FH_WOO_SIDEBAR_B_NAME',     'WooCommerce B Sidebar' );
DEFINE( 'FH_WOO_SIDEBAR_A_ENABLED',  true );
DEFINE( 'FH_WOO_SIDEBAR_B_ENABLED',  true );
DEFINE( 'FH_WOO_SIDEBAR_A_ID',       'woocommerce_sidebar_a' );
DEFINE( 'FH_WOO_SIDEBAR_B_ID',       'woocommerce_sidebar_b' );
DEFINE( 'FH_WOO_SIDEBAR_A_SEGMENTS', 1 );
DEFINE( 'FH_WOO_SIDEBAR_B_SEGMENTS', 1 );

//
//  Yoast Support
//
DEFINE( 'FH_YOAST_DISABLED_FOR_NON_ADMINS', true );

//
//  Setup array of super admins capable of seeing administrative features
//
$fh_super_admins = array(
    'jmparks',
    'eagrego'
);
DEFINE( 'FH_DIPSHIT_MODE_ENABLED', true ); // Hides areas of admin area from those not in array above

//
//  Array for font-awesome classes (used also as the keys for ACF options page)
//  - Associates class to social network name with font-awesome and enables for user profiles
//  - Order of keys dictates sorting for both social icons and user_contactmethod
//  - "author" boolean dictates if social option shows up on user profiles via user_contactmethod
//
$fh_social_icons = array(

    // Popular Networks (sorted by general popularity):
    'facebook'     => array( 'join' => ' on ', 'network' => 'Facebook',  'author' => true ),
    'twitter'      => array( 'join' => ' on ', 'network' => 'Twitter',   'author' => true ),
    'google-plus'  => array( 'join' => ' on ', 'network' => 'Google+',   'author' => true ),
    'linkedin'     => array( 'join' => ' on ', 'network' => 'LinkedIn',  'author' => true ),
    'tumblr'       => array( 'join' => ' on ', 'network' => 'Tumblr',    'author' => true ),
    'flickr'       => array( 'join' => ' on ', 'network' => 'flickr',    'author' => true ),
    'instagram'    => array( 'join' => ' on ', 'network' => 'Instagram', 'author' => true ),
    'pinterest'    => array( 'join' => ' on ', 'network' => 'Pinterest', 'author' => true ),
    'youtube'      => array( 'join' => ' on ', 'network' => 'YouTube',   'author' => true ),
    'vimeo-square' => array( 'join' => ' on ', 'network' => 'Vimeo',     'author' => false ),
    'vine'         => array( 'join' => ' on ', 'network' => 'Vine',      'author' => false ),
    'github'       => array( 'join' => ' on ', 'network' => 'GitHub',    'author' => false ),
    'dribbble'     => array( 'join' => ' on ', 'network' => 'Dribble',   'author' => false ),

    // Less popular networks (sorted alphabetically):
    'adn'            => array( 'join' => ' on ', 'network' => 'ADN',            'author' => false ),
    'android'        => array( 'join' => ' - ',  'network' => 'Android',        'author' => false ),
    'apple'          => array( 'join' => ' - ',  'network' => 'Apple',          'author' => false ),
    'behance'        => array( 'join' => ' on ', 'network' => 'Behance',        'author' => false ),
    'bitbucket'      => array( 'join' => ' on ', 'network' => 'Bitbucket',      'author' => false ),
    'codepen'        => array( 'join' => ' on ', 'network' => 'CodePen',        'author' => false ),
    'delicious'      => array( 'join' => ' on ', 'network' => 'Delicious',      'author' => false ),
    'deviantart'     => array( 'join' => ' on ', 'network' => 'deviantART',     'author' => false ),
    'digg'           => array( 'join' => ' on ', 'network' => 'Digg',           'author' => false ),
    'dropbox'        => array( 'join' => ' on ', 'network' => 'DropBox',        'author' => false ),
    'foursquare'     => array( 'join' => ' on ', 'network' => 'Foursqure',      'author' => false ),
    'git'            => array( 'join' => ' - ',  'network' => 'Git',            'author' => false ),
    'gittip'         => array( 'join' => ' on ', 'network' => 'Gittip',         'author' => false ),
    'hacker-news'    => array( 'join' => ' on ', 'network' => 'Hacker News',    'author' => false ),
    'linux'          => array( 'join' => ' - ',  'network' => 'Linux',          'author' => false ),
    'openid'         => array( 'join' => ' - ',  'network' => 'OpenID',         'author' => false ),
    'pagelines'      => array( 'join' => ' on ', 'network' => 'PageLines',      'author' => false ),
    'reddit'         => array( 'join' => ' on ', 'network' => 'reddit',         'author' => false ),
    'renren'         => array( 'join' => ' on ', 'network' => 'Renren',         'author' => false ),
    'skype'          => array( 'join' => ' on ', 'network' => 'Skype',          'author' => false ),
    'soundcloud'     => array( 'join' => ' on ', 'network' => 'SoundCloud',     'author' => false ),
    'spotify'        => array( 'join' => ' on ', 'network' => 'Spotify',        'author' => false ),
    'stack-exchange' => array( 'join' => ' on ', 'network' => 'Stack Exchange', 'author' => false ),
    'stack-overflow' => array( 'join' => ' on ', 'network' => 'Stack Overflow', 'author' => false ),
    'steam'          => array( 'join' => ' on ', 'network' => 'Steam',          'author' => false ),
    'stumbleupon'    => array( 'join' => ' on ', 'network' => 'StumbleUpon',    'author' => false ),
    'trello'         => array( 'join' => ' on ', 'network' => 'Trello',         'author' => false ),
    'vk'             => array( 'join' => ' on ', 'network' => 'VK',             'author' => false ),
    'weibo'          => array( 'join' => ' on ', 'network' => 'Weibo',          'author' => false ),
    'windows'        => array( 'join' => ' on ', 'network' => 'Windows Live',   'author' => false ),
    'wordpress'      => array( 'join' => ' on ', 'network' => 'WordPress.com',  'author' => false ),
    'xing'           => array( 'join' => ' on ', 'network' => 'Xing',           'author' => false ),
    'yahoo'          => array( 'join' => ' on ', 'network' => 'Yahoo',          'author' => false ),

    // RSS:
    'rss' => array( 'join' => ' ', 'network' => 'RSS Feed', 'author' => true )
);

// Set the configuration set flag
DEFINE( 'FH_CONFIG_SET',         true );
DEFINE( 'FH_CHILD_THEME_LOADED', false ); // Should be true in child theme config.php

?>
