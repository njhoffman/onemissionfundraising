<?php
/**
 * Single Product Sale Flash
 *
 * FH Revisions:
 * + Added custom class to differentiate from other sales spans
 * + Updated "Sale!" -> "On Sale!"
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $post, $product;
?>
<?php if ( $product->is_on_sale() ) : ?>

	<?php echo apply_filters( 'woocommerce_sale_flash', '<span class="single_onsale">' . __( 'On Sale!', 'woocommerce' ) . '</span>', $post, $product ); ?>

<?php endif; ?>