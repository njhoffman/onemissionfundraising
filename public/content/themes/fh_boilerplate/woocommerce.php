<?php

/**
 *
 * Default Boilerplate WooCommerce "woocommerce.php" Support
 *
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

if ( ! defined( 'ABSPATH' ) ) exit( 'No direct access allowed.' );

// Set the flag to use woocommerce_content() rather than the_content();
$woocommerce_content_enable = true;

//
//	Form overrides
//	- Must reside here or will override default comment form
//
//	Author, email
if ( ! function_exists( '_fh_woo_review_fields' ) ) :
function _fh_woo_review_fields( $comment_form ) {

	$comment_form['fields'] = array(
		'author' =>
			'<div class="comment-form-author form-group">' . '<label for="author">' . __( 'Name', 'woocommerce' ) . ' <span class="required">*</span></label> ' .
			'<input id="author" class="form-control" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" aria-required="true" /></div>',
		'email'  =>
			'<div class="comment-form-email form-group"><label for="email">' . __( 'Email', 'woocommerce' ) . ' <span class="required">*</span></label> ' .
			'<input id="email" class="form-control" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" aria-required="true" /></div>',
		);

	return $comment_form;
}
add_filter( 'woocommerce_product_review_comment_form_args', '_fh_woo_review_fields' );
endif;

//	Textarea, ratings
if ( ! function_exists( '_fh_woo_comment_form_field_comment' ) ) :
function _fh_woo_comment_form_field_comment( $field ) {

	$html = '';

	if ( get_option( 'woocommerce_enable_review_rating' ) === 'yes' ) {

		$html .=
			'<div class="form-group"><label for="rating-dropdown">' . __( 'Your Rating', 'woocommerce' ) .'</label>' .
			'<select name="rating" id="rating-dropdown" class="form-control">
				<option value="">' . __( 'Rate&hellip;', 'woocommerce' ) . '</option>
				<option value="5">' . __( 'Perfect (5)', 'woocommerce' ) . '</option>
				<option value="4">' . __( 'Good (4)', 'woocommerce' ) . '</option>
				<option value="3">' . __( 'Average (3)', 'woocommerce' ) . '</option>
				<option value="2">' . __( 'Not that bad (2)', 'woocommerce' ) . '</option>
				<option value="1">' . __( 'Poor (1)', 'woocommerce' ) . '</option>
			</select></div>';
	}

	$comment_form['comment_field'] .= '<p class="comment-form-comment"><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea>' . wp_nonce_field( 'woocommerce-comment_rating', '_wpnonce', true, false ) . '</p>';

	$html .=
		'<div class="form-group comment-form-comment">' .
		'<label for="comment">' . __( 'Your Review', 'woocommerce' ) . '</label>' .
		'<textarea id="comment" class="form-control" name="comment" cols="45" rows="8" title="' . __( 'Comment','folkhack' ) . '" placeholder="' . __( 'Comment','folkhack' ) . '" aria-required="true"></textarea>' .
		wp_nonce_field( 'woocommerce-comment_rating', '_wpnonce', true, false ) .
		'</div>';

	return $html;
}
remove_filter('comment_form_field_comment', '_fh_comment_form_field_comment');
add_filter('comment_form_field_comment', '_fh_woo_comment_form_field_comment');
endif;

//
// Conditional checks for child theme template, if doesn't exist defaults to boilerplate
//
if( file_exists( get_stylesheet_directory() . '/templates/' . FH_DEFAULT_WOOCOMMERCE_TEMPLATE ) ) {

	require get_stylesheet_directory() . '/templates/' . FH_DEFAULT_WOOCOMMERCE_TEMPLATE;

} else {

	require get_template_directory() . '/templates/' . FH_DEFAULT_WOOCOMMERCE_TEMPLATE;
}

?>