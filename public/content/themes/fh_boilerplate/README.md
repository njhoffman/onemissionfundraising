# Folkhack's WordPress Theme

---

# Resources

### Tools

* [WordPress Gear](http://wpgear.org/) - HUGE repository of WordPress tools
* [GenerateWP](http://generatewp.com/) - Repository of WordPress code generators

### Documentation / etc.

* [WordPress Codex](http://codex.wordpress.org)
* [WordPress Blog](http://wordpress.org/news/)
* [WordPress Trac](https://core.trac.wordpress.org/)
* [WordPress Theme Unit Test](http://codex.wordpress.org/Theme_Unit_Test)
* [WordPress Time Saving Copy/Paste Cheat Sheet](http://www.buildyourownblog.net/blog/time-saving-copypaste-wordpress-cheat-sheet/)
* [WP TEST Unit Test](http://wptest.io/)
* [WPD Unofficial Plugin Directory](http://wpplugindirectory.org/)
* [Gravity Forms Docs](http://www.gravityhelp.com/documentation/page/Gravity_Forms_Documentation)
* [Gravity Forms Blog](http://www.gravityhelp.com/)
* [Gravity Forms Forums](http://www.gravityhelp.com/forums/)
* [Gravity Forms Hooks and Filters Reference](http://www.gravityhelp.com/documentation/page/Developer_Docs#Hooks_and_Filters)
* [WooCommerce Base Docs](http://docs.woothemes.com/documentation/plugins/woocommerce/)
* [WooCommerce API Docs](http://docs.woothemes.com/wc-apidocs/)
* [WooCommerce Hooks and Filters Reference](http://docs.woothemes.com/document/hooks/)
* [WooCommerce GitHub Repository](https://github.com/woothemes/woocommerce)
* [luetkemj Query Comprehensive Reference](https://gist.github.com/luetkemj/2023628)
* [WordPress Development Stack Exchange](http://wordpress.stackexchange.com/)
* [Reddit.com r/WordPress](http://www.reddit.com/r/wordpress)
* [Reddit.com r/ProWordPress](http://www.reddit.com/r/prowordpress)
* [ManageWP.org WordPress News Aggregate](http://managewp.org/)
* [hookr.io Hook/API Index](http://hookr.io/#)

---

# TODO

### Ongoing
* Convert all max-width/min-width to use Bootstrap config values within Boilerplate + BS extensions
* Convert all tabs to spaces within assets and set precedent for all core development
* Better documentation
* Browser testing
* Add in events/filters for API/extending
* Improve accessibility support (alt tags, etc.)

### Roadmap
* Sticky/affixed sidebars
* Related/popular/helpful links (in a tabbed interface?)
* Multilingual Support - http://wpml.org/
* BuddyPress Support - http://www.dezzain.com/wordpress-themes/mesocolumn/
* bbPress Support - http://bbpress.org/
* Annotated YT integration
* Post submission/moderation
* Post upvoting/downvoting and sorting like Reddit
* Purechat integration - https://www.purechat.com/

---

# Boilerplate Hook/Action Reference

### Actions
* "_fh_the_crumbs"
* "_fh_the_title"
* "_fh_header_prepend"
* "_fh_header_prepend_in_wrap"
* "_fh_header_append_in_wrap"
* "_fh_before_content"
* "_fh_content_prepend"
* "_fh_the_content"
* "_fh_content_append"
* "_fh_after_content"
* "_fh_sidebar_right_prepend"
* "_fh_sidebar_right_append"
* "_fh_sidebar_left_prepend"
* "_fh_sidebar_left_append"
* "_fh_sidebar_right_content"
* "_fh_sidebar_left_content"
* "_fh_sidebar_a_prepend"
* "_fh_sidebar_a_append"
* "_fh_sidebar_b_prepend"
* "_fh_sidebar_b_append"
* "_fh_footer_prepend"
* "_fh_footer_inside_prepend"
* "_fh_footer_cw_prepend"
* "_fh_footer_cw_content"
* "_fh_footer_cw_append"
* "_fh_footer_inside_append"
* "_fh_footer_append"

 ### Filters
 * _fh_display_post_meta

 ### To add
 * Header actions (ex: _fh_header_prepend) like footer in part_header.php
 * HTML Header/footer actions in part_html_head/foot.php

---

# Plugins

The following are the plugins we use to extend Folkhack's Boilerplate

### Advanced Custom Fields
* http://www.advancedcustomfields.com/
* http://wordpress.org/plugins/advanced-custom-fields/

Used for custom fields on contact us, gallery posts, image posts, links, etc. Displays additional fields for editing and displaying depending on conditionals.

```php
// Display a field value from the current post
the_field( "text_field" );

// Display a field value from another post
the_field( "text_field", 123 );

// Return a field value from the current post
get_field( "text_field" );

// Return a field value from another post
get_field( "text_field", 123 );
```

---

### Advanced Custom Fields: Gallery Field [paid]
* http://www.advancedcustomfields.com/add-ons/gallery-field/

Used for image galleries within posts and pages.

```php
// Data returned:
Array
(
    [id] => 540
    [alt] => A Movie
    [title] => Movie Poster: UP
    [caption] => sweet image
    [description] => a man and a baloon
    [url] => http://localhost:8888/acf/wp-content/uploads/2012/05/up.jpg
    [sizes] => Array
        (
            [thumbnail] => http://localhost:8888/acf/wp-content/uploads/2012/05/up-150x150.jpg
            [medium] => http://localhost:8888/acf/wp-content/uploads/2012/05/up-300x119.jpg
            [large] => http://localhost:8888/acf/wp-content/uploads/2012/05/up.jpg
            [post-thumbnail] => http://localhost:8888/acf/wp-content/uploads/2012/05/up.jpg
            [large-feature] => http://localhost:8888/acf/wp-content/uploads/2012/05/up.jpg
            [small-feature] => http://localhost:8888/acf/wp-content/uploads/2012/05/up-500x199.jpg
        )
)

// Debugging
var_dump( get_field('gallery') );

// Simple iteration
$images = get_field('gallery');

if( $images ) {

	foreach( $images as $image ) {

		echo $image['url'];
		echo $image['alt'];
		echo $image['caption'];
	}
}
```

---

### Advanced Custom Fields: Location Field
* http://www.advancedcustomfields.com/add-ons/google-maps/

This add-on adds a Google Map Location field type to the Advanced Custom Fields plugin.

```php
// Retrieving the value(s) on the front-end differs according to the Map address options

// Coordinates & address:
$location = get_field('location');
echo $location['address'];
echo $location['coordinates'];

// Display Coordinates
the_field('location');
```

---

### Advanced Custom Fields: Options Page [paid]
* http://www.advancedcustomfields.com/add-ons/options-page/

All the API functions can be used with the “Options Page’s” fields. However, a second parameter is required to target the options page. This is similar to passing through a post_id to target a specific post object.

```php
// The Options Page Add-on contains a filter to customize the admin title, access level and even sub option pages
acf_add_options_sub_page()
acf_set_options_page_title()
acf_set_options_page_capability()
acf_set_options_page_menu()

// Display option field (notice second option)
the_field('field_name', 'option');

// Return option field
$variable = get_field('field_name', 'option');
```

---

### Advanced Custom Fields: Repeater Field [paid]
* http://www.advancedcustomfields.com/add-ons/repeater-field/

The Repeater field acts as a table of data where you can define the columns (sub fields) and add infinite rows.

```php
// Basic PHP approach
$rows = get_field('repeater_field_name');

if( $rows ) {

	foreach( $rows as $row ) {

		echo $row['sub_field_1'];
		echo $row['sub_field_2'];
	}
}

// Using the repeater field with ACF Options Page
if( get_field('repeater_field_name', 'option' ) ) {

	while( has_sub_field( 'repeater_field_name', 'option' ) ) {

		the_sub_field('sub_field_1');
		the_sub_field('sub_field_2');
	}
}
```

---

### Advanced Responsive Video Embedder
* http://wordpress.org/plugins/advanced-responsive-video-embedder/
* http://nextgenthemes.com/plugins/advanced-responsive-video-embedder/documentation/
* https://github.com/nextgenthemes/advanced-responsive-video-embedder

Embeds via pasting the URL in its own line just like WordPress!

```
[vimeo id=23316783]
[youtube id=U4n_8R5lKnw]
```

* id – required for shortcodes and automatically generated within the shortcode button, obsolete for embeds via URL
* mode – normal/thumbnail, optional option override
* autoplay – optional option override
* maxwidth – maximal width for videos in normal mode, optional option override
* align – left/right/center
* start – only for youtube (in seconds) and vimeo (1m2s format)
* end – only for youtube (in seconds)

---

### Categories Images

Allow you to add an image to categories or any custom term.

```php
$icon = ( function_exists('z_taxonomy_image_url') ) ?
	z_taxonomy_image_url( $term->term_taxonomy_id ) :
	get_template_directory_uri() . '/img/icons/unknown.png';
```

---

### Gravity Forms [paid]
* http://www.gravityforms.com/

Easily create web forms and manage form entries within the WordPress admin. Specifically styled utilzing Bootstrap 3's LESS mixins within "/less/_gravity_forms.less"

#### Shortcodes:
* http://www.gravityhelp.com/documentation/page/Shortcodes
* http://www.gravityhelp.com/documentation/page/Embedding_A_Form

```
[gravityform id=1 title=false description=false ajax=true]
```

* id - The id of the form to be embedded. (required)
* title - Whether or not do display the form title. Defaults to 'false'. (optional)
* description - Whether or not do display the form description. Defaults to 'false'. (optional)
* ajax - Specify whether or not to use AJAX to submit the form.
* tabindex - Specify the starting tab index for the fields of this form.

#### PHP Embedding:
* http://www.gravityhelp.com/documentation/page/Embedding_A_Form

```php
gravity_form(
	$id_or_title,
	$display_title = true,
	$display_description = true,
	$display_inactive = false,
	$field_values = null,
	$ajax = false,
	$tabindex
);

// Basic function call
gravity_form( 1 );

// Using the form title instead of submission ID
gravity_form('Contact Us', false, false, false, '', false);
```

---

### Simple Image Sizes
* http://wordpress.org/plugins/simple-image-sizes/
* https://github.com/Rahe/Simple-image-sizes

Regenerates custom image sizes. Add options in media setting page for images sizes. MUST HAVE if utilzing thumbnails, resizing, etc.

---

### Timber
* http://www.wpmayor.com/articles/timber-templating-language-wordpress/
* http://twig.sensiolabs.org/
* http://jarednova.github.io/timber/
* http://wordpress.org/plugins/timber-library/

Twig based templating language. Utilized within templates with the "Holy Grail" content configuration.

---

### Widget Context
* http://wordpress.org/plugins/widget-context/
* https://github.com/kasparsd/widget-context-wporg

Widget Context allows you to show or hide widgets on certain sections of your site - front page, posts, pages, archives, search, etc. It also features section targeting by URLs (with wildcard support) for maximum flexibility.

---

### Black Studio TinyMCE Widget
* http://wordpress.org/plugins/black-studio-tinymce-widget/

This plugin adds a WYSIWYG text widgets based on the standard TinyMCE WordPress visual editor. Basically just a WYSIYWG for the widgets section of your theme.

---

### Relevanssi
* http://wordpress.org/plugins/relevanssi/
* http://www.relevanssi.com/
* http://www.relevanssi.com/buy-premium/

Let’s face the facts: WordPress search sucks. It doesn’t list best results first.

Relevanssi is a WordPress plugin that fixes the search. You get best results first, with highlights showing the exact part of the document that matched the query.

The users of your blog will thank you for making their search experience simply work. They’ll be able to find what they’re looking for.

---

### Yet Another Related Posts Plugin (YARPP)
* http://wordpress.org/plugins/yet-another-related-posts-plugin/
* http://www.yarpp.com/

Yet Another Related Posts Plugin (YARPP) displays pages, posts, and custom post types related to the current entry, introducing your readers to other relevant content on your site.

---

### Yoast's WordPress SEO Plugin ###
* https://yoast.com/wordpress/plugins/seo/
* https://yoast.com/wordpress/plugins/seo/#breadcrumbs

Use this for the breadcrumbs on the site - need to be able to navigate all points and is added into twig template as well as all page/blog templates. Need to turn on in Yoast's settings!

---

### WooCommerce
### WooCommerce Payment Plugin - Stripe
### WooCommerce Shipping Plugin - USPS
### WooCommerce Shipping Plugin - FedEx
### Gravity Forms - Mailchimp Integration
### Gravity Forms - User Registration Add-on
### Admin Bar Queries
### Avatar Manager
### Black Studio TinyMCE Widget
### Debug Objects
### Disable WordPress Updates
### Google Analytics for WordPress (Yoast)
### HTML Editor Syntax Highlighter
### Raw HTML
### Really Simple Captcha
### Better Search
### WordPress Importer
### WordPress Piwik support
### SSL Insecure Content Fixer
### User Switching
### Simple 301 Re-directs
### Nav Menu Roles
### Disqus Commennt System