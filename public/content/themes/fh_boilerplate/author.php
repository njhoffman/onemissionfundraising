<?php

/**
 *
 * Default Boilerplate WordPress Author Page
 *
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

//
//  Redirect if author is disabled
//
if( ! FH_DISPLAY_AUTHOR_INFO ) {

    header( 'HTTP/1.1 301 Moved Permanently' );
    header( 'Location: ' . home_url() );
}

//
//  Custom author heading
//
if( ! function_exists( '_fh_blog_author_title' ) ):
function _fh_blog_author_title() {

    echo '<h1>Author: ' . get_the_author() . '</h1>';
}
add_action( '_fh_the_title', '_fh_blog_author_title' );
endif;

//
//  Prepend blog content with author block
//
if( ! function_exists( '_fh_blog_prepend_author' ) ):
function _fh_blog_prepend_author() {

    // Display author block
    if( FH_DISPLAY_AUTHOR_BLOCK && FH_DISPLAY_AUTHOR_INFO ) {

        if ( have_posts() ) {

            // Grab the first post off of the stack (so we can use get_the_author)
            the_post();
        }

        // Get custom author block
        _fh_author_block( get_the_author_meta( 'ID' ), true, 'author_img_right' );

        // Rewind back one (1) post (due to previous the_post() call)
        rewind_posts();
    }
}
add_action( '_fh_content_prepend', '_fh_blog_prepend_author' );
endif;

// Unload default content action and load blog loop
remove_action( '_fh_the_content', '_fh_content' );
add_action( '_fh_the_content', '_fh_blog_content' );

$holy_grail_class = FH_DEFAULT_BLOG_FORMAT;
include( 'templates/holy_grail_master.php');

?>