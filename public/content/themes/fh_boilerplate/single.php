<?php

/**
 *
 * Default Boilerplate WordPress Single Post Page
 *
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

//
//  Custom single blog post heading
//
if( ! function_exists( '_fh_blog_single_title' ) ):
function _fh_blog_single_title() {

	$get_title = get_the_title();

    $title = '';

	// "aside" format hides title
    if(
    	(
    		! FH_ASIDE_POST_FORMAT_ENABLED ||
    		( FH_ASIDE_POST_FORMAT_ENABLED && ! has_post_format( 'aside' ) )
    	) &&
    	! empty( $get_title )
    ) {

    	$title = '<h1>' . $get_title . '</h1>';
    }

    echo $title;
}
add_action( '_fh_the_title', '_fh_blog_single_title' );
endif;

//
//	Custom single blog post content
//
if( ! function_exists( '_fh_blog_single_content' ) ):
function _fh_blog_single_content() {

	// Load post into global $post context
	$post = Timber::get_post();

	// Grab post type and format for article class
	$article_class_arr = array();
	$get_title = get_the_title();

	if( get_post_format() ) {

		array_push( $article_class_arr, 'fh_post_format_' . get_post_format() );

	} else {

		array_push( $article_class_arr, 'fh_post_format_none' );
	}

	if( get_post_type() ) {

		array_push( $article_class_arr, 'fh_post_type_' . get_post_type() );
	}

	if( empty( $get_title ) ) {

		array_push( $article_class_arr, 'fh_empty_title' );
	}

	$article_class = 'class="' . implode( ' ', $article_class_arr ) . '"';

	?>
	<!-- Single post -->
	<article <?php echo $article_class; ?>>
		<?php

			// Display post meta
			if( ! is_attachment() || FH_ARTICLE_META_ON_ATTACHMENTS_ENABLED ) {

				_fh_display_post_meta( $post );
			}

			if( FH_GALLERIES_ENABLED && FH_POST_GALLERIES_ENABLED && has_post_format( 'gallery' ) ):

				require 'templates/gallery_content.php';

			elseif( FH_LINK_POST_FORMAT_ENABLED && has_post_format( 'link' ) ):

				// Start link post format
				$link_url = get_field( 'link_url' );
				$show_link_url = ( strlen( $link_url ) >= FH_LINK_POST_URL_CHAR_LIMIT ) ?
					substr( $link_url, 0, $link_show_url_chars ) . '...' :
					$link_url;

				// Display link
				echo ( ! empty( $link_url ) ) ? '<div class="display_link"><a href="' . $link_url . '" rel="nofollow" target="_blank"><span class="fa fa-link"></span>' . $show_link_url . '</a></div>' : '';

				// Display post content
				do_action( 'pre_blog_article' );
				_fh_content();
				do_action( 'post_blog_article' );

			elseif( FH_IMAGE_POST_FORMAT_ENABLED && has_post_format( 'image' ) ):

				// Start image post format
				$image = get_field( 'image' );

				if( $image ) {

					// Display image
					echo '<div class="display_image magnific text-center"><a href="' . $image['url'] . '" rel="nofollow" target="_blank"><img src="' . $image['url'] . '" alt="'. $image['alt'] .'" title="' . $image['title'] .'" /></a></div>';
					echo '<hr>';
				}

				// Display post content
				do_action( 'pre_blog_article' );
				_fh_content();
				do_action( 'post_blog_article' );

			elseif( FH_QUOTE_POST_FORMAT_ENABLED && has_post_format( 'quote' ) ):

				// Start quote post format
				$quote_html = trim( get_field( 'quote' ) );

				echo '<blockquote>' . $quote_html . '</blockquote>';

				// Display post content
				do_action( 'pre_blog_article' );
				_fh_content();
				do_action( 'post_blog_article' );

				// End quote post format

			elseif( true && has_post_format( 'status' ) ):

				// Start status post format
				$status_html = trim( get_field( 'status' ) );
				$meta_string = '';
				$meta_string .= ( FH_DISPLAY_AUTHOR_INFO ) ?
					'---' . '<br>' . get_the_author_meta( 'display_name' ) :
					'';
				$meta_string .= '<br>' . get_the_date( 'n/j/Y' );

				echo '<div id="fh_status">' . $status_html . $meta_string . '</div><br>';

				// Display post content
				do_action( 'pre_blog_article' );
				_fh_content();
				do_action( 'post_blog_article' );

				// End status post format

			else:

				// Display post content
				do_action( 'pre_blog_article' );
				_fh_content();
				do_action( 'post_blog_article' );

				// End formats conditional
			endif;

			//
			//	Post page links (if applicable)
			//
			$args = array(
				'before' => '<ul class="pagination">',
				'after' => '</ul>',
				'before_link' => '<li>',
				'after_link' => '</li>',
				'current_before' => '<li class="active">',
				'current_after' => '</li>',
				'previouspagelink' => '&laquo;',
				'nextpagelink' => '&raquo;',
				'echo' => 0
			);

			$link_pagination = _fh_bootstrap_link_pages( $args );
			echo ( $link_pagination ) ? '<div class="text-center">' . $link_pagination . '</div>' : '';
		?>

		<?php if( ! is_attachment() ): ?>

			<?php _fh_prev_next_posts(); ?>

			<hr>

			<!-- Taxonomy & social sharing -->
			<div class="article_actions clearfix">

				<?php

					_fh_display_post_taxonomy( $post, 'taxonomy' );

					// if( FH_SOCIAL_SHARING_ENABLED && FH_SOCIAL_SHARE_ON_POSTS ) {
					if( FH_SOCIAL_SHARING_ENABLED && FH_SOCIAL_SHARE_ON_POSTS ) {

						_fh_get_social_sharing( get_permalink(), get_the_title(), 'pull-right' );
					}
				?>

			</div>

			<hr>

			<?php
				// Author block
				if( FH_DISPLAY_AUTHOR_BLOCK && FH_DISPLAY_AUTHOR_INFO ) {

					$author_block = _fh_author_block( get_the_author_meta( 'ID' ), true, 'author_img_right', false );
					echo ( $author_block ) ? $author_block . '<hr>' : '';
				}

				// Comments template
				if( FH_DISPLAY_COMMENTS_ENABLED ) {

					comments_template( '', true );
				}

				// Get posts page URL
				if ( get_option( 'show_on_front' ) == 'page' ) {

					$posts_page_url = get_permalink( get_option( 'page_for_posts' ) );

				} else {

					$posts_page_url = get_bloginfo( 'url' );
				}
			?>

		<!-- Back to Articles Link -->
		<p class="text-center hidden-print"><a href="<?php echo $posts_page_url ?>" class="btn btn-default btn-md btn-primary"><span class="fa fa-reply"></span>&nbsp; Back to Articles</a></p>

		<?php else: ?>

			<!-- Taxonomy & social sharing -->
			<div class="article_actions clearfix">

				<?php

					if( FH_SOCIAL_SHARING_ENABLED && FH_SOCIAL_SHARE_ON_ATTACHMENTS ) {

						_fh_get_social_sharing( get_permalink(), get_the_title() );
					}
				?>

			</div>

			<?php if( FH_COMMENTS_ON_ATTACHMENTS_ENABLED && FH_DISPLAY_COMMENTS_ENABLED ): ?>

				<hr>

				<?php

					// Comments template
					comments_template( '', true );
				?>

			<?php endif; ?>

		<?php endif; ?>

	</article>

	<?php
	// End content generation block
}
remove_action( '_fh_the_content', '_fh_content' );
add_action( '_fh_the_content', '_fh_blog_single_content' );
endif;

$holy_grail_class = FH_DEFAULT_SINGLE_FORMAT;
include( 'templates/holy_grail_master.php');

?>
