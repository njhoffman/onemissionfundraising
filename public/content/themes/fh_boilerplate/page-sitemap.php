<?php

/**
 *
 * Default Boilerplate WordPress Custom Site-map Page
 *
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

function custom_content() {
	// Menu location for the sitemap
	wp_nav_menu( array( 'theme_location' => 'site-map' ) );
}
add_action( '_fh_the_content', 'custom_content' );

$holy_grail_class = 'one_full';
include( 'templates/holy_grail_master.php');

?>