<?php

/**
 *
 * Default Boilerplate WordPress "index.php" Page
 *
 *
 * ╔═══════════════════════════════════╗
 * ║ © Copyright 2014 Folkhack Studios ║
 * ╚═══════════════════════════════════╝
 *
 */

//
//  Custom heading
//
if( ! function_exists( '_fh_blog_index_title' ) ):
function _fh_blog_index_title() {

	echo '<h1>' . FH_BLOG_H1 . '</h1>';
}
add_action( '_fh_the_title', '_fh_blog_index_title' );
endif;

// Unload default content action and load blog loop
remove_action( '_fh_the_content', '_fh_content' );
add_action( '_fh_the_content', '_fh_blog_content' );

$holy_grail_class = FH_DEFAULT_BLOG_FORMAT;
include( 'templates/holy_grail_master.php');

?>
