module.exports = function(grunt) {

grunt.loadNpmTasks('grunt-bowercopy');        // Download/install bower assets
grunt.loadNpmTasks('grunt-contrib-sass');     // SASS compilation
grunt.loadNpmTasks('grunt-contrib-watch');    // Watch directories for changed assets
grunt.loadNpmTasks('grunt-contrib-cssmin');   // CSS minification
grunt.loadNpmTasks('grunt-newer');            // Newer asset comparison for compilation
grunt.loadNpmTasks('grunt-contrib-concat');   // Concatination
grunt.loadNpmTasks('grunt-available-tasks');  // Available tasks documentation
grunt.loadNpmTasks('grunt-contrib-csslint');  // CSS Linter
grunt.loadNpmTasks('grunt-bless');            // Blesses CSS files so they work in IE

// Get and install site assets from Bower
grunt.registerTask('install', ['bowercopy', 'concat', 'compile-site']);

// Compile full public website (SASS, Handlebars Templates/etc., CSS Minification, JS Concatination)
grunt.registerTask('compile-site', ['compile-sass', 'concat']);

// Compiles site SASS and runs CSS minification, blessing, and csslinter
grunt.registerTask('compile-sass', ['sass', 'cssmin', 'bless', 'csslint']);

// Display available Grunt tasks
grunt.registerTask('help', ['availabletasks']);
grunt.registerTask('default', ['availabletasks']);

grunt.initConfig({
pkg:  grunt.file.readJSON('package.json'),

//
//  Available Tasks:
//
availabletasks: {
    tasks: {
        options: {
            sort: ['install', 'compile-site', 'compile-sass', 'watch', 'help'],
            filter: 'exclude',
            tasks: ['any-newer', 'availabletasks', 'bowercopy', 'concat', 'newer', 'newer-clean', 'newer-postrun', 'default'],
            descriptions: {
                'install': 'Get and install site assets from Bower',
                'compile-site': 'Compile full public website (SASS, CSS, and JS)',
                'compile-sass': 'Compiles site SASS and runs CSS minification, blessing, and csslinter',
                'watch': 'Watch SASS asset dir and compile assets',
                'help': 'Display this help doc. (also default)... also sup baby ;)'
            },
            groups: {
                'Folkhack Tasks': [ 'install', 'compile-site', 'compile-sass', 'watch', 'help' ],
                'Base Tasks': ['cssmin', 'sass']
            }
        }
    }
},

//
//  Bower Copy
//
bowercopy: {
    options: {
        clean: false
    },
    bootstrap: {
        options: {
            destPrefix: ''
        },
        files: {
            './fonts': 'twbs-bootstrap-sass/assets/fonts',
            './js/vendor/bootstrap': 'twbs-bootstrap-sass/assets/javascripts',
            './sass/vendor/bootstrap': 'twbs-bootstrap-sass/assets/stylesheets/bootstrap'
        }
    },
    jquery: {
        options: {
            destPrefix: './js/vendor/jquery'
        },
        files: {
            '': 'jquery/dist/jquery.*'
        }
    },
    masonry: {
        options: {
            destPrefix: './js/vendor/masonry'
        },
        files: {
            'jquery.masonry.pkgd.js':     'masonry/dist/masonry.pkgd.js',
            'jquery.masonry.pkgd.min.js': 'masonry/dist/masonry.pkgd.min.js'
        }
    },
    magnificpopup: {
        options: {
            destPrefix: './js/vendor/magnific'
        },
        files: {
            'jquery.magnific.js':    'magnific-popup/dist/jquery.magnific-popup.js',
            'jquery.magnific.min.js': 'magnific-popup/dist/jquery.magnific-popup.min.js',
            'magnific.css':  'magnific-popup/dist/magnific-popup.css'
        }
    },
    modernizr: {
        options: {
            destPrefix: './js/vendor'
        },
        files: {
            'modernizr.js': 'modernizr/modernizr.js'
        }
    },
    lodash: {
        options: {
            destPrefix: './js/vendor'
        },
        files: {
            'lodash': 'lodash/dist'
        }
    },
    opensans: {
        options: {
            destPrefix: ''
        },
        files: {
            './fonts/open-sans': 'open-sans-fontface/fonts',
            './sass/fonts/open-sans': 'open-sans-fontface/sass'
        }
    },
    fontawesome: {
        options: {
            destPrefix: ''
        },
        files: {
            './fonts/font-awesome': 'font-awesome/fonts',
            './sass/fonts/font-awesome': 'font-awesome/scss'
        }
    },
    mixitup: {
        options: {
            destPrefix: './js/vendor/mixitup'
      },
      files: {
            'jquery.mixitup.js':   'mixitup/src/jquery.mixitup.js',
            'jquery.mixitup.min.js': 'mixitup/build/jquery.mixitup.min.js'
      }
    },
    imagesloaded: {
      options: {
            destPrefix: './js/vendor/'
      },
      files: {
        'imagesloaded.js':   'imagesloaded/imagesloaded.js'
      }
    },
    qrjs: {
        options: { destPrefix: './js/vendor/qrjs' },
        files: {
            'qr.js': 'qr-js/qr.js',
            'qr.min.js': 'qr-js/qr.min.js',
            'qr.min.map': 'qr-js/qr.min.map'
        }
    },
    sidr: {
        options: { destPrefix: './js/vendor/sidr' },
        files: {
            'sidr.light.css': 'sidr/stylesheets/jquery.sidr.light.css',
            'sidr.dark.css': 'sidr/stylesheets/jquery.sidr.dark.css',
            'jquery.sidr.min.js': 'sidr/jquery.sidr.min.js'
        }
    },
    jqueryscroller: {
        options: { destPrefix: './js/vendor/smooth-scroll' },
        files: {
            'jquery.smooth-scroll.js': 'jquery.smooth-scroll/jquery.smooth-scroll.js',
            'jquery.smooth-scroll.min.js': 'jquery.smooth-scroll/jquery.smooth-scroll.min.js',
        }
    },
    respond: {
        options: { destPrefix: './js/vendor/respond' },
        files: {
            'respond.js': 'respond/dest/respond.src.js',
            'respond.min.js': 'respond/dest/respond.min.js'
        }
    },
    select2_bootstrap_3: {
        options: { destPrefix: './sass/vendor/select2' },
        files: {
            'select2-bootstrap.scss': 'select2-bootstrap3-css/lib/select2-bootstrap.scss'
        }
    },
    // pikabu: {
    //     options: { destPrefix: './js/vendor/pikabu' },
    //     files: {
    //         'pikabu.css': 'pikabu/build/pikabu.css',
    //         'pikabu.min.js': 'pikabu/build/pikabu.min.js',
    //         'pikabu.js': 'pikabu/src/pikabu.js'
    //     }
    // }
    // jqueryequalheights: {
    //   options: {
    //   destPrefix: './js/vendor'
    //   },
    //   files: {
    //   'jquery.equalheights.js':   'jquery.equalheights/jquery.equalheights.js',
    //   'jquery.equalheights.min.js': 'jquery.equalheights/jquery.equalheights.min.js'
    //   }
    // },
},

//
//  SASS Compilation
//
sass: {
    dist: {
        options: {
            style: 'compressed',
            lineNumbers: false,
            precision: 10
        },
		files: {
            // Base Assets:
            './css/bootstrap.css': 'sass/bootstrap.scss',
            './css/boilerplate.css': 'sass/boilerplate.scss',

            // Misc Assets:
            './css/login.css': 'sass/login.scss',
            './css/admin.css': 'sass/admin.scss',

            // Fonts:
            './css/font_awesome.css': 'sass/font_awesome.scss',
            './css/open_sans.css': 'sass/open_sans.scss'
        }
    }
},

//
//  CSS Minification
//
cssmin: {
    minify: {
        expand: true,
        files: {
            './css/bootstrap.min.css': './css/bootstrap.css',
            './css/boilerplate.min.css': './css/boilerplate.css',
            './css/open_sans.min.css': './css/open_sans.css',
            './css/font_awesome.min.css': './css/font_awesome.css'
        }
    }
},

//
//  Watch
//
watch: {
    css: {
        files: ['./sass/**/*.scss'],
        tasks: ['compile-sass']
    },
    livereload: {
        files: [
            './**/*.php',
            './js/**/*.{js,json}',
            './css/**/*.css',
            './img/**/*.{png,jpg,jpeg,gif,webp,svg}'
        ],
        options: { livereload: true }
    }
},

//
//  JS Concat
//
concat: {
    options: {
        separator: ';'
    },
    bootstrap: {
        src: [
            './js/vendor/bootstrap/bootstrap/affix.js',
            './js/vendor/bootstrap/bootstrap/carousel.js',
            './js/vendor/bootstrap/bootstrap/dropdown.js',
            './js/vendor/bootstrap/bootstrap/transition.js',
            './js/vendor/bootstrap/bootstrap/collapse.js',
            // './js/vendor/bootstrap/bootstrap/button.js',
            // './js/vendor/bootstrap/bootstrap/alert.js',
            // './js/vendor/bootstrap/bootstrap/tab.js',
            './js/vendor/bootstrap/bootstrap/scrollspy.js',
            // './js/vendor/bootstrap/bootstrap/modal.js',
            // './js/vendor/bootstrap/bootstrap/tooltip.js',
            // './js/vendor/bootstrap/bootstrap/popover.js',
        ],
        dest: './js/vendor/bootstrap/bootstrap.js'
    }
},

csslint: {
    options: {
        csslintrc: '.csslintrc'
    },
    strict: {
        options: {
            import: 2
        },
        src: [
            // './css/bootstrap.css',
            './css/boilerplate.css',
            './css/login.css',
            './css/admin.css'
        ]
    },
    lax: {
        options: {
            import: false
        },
        src: [
            // './css/bootstrap.css',
            './css/boilerplate.css',
            './css/login.css',
            './css/admin.css'
        ]
    }
},

bless: {
    css: {
        options: { cacheBuster: true },
        files: {
            './css/bootstrap.blessed.min.css': './css/bootstrap.min.css',
            './css/bootstrap.blessed.css': './css/bootstrap.css'
        }
    }
}
});
}

