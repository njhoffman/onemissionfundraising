<?php

/*
Plugin Name: One Mission Admin Fundraiser
Description: Custom fundraiser admin area
Author: Nick Hoffman
Version: 1.0
*/

function fundraiser_admin_theme_style() {
	if (true && !is_super_admin()) {
		wp_enqueue_style('fundraiser-admin-theme',    plugins_url('onemission-admin-fundraiser.css?ts=1', __FILE__));
		wp_enqueue_style('fundraiser-profile-editor', plugins_url('profile-editor.css?ts=1', __FILE__ ));
		wp_enqueue_style('fundraiser-supporters',     plugins_url('supporters.css?ts=1', __FILE__ ));
		wp_enqueue_style('fundraiser-promotion',      plugins_url('promotion.css?ts=1', __FILE__ ));
		wp_enqueue_style('fundraiser-dashboard',      plugins_url('dashboard.css?ts=1', __FILE__ ));
		wp_enqueue_style('fundraiser-post-update',    plugins_url('post-update.css?ts=1', __FILE__ ));
		add_action('in_admin_header', '_om_fundraiser_admin');
	}
}
add_action('admin_enqueue_scripts', 'fundraiser_admin_theme_style');
add_action('login_enqueue_scripts', 'fundraiser_admin_theme_style');


function _om_fundraiser_admin() {

	$page_name = basename($_SERVER['PHP_SELF']);
	if (($page_name === 'admin.php' || $page_name === 'index.php' ) && isset($_GET['page'])) {
		$page_name = $_GET['page'];
	}
	$page_class_name = str_replace('.php', '', $page_name);

	$show_setup_dialog = false;

	$profile_link = admin_url('?page=fundraiser-profile-editor&step=');

	$user_id = get_current_user_id();
	$profile_id = get_user_meta( $user_id, 'fundraiser_profile_id' )[0];

	$fundraiser_post = get_page($profile_id);
	$fundraiser_meta = get_post_meta($profile_id);
	$fundraiser_title = $fundraiser_post->post_title;
	$fundraiser_name = $fundraiser_post->post_name;
	$fundraiser_url = get_permalink($fundraiser_post);
	$fundraiser_meta['fundraiser_title'] = Array($fundraiser_title);
	$fundraiser_meta['fundraiser_name'] = Array($fundraiser_name);
	$field_map = _om_fundraiser_profile_editor_set_field_map($fundraiser_meta, $fundraiser_post);

	$profile_pages = _om_fundraiser_profile_editor_get_pages($field_map, $fundraiser_meta);
	$profile_curr = _om_fundraiser_profile_editor_get_step($profile_pages);
	$completion_status = _om_fundraiser_profile_editor_get_completion_status($profile_pages);
	$user_first_name = get_user_meta($user_id, 'first_name')[0];
	$user_last_name = get_user_meta($user_id, 'last_name')[0];


	// $subheader_title = $completion_status == 'approved' ? 'Manage Your Fund:' : "Let's Create Your Fund";
	$fundraiser_href = $completion_status == 'approved' || $completion_status == 'pending'
		? '<a href="' . $fundraiser_url . '" target="_blank">' . $fundraiser_title . '</a>' : $fundraiser_title;

	$subheader_title = '';
	$view_fund_url = $fundraiser_url;
	if ($fundraiser_post->post_status == 'pending') {
		$subheader_title = 'Your Fund is Currently Pending Approval';
		$view_fund_url = $fundraiser_url . '&preview=true&warn=true';
	} else if ($fundraiser_post->post_status == 'private') {
		$subheader_title = 'Your Fund is Currently Private';
		$view_fund_url = $fundraiser_url . '?preview=true&warn=true';
	} else if ($fundraiser_post->post_status == 'publish') {
	  	$subheader_title = 'Your Fund is Currently Public';
	} else if ($fundraiser_post->post_status == 'draft') {
		$view_fund_url = '';
	}

	foreach ($profile_pages as $idx => $page) {
		$profile_pages[$idx]['active'] = ($profile_curr == ($idx + 1));
	}

?>

<style>

	@import url('/content/themes/fh_boilerplate-child/css/bootstrap.blessed.min.css');
	@import url('/content/themes/fh_boilerplate-child/css/bootstrap.blessed.min-blessed1.css');
	@import url('/content/themes/fh_boilerplate-child/css/boilerplate.min.css');
	@import url('/content/themes/fh_boilerplate-child/css/style.css');

	html.wp-toolbar {
		padding-top: 0;
	}

	.toplevel_page_fundraiser-profile-editor #wpbody {
		padding-top: 0;
	}

	.toplevel_page_fundraiser-setup #adminmenumain {
		display: none;
	}
	.toplevel_page_fundraiser-setup #wpcontent {
		margin-left: 0;
		padding-left: 0;
	}
	.toplevel_page_fundraiser-setup #wpadminbar {
		display: none;
	}

	.auto-fold #wpcontent, .auto-fold #wpfooter {
		margin-left: 0 !important;
		padding-left: 0;
	}

	.navbar-header {
		width: 100%;
	}

</style>

<div id="om_affix" class="fund-setup-nav">
	<nav id="nav_header" class="navbar navbar-inverse" role="navigation">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#adminNav">MENU</button>
			<a class="navbar-brand" href="https://onemission.fund">
				<h1><img src="https://onemission.fund/content/themes/fh_boilerplate-child/img/logo_nav.png" title="One Mission" alt="One Mission"></h1>
			</a>
		</div>
			<!-- <div class="admin-nav-right pull-right"> -->
		<div id="adminNav" class="collapse navbar-collapse menu-header">
			<ul class="nav navbar-nav navbar-right">
				<li class="menu-item"><p class="navbar-btn"><a class="btn btn-purple" href="https://onemission.fund/help-center/" target="_blank">Help Center</a></p></li>
				<!-- <li class="menu&#45;item"><p class="navbar&#45;btn"><a class="btn btn&#45;success" href="#">Save for Later</a></p></li> -->
				<?php if(!empty($view_fund_url)): ?>
				<li class="menu-item"><a title="View My Fund" href="<?php echo $view_fund_url; ?>" target="_blank">View My Fund</a></li>
				<?php endif; ?>
				<li class="menu-item"><a title="My Orders" href="/my-account/">My Orders</a></li>
				<li class="menu-item"><a title="Logout" href="<?php echo wp_logout_url(); ?>">Logout</a></li>
				<li class="hidden-lg hidden-md hidden-sm">
					<div class="help text-center">
						<hr>
						<h5 class="text-white">Need Help? Contact Your Fund Ambassador:</h5>

								<p class="text-white"><strong>John</strong><br>
								<a href="mailto:john@onemission.fund">john@onemission.fund</a><br>
								319-895-4072</p>
					</div>
				</li>
			</ul>
		</div>
	</nav>
</div>

<div class="fund-admin <?php echo $completion_status; ?>">
	<div class="admin-footer text-center hidden-lg hidden-md hidden-sm">
				<a href="https://onemission.fund/help-center/" class="btn btn-sm btn-block btn-purple" target="_blank">Need Help? Go to the Help Center &rarr;</a>
			</div>
	<div class="fund-header">
		<div class="container">
			<div class="table-display">
				<div class="heading">
					<h2>Welcome <?php echo $user_first_name; ?></h2>
					<?php if (isset($fundraiser_href)): ?>
					<h4><span><?php echo $fundraiser_href; ?></span></h4>
					<h4><span><?php echo $subheader_title; ?></span></h4>
					<?php endif; ?>
				</div>
				<div class="help hidden-xs">
					<h4>Need Help? Contact Your Fund Ambassador:</h4>
					<div class="table-display">
						<div class="help-pic">
							<img src="/content/themes/fh_boilerplate-child/img/john.jpg" title="John" alt="John">
						</div>
						<div class="help-info">
							<p><strong>John</strong></p>
							<p><a href="mailto:john@onemission.fund">john@onemission.fund</a></p>
							<p>319-895-4072</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container space-top-4">
		<div class="row">
			<ul class="col-md-3" id="adminmenu-custom">
				<li class="menu-top menu-icon-dashboard" id="menu-dashboard">
					<a href="index.php"
						class="menu-top menu-icon-dashboard <?php echo $page_name === 'index.php' ? 'active' : '' ?> ">
						<div class="wp-menu-arrow"><div></div></div>
						<div class="wp-menu-image dashicons-before dashicons-dashboard">
							<br>
						</div>
						<div class="wp-menu-name">Dashboard</div>
					</a>
				</li>
				<li class="menu-top menu-icon-promotion" id="menu-promotion">
					<a href="admin.php?page=fundraiser-promotion"
						class="menu-top menu-icon-dashboard <?php echo $page_name === 'fundraiser-promotion' ? 'active' : '' ?> ">
						<div class="wp-menu-arrow"><div></div></div>
						<div class="wp-menu-image dashicons-before dashicons-networking">
							<br>
						</div>
						<div class="wp-menu-name">Promotion</div>
					</a>
				</li>
				<li class="menu-top menu-icon-dashboard">
					<a href="#"
						class="checklist-trigger menu-top menu-icon-dashboard <?php echo $page_name === 'fundraiser-profile-editor' ? 'active' : '' ?>">
						<div class="wp-menu-arrow"><div></div></div>
						<div class="wp-menu-image dashicons-before dashicons-desktop">
							<br>
						</div>
						<div class="wp-menu-name">Profile</div>
					</a>
					<a href="#" id="registrationChecklistTrigger" class="checklist-trigger">Checklist <span class="pull-right">&#9660;</span></a>
					<ul class="checklist <?php echo $page_name === 'fundraiser-profile-editor' ? 'open' : '' ?>">
					<?php
						foreach ($profile_pages as $idx => $page) {
							$page_link = $page['status'] === 'disabled' ? '#' : $profile_link . ($idx + 1);
							echo '<li class="' . $page['status'] . ($page['active'] ? ' active' : '') . '">' .
								'<a href="' . $page_link . '">' . $page['title'] . '</a></li>';
						}
					?>
					</ul>
				</li>
				<li class="menu-top menu-icon-supporters"  id="menu-supporters">
					<a href="admin.php?page=fundraiser-supporters"
						class="menu-top menu-icon-dashboard <?php echo $page_name === 'fundraiser-supporters' ? 'active' : '' ?> ">
						<div class="wp-menu-arrow"><div></div></div>
						<div class="wp-menu-image dashicons-before dashicons-chart-line">
							<br>
						</div>
						<div class="wp-menu-name">Supporters</div>
					</a>
				</li>
				<li class="wp-first-item menu-top menu-icon-post " id="menu-post-update">
					<a href="edit.php"
						class="menu-top menu-icon-dashboard <?php echo $page_name === 'edit.php' || $page_name === 'post-new.php' || $page_name === 'post.php' ? 'active' : '' ?> ">
						<div class="wp-menu-arrow"><div></div></div>
						<div class="wp-menu-image dashicons-before dashicons-format-aside">
							<br>
						</div>
						<div class="wp-menu-name">Post an Update</div>
					</a>
				</li>
			</ul>


			<div class="col-md-9 page-pane <?php echo $page_class_name; ?>">
			<!-- start skin-independent output -->
			<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

			<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

			<script type="text/javascript">
				$(document).ready(function() {
					if (window.location.pathname.split('/').pop().indexOf('post-new.php') !== -1) {
						// new post page
						$('.post-new #wpbody-content h1')
							.after('<p>Have any news to share with your supporters?  Post an update to you Fund page.</p>')
							.after('<h1 class="visible">Post an Update</h1>');
						$('.post-new #titlediv').before('<h4>Post Title</h4>');
					} else if (window.location.pathname.split('/').pop().indexOf('edit.php') !== -1) {
						// list posts page
					} else if (window.location.pathname.split('/').pop().indexOf('post.php') !== -1) {
						// edit post page
					}

					$('.checklist-trigger').on('click', this, function(e) {
						e.preventDefault();
						$('.checklist').slideToggle(function() {
							$(this).toggleClass('open');
						});
					});


					if ($(window).width() <= 992){
						$('.fund-admin.approved .checklist').removeClass('open');
					}

					$('[data-toggle="tooltip"]').tooltip();

			<?php
				echo 'var showStartDialog = ' . ($show_setup_dialog ? 'true' : 'false') . ";\n";
			?>
				if (window.location.search.indexOf('initial') !== -1) {
					$.magnificPopup.open({
						items: {
							type: 'inline',
								src: '#setupfund-form',
						},
						closeOnBgClick: false,
						preloader: false,
						callbacks: {
							beforeOpen: function() { }
						}
					});
				}
				if (showStartDialog) {
					$.magnificPopup.open({
						items: {
							type: 'inline',
								src: '#startfund-form',
						},
						closeOnBgClick: false,
						preloader: false,
						callbacks: {
							beforeOpen: function() { }
						}
					});
				}

				// inject crap into posts forms, fu wordpress

				});
			</script>


<?php
}
?>


