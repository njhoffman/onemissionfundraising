<?php
/*
	Plugin Name: Notification Menu
	Description: Consolidates notifications into admin menu to reduce clutter and hide from non-super admins
	Version: 1.0.0
	Author: Nick Hoffman
*/

include 'src/Pimple/Container.php';
include 'src/Pimple/ServiceProviderInterface.php';
include 'src/AdminBar.php';
include 'src/AdminNoticeHandler.php';
include 'src/File.php';
include 'src/Notification.php';
include 'src/Plugin.php';
include 'src/PluginServiceProvider.php';

if ( ! defined( 'ABSPATH' ) ) {
	header( 'Status: 403 Forbidden' );
	header( 'HTTP/1.1 403 Forbidden' );
	exit;
}

function notification_menu() {

	static $instance;
	if ( is_null( $instance ) ) {
		$instance = new \NotificationMenu\Plugin( '1.0.0', __FILE__ );
	}

	return $instance;

}

function __load_notification_menu() {
	// fetch instance and store in global
	return notification_menu();
}

add_action( 'plugins_loaded', '__load_notification_menu', 20 );
