<?php

namespace NotificationMenu;

class Plugin extends Pimple\Container {

	private $version = '1.0';

	public function __construct( $version, $file ) {

		// set version
		$this->version = $version;

		// Pimple Container construct
		parent::__construct();

		// register file service
		$this['file'] = function () use ( $file ) {
			return new File( $file );
		};

		// register services early since some add-ons need 'm
		$this->register_services();

		if ( is_admin() ) {
			// alias for Pimple container
			$c = $this;

			// enqueue CSS
			add_action( 'admin_enqueue_scripts', function () use ( $c ) {
				wp_enqueue_style(
					'wpcm_admin',
					$c['file']->plugin_url( '/assets/css/notification-menu.css' ),
					array(),
					$c->get_version()
				);
			} );

			// catch admin notices
			$this['admin_notice_handler']->catch_admin_notices();

			// setup admin bar
			$admin_bar = new AdminBar();
			$admin_bar->setup();
		}

	}

	public function get_version() {
		return $this->version;
	}


	private function register_services() {
		$provider = new PluginServiceProvider();
		$provider->register( $this );
	}

	public function service( $key ) {
		return $this[ $key ];
	}
}
