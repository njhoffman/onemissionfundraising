export const domElements = {
  iframe: '#leadin-iframe',
  spaNavigationButtons:
    '.toplevel_page_leadin > a, a[href="admin.php?page=leadin_forms"], a[href="admin.php?page=leadin_settings"]',
  subMenuButtons: '.toplevel_page_leadin > ul > li',
  deactivatePluginButton: '[data-slug="leadin"] .deactivate a',
  deactivateFeedbackForm: 'form.leadin-deactivate-form',
  deactivateFeedbackSubmit: 'button#leadin-feedback-submit',
  deactivateFeedbackSkip: 'button#leadin-feedback-skip',
  thickboxModalClose: '.leadin-modal-close',
  thickboxModalWindow: 'div#TB_window.thickbox-loading',
  thickboxModalContent: 'div#TB_ajaxContent.TB_modal',
};
