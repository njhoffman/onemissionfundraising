<?php
/*
Plugin Name: One Mission Action Traces
Version: 1.0
Description: Shows all called actions
Author: Nick Hoffman
 */
function _om_action_trace() {
	/*
	 * Even though this plugin should never EVER be used in production, this is
	 * a safety net. You have to actually set the showTrace=1 flag in the query
	 * string for it to operate. If you don't it will still slow down your
	 * site, but it won't do anything.
	 */
	if (!isset($_GET['showDebugTrace']) || (bool)$_GET['showDebugTrace']!==true) {
		return;
	}
	/*
	 * There are 2 other flags you can set to control what is output
	 */
	$showArgs = (isset($_GET['showDebugArgs']) ? (bool)$_GET['showDebugArgs'] : false);
	$showTime = (isset($_GET['showDebugTime']) ? (bool)$_GET['showDebugTime'] : false);
	$filter = (isset($_GET['filter']) ? $_GET['filter'] : false);
	/*
	 * This is the main array we are using to hold the list of actions
	 */
	static $actions = [];
	$excludeActions = ['gettext','gettext_with_context'];
	$thisAction     = current_filter();
	$thisArguments  = func_get_args();

	if (!in_array( $thisAction, $excludeActions )) {
		if (!$filter || strpos($thisAction, $filter) !== false) {
			$actions[] = [
				'action'    => $thisAction,
				'time'      => microtime(true),
				'arguments' => print_r($thisArguments,true)
			];
		}
	}

	/*
	 * Shutdown is the last action, process the list.
	 */
	if ($thisAction==='shutdown') {
		_om_format_debug_output_log($actions, $showArgs, $showTime);
	}
	return;
}
function _om_format_debug_output_log($actions=[], $showArgs = false, $showTime = false) {
	/*
	 * Let's do a little formatting here.
	 * The class "debug" is so you can control the look and feel
	 */

	foreach($actions as $thisAction) {
		$action_str = "";
		$action_str .= "Action Name : ";
		/*
		 * if you want the timings, let's make sure everything is padded out properly.
		 */
		if ($showTime) {
			$timeParts = explode('.',$thisAction['time']);
			$action_str .= '(' . $timeParts[0] . '.' .  str_pad($timeParts[1],4,'0') . ') ';
		}
		$action_str .= $thisAction['action'] . PHP_EOL;
		/*
		 * If you've requested the arguments, let's display them.
		 */
		if ($showArgs && count($thisAction['arguments'])>0) {
			$action_str .= "Args:" . PHP_EOL . print_r($thisAction['arguments'],true);
			$action_str .= PHP_EOL;
		}
		error_log($action_str);
	}

	return;
}


function _om_format_debug_output_page($actions=[], $showArgs = false, $showTime = false) {
	/*
	 * Let's do a little formatting here.
	 * The class "debug" is so you can control the look and feel
	 */

	echo '<pre class="debug">';
	foreach($actions as $thisAction) {
		echo "Action Name : ";
		/*
		 * if you want the timings, let's make sure everything is padded out properly.
		 */
		if ($showTime) {
			$timeParts = explode('.',$thisAction['time']);
			echo '(' . $timeParts[0] . '.' .  str_pad($timeParts[1],4,'0') . ') ';
		}
		echo $thisAction['action'] . PHP_EOL;
		/*
		 * If you've requested the arguments, let's display them.
		 */
		if ($showArgs && count($thisAction['arguments'])>0) {
			echo "Args:" . PHP_EOL . print_r($thisAction['arguments'],true);
			echo PHP_EOL;
		}
	}
	echo '</pre>';

	return;
}

// add_action( 'all', '_om_action_trace', 99999, 99 );
add_filter( 'all', '_om_action_trace');
