
<?php
/*
Plugin Name: One Mission Plugin Overrides
Version: 1.0
Author: Nick Hoffman
*/

// Fixes to disregard Hidden products and to add Author Billing field to Better Search plugin queries
// DEFINE( 'OM_SEARCH_AUTHOR', true );
function _om_cache_flush() {
	global $wp_object_cache;
	return $wp_object_cache->flush();
}
// _om_cache_flush();

function filter_bsearch_posts_join( $join, $search_term ) {
	global $wpdb;
	// if need to access postmeta
	// $join .= " LEFT JOIN $wpdb->postmeta AS pm ON ($wpdb->posts.ID = pm.post_id)";

	if ( OM_SEARCH_AUTHOR ) {
		$join .= " LEFT JOIN $wpdb->usermeta AS pum ON ($wpdb->posts.post_author = pum.user_id AND meta_key='last_name') ";
	}
	return $join;
}
// add_filter( 'bsearch_posts_join', 'filter_bsearch_posts_join', 10, 2);

function filter_bsearch_posts_where( $where, $search_term ) {
	global $wpdb;
	$where .= " OR ( meta_key='last_name' AND post_type='page' ) ";
	// error_log($where);
	return $where;
}
// add_filter( 'bsearch_posts_where', 'filter_bsearch_posts_where', 10, 2 );

function filter_bsearch_posts_match( $match, $search_term ) {
	global $wpdb;
	// $author_search = $wpdb->get_results( $wpdb->prepare( "
	// SELECT post_id FROM $wpdb->postmeta
	// WHERE `meta_key` = 'wpcf-author-billing' AND `meta_value` LIKE '%s'
	// ", "%".$search_term."%" ) );
	if (OM_SEARCH_AUTHOR) {
		$match = str_replace(
			"MATCH (post_title,post_content)",
			"(MATCH(meta_value) AGAINST ('" . $search_term . "' IN BOOLEAN MODE) OR MATCH(post_title,post_content)",
			$match
		);
		$match .= ')';
	}
	// error_log("\n\nMatch:\n$match\n\n\n");
	return $match;
}
// add_filter( 'bsearch_posts_match', 'filter_bsearch_posts_match', 10, 2 );

function filter_bsearch_posts_orderby( $orderby, $search_term ) {
	global $wpdb;
	$orderby = "FIELD(post_type, 'page', 'post', 'product'), score DESC";
	// $orderby = "FIELD(post_type, 'attachment', 'product', 'post', 'page'), score DESC";
	// error_log("\n\nOrderBy :\n$orderby\n\n\n");
	return $orderby;
}
// add_filter( 'bsearch_posts_orderby', 'filter_bsearch_posts_orderby', 10, 2 );

function log_bsearch_query($query, $search_term) {
	// if (strpos($query, 'IN BOOLEAN MODE') !== false )  {
	// 	$query = str_replace("IN BOOLEAN MODE ) * 1 ) AS score",
	// 		"IN BOOLEAN MODE ) * 1 ) + (MATCH(meta_value) AGAINST ('" . $search_term[0] . "' IN BOOLEAN MODE) * 1) AS score", $query);
	// } else {
		$query = str_replace(" ) * 1 ) AS score", "  ) * 1 ) + (MATCH(meta_value) AGAINST ('" . $search_term[0] . "' IN BOOLEAN MODE) * 1) AS score", $query);
	// }
	error_log("\n\nSearch:\n$query\n\n\n");
}
// add_filter( 'bsearch_sql_prepare', 'log_bsearch_query', 10, 2 );
?>
