#!/bin/bash

wordpress_file="latest.tar.gz"
wordpress_url="https://wordpress.org/$wordpress_file"

wget "$wordpress_url"
mv "$wordpress_file" src
tar xzf "./src/$wordpress_file" --directory "./src"
cp -rv "./src/wordpress/." ./public

# install kinsta mu plugin
kinsta_file="kinsta-mu-plugins.zip"
wget "https://kinsta.com/kinsta-tools/$kinsta_file"
mv "$kinsta_file" src
unzip "./src/$kinsta_file"
mv ./src/kinsta-mu-plugins ./public/content/mu-plugins
mv ./src/kinsta-mu-plugins.php ./public/content/mu-plugins
