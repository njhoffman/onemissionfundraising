<?php
	// common
	define( 'DB_CHARSET', 'utf8' );
	define( 'DB_COLLATE', '' );
	define( 'WP_MAX_MEMORY_LIMIT', '1024M' );
	define( 'WP_MEMORY_LIMIT', '1024M' );
	define( 'WPLANG', '' );
	set_time_limit(60);

	// Disable cron (running manually)
	define( 'DISABLE_WP_CRON', 'true' );

	// Table prefix
	// - Change if multiple installs in same DB
	$table_prefix  = 'wp_';

	// Development DB Creds
	define( '_FHBS_DEV_DB_USER' , 'om_site_usr' );
	define( '_FHBS_DEV_DB_PASS' , 'YOEebwNHmPgmQrhb9BAH' );
	define( '_FHBS_DEV_DB_DB'   , 'one_mission_dev' );
	define( '_FHBS_DEV_DB_HOST' , 'localhost' );

	// Production DB Creds
	define( '_FHBS_PROD_DB_USER', 'om_site_usr' );
	define( '_FHBS_PROD_DB_PASS', 'YOEebwNHmPgmQrhb9BAH' );
	define( '_FHBS_PROD_DB_DB'  , 'one_mission_wp' );
	define( '_FHBS_PROD_DB_HOST', 'localhost' );


	/* dev site settings */

	define( 'DB_USER'    , _FHBS_DEV_DB_USER );
	define( 'DB_PASSWORD', _FHBS_DEV_DB_PASS );
	define( 'DB_NAME'    , _FHBS_DEV_DB_DB   );
	define( 'DB_HOST'    , _FHBS_DEV_DB_HOST );

	define('FORCE_SSL_ADMIN', false);
	define('FORCE_SSL_LOGIN', false);

	define('WP_HOME', 'http://dev.onemission.fund');
	define('WP_SITEURL', 'http://dev.onemission.fund/wp');

	define('STATSD_NAMESPACE', 'dev.onemission.fund');

	// Debugging
	ini_set( 'display_errors', E_ALL );
	define( 'WP_DEBUG', true);
	define( 'WP_DEBUG_LOG', true);
	define( 'WP_DEBUG_DISPLAY', false);
	define( 'WP_LOCAL_DEV', true );
	define( 'SAVEQUERIES', true );

	// Salts, for security
	// - Grab from: https://api.wordpress.org/secret-key/1.1/salt
	//
	define('AUTH_KEY',         '`$EP|cqo`{[$8phs-V:Ovx|#;+Ay$iIrD~fRZ.vm+QF-ZVJcnCJg]IK>&q&P dK,');
	define('SECURE_AUTH_KEY',  'i@G`ua:n%L!Dzi7]6fsNwDDM1R/fY=5cVZ,aAf<`e0**Szv*%r1qxK5yKaD+,cx-');
	define('LOGGED_IN_KEY',    'SE/f]hU1O1&KJH~#s2xr4&UMAZ@C;vofb^a?FJ{`Pzw<KQTPW]If}HrLbKc6y9aE');
	define('NONCE_KEY',        'ti&<+&HAtiXUnGZr<nZc%oNcaKnXIn]^o&7ltD?5f%ds)9Czjj?%-c=RnOKr:B*S');
	define('AUTH_SALT',        'KlJdl^Eij_=^]zNl+to]7<--MI|F96At`bL&Ci1n5&+ :nzvC$itwzX^0)5diCK;');
	define('SECURE_AUTH_SALT', 'I7kOJo~=5R_%1#<&DPifjx4cqPy]X}quutz1~:)y2*)OmcVy9U`(jPQw/OP]s*=u');
	define('LOGGED_IN_SALT',   'h>tI.@49;,YSp?62gC|;,-RY-EfCTg46&AQJvDKU-VH3xff[~dG|mMUE<U?S nKU');
	define('NONCE_SALT',       'Mk .Y6exO%h{<&wGVRyQF$_JAFF4?Tg7>oj-*bPQn3R+,Za[RUL@FMS+jg8T+p@O');

