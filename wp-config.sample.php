<?php

	//
	// Define Custom Content Directory
	//
	define( 'WP_CONTENT_DIR', __DIR__ . '/content' );
	define( 'WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/content' );

	//
	// Grab WordPress configuration stored out of webroot
	//
	$_fh_site_config        = '/./wp-bootstrap-config.php';
	$_fh_site_config_sample = '/./wp-bootstrap-config.sample.php';

	if( file_exists( __DIR__ . $_fh_site_config ) ) {

		// Use configuration in place
		include( __DIR__ . $_fh_site_config );

	} elseif( file_exists( __DIR__ . $_fh_site_config_sample ) ) {

		// Die on sample site configuration found error
		die( 'Sample site configuration file "' . $_fh_site_config_sample . '" still in place. Set proper site configuration options and rename to "' . $_fh_site_config . '".' );

	} else {

		// Die on no site configuration found error
		die( 'Error: Site configuration file "' . $_fh_site_config . '" not found.' );
	}

	//
	// Bootstrap WordPress from separate directory
	//
        // define( 'ABSPATH', __DIR__ . '/wp/' );

	require_once( ABSPATH . 'wp-settings.php' );
