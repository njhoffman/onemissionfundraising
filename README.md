OneMission is built on WordPress version 4.3.1

Run "composer install" in root directory to install mu-plugins and wordpress core.  Plugins and MU-Plugins are both tracked until there is certainty each plugin is reflected in composer.json, then these should be removed from git unless there are custom plugins/modifications (must move distribution URL's out of folkhack as well).  To add dev only plugins that won't transfer to live site add folder name to .gitignore.

Two git branches: development and master, dev.onemissionfundraising.com should always be on development, live site should always be on master.  When development changes are approved, should be merged into master on live site.  Currently all dev work on one site on the server (dev.onemissionfundraising.com), if needed additional dev sites can be created so multiple people can work at once (on either the same development branch or their own branches).

For generic WordPress information and helpful tips visit the wiki: 

* [One Mission Wiki Home](../../wiki/Home)
* [Git Workflow Instructions](../../wiki/GitWorkflow)
* [Plugins Reference](../../wiki/Plugins)


TODO: Figure out how to deploy locally with vagrant, write instructions here.